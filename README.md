# Octopus API App v2.1

Requerimientos para configurar el proyecto:
  - git
  - docker
  - docker-compose


### Instalación

Clonar el repositorio
```sh
$ git clone https://github.com/imarabotti/octopus-api-app.git
$ cd octopus-api-app
```

Copiar el archivo de environment
```sh
$ cp .env.example .env
```

Levantar los servicios
```sh
$ docker-compose up -d
```

### Migraciones y Seeders

Ingresar al container de la api
```sh
$ docker-compose exec php-api sh
```
Instalar dependencias de composer
```
composer install
```

Ejecutar migraciones y seeders
```sh
$ php artisan migrate:fresh --seed
```

### Permisos
```
chmod -R 777 /var/www/html/storage/
```

### Tests
Ejecutar todos los test de la app
```sh
$ vendor/bin/phpunit
```
o bien
```sh
$ composer test
```
Ejecutar un test individual
```sh
$ vendor/bin/phpunit tests/EntidadTest.php
```
Ejecutar solo un metodo de un test individual
```sh
$ vendor/bin/phpunit tests/EntidadTest.php --filter=testCreate
```

### Swagger Doc
Generar la documentación desde el container de la API

```sh
$ php artisan swagger-lume:generate
```

Por defecto la documentación de la API se visualiza en http://localhost/api/documentation

### Configuracion NGINX
Por defecto nginx apunta a 127.0.0.1:9000 para buscar el php-fpm debido a la forma de deploy a produccion elegida.
Para lograr que ande localmente cambiar la linea 18 del archivo docker/nginx.conf por la siguiente
fastcgi_pass php-api:9000;
