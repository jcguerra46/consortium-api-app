<?php

namespace App\Events;

use App\Models\CashboxMovement;
use Illuminate\Queue\SerializesModels;

class CashboxMovementCreatedEvent extends Event
{
    use SerializesModels;

    public $cashboxMovement;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(CashboxMovement $cashboxMovement)
    {
        $this->cashboxMovement = $cashboxMovement;
    }
}
