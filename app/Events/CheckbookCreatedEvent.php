<?php

namespace App\Events;

use App\Models\Checkbook;
use Illuminate\Queue\SerializesModels;

class CheckbookCreatedEvent extends Event
{
    use SerializesModels;

    public $checkbook;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Checkbook $checkbook)
    {
        $this->checkbook = $checkbook;
    }
}
