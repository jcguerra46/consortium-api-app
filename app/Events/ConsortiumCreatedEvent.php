<?php

namespace App\Events;

use App\Models\Consortium;
use App\Models\ConsortiumProfile;
use Illuminate\Queue\SerializesModels;

class ConsortiumCreatedEvent extends Event
{
    use SerializesModels;

    public $consortium;

    public $consortiumProfile;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Consortium $consortium, ConsortiumProfile $consortiumProfile)
    {
        $this->consortium = $consortium;

        $this->consortium_profile = $consortiumProfile;
    }
}
