<?php

namespace App\Events;

use App\Models\Employee;
use Illuminate\Queue\SerializesModels;

class EmployeeCreatedEvent extends Event
{
    use SerializesModels;

    public $employee;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }
}
