<?php

namespace App\Events;

use App\Models\Payment;
use Illuminate\Queue\SerializesModels;

class PaymentCreatedEvent extends Event
{
    use SerializesModels;

    public $payment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }
}
