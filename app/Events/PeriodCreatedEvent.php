<?php

namespace App\Events;

use App\Models\Period;
use Illuminate\Queue\SerializesModels;

class PeriodCreatedEvent extends Event
{
    use SerializesModels;

    public $period;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Period $period)
    {
        $this->period = $period;
    }
}
