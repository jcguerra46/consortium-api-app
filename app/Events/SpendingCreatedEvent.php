<?php

namespace App\Events;

use App\Models\Consortium;
use App\Models\Spending;
use Illuminate\Queue\SerializesModels;

class SpendingCreatedEvent extends Event
{
    use SerializesModels;

    public $spending;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Spending $spending)
    {
        $this->spending = $spending;
    }
}
