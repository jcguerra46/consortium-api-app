<?php

namespace App\Exceptions;

use App\Http\Traits\ApiResponser;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class FunctionalUnitsException extends Exception
{
    use ApiResponser;

    /**
     * Report the exception.
     *
     * @return void
     */
     public function report()
     {
        //
    }

    /**
     * Render the exception as an HTTP response.
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return $this->failResponse(
            'The Functional Unit does not correspond to your Consortium or Administration.',
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }
}
