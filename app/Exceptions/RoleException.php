<?php

namespace App\Exceptions;

use App\Http\Traits\ApiResponser;
use Exception;
use Symfony\Component\HttpFoundation\Response;

class RoleException extends Exception
{
    use ApiResponser;

    /**
     * Report the exception.
     *
     * @return void
     */
     public function report()
     {
        //
    }

    /**
     * Render the exception as an HTTP response.
     *
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return $this->failResponse(
            'Unauthorized. You do not have the user role to continue, contact the system administrator.',
            Response::HTTP_UNAUTHORIZED
        );
    }
}
