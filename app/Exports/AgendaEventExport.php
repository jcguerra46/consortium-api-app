<?php

namespace App\Exports;

use App\Models\AgendaEvent;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AgendaEventExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'administration_id',
            'consortium_id',
            'eventable',
            'title',
            'description',
            'date',
            'trans',
            'service_class_name',
            'route_class_name',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return AgendaEvent::fromAdministration()->get();
    }
}
