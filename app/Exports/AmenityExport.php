<?php

namespace App\Exports;

use App\Models\Amenity;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AmenityExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'name',
            'location',
            'enabled',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Amenity::fromAdministration()->get();
    }
}
