<?php

namespace App\Exports;

use App\Models\Assembly;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AssemblyExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'date',
            'time',
            'second_time',
            'place',
            'issues',
            'send',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Assembly::fromAdministration()->get();
    }
}
