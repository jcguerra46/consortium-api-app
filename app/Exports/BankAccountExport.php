<?php

namespace App\Exports;

use App\Models\BankAccount;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BankAccountExport implements FromCollection, WithHeadings
{
    public function __construct($consortiumId)
    {
        $this->consortium_id = $consortiumId;
    }

    public function headings(): array
    {
        return [
            'id',
            'bank_id',
            'account_number',
            'cbu',
            'alias',
            'cuit',
            'type',
            'branch_office',
            'owner',
            'show_data_in_expense',
            'email',
            'signatorie_1',
            'signatorie_2',
            'signatorie_3',
            'consortium_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return BankAccount::where('consortium_id', $this->consortium_id)->get();
    }
}
