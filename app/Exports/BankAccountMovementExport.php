<?php

namespace App\Exports;

use App\Models\BankAccount;
use App\Models\BankAccountMovement;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BankAccountMovementExport implements FromCollection, WithHeadings
{
    public function __construct($bankAccountId)
    {
        $this->bank_account_id = $bankAccountId;
    }

    public function headings(): array
    {
        return [
            'id',
            'bank_account_id',
            'description',
            'date',
            'amount',
            'type',
            'type_movement',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return BankAccountMovement::where('bank_account_id', $this->bank_account_id)
            ->ordered()
            ->get();
    }
}
