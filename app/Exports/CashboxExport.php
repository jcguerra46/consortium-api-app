<?php

namespace App\Exports;

use App\Models\Cashbox;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CashboxExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'name',
            'consortium_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Cashbox::fromAdministration()->get();
    }
}
