<?php

namespace App\Exports;

use App\Models\Cashbox;
use App\Models\CashboxMovement;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CashboxMovementExport implements FromCollection, WithHeadings
{
    private $cashboxId;

    public function __construct($cashboxId)
    {
        $this->cashboxId = $cashboxId;
    }

    public function headings(): array
    {
        return [
            'id',
            'cashbox_id',
            'date',
            'description',
            'identified',
            'total_amount',
            'cash_amount',
            'type_movement',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return CashboxMovement::where([
            'cashbox_id' => $this->cashboxId
        ])->get();
    }
}
