<?php

namespace App\Exports;

use App\Models\Check;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CheckExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'number',
            'deposit_date',
            'issuance_date',
            'amount',
            'crossed',
            'is_to_the_order',
            'endorsements',
            'own',
            'bank_id',
            'checkbook_id',
            'consortium_id',
            'administration_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Check::fromAdministration()->get();
    }
}
