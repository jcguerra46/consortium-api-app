<?php

namespace App\Exports;

use App\Models\Checkbook;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CheckbookExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'administration_id',
            'number',
            'first_check',
            'last_check',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Checkbook::fromAdministration()->get();
    }
}
