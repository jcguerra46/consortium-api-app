<?php

namespace App\Exports;

use App\Models\Claim;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ClaimExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'functional_unit_id',
            'user_portal_id',
            'title',
            'body',
            'state',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return Claim::fromFunctionalunit()->get();
    }
}
