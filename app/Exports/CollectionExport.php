<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Collection;

class CollectionExport implements FromCollection, WithHeadings
{
    private $periodId;

    public function __construct($periodId)
    {
        $this->periodId = $periodId;
    }

    public function headings(): array
    {
        return [
            'id',
            'description',
            'date',
            'total_amount',
            'cashbox_movement_id',
            'period_id',
            'functional_unit_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        $qb = Collection::fromAdministration();
        if (isset($this->periodId)) {
            $qb->where('period_id', $this->periodId);
        }
        return $qb->get();
    }
}
