<?php

namespace App\Exports;

use App\Models\Consortium;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ConsortiumExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'administration_id',
            'code',
            'fancy_name',
            'business_name',
            'address_number',
            'address_street',
            'province_id',
            'postal_code',
            'cuit',
            'start_date',
            'payment_method_id',
            'suterh_code',
            'siro_code',
            'pmc_code',
            'pme_code',
            'ep_code',
            'siro_verification_number',
            'count_floors',
            'count_functional_units',
            'type_of_building',
            'state',
            'category',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return Consortium::fromAdministrationIndex()->get();
    }
}
