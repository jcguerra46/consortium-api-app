<?php

namespace App\Exports;

use App\Models\Consortium;
use App\Models\ConsortiumFinancialMovement;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ConsortiumMovementExport implements FromCollection, WithHeadings
{
    private $consortium_id;

    public function __construct($consortiumId)
    {
        $this->consortium_id = $consortiumId;
    }

    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'spending_id',
            'amount',
            'date',
            'description',
            'type_movement',
            'type',
            'expense_id',
            'identified',
            'incomplete',
            'created_from_recurrent',
            'included_in_the_financial_statement',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return ConsortiumFinancialMovement::where('consortium_id', $this->consortium_id)
            ->ordered()
            ->get();
    }
}
