<?php

namespace App\Exports;

use App\Models\PercentageConsortium;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ConsortiumPercentageExport implements FromCollection, WithHeadings
{
    public function __construct($consortiumId)
    {
        $this->consortium_id = $consortiumId;
    }

    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'name',
            'second_line_name',
            'description',
            'type',
            'position',
            'last_fixed_amount',
            'particular_percentage',
            'hidden_expense_spending',
            'hidden_in_proration',
            'hidden_percentage_value',
            'created_at',
            'updated_at',
            'deleted_at',
        ];
    }

    public function collection()
    {
        return PercentageConsortium::where('consortium_id', $this->consortium_id)->ordered()->get();
    }
}
