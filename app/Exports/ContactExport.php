<?php

namespace App\Exports;

use App\Models\Contact;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ContactExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'administration_id',
            'first_name',
            'last_name',
            'email',
            'dni',
            'cuil',
            'birth_date',
            'note',
            'address_street',
            'address_number',
            'postal_code',
            'created_at',
            'updated_at',
            'deleted_at',
        ];
    }

    public function collection()
    {
        return Contact::fromAdministration()->get();
    }
}
