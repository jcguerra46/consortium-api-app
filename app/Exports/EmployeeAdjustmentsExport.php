<?php

namespace App\Exports;

use App\Models\EmployeeAdjustment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeeAdjustmentsExport implements FromCollection, WithHeadings
{
    private $employeeId;

    /**
     * EmployeeAdjustmentsExport constructor.
     * @param $employeeId
     */
    public function __construct($employeeId)
    {
        $this->employeeId = $employeeId;
    }

    public function headings(): array
    {
        return [
            'id',
            'employee_id',
            'type',
            'haber_descuento',
            'description',
            'value',
            'used',
            'suma_sueldo_jornal',
            'recurrent',
            'es_remunerativo',
            'unit',
            'percentage',
            'porcentaje_sobre_base',
            'active',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return EmployeeAdjustment::where('employee_id', $this->employeeId)
            ->get();
    }
}
