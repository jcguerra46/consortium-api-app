<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeeExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'professional_function_id',
            'first_name',
            'last_name',
            'birth_date',
            'type_dni',
            'dni',
            'cuil',
            'number_docket',
            'active',
            'employee_status_id',
            'consortium_id',
            'created_at',
            'updated_at',
            'deleted_at',
        ];
    }

    public function collection()
    {
        return Employee::fromAdministration()->get();
    }
}
