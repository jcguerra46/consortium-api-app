<?php

namespace App\Exports;

use App\Models\ExpenseHeader;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExpenseHeaderExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'administration_id',
            'name_header',
            'cuit',
            'name',
            'address_street',
            'address_number',
            'email',
            'phone',
            'fiscal_situation',
            'postal_code',
            'rpa',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return ExpenseHeader::fromAdministration()->get();
    }
}
