<?php

namespace App\Exports;

use App\Models\FunctionalUnit;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FunctionalUnitExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'package_id',
            'floor',
            'department',
            'number',
            'type',
            'type_forgive_interest',
            'legal_state',
            'm2',
            'order',
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    }

    public function collection()
    {
        return FunctionalUnit::fromConsortiums()->get();
    }
}
