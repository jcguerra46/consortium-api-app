<?php

namespace App\Exports;

use App\Models\FunctionalUnit;
use App\Models\FunctionalUnitMovement;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FunctionalUnitMovementExport implements FromCollection, WithHeadings
{
    public function __construct($functionalUnitId)
    {
        $this->functional_unit_id = $functionalUnitId;
    }

    public function headings(): array
    {
        return [
            'id',
            'functional_unit_id',
            'amount',
            'date',
            'description',
            'type',
            'type_movement',
            'identified',
            'accumulate_to_expense',
            'expense_id',
            'spending_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return FunctionalUnitMovement::where('functional_unit_id', $this->functional_unit_id)
            ->ordered()
            ->get();
    }
}
