<?php

namespace App\Exports;


use App\Models\FunctionalUnitType;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FunctionalUnitTypeExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'description',
            'active',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return FunctionalUnitType::all();
    }
}
