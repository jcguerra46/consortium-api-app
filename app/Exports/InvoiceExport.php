<?php

namespace App\Exports;

use App\Models\Invoice;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InvoiceExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'provider_id',
            'invoice_number',
            'description',
            'date',
            'amount',
            'self_generated',
            'administration_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Invoice::fromAdministration()->ordered()->get();
    }
}
