<?php

namespace App\Exports;

use App\Models\Judgment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class JudgmentExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'cover',
            'number_expedient',
            'judged',
            'objet',
            'state',
            'reclaimed_amount',
            'active',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Judgment::fromAdministration()->get();
    }
}
