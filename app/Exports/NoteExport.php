<?php

namespace App\Exports;

use App\Models\Note;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class NoteExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'title',
            'content',
            'is_debt_note',
            'prorrateo_in_expense',
            'limit_date',
            'administration_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Note::fromAdministration()->get();
    }
}
