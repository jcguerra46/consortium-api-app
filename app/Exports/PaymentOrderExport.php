<?php

namespace App\Exports;

use App\Models\PaymentOrder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PaymentOrderExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'provider_id',
            'administration_id',
            'amount',
            'description',
            'status',
            'date',
            'created_at',
            'updated_at',
            'deleted_at',
        ];
    }

    public function collection()
    {
        return PaymentOrder::fromAdministration()->get();
    }
}
