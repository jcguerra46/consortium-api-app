<?php

namespace App\Exports;

use App\Models\Period;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PeriodExport implements FromCollection, WithHeadings
{
    public function __construct($consortiumId)
    {
        $this->consortium_id = $consortiumId;
    }

    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'description',
            'start_date',
            'end_date',
            'state',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Period::where('consortium_id', $this->consortium_id)->ordered()->get();
    }
}
