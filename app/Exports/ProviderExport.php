<?php

namespace App\Exports;

use App\Models\Consortium;
use App\Models\Provider;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProviderExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'business_name',
            'name',
            'address',
            'location',
            'province_id',
            'postal_code',
            'cuit',
            'note',
            'license',
            'attention_hours',
            'fiscal_situation',
            'administration_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return Provider::fromAdministration()->get();
    }
}
