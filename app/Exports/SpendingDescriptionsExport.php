<?php

namespace App\Exports;

use App\Models\SpendingDescription;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SpendingDescriptionsExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'administration_id',
            'title',
            'description',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return SpendingDescription::fromAdministration()->get();
    }
}
