<?php

namespace App\Exports;

use App\Models\Spending;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SpendingExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'number',
            'amount',
            'date',
            'prevision',
            'prorate_in_expenses',
            'affect_financial_section',
            'description',
            'type',
            'category_id',
            'spending_recurrent_id',
            'consortium_id',
            'functional_unit_id',
            'provider_id',
            'service_id',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return Spending::fromAdministration()->ordered()->get();
    }
}
