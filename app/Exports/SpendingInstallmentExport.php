<?php

namespace App\Exports;

use App\Models\SpendingInstallment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SpendingInstallmentExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'number',
            'amount',
            'date_pay',
            'spending_id',
            'invoince_id',
            'period_id',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return SpendingInstallment::fromAdministration()
            ->ordered()
            ->get();
    }
}
