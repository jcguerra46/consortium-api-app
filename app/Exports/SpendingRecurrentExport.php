<?php

namespace App\Exports;

use App\Models\SpendingRecurrent;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SpendingRecurrentExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'provider_id',
            'percentage_consortium_id',
            'category_id',
            'description',
            'amount',
            'created_at',
            'updated_at',
            'deleted_at',
        ];
    }

    public function collection()
    {
        return SpendingRecurrent::fromAdministration()
                                ->get();
    }
}
