<?php

namespace App\Exports;

use App\Models\UserAdministration;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UserAdministrationExport implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'id',
            'administration_id',
            'principal',
            'first_name',
            'last_name',
            'email',
            'password',
            'reset_password_token',
            'reset_password_expired',
            'active',
            'menu',
            'created_at',
            'updated_at'
        ];
    }

    public function collection()
    {
        return UserAdministration::all();
    }
}
