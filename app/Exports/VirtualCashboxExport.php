<?php

namespace App\Exports;

use App\Models\VirtualCashbox;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VirtualCashboxExport implements FromCollection, WithHeadings
{
    private $consortium_id;

    public function __construct($consortiumId)
    {
        $this->consortium_id = $consortiumId;
    }

    public function headings(): array
    {
        return [
            'id',
            'consortium_id',
            'name',
            'description',
            'show_in_expense',
            'sum_in_expense',
            'affects_cashbox_consortium',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return VirtualCashbox::fromConsortium($this->consortium_id)
            ->get();
    }
}
