<?php

namespace App\Exports;

use App\Models\VirtualCashboxMovement;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class VirtualCashboxMovementExport implements FromCollection, WithHeadings
{
    private $virtual_cashbox_id;

    public function __construct($virtualCashboxId)
    {
        $this->virtual_cashbox_id = $virtualCashboxId;
    }

    public function headings(): array
    {
        return [
            'id',
            'period_id',
            'virtual_cashbox_id',
            'amount',
            'date',
            'description',
            'type',
            'created_at',
            'updated_at',
        ];
    }

    public function collection()
    {
        return VirtualCashboxMovement::fromVirtualCashbox($this->virtual_cashbox_id)
            ->get();
    }
}
