<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\AfipActivity;
use App\Resources\PaginationResource;
use App\Validators\Rules\AfipActivityRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AfipActivityController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, AfipActivityRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(AfipActivity::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $afipActivity = AfipActivity::findOrFail($id);
        return $this->successResponse($afipActivity, 'Ok', Response::HTTP_OK);
    }

}
