<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\AfipCondition;
use App\Resources\PaginationResource;
use App\Validators\Rules\AfipConditionRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AfipConditionController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, AfipConditionRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(AfipCondition::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $afipCondition = AfipCondition::findOrFail($id);
        return $this->successResponse($afipCondition, 'Ok', Response::HTTP_OK);
    }
}
