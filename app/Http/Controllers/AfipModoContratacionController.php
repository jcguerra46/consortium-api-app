<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\AfipModoContratacion;
use App\Resources\PaginationResource;
use App\Validators\Rules\AfipModoContratacionRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AfipModoContratacionController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, AfipModoContratacionRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(AfipModoContratacion::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $afipModoContratacion = AfipModoContratacion::findOrFail($id);
        return $this->successResponse($afipModoContratacion, 'Ok', Response::HTTP_OK);
    }
}
