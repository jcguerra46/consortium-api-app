<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\AfipProvincia;
use App\Resources\PaginationResource;
use App\Validators\Rules\AfipProvinciaRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AfipProvinciaController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, AfipProvinciaRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(AfipProvincia::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $afipProvincia = AfipProvincia::findOrFail($id);
        return $this->successResponse($afipProvincia, 'Ok', Response::HTTP_OK);
    }

}
