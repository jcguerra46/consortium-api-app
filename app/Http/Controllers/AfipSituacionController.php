<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\AfipSituacion;
use App\Resources\PaginationResource;
use App\Validators\Rules\AfipSituacionRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AfipSituacionController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, AfipSituacionRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(AfipSituacion::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $afipSituacion = AfipSituacion::findOrFail($id);
        return $this->successResponse($afipSituacion, 'Ok', Response::HTTP_OK);
    }
}
