<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\AfipZona;
use App\Resources\PaginationResource;
use App\Validators\Rules\AfipZonaRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AfipZonaController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, AfipZonaRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(AfipZona::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $afipZona = AfipZona::findOrFail($id);
        return $this->successResponse($afipZona, 'Ok', Response::HTTP_OK);
    }
}
