<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\AgendaEvent;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Validators\Rules\AgendaEventRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\AgendaEventExport;

class AgendaEventController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, AgendaEventRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(AgendaEvent::fromAdministration()->paginate($pageSize)));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, AgendaEventRules::storeRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $agendaEvent = AgendaEvent::create($data);
        return $this->successResponse(
            $agendaEvent,
            'Agenda Event created',
            201);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AgendaEventRules::showRules());
        $agendaEvent = AgendaEvent::findOrFail($id);
        return $this->successResponse($agendaEvent);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AgendaEventRules::updateRules());
        $request['administration_id'] = Auth::user()->administration_id;
        $agendaEvent = AgendaEvent::findOrFail($id);
        $agendaEvent->update($request->all());
        return $this->successResponse(
            $agendaEvent,
            'Agenda Event edited',
            200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AgendaEventRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(AgendaEvent::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new AgendaEventExport(), 'agendaevents.xlsx');
    }
}
