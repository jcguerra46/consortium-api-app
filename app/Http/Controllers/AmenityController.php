<?php

namespace App\Http\Controllers;

use App\Exports\AmenityExport;
use App\Models\Amenity;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\AmenityRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AmenityController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, AmenityRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Amenity::fromAdministration()->paginate($pageSize)));
    }

    /**
     * Store a new resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, AmenityRules::createRules());
        $data = $request->all();
        $amenity = Amenity::create($data);
        return $this->successResponse(
            $amenity,
            'Amenity created',
            201);
    }

    /**
     * Update the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AmenityRules::editRules());
        $amenity = Amenity::findOrFail($id);
        $amenity->fill($request->all());
        $amenity->save();
        return $this->successResponse(
            $amenity,
            'Amenity edited',
            200);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AmenityRules::showRules());
        $amenity = Amenity::findOrFail($id);
        return $this->successResponse($amenity);
    }

    /**
     * Remove the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AmenityRules::destroyRules());
        return $this->successResponse(new DestroyResource(Amenity::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new AmenityExport(), 'amenities.xlsx');
    }
}
