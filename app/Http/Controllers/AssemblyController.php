<?php

namespace App\Http\Controllers;

use App\Exports\AssemblyExport;
use App\Models\Assembly;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\AssemblyRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AssemblyController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, AssemblyRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Assembly::fromAdministration()->paginate($pageSize)));
    }

    /**
     * Store a new resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, AssemblyRules::createRules());
        $data = $request->all();
        $assembly = Assembly::create($data);
        return $this->successResponse(
            $assembly,
            'Assembly created',
            201);
    }

    /**
     * Update the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AssemblyRules::editRules());
        $assembly = Assembly::findOrFail($id);
        $assembly->fill($request->all());
        $assembly->save();
        return $this->successResponse(
            $assembly,
            'Assembly edited',
            200);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AssemblyRules::showRules());
        $assembly = Assembly::findOrFail($id);
        return $this->successResponse($assembly);
    }

    /**
     * Remove the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, AssemblyRules::destroyRules());
        return $this->successResponse(new DestroyResource(Assembly::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new AssemblyExport(), 'assemblies.xlsx');
    }
}
