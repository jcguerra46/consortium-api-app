<?php


namespace App\Http\Controllers;

use App\Mail\SendResetLinkEmail;
use App\Models\UserAdministration;
use App\Http\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    use ApiResponser;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => [
            'login',
            'sendResetLinkEmail',
            'resetPassword'
        ]]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
        $credentials = $request->only(['email', 'password']);

        $user = UserAdministration::where([
            'email' => $credentials['email'],
            'active' => true,
        ])->first();

        if (!$user || !$token = Auth::attempt($credentials)) {
            return $this->failResponse('Unauthorized', Response::HTTP_UNAUTHORIZED);
        }

        return $this->successResponse([
            'token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => Auth::factory()->getTTL() * 60,
            'user' => \auth()->user()
        ],
            'Token returned'
        );
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return $this->successResponse(auth()->user(), 'OK', Response::HTTP_OK);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return $this->successResponse(null, 'Successfully logged out', Response::HTTP_OK);
    }

    public function renovateToken(Request $request)
    {
    }

    /**
     * Get user authenticated
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthUser(Request $request)
    {
        return $this->successResponse(
            \auth()->user(),
            'User logged in'
        );
    }

    /**
     * Send Reset Link Email
     *
     * @param Request $request
     * @param UserAdministration $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request, UserAdministration $user)
    {
        $userToResetPassword = UserAdministration::where('email', $request->email)->first();
        if(!$userToResetPassword) {
            $message = 'The email address ' . $request->email . ' is not associated with any account. Please verify your email address again and try again.';
            return $this->failResponse($message, Response::HTTP_UNAUTHORIZED);
        }
        if(!$user->isVerified($userToResetPassword->active)){
            $message = 'The user is not verified';
            return $this->failResponse(
                $message,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $userToResetPassword->reset_password_token = UserAdministration::generateToken(60);
        $userToResetPassword->reset_password_expired = Carbon::now()->addHours(8);
        $userToResetPassword->save();
        $data = array(
            'email' => $userToResetPassword->email,
            'first_name' => $userToResetPassword->first_name,
            'last_name' => $userToResetPassword->last_name,
            'url' => 'localhost:82/auth/resetPassword?token=' . $userToResetPassword->reset_password_token
        );
        Mail::to($userToResetPassword->email)->send(new SendResetLinkEmail($userToResetPassword->reset_password_token));
        return $this->successResponse($data, 'Email Sent', Response::HTTP_OK);
    }

    /**
     * Reset Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetPassword(Request $request)
    {
        $rules = [
            'token' => 'required|string',
            'old_password' => 'required|string',
            'password' => 'required|confirmed'
        ];
        $this->validate($request, $rules);
        $user = UserAdministration::where('reset_password_token', $request->token)
            ->where('reset_password_expired', '>', Carbon::now())->first();
        if(!Hash::check($request->old_password, $user->password)){
            return $this->failResponse(
                'Old Password is Incorrect',
                Response::HTTP_NOT_ACCEPTABLE
            );
        }
        if(!$user){
            $message = 'The token is invalid or has expired';
            return $this->failResponse(
                $message,
                Response::HTTP_UNAUTHORIZED);
        }
        $user->password = Hash::make($request->password);
        $user->reset_password_token = null;
        $user->reset_password_expired = null;
        $user->save();
        return $this->successResponse(
            $user,
            'Successfully Password Reset',
            Response::HTTP_OK
        );
    }
}
