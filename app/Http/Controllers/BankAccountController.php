<?php

namespace App\Http\Controllers;

use App\Exports\BankAccountExport;
use App\Models\BankAccount;
use App\Resources\DestroyResource;
use App\Validators\Rules\BankAccountRules;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class BankAccountController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class BankAccountController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, BankAccountRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $bankAccounts = BankAccount::where('consortium_id', $consortiumId)
            ->applyFilters()
            ->with('bank')
            ->addBalances()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($bankAccounts),
            'Bank Accounts lists returned');
    }

    /**
     * Store a new resource.
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, BankAccountRules::createRules());
        $bankAccount = BankAccount::create($request->all());
        return $this->successResponse(
            $bankAccount,
            'Bank Account created',
            201);
    }


    /**
     * Update the specified resource.
     * @param Request $request
     * @param $consortiumId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, BankAccountRules::editRules());
        $bankAccount = BankAccount::findOrFail($id);
        $bankAccount->fill($request->all());
        $bankAccount->save();
        return $this->successResponse(
            $bankAccount,
            'Bank Account edited',
            200);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $consortiumId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, BankAccountRules::showRules());
        $bankAccount = BankAccount::addBalances()->with('bank')->findOrFail($id);
        return $this->successResponse($bankAccount,
            'Bank Account returned');
    }

    /**
     * Remove the specified resource.
     * @param Request $request
     * @param $consortiumId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, BankAccountRules::destroyRules());
        return $this->successResponse(new DestroyResource(BankAccount::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     * @param Request $request
     * @param $consortiumId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, BankAccountRules::exportRules());
        return Excel::download(new BankAccountExport($consortiumId), 'bank-accounts.xlsx');
    }
}
