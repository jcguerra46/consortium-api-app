<?php

namespace App\Http\Controllers;

use App\Exports\BankAccountMovementExport;
use App\Models\BankAccountMovement;
use App\Resources\DestroyResource;
use App\Validators\Rules\BankAccountMovementsRules;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class BankAccountMovementController
 * @package App\Http\Controllers
 */
class BankAccountMovementController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $bankAccountId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $consortiumId, $bankAccountId)
    {
        $request['bank_account_id'] = $bankAccountId;
        $this->validate($request, BankAccountMovementsRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $movements = BankAccountMovement::where('bank_account_id', $bankAccountId)
            ->applyFilters()
            ->addBalanceLists($bankAccountId)
            ->ordered()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($movements));
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $bankAccountId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $consortiumId, $bankAccountId)
    {
        $request['bank_account_id'] = $bankAccountId;
        $this->validate($request, BankAccountMovementsRules::createRules());
        $bankAccountMovement = BankAccountMovement::create($request->except('cashbox_movement_id'));
        return $this->successResponse(
            $bankAccountMovement,
            'Bank Account Movement created'
        );
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $bankAccountId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit(Request $request, $consortiumId, $bankAccountId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, BankAccountMovementsRules::editRules());
        $bankAccountMovement = BankAccountMovement::find($id);
        $bankAccountMovement->fill($request->except('cashbox_movement_id', 'bank_account_id'));
        $bankAccountMovement->save();
        return $this->successResponse(
            $bankAccountMovement,
            'Bank Account Movement edited'
        );
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $bankAccountId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function delete(Request $request, $consortiumId, $bankAccountId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, BankAccountMovementsRules::deleteRules());
        return $this->successResponse(
            new DestroyResource(BankAccountMovement::destroy($id)),
            'Bank Account Movement deleted'
        );
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, BankAccountMovementsRules::showRules());
        $bankAccountMovement = BankAccountMovement::findOrFail($id);
        return $this->successResponse($bankAccountMovement,
            'Bank Account Movement returned');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $bankAccountId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function export(Request $request, $consortiumId, $bankAccountId)
    {
        $request['bank_account_id'] = $bankAccountId;
        $this->validate($request, BankAccountMovementsRules::exportRules());
        return Excel::download(new BankAccountMovementExport($bankAccountId), 'bank-account-movements.xlsx');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $bankAccountId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getBalance(Request $request, $consortiumId, $bankAccountId)
    {
        $request['bank_account_id'] = $bankAccountId;
        $this->validate($request, BankAccountMovementsRules::indexRules());
        $bankAccountBalance = BankAccountMovement::balance($bankAccountId);
        return $this->successResponse(
            $bankAccountBalance,
            'Bank Account Balance returned'
        );
    }
}
