<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\BankRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Resources\PaginationResource;

class BankController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, BankRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $banks = Bank::applyFilters()->paginate($pageSize);
        return $this->successResponse(new PaginationResource($banks));
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $bank = Bank::findOrFail($id);
        return $this->successResponse($bank, 'Ok', Response::HTTP_OK);
    }
}
