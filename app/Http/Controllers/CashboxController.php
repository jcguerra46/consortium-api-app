<?php

namespace App\Http\Controllers;

use App\Exports\CashboxExport;
use App\Models\Cashbox;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\CashboxRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CashboxController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, CashboxRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Cashbox::fromAdministration()->paginate($pageSize)));
    }

    /**
     * Update the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CashboxRules::editRules());
        $cashbox = Cashbox::findOrFail($id);
        $cashbox->fill($request->except('consortium_id'));
        $cashbox->save();
        return $this->successResponse(
            $cashbox,
            'Cashbox edited',
            200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CashboxRules::showRules());
        $cashbox = Cashbox::findOrFail($id);
        return $this->successResponse($cashbox);
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new CashboxExport(), 'cashboxes.xlsx');
    }
}
