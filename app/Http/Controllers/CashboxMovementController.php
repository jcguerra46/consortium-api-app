<?php

namespace App\Http\Controllers;

use App\Exports\CashboxMovementExport;
use App\Models\CashboxMovement;
use App\Services\CashboxMovementService;
use App\Validators\Rules\CashboxMovementRules;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CashboxMovementController extends Controller
{
    use ApiResponser;

    /** @var CashboxMovementService */
    private $cashboxMovementService;

    /**
     * CashboxMovementController constructor.
     * @param CashboxMovementService $cashboxMovementService
     */
    public function __construct(CashboxMovementService $cashboxMovementService)
    {
        $this->cashboxMovementService = $cashboxMovementService;
    }

    public function index(Request $request, $cashboxId)
    {
        $this->validate($request, CashboxMovementRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $cashboxMovements = CashboxMovement::where('cashbox_id', $cashboxId)
            ->applyFilters()
            ->addBalanceLists($cashboxId)
            ->orderBy('date', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate($pageSize);
        return $this->successResponse(
            new PaginationResource($cashboxMovements),
            'Cashbox movement pagination'
        );
    }

    public function show(Request $request, $cashboxId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CashboxMovementRules::showRules());

        $cashbox = CashboxMovement::with('functionalUnits', 'transfers', 'checksIncome', 'checksOutflow')->find($id);
        return $this->successResponse($cashbox,
            'Cashbox movement returned');
    }

    public function export(Request $request, $cashboxId)
    {
        return Excel::download(new CashboxMovementExport($cashboxId), 'cashbox-movements.xlsx');
    }

    public function createIncome(Request $request, $cashboxId)
    {
        $request['cashbox_id'] = $cashboxId;
        $this->validate($request, CashboxMovementRules::createIncomeRules());
        $data = $request->all();
        $checks = $data['checks'] ?? [];
        $bankAccountMovements = $data['bank_account_movements'] ?? [];
        $functionalUnits = $data['functional_units'] ?? [];

        $income = $this->cashboxMovementService->createIncome($data, $checks, $bankAccountMovements, $functionalUnits);
        return $this->successResponse(
            $income,
            'Income created',
            201
        );
    }

    public function createOutflow(Request $request, $cashboxId)
    {
        $request['cashbox_id'] = $cashboxId;
        $this->validate($request, CashboxMovementRules::createOutflowRules(), [], [], true);
        $data = $request->all();
        $checks = $data['checks'] ?? [];
        $bankAccountMovements = $data['bank_account_movements'] ?? [];

        $outflow = $this->cashboxMovementService->createOutflow($data, $checks, $bankAccountMovements);
        return $this->successResponse(
            $outflow,
            'Outflow created',
            201
        );
    }

    public function createInitialBalance(Request $request, $cashboxId)
    {
        $request['cashbox_id'] = $cashboxId;
        $this->validate($request, CashboxMovementRules::createIncomeRules());
        $data = $request->all();
        $checks = $data['checks'] ?? [];
        $bankAccountMovements = $data['bank_account_movements'] ?? [];

        $initialBalance = $this->cashboxMovementService->createInitialBalance($data, $checks, $bankAccountMovements);
        return $this->successResponse(
            $initialBalance,
            'Initial Balance created',
            201
        );
    }

    public function getBalances(Request $request, $cashboxId)
    {
        $request['cashbox_id'] = $cashboxId;
        $this->validate($request, CashboxMovementRules::getBalancesRules());

        $balances = $this->cashboxMovementService->getBalances($cashboxId);
        return $this->successResponse($balances,
            'Cashbox balances returned');
    }
}
