<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Resources\DestroyResource;
use App\Http\Traits\ApiResponser;
use App\Resources\PaginationResource;
use App\Validators\Rules\CategoryRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, CategoryRules::indexRules());
        $pageSize = $request->get('page_size', 30);
        $categories = Category::fromAdministration()
            ->applyFilters()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($categories),
            'Categories list returned');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, CategoryRules::storeRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $category = Category::create($data);
        return $this->successResponse(
            $category,
            'Category created',
            201);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CategoryRules::showRules());
        $category = Category::findOrFail($id);
        return $this->successResponse($category);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CategoryRules::updateRules());
        $category = Category::findOrFail($id);
        $category->fill($request->except('administration_id'));
        $category->save();
        return $this->successResponse(
            $category,
            'Category edited',
            200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CategoryRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(Category::destroy($id)));
    }
}
