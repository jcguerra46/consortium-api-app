<?php

namespace App\Http\Controllers;

use App\Exports\CheckExport;
use App\Models\Check;
use App\Validators\Rules\CheckRules;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class CheckController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, CheckRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $check = Check::fromAdministration()->applyFilters()->with('bank')->paginate($pageSize);
        return $this->successResponse(
            new PaginationResource($check)
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, CheckRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $check = Check::create($data);
        return $this->successResponse(
            $check,
            'Check created',
            201
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CheckRules::showRules());
        $check = Check::where('id', $id)->with('bank')->first();
        return $this->successResponse(
            $check,
            'Check returned'
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CheckRules::editRules());
        $check = Check::findOrFail($id);
        $check->update($request->all());
        return $this->successResponse(
            $check,
            'Check edited',
            200
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CheckRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(Check::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new CheckExport(), 'checks.xlsx');
    }
}
