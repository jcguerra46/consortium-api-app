<?php

namespace App\Http\Controllers;

use App\Exports\CheckbookExport;
use App\Models\Checkbook;
use App\Validators\Rules\CheckbookRules;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class CheckbookController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, CheckbookRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Checkbook::fromAdministration()->paginate($pageSize)));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, CheckbookRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $checkbook = DB::transaction(function () use ($data) {
            return Checkbook::create($data);
        });
        return $this->successResponse(
            $checkbook,
            'Checkbook created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CheckbookRules::showRules());
        $checkbook = Checkbook::with('checks')->findOrFail($id);
        return $this->successResponse($checkbook,
            'Checkbook returned');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CheckbookRules::editRules());
        $checkbook = Checkbook::findOrFail($id);
        $checkbook->update($request->only('number'));
        return $this->successResponse(
            $checkbook,
            'Checkbook edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CheckbookRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(Checkbook::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new CheckbookExport(), 'checkbooks.xlsx');
    }
}
