<?php

namespace App\Http\Controllers;

use App\Exports\ClaimExport;
use App\Models\Claim;
use App\Models\ClaimComment;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ClaimCommentRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ClaimCommentController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $id)
    {
        $this->validate($request, ClaimCommentRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(ClaimComment::where('claim_id', $id)->paginate($pageSize)));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, ClaimCommentRules::storeRules());
        $data = $request->all();
        $data['claim_id'] = $id;
        $claim = ClaimComment::create($data);
        return $this->successResponse(
            $claim,
            'Claim created',
            201);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @param $comment_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id, $comment_id)
    {
        $this->validate($request, ClaimCommentRules::showRules());
        $claimComment = ClaimComment::findOrFail($comment_id);
        return $this->successResponse($claimComment);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @param $comment_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $comment_id)
    {
        $this->validate($request, ClaimCommentRules::updateRules());
        $claimComment = ClaimComment::findOrFail($comment_id);
        $claimComment->update($request->all());
        return $this->successResponse(
            $claimComment,
            'Claim Comment updated',
            200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param $id
     * @param $comment_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id, $comment_id)
    {
        $this->validate($request, ClaimCommentRules::destroyRules());
        return $this->successResponse(new DestroyResource(ClaimComment::destroy($comment_id)));
    }
}
