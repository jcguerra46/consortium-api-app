<?php

namespace App\Http\Controllers;

use App\Exports\ClaimExport;
use App\Models\Claim;
use App\Models\ClaimComment;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ClaimCommentRules;
use App\Validators\Rules\ClaimRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ClaimController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, ClaimRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Claim::fromFunctionalunit()->paginate($pageSize)));
    }

    /**
     * Store a new resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, ClaimRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $claim = Claim::create($data);
        return $this->successResponse(
            $claim,
            'Claim created',
            201);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ClaimRules::showRules());
        $claim = Claim::findOrFail($id);
        return $this->successResponse($claim);
    }

    /**
     * Update the specified resource.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ClaimRules::editRules());
        $request['administration_id'] = Auth::user()->administration_id;
        $claim = Claim::findOrFail($id);
        $claim->update($request->all());
        return $this->successResponse(
            $claim,
            'Claim edited',
            200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ClaimRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(Claim::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new ClaimExport(), 'claim.xlsx');
    }
}
