<?php

namespace App\Http\Controllers;

use App\Exports\CollectionExport;
use App\Models\Collection;
use App\Services\CollectionService;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\CollectionRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CollectionController extends Controller
{
    use ApiResponser;

    /**  @var CollectionService */
    private $collectionService;

    public function __construct(CollectionService $collectionService)
    {
        $this->collectionService = $collectionService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, CollectionRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $periodId = $request->get('period_id', null);
        $qb = Collection::fromAdministration();
        isset($periodId) ? $qb->where('period_id', $periodId) : null;

        return $this->successResponse(new PaginationResource($qb->paginate($pageSize)),
            'Collection pagination');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, CollectionRules::createRules());
        $data = $request->all();
        $checks = $data['checks'] ?? [];
        $transfers = $data['transfers'] ?? [];
        $collection = $this->collectionService->create($data, $checks, $transfers);
        return $this->successResponse(
            $collection,
            'Collection created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, CollectionRules::showRules());
        $collection = Collection::findOrFail($id);
        return $this->successResponse($collection,
            'Collection returned');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        $periodId = $request->get('period_id', null);
        return Excel::download(new CollectionExport($periodId), 'collections.xlsx');
    }
}
