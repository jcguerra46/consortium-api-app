<?php

namespace App\Http\Controllers;

use App\Events\ConsortiumCreatedEvent;
use App\Exports\ConsortiumExport;
use App\Models\Cashbox;
use App\Models\Check;
use App\Models\Consortium;
use App\Models\ConsortiumProfile;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ConsortiumRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class Consortium
 * @package OctopusApiAppV2\controllers
 * @author  Juan Carlos Guerra <jguerra@octopus.com.ar>
 */
class ConsortiumController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, ConsortiumRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $pagination = Consortium::fromAdministrationIndex()
            ->applyFilters()
            ->applyOrder()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($pagination),
            'Consortium pagination returned');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, ConsortiumRules::createRules());
        $request['administration_id'] = Auth::user()->administration_id;
        $consortium = Consortium::create($request->all());
        $consortium->users()->attach(Auth::user()->id);

        /** @var  $consortiumProfile */
        $consortiumProfile = new ConsortiumProfile();
        $consortiumProfile->consortium_id = $consortium->id;
        $consortiumProfile->first_due_date = $request->first_due_date;

        /** Dispatch consortium created event */
        event(new ConsortiumCreatedEvent($consortium, $consortiumProfile));

        return $this->successResponse(
            $consortium,
            'Consortium created',
            201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::editRules());
        $consortium = Consortium::findOrFail($id);
        $consortium->fill($request->except('administration_id'));
        $consortium->save();
        return $this->successResponse(
            $consortium,
            'Consortium edited',
            200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::showRules());
        $consortium = Consortium::findOrFail($id);
        return $this->successResponse($consortium);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(Consortium::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new ConsortiumExport(), 'consortia.xlsx');
    }

    /**
     * Display a listing of functional units of the resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFunctionalUnits(Request $request, $consortiumId)
    {
        $request['id'] = $consortiumId;
        $this->validate($request, ConsortiumRules::getFunctionalUnitsRules());

        $pageSize = $request->get('page_size', 15);
        $consortium = Consortium::find($consortiumId);

        $functionalUnits = $consortium->functionalUnits()
            ->applyFilters()
            ->paginate($pageSize);

        return $this->successResponse(
            new PaginationResource($functionalUnits),
            'Functional Units from Consortium');
    }

    /**
     * Show a cashbox of the consoritum.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCashbox(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::getCashboxRules($id));
        $cashbox = Cashbox::where('consortium_id', $id)->first();
        return $this->successResponse($cashbox);
    }

    /**
     * Display a listing of checks of the consortium.
     *
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChecks(Request $request, $consortiumId)
    {
        $request['id'] = $consortiumId;
        $this->validate($request, ConsortiumRules::getChecksRules($consortiumId));
        $checks = Check::where('consortium_id', $consortiumId)
            ->applyFilters()
            ->get();
        return $this->successResponse($checks);
    }
}
