<?php

namespace App\Http\Controllers;

use App\Exports\ConsortiumMovementExport;
use App\Models\ConsortiumFinancialMovement;
use App\Models\ConsortiumNote;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ConsortiumMovementRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class ConsortiumMovementController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class ConsortiumMovementController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumMovementRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $consortiumMovements = ConsortiumFinancialMovement::where('consortium_id', $consortiumId)
            ->applyFilters()
            ->addBalanceLists($consortiumId)
            ->ordered()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($consortiumMovements),
            'Consortium Financial Movements pagination');
    }

    /**
     * @param Request $request
     * @param $id
     * @param $consortiumNoteId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumMovementRules::showRules());
        $consortiumMovement = ConsortiumFinancialMovement::findOrFail($id);
        return $this->successResponse($consortiumMovement,
            'Consortium Movement returned');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function export(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumMovementRules::paginateRules());
        return Excel::download(new ConsortiumMovementExport($consortiumId), 'consortium-movements.xlsx');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getBalance(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumMovementRules::getBalanceRules());
        $balance = ConsortiumFinancialMovement::balance($consortiumId);
        return $this->successResponse(
            $balance,
            'Consortium Financial balance returned'
        );
    }
}
