<?php

namespace App\Http\Controllers;

use App\Models\ConsortiumNote;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ConsortiumNoteRules;
use Illuminate\Http\Request;

/**
 * Class ConsortiumNoteController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class ConsortiumNoteController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $id)
    {
        $this->validate($request, ConsortiumNoteRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(ConsortiumNote::where('consortium_id', $id)
            ->ordered()
            ->paginate($pageSize)));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $id)
    {
        $request['consortium_id'] = $id;
        $this->validate($request, ConsortiumNoteRules::createRules());
        $data = $request->all();
        $consortiumNote = ConsortiumNote::create($data);
        return $this->successResponse(
            $consortiumNote,
            'Consortium Note created',
            201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $consortiumNoteId)
    {
        $request['consortium_id'] = $id;
        $request['id'] = $consortiumNoteId;
        $this->validate($request, ConsortiumNoteRules::editRules());
        $consortiumNote = ConsortiumNote::findOrFail($consortiumNoteId);
        $consortiumNote->fill($request->all());
        $consortiumNote->save();
        return $this->successResponse(
            $consortiumNote,
            'Consortium Note edited',
            200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id, $consortiumNoteId)
    {
        $request['id'] = $consortiumNoteId;
        $this->validate($request, ConsortiumNoteRules::showRules());
        $consortiumNote = ConsortiumNote::findOrFail($consortiumNoteId);
        return $this->successResponse($consortiumNote);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id, $consortiumNoteId)
    {
        $request['id'] = $consortiumNoteId;
        $this->validate($request, ConsortiumNoteRules::destroyRules());
        return $this->successResponse(new DestroyResource(ConsortiumNote::destroy($consortiumNoteId)));
    }

}
