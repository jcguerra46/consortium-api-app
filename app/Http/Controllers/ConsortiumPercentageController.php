<?php

namespace App\Http\Controllers;

use App\Exports\ConsortiumPercentageExport;
use App\Models\PercentageConsortium;
use App\Models\PercentageFunctionalUnit;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Services\ConsortiumPercentageService;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ConsortiumPercentageRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class ConsortiumPercentageController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class ConsortiumPercentageController extends Controller
{
    use ApiResponser;

    /**  @var ConsortiumPercentageService */
    private $consortiumPercentageService;

    /**
     * ConsortiumPercentageController constructor.
     * @param ConsortiumPercentageService $consortiumPercentageService
     */
    public function __construct(ConsortiumPercentageService $consortiumPercentageService)
    {
        $this->consortiumPercentageService = $consortiumPercentageService;
    }

    /**
     * Get listing of the resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumPercentageRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            PercentageConsortium::byConsortium($consortiumId)
            ->applyFilters()
            ->ordered()
            ->paginate($pageSize)));
    }

    /**
     * Store a new resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumPercentageRules::createRules($consortiumId));
        $data = $request->all();
        $percentageConsortium = $this->consortiumPercentageService->create($data);
        return $this->successResponse(
            $percentageConsortium,
            'Consortium Percentage created',
            201);
    }

    /**
     * Show a specified resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @param $consortiumPercentageId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $consortiumId, $percentageId)
    {
        $request['id'] = $percentageId;
        $this->validate($request, ConsortiumPercentageRules::showRules());
        $percentageConsortium = PercentageConsortium::findOrFail($percentageId);
        return $this->successResponse($percentageConsortium);
    }

    /**
     * Update a specified resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @param $consortiumPercentageId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $consortiumId, $percentageId)
    {
        $request['id'] = $percentageId;
        $this->validate($request, ConsortiumPercentageRules::editRules($consortiumId, $percentageId));
        $percentageConsortium = PercentageConsortium::findOrFail($percentageId);
        $percentageConsortium->fill($request->except('consortium_id'));
        $percentageConsortium->save();
        return $this->successResponse(
            $percentageConsortium,
            'Consortium Percentage edited',
            200);
    }

    /**
     * Delete a specified resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @param $consortiumPercentageId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $consortiumId, $percentageId)
    {
        $request['id'] = $percentageId;
        $this->validate($request, ConsortiumPercentageRules::destroyRules());
        return $this->successResponse(new DestroyResource(PercentageConsortium::destroy($percentageId)));
    }

    /**
     * Export listing of the resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request, $consortiumId)
    {
        return Excel::download(new ConsortiumPercentageExport($consortiumId), 'consortium-percentages.xlsx');
    }

    /**
     * Duplicate a specified resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @param $consortiumPercentageId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function duplicate(Request $request, $consortiumId, $percentageId)
    {
        $request['id'] = $percentageId;
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumPercentageRules::duplicateRules());
        $data = $request->all();
        $percentageConsortium = $this->consortiumPercentageService->duplicate($data);
        return $this->successResponse(
            $percentageConsortium,
            'Consortium Percentage has been duplicated',
            200);
    }

    /**
     * Store a batch of the resource.
     *
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     */
    public function massStore(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumPercentageRules::massCreate());
        $data = $request->all();
        $percentages = $this->consortiumPercentageService->massCreate($consortiumId, $data['percentages']);
        return $this->successResponse(
            $percentages,
            'Batch of Consortium Percentages created',
            201);
    }

    /**
     * Get percentages of the functional unit.
     *
     * @param Request $request
     * @param $consortiumId
     * @param $percentageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFunctionalUnitsPercentages(Request $request, $consortiumId, $percentageId)
    {
        $request['id'] = $percentageId;
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumPercentageRules::getFunctionalUnitsPercentagesRules());
        $percentageFunctionalUnits = PercentageFunctionalUnit::byPercentageConsortium($percentageId)->get();
        return $this->successResponse(
            $percentageFunctionalUnits,
            'Functional Units Percentages Values returned'
        );
    }

    /**
     * Update a batch of percentages of the functional unit.
     *
     * @param Request $request
     * @param $consortiumId
     * @param $percentageId
     * @return \Illuminate\Http\JsonResponse
     */
    public function massUpdateFunctionalUnitsPercentages(Request $request, $consortiumId, $percentageId)
    {
        $request['id'] = $percentageId;
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, ConsortiumPercentageRules::massUpdateFunctionalUnitsPercentagesRules());

        $data = $request->all();
        $percentageFunctionalUnits = $this->consortiumPercentageService->massUpdateFunctionalUnitsValue($percentageId, $data['functional_units']);

        return $this->successResponse(
            $percentageFunctionalUnits,
            'Functional Units Percentages Values edited'
        );
    }
}
