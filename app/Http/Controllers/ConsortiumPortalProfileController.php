<?php

namespace App\Http\Controllers;

use App\Models\ConsortiumPortalProfile;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ConsortiumRules;
use Illuminate\Http\Request;

/**
 * Class ConsortiumProfileController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class ConsortiumPortalProfileController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::showRules());
        $consortiumPortalProfile = ConsortiumPortalProfile::firstWhere(['consortium_id' => $id]);
        return $this->successResponse(
            $consortiumPortalProfile,
            'Consortium Profile returned');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function edit(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::editPortalProfileRules());
        $consortiumPortalProfile = ConsortiumPortalProfile::firstWhere(['consortium_id' => $id]);
        $consortiumPortalProfile->update($request->except('consortium_id'));
        return $this->successResponse(
            $consortiumPortalProfile,
            'Consortium Profile edited',
            200);
    }
}