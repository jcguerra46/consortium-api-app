<?php

namespace App\Http\Controllers;

use App\Models\ConsortiumProfile;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ConsortiumRules;
use Illuminate\Http\Request;

/**
 * Class ConsortiumProfileController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class ConsortiumProfileController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::showRules());
        $consortiumProfile = ConsortiumProfile::firstWhere(['consortium_id' => $id]);
        return $this->successResponse(
            $consortiumProfile,
            'Consortium Profile returned');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ConsortiumRules::editProfileRules());
        $consortiumProfile = ConsortiumProfile::firstWhere(['consortium_id' => $id]);
        $consortiumProfile->update($request->except('consortium_id'));
        return $this->successResponse(
            $consortiumProfile,
            'Consortium Profile edited',
            200);
    }
}
