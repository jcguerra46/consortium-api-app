<?php

namespace App\Http\Controllers;

use App\Exports\ContactExport;
use App\Models\Contact;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ContactRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ContactController extends Controller
{
    use ApiResponser;

    public function index(Request $request)
    {
        $this->validate($request, ContactRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Contact::fromAdministration()->paginate($pageSize)));
    }

    public function create(Request $request)
    {
        $this->validate($request, ContactRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $contact = Contact::create($data);
        return $this->successResponse(
            $contact,
            'Contact created',
            201);
    }

    public function edit(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ContactRules::editRules());
        $contact = Contact::find($id);
        $contact->fill($request->all());
        $contact->save();
        return $this->successResponse(
            $contact,
            'Contact edited',
            200);
    }

    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ContactRules::showRules());
        $contact = Contact::find($id);
        return $this->successResponse($contact);
    }

    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ContactRules::destroyRules());
        return $this->successResponse(new DestroyResource(Contact::destroy($id)));
    }

    public function export()
    {
        return Excel::download(new ContactExport(), 'contacts.xlsx');
    }
}
