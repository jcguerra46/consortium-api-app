<?php

namespace App\Http\Controllers;

use App\Validators\SequenceValidator;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @SWG\Swagger(
     *     @OA\Info (
     *          title = "Octopus API App",
     *          version = "2.1",
     *          @OA\Contact(
     *              email="jguerra@octopus.com.ar"
     *          ),
     *     )
     * )
     */
    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = [], bool $stopOnFirstFail = false)
    {
        if ($stopOnFirstFail) {
            $translator = app('validator')->getTranslator();
            $validator = new SequenceValidator($translator, $request->all(), $rules, $messages, $customAttributes);
            $validator->setPresenceVerifier(app()['validation.presence']);
        } else {
            $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);
        }

        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }

        return $this->extractInputFromRules($request, $rules);
    }
}
