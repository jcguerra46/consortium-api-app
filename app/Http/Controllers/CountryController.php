<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\Country;
use App\Resources\PaginationResource;
use App\Validators\Rules\CountryRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CountryController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, CountryRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Country::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $country = Country::findOrFail($id);
        return $this->successResponse($country, 'Ok', Response::HTTP_OK);
    }

}
