<?php

namespace App\Http\Controllers;

use App\Exports\ConsortiumExport;
use App\Exports\ProviderMovementExport;
use App\Models\ProviderFinancialMovement;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\CurrentAccountProviderRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class CurrentAccountProviderController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class CurrentAccountProviderController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $providerId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $providerId)
    {
        $request['provider_id'] = $providerId;
        $this->validate($request, CurrentAccountProviderRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $providerMovements = ProviderFinancialMovement::where('provider_id', $providerId)
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($providerMovements),
            'Provider movements returned');
    }

    /**
     * @param Request $request
     * @param $providerId
     * @param $movementId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $providerId, $movementId)
    {
        $request['movement_id'] = $movementId;
        $this->validate($request, CurrentAccountProviderRules::showRules());
        $movement = ProviderFinancialMovement::findOrFail($movementId);
        return $this->successResponse($movement);
    }

    /**
     * @param Request $request
     * @param $providerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBalance(Request $request, $providerId)
    {
        $request['provider_id'] = $providerId;
        $this->validate($request, CurrentAccountProviderRules::getBalance());
        $response = ProviderFinancialMovement::balance($providerId);
        return $this->successResponse($response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new ProviderMovementExport(), 'provider_movements.xlsx');
    }
}
