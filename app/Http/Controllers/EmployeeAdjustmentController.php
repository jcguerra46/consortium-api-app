<?php

namespace App\Http\Controllers;

use App\Models\EmployeeAdjustment;
use App\Validators\Rules\EmployeeAdjustmentRules;
use App\Exports\EmployeeAdjustmentsExport;
use App\Http\Traits\ApiResponser;
use App\Resources\PaginationResource;
use Illuminate\Http\Request;
use App\Resources\DestroyResource;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeAdjustmentController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $employeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $employeeId)
    {
        $request['employee_id'] = $employeeId;
        $this->validate($request, EmployeeAdjustmentRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(
            new PaginationResource(
                EmployeeAdjustment::where('employee_id', $employeeId)
                    ->paginate($pageSize)
            ),
            'Employee Adjustments pagination'
        );
    }

    /**
     * @param Request $request
     * @param $employeeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $employeeId)
    {
        $request['employee_id'] = $employeeId;
        $this->validate($request, EmployeeAdjustmentRules::createRules());
        $data = $request->all();
        $employeeAdjustment = EmployeeAdjustment::create($data);
        return $this->successResponse(
            $employeeAdjustment,
            'Employee Adjustment created',
            201);
    }

    /**
     * @param Request $request
     * @param $employeeId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $employeeId, $id)
    {
        $request['id'] = $id;
        $request['employee_id'] = $employeeId;
        $this->validate($request, EmployeeAdjustmentRules::showRules());
        $employeeAdjustment = EmployeeAdjustment::findOrFail($id);
        return $this->successResponse(
            $employeeAdjustment,
            'Employee Adjustment returned',
            200
        );
    }

    /**
     * @param Request $request
     * @param $employeeId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $employeeId, $id)
    {
        $request['employee_id'] = $employeeId;
        $request['id'] = $id;
        $this->validate($request, EmployeeAdjustmentRules::editRules());
        $employeeAdjustment = EmployeeAdjustment::findOrFail($id);
        $employeeAdjustment->update($request->all());
        return $this->successResponse(
            $employeeAdjustment,
            'Employee Adjustment edited',
            200);
    }

    /**
     * @param Request $request
     * @param $employeeId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $employeeId, $id)
    {
        $request['employee_id'] = $employeeId;
        $request['id'] = $id;
        $this->validate($request, EmployeeAdjustmentRules::destroyRules());
        return $this->successResponse(new DestroyResource(EmployeeAdjustment::destroy($id)));
    }

    /**
     * @param Request $request
     * @param $employeeId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request, $employeeId)
    {
        $request['employee_id'] = $employeeId;
        $this->validate($request, EmployeeAdjustmentRules::exportRules());
        return Excel::download(new EmployeeAdjustmentsExport($employeeId), 'employee-adjustments.xlsx');
    }
}
