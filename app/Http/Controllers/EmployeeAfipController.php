<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Models\Employee;
use App\Models\EmployeeAfip;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\EmployeeRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class EmployeeAfipController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class EmployeeAfipController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::showRules());
        $employeeAfip = EmployeeAfip::firstWhere(['employee_id' => $id]);
        return $this->successResponse(
            $employeeAfip,
            'Employee AFIP data returned');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::editAfipRules());
        $employeeAfip = EmployeeAfip::firstWhere(['employee_id' => $id]);
        $employeeAfip->fill($request->except('employee_id'));
        $employeeAfip->save();
        return $this->successResponse(
            $employeeAfip,
            'Employee AFIP data edited',
            200);
    }
}
