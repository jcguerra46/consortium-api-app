<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\Employee;
use App\Models\EmployeeBeneficiary;
use App\Resources\PaginationResource;
use App\Validators\Rules\EmployeeBeneficiaryRules;
use Illuminate\Http\Request;
use App\Resources\DestroyResource;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\EmployeeBeneficiaryExport;

class EmployeeBeneficiaryController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $id)
    {
        $this->validate($request, EmployeeBeneficiaryRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(
            new PaginationResource(
                EmployeeBeneficiary::where('employee_id', $id)
                    ->paginate($pageSize)
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $id)
    {
        $request['employee_id'] = $id;
        $this->validate($request, EmployeeBeneficiaryRules::storeRules());
        $data = $request->all();
        $beneficiary = EmployeeBeneficiary::create($data);
        return $this->successResponse(
            $beneficiary,
            'Employee Beneficiary created',
            201);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @param $beneficiary_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id, $beneficiary_id)
    {
        $request['employee_id'] = $id;
        $request['beneficiary_id'] = $beneficiary_id;
        $this->validate($request, EmployeeBeneficiaryRules::showRules());
        $employeeBeneficiary = EmployeeBeneficiary::where('employee_id', $id)
            ->where('id', $beneficiary_id)
            ->get();
        return $this->successResponse(
            $employeeBeneficiary,
            'success',
            200
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @param $beneficiary_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $beneficiary_id)
    {
        $request['employee_id'] = $id;
        $request['beneficiary_id'] = $beneficiary_id;
        $this->validate($request, EmployeeBeneficiaryRules::updateRules());
        $employeeBeneficiary = EmployeeBeneficiary::findOrFail($beneficiary_id);
        $employeeBeneficiary->update($request->all());
        return $this->successResponse(
            $employeeBeneficiary,
            'Employee Beneficiary updated',
            200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param $id
     * @param $beneficiary_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id, $beneficiary_id)
    {
        $request['employee_id'] = $id;
        $request['beneficiary_id'] = $beneficiary_id;
        $this->validate($request, EmployeeBeneficiaryRules::destroyRules());
        return $this->successResponse(new DestroyResource(EmployeeBeneficiary::destroy($beneficiary_id)));
    }

}
