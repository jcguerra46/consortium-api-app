<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Models\Employee;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\EmployeeRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class EmployeeController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class EmployeeController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, EmployeeRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $employee = Employee::fromAdministration()
            ->applyFilters()
            ->with('ProfessionalFunction')
            ->paginate($pageSize);
        return $this->successResponse(new PaginationResource($employee));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, EmployeeRules::createRules());
        $data = $request->all();
        $employee = Employee::create($data);
        return $this->successResponse(
            $employee,
            'Employee created',
            201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::editRules());
        $employee = Employee::findOrFail($id);
        $employee->fill($request->all());
        $employee->save();
        return $this->successResponse(
            $employee,
            'Employee edited',
            200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::showRules());
        $employee = Employee::findOrFail($id);
        return $this->successResponse($employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::destroyRules());
        return $this->successResponse(new DestroyResource(Employee::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new EmployeeExport(), 'employees.xlsx');
    }
}
