<?php

namespace App\Http\Controllers;

use App\Models\EmployeeData;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\EmployeeRules;
use Illuminate\Http\Request;

/**
 * Class EmployeeDataController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class EmployeeDataController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::showRules());
        $employeeData = EmployeeData::firstWhere(['employee_id' => $id]);
        return $this->successResponse(
            $employeeData,
            'Employee Data returned');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::editDataRules());
        $employeeData = EmployeeData::firstWhere(['employee_id' => $id]);
        $employeeData->fill($request->except('employee_id'));
        $employeeData->save();
        return $this->successResponse(
            $employeeData,
            'Employee Data edited',
            200);
    }
}
