<?php

namespace App\Http\Controllers;

use App\Models\EmployeeProfile;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\EmployeeRules;
use Illuminate\Http\Request;

/**
 * Class EmployeeProfileController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class EmployeeProfileController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::showRules());
        $employeeProfile = EmployeeProfile::firstWhere(['employee_id' => $id]);
        return $this->successResponse(
            $employeeProfile,
            'Employee Profile returned');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, EmployeeRules::editProfileRules());
        $employeeProfile = EmployeeProfile::firstWhere(['employee_id' => $id]);
        $employeeProfile->fill($request->except('employee_id'));
        $employeeProfile->save();
        return $this->successResponse(
            $employeeProfile,
            'Employee Profile edited',
            200);
    }
}
