<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\EmployeeStatus;
use App\Resources\PaginationResource;
use App\Validators\Rules\EmployeeStatusRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EmployeeStatusController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, EmployeeStatusRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(EmployeeStatus::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $employeeStatus = EmployeeStatus::findOrFail($id);
        return $this->successResponse($employeeStatus, 'Ok', Response::HTTP_OK);
    }
}
