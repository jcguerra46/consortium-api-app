<?php

namespace App\Http\Controllers;

use App\Exports\ExpenseHeaderExport;
use App\Http\Traits\ApiResponser;
use App\Models\ExpenseHeader;
use App\Validators\Rules\ExpenseHeaderRules;
use Illuminate\Http\Request;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ExpenseHeaderController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, ExpenseHeaderRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            ExpenseHeader::fromAdministration()
                ->paginate($pageSize)));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, ExpenseHeaderRules::storeRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $header = ExpenseHeader::create($data);
        return $this->successResponse(
            $header,
            'Expense Header created',
            201);
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ExpenseHeaderRules::showRules());
        $expenseHeader = ExpenseHeader::findOrFail($id);
        return $this->successResponse($expenseHeader, 'OK', 200);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ExpenseHeaderRules::updateRules());
        $expenseHeader = ExpenseHeader::findOrFail($id);
        $expenseHeader->fill($request->except('administration_id'));
        $expenseHeader->save();
        return $this->successResponse(
            $expenseHeader,
            'Expense Header edited',
            200);
    }

    /**
     * Remove the specified resource from storage.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ExpenseHeaderRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(ExpenseHeader::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new ExpenseHeaderExport(), 'expenseheaders.xlsx');
    }
}
