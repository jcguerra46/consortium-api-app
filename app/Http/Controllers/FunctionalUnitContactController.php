<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Contact;
use App\Models\FunctionalUnitContact;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ContactRules;
use App\Validators\Rules\FunctionalUnitContactRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class FunctionalUnitContactController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $functional_unit_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $functional_unit_id)
    {
        $this->validate($request, FunctionalUnitContactRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $contacts = FunctionalUnitContact::contactsFromFunctionalUnit($functional_unit_id)
            ->applyFilters()
            ->paginate($pageSize);
        return $this->successResponse(new PaginationResource($contacts),
            'Contacts selected');
    }

    /**
     * @param Request $request
     * @param Int $functional_unit_id
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function store(Request $request, $functional_unit_id)
    {
        $this->validate($request, ContactRules::createRules());

        try {
            DB::beginTransaction();
            $request['administration_id'] = Auth::user()->administration_id;
            $contact = Contact::create($request->all());
            $request['contact_id'] = $contact->id;
            $request['functional_unit_id'] = $functional_unit_id;
            $validator = Validator::make($request->all(),FunctionalUnitContactRules::createRules());
            if ($validator->fails()) {
                DB::rollBack();
                return $this->failResponse($validator->errors(), 422, null);
            }

            $data = $request->all();
            $functionalUnitContact = FunctionalUnitContact::create($data);
            DB::commit();
            return $this->successResponse(
                $functionalUnitContact,
                'Functional Unit Contact created',
                Response::HTTP_CREATED
            );
        } catch (Exception $e) {
            DB::rollBack();
            return $this->failResponse($e->getMessage(), 422, null);
        }

    }

    /**
     * @param Request $request
     * @param $functional_unit_id
     * @param $functionalUnitContactId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $functional_unit_id, $functionalUnitContactId)
    {
        $request['id'] = $functionalUnitContactId;
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitContactRules::editRules());
        $functionalUnitContact = FunctionalUnitContact::findOrFail($functional_unit_id);
        $functionalUnitContact->update($request->all());
        return $this->successResponse(
            $functionalUnitContact,
            'Functional Unit Contact edited',
            200
        );
    }

    /**
     * @param Request $request
     * @param $id
     * @param $functionalUnitContactId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $functional_unit_id, $functionalUnitContactId)
    {
        $request['id'] = $functionalUnitContactId;
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitContactRules::showRules());
        $functionalUnitContact = FunctionalUnitContact::where('id', $functionalUnitContactId)
            ->where('functional_unit_id', $functional_unit_id)->get();
        return $this->successResponse($functionalUnitContact);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $functionalUnitContactId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $functional_unit_id, $functionalUnitContactId)
    {
        $request['functional_unit_id'] = $functional_unit_id;
        $request['id'] = $functionalUnitContactId;
        $this->validate($request, FunctionalUnitContactRules::destroyRules());
        return $this->successResponse(new DestroyResource(FunctionalUnitContact::destroy($functionalUnitContactId)));
    }
}
