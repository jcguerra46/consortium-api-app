<?php

namespace App\Http\Controllers;

use App\Exports\FunctionalUnitExport;
use App\Models\FunctionalUnit;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\FunctionalUnitRules;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class FunctionalUnitController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $consortium_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, FunctionalUnitRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $functionalUnits = FunctionalUnit::fromConsortiums()
            ->byConsortium($request->consortium_id)
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($functionalUnits),
            'Functional Units list returned'
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, FunctionalUnitRules::createRules());
        $data = $request->all();
        $functionalUnit = FunctionalUnit::create($data);
        return $this->successResponse(
            $functionalUnit,
            'Functional Unit created',
            201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, FunctionalUnitRules::editRules());
        $functionalUnit = FunctionalUnit::findOrFail($id);
        $functionalUnit->fill($request->all());
        $functionalUnit->save();
        return $this->successResponse(
            $functionalUnit,
            'Functional Unit edited',
            200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, FunctionalUnitRules::showRules());
        $functionalUnit = FunctionalUnit::findOrFail($id);
        return $this->successResponse($functionalUnit);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, FunctionalUnitRules::destroyRules());
        return $this->successResponse(new DestroyResource(FunctionalUnit::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new FunctionalUnitExport(), 'functional_units.xlsx');
    }

    /**
     * Store a batch created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeBatchOfFunctionalUnits(Request $request)
    {
        $this->validate($request, FunctionalUnitRules::massCreate());
        try {
            DB::beginTransaction();
            foreach ($request->functional_units as $functional_unit) {
                $functional_unit['consortium_id'] = $request->consortium_id;
                $functional_unit['created_at'] = Carbon::now();
                $functional_unit['updated_at'] = Carbon::now();
                $fullData[] = $functional_unit;
            }

            $chunks = array_chunk($fullData, 100);
            foreach ($chunks as $chunk) {
                FunctionalUnit::insert($chunk);
            }
            DB::commit();

            $functionalUnitsInserted = FunctionalUnit::byConsortium($request->consortium_id)
                ->get();
            return $this->successResponse(
                $functionalUnitsInserted,
                'Functional Units Created',
                Response::HTTP_CREATED
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->errorResponse(
                'Batch of Functional Units Unprocessable. ' . $exception->getMessage(),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                null
            );
        }
    }
}
