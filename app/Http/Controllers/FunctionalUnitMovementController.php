<?php

namespace App\Http\Controllers;

use App\Exports\FunctionalUnitMovementExport;
use App\Models\FunctionalUnitMovement;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Services\FunctionalUnitMovements\FunctionalUnitMovementService;
use App\Validators\Rules\FunctionalUnitMovementRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FunctionalUnitMovementController extends Controller
{
    use ApiResponser;

    /** @var FunctionalUnitMovementService */
    private $functionalUnitMovementService;

    /**
     * SpendingController constructor.
     *
     * FunctionalUnitMovementController constructor.
     * @param FunctionalUnitMovementService $functionalUnitMovementService
     */
     public function __construct(FunctionalUnitMovementService $functionalUnitMovementService)
     {
         $this->functionalUnitMovementService = $functionalUnitMovementService;
     }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param $functional_unit_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $functional_unit_id)
    {
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitMovementRules::indexRules());
        $pageSize = $request->get('page_size', 15);
         return $this->successResponse(
             new PaginationResource(
                 FunctionalUnitMovement::byFunctionalUnit($functional_unit_id)
                     ->addBalanceList($functional_unit_id)
                     ->ordered()
                     ->paginate($pageSize)
             ),
             'Functional Unit Movements pagination'
         );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $functional_unit_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $functional_unit_id)
    {
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitMovementRules::storeRules($request->all()));
        $data = $request->all();
        $movement = $this->functionalUnitMovementService->storeService($data);
        return $this->successResponse(
            $movement,
            'Functional Unit Movement created',
            201);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $functional_unit_id
     * @param $functionalUnitMovementId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $functional_unit_id, $functionalUnitMovementId)
    {
        $request['id'] = $functionalUnitMovementId;
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitMovementRules::showRules());
        $functionalUnitMovement = FunctionalUnitMovement::findOrFail($functionalUnitMovementId);
        return $this->successResponse(
            $functionalUnitMovement,
            'Functional Unit Movement returned'
        );
    }

    /**
     * Export a listing of the resource.
     *
     * @param Request $request
     * @param $functional_unit_id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request, $functional_unit_id)
    {
        return Excel::download(new FunctionalUnitMovementExport($functional_unit_id), 'functional_units_movements.xlsx');
    }

    /**
     * Display a balance of functional unit.
     *
     * @param Request $request
     * @param $functional_unit_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBalance(Request $request, $functional_unit_id)
    {
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitMovementRules::getBalanceRules());
        $balance = FunctionalUnitMovement::balance($functional_unit_id);
        return $this->successResponse(
            $balance,
            'Functional Unit Balance returned'
        );
    }

    /**
     * Display debts of functional unit.
     *
     * @param Request $request
     * @param $functional_unit_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDebts(Request $request, $functional_unit_id)
    {
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitMovementRules::getDebtsRules());
        $capitalDebt = FunctionalUnitMovement::byFunctionalUnit($functional_unit_id)
            ->accumulatedCapital($functional_unit_id)
            ->first();
        $interestDebt = FunctionalUnitMovement::byFunctionalUnit($functional_unit_id)
            ->accumulatedInterest($functional_unit_id)
            ->first();
        return $this->successResponse(
            $debts = [
                'capital' => $capitalDebt->capital,
                'interest' => $interestDebt->interest
            ],
            'Functional Unit Debts returned'
        );
    }
}
