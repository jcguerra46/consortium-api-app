<?php

namespace App\Http\Controllers;

use App\Models\PercentageFunctionalUnit;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Services\FunctionalUnitPercentageService;
use App\Validators\Rules\FunctionalUnitPercentageRules;
use Illuminate\Http\Request;

class FunctionalUnitPercentageController extends Controller
{
    use ApiResponser;

    /** @var FunctionalUnitPercentageService */
    protected $functionalUnitPercentageService;

    /**
     * FunctionalUnitPercentageController constructor.
     * @param FunctionalUnitPercentageService $functionalUnitPercentageService
     */
    public function __construct(FunctionalUnitPercentageService $functionalUnitPercentageService)
    {
        $this->functionalUnitPercentageService = $functionalUnitPercentageService;
    }

    /**
     * @param Request $request
     * @param $functionalUnitId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $functional_unit_id)
    {
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitPercentageRules::indexPercentageFU());
        $pageSize = $request->get('page_size', 15);
        $percentagesFUs = PercentageFunctionalUnit::where('functional_unit_id', $functional_unit_id)
            ->with('percentageConsortium')
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($percentagesFUs),
            'Functional Units Percentages returned');
    }

    /**
     * @param Request $request
     * @param $functional_unit_id
     * @param $percentageFUId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $functional_unit_id, $percentageFUId)
    {
        $request['id'] = $percentageFUId;
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitPercentageRules::editRules());
        $percentageFU = PercentageFunctionalUnit::findOrFail($percentageFUId);
        $percentageFU->update($request->only('value'));
        return $this->successResponse(
            $percentageFU,
            'Percentage Functional Unit edited',
            200);
    }

    /**
     * @param Request $request
     * @param $functional_unit_id
     * @param $percentageFUId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $functional_unit_id, $percentageFUId)
    {
        $request['id'] = $percentageFUId;
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitPercentageRules::showRules());
        $percentageFU = PercentageFunctionalUnit::findOrFail($percentageFUId);
        return $this->successResponse($percentageFU);
    }

    /**
     * @param Request $request
     * @param $functional_unit_id
     */
    public function massUpdate(Request $request, $functional_unit_id)
    {
        $request['functional_unit_id'] = $functional_unit_id;
        $this->validate($request, FunctionalUnitPercentageRules::massUpdateRules());
        $percentages = $request->percentages;
        $percentagesUpdated = $this->functionalUnitPercentageService->massUpdate($percentages);

        return $this->successResponse($percentagesUpdated,
            'Functional Units percentages updated');
    }
}
