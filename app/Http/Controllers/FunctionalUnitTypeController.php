<?php

namespace App\Http\Controllers;

use App\Exports\FunctionalUnitTypeExport;
use App\Http\Traits\ApiResponser;
use App\Models\FunctionalUnitType;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Validators\Rules\FunctionalUnitTypeRules;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class FunctionalUnitTypeController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, FunctionalUnitTypeRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            FunctionalUnitType::paginate($pageSize)
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, FunctionalUnitTypeRules::storeRules());
        $functional_unit_type = FunctionalUnitType::create($request->all());
        return $this->successResponse(
            $functional_unit_type,
            'Functional unit type created',
            Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, FunctionalUnitTypeRules::showRules());
        $functional_unit_type = FunctionalUnitType::findOrFail($id);
        return $this->successResponse($functional_unit_type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, FunctionalUnitTypeRules::updateRules());
        $functional_unit_type = FunctionalUnitType::findOrFail($id);
        $functional_unit_type->update($request->all());
        return $this->successResponse(
            $functional_unit_type,
            'Functional unit type edited',
            Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, FunctionalUnitTypeRules::destroyRules($id));
        return $this->successResponse(new DestroyResource(FunctionalUnitType::destroy($id)));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new FunctionalUnitTypeExport(), 'functionalunittypes.xlsx');
    }
}
