<?php

namespace App\Http\Controllers;

use App\Exports\InvoiceExport;
use App\Models\Invoice;
use App\Validators\Rules\InvoiceRules;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class InvoiceController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, InvoiceRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            Invoice::fromAdministration()->paginate($pageSize)
        ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, InvoiceRules::createRules());
        $data = $request->except(['self_generated', 'administration_id']);
        $data['administration_id'] = Auth::user()->administration_id;
        $invoice = Invoice::create($data);
        return $this->successResponse(
            $invoice,
            'Invoice created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, InvoiceRules::editRules());
        $invoice = Invoice::find($id);
        $invoice->fill($request->except(['self_generated', 'administration_id']));
        $invoice->save();
        return $this->successResponse(
            $invoice,
            'Invoice edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, InvoiceRules::showRules());
        $invoice = Invoice::findOrFail($id);
        return $this->successResponse($invoice);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, InvoiceRules::destroyRules());
        return $this->successResponse(new DestroyResource(Invoice::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new InvoiceExport(), 'invoices.xlsx');
    }
}
