<?php

namespace App\Http\Controllers;

use App\Exports\JudgmentExport;
use App\Models\Judgment;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\JudgmentRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class JudgmentController extends Controller
{
    use ApiResponser;

    public function index(Request $request)
    {
        $this->validate($request, JudgmentRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            Judgment::fromAdministration()->paginate($pageSize)
        ));
    }

    public function create(Request $request)
    {
        $this->validate($request, JudgmentRules::createRules());
        $data = $request->all();
        $judgment = Judgment::create($data);
        return $this->successResponse(
            $judgment,
            'Judgment created',
            201);
    }

    public function edit(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, JudgmentRules::editRules());
        $judgment = Judgment::find($id);
        $judgment->fill($request->all());
        $judgment->save();
        return $this->successResponse(
            $judgment,
            'Judgment edited',
            200);
    }

    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, JudgmentRules::showRules());
        $judgment = Judgment::find($id);
        return $this->successResponse($judgment);
    }

    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, JudgmentRules::destroyRules());
        return $this->successResponse(new DestroyResource(Judgment::destroy($id)));
    }

    public function export()
    {
        return Excel::download(new JudgmentExport(), 'judgments.xlsx');
    }
}
