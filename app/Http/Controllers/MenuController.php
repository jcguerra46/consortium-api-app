<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\UserAdministration;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\MenuRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends Controller
{
    use ApiResponser;

    /**
     * Save a newly menu in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function setUserMenu(Request $request)
    {
        $this->validate($request, MenuRules::userMenuRules());
        $userAdministration = UserAdministration::findOrFail(Auth::user()->id);
        $userAdministration->menu = $request['menu'];
        if($userAdministration->save()) {
            return $this->successResponse(
                json_decode($userAdministration->menu),
                'OK',
                Response::HTTP_OK
            );
        }
        return $this->failResponse(
            'Unprocessable Entity',
            Response::HTTP_UNPROCESSABLE_ENTITY,
            null
        );
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        $custom_menu = Auth::user()->menu;
        if(!$custom_menu) {
            $defaultMenu = Menu::menuActive()->data;
            return $this->successResponse(
                json_decode($defaultMenu),
                'OK',
                Response::HTTP_OK
            );
        }
        return $this->successResponse(
            json_decode($custom_menu),
            'OK',
            Response::HTTP_OK
        );
    }

    /**
     * Display the specified resource.
     *
     * @return mixed
     */
    public function getMenuDefault()
    {
        $menu = Menu::menuActive()->data;
        return $this->successResponse(
            json_decode($menu),
            'OK',
            Response::HTTP_OK
        );
    }

    /**
     * Set menu in null in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setMenuDefault(Request $request)
    {
        $userAdministration = UserAdministration::findOrFail(Auth::user()->id);
        $userAdministration->menu = null;
        if($userAdministration->save()) {
            return $this->successResponse(
                json_decode($userAdministration->menu),
                'OK',
                Response::HTTP_OK
            );
        }
        return $this->failResponse(
            'Unprocessable Entity',
            Response::HTTP_UNPROCESSABLE_ENTITY,
            null
        );
    }
}
