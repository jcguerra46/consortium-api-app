<?php

namespace App\Http\Controllers;

use App\Exports\NoteExport;
use App\Models\Note;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\NoteRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class NoteController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, NoteRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            Note::fromAdministration()->paginate($pageSize)
        ));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, NoteRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $note = Note::create($data);
        return $this->successResponse(
            $note,
            'Note created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, NoteRules::editRules());
        $note = Note::find($id);
        $note->fill($request->except('administration_id'));
        $note->save();
        return $this->successResponse(
            $note,
            'Note edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, NoteRules::showRules());
        $note = Note::find($id);
        return $this->successResponse($note);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, NoteRules::destroyRules());
        return $this->successResponse(new DestroyResource(Note::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new NoteExport(), 'notes.xlsx');
    }
}
