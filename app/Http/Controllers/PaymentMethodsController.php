<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Resources\PaginationResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Validators\Rules\PaymentMethodsRules;
use App\Models\PaymentMethod;

class PaymentMethodsController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, PaymentMethodsRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            PaymentMethod::paginate($pageSize)
        ),
            'Payment Method pagination');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentMethodsRules::showRules());
        $paymentMethod = PaymentMethod::findOrFail($id);
        return $this->successResponse($paymentMethod,
            'Payment Method returned',
            Response::HTTP_OK);
    }
}
