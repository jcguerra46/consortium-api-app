<?php

namespace App\Http\Controllers;

use App\Exports\PaymentOrderExport;
use App\Http\Traits\ApiResponser;
use App\Models\PaymentOrder;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Services\PaymentOrderService;
use App\Validators\Rules\PaymentOrderRules;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;

class PaymentOrderController extends Controller
{
    use ApiResponser;

    /** @var PaymentOrderService */
    private $paymentOrderService;

    /**
     * PaymentOrderController constructor.
     * @param PaymentOrderService $paymentOrderService
     */
    public function __construct(PaymentOrderService $paymentOrderService)
    {
        $this->paymentOrderService = $paymentOrderService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, PaymentOrderRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(
            new PaginationResource(
                PaymentOrder::fromAdministration()
                    ->applyFilters()
                    ->with('provider')
                    ->paginate($pageSize)
            ),
            'Payment Orders pagination'
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, PaymentOrderRules::createRules());
        $data = $request->only(['provider_id', 'description', 'installments', 'date', 'payment_order_type']);

        $paymentOrder = $this->paymentOrderService->create($data);

        return $this->successResponse($paymentOrder, 'Payment Order created', 201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentOrderRules::editRules());
        $data = $request->only(['description', 'installments', 'date']);

        $paymentOrder = $this->paymentOrderService->edit($id, $data);

        return $this->successResponse($paymentOrder, 'Payment Order edited', 200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentOrderRules::showRules());
        $paymentOrder = PaymentOrder::with(['installments', 'installments.spending', 'installments.spending.consortium', 'provider'])->findOrFail($id);
        return $this->successResponse($paymentOrder, 'Payment Order returned');
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentOrderRules::destroyRules());
        return $this->successResponse(new DestroyResource(PaymentOrder::destroy($id)));
    }

    /**
     * @return BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new PaymentOrderExport(), 'payment-orders.xlsx');
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function duplicate(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentOrderRules::duplicateRules());
        $paymentOrder = $this->paymentOrderService->duplicate($id);
        return $this->successResponse($paymentOrder,
            'Payment Order duplicated');
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function annular(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentOrderRules::annularRules());
        $paymentOrder = PaymentOrder::findOrFail($id);
        $paymentOrder->status = 'annulled';
        $paymentOrder->save();
        return $this->successResponse($paymentOrder,
            'Payment Order annulled');
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws Throwable
     */
    public function printOrder(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentOrderRules::printOrderRules());
        $paymentOrder = PaymentOrder::findOrFail($id);
        $response['html'] = $this->paymentOrderService->generateHtml($paymentOrder);
        return $this->successResponse($response,
            'Payment Order HTML');
    }

    public function doPayment(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentOrderRules::doPayment());
        $paymentOrder = PaymentOrder::findOrFail($id);

        $this->paymentOrderService->doPayment($request, $paymentOrder);

        return $this->successResponse($paymentOrder, 'Payment order succesfully payed');
    }
}
