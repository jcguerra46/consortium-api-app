<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Services\PaymentService;
use App\Validators\Rules\PaymentsRules;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Resources\PaginationResource;
use Maatwebsite\Excel\Facades\Excel;


class PaymentsController extends Controller
{
    use ApiResponser;

    /**  @var PaymentService */
    private $paymentService;

    /**
     * PaymentsController constructor.
     * @param PaymentService $paymentService
     */
    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, PaymentsRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $payments = Payment::where('provider_id', $request->get('provider_id'))
            ->paginate($pageSize);
        return $this->successResponse(new PaginationResource($payments),
            'Payment pagination');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, PaymentsRules::createRules(), [], [], true);
        $dataPayment = $request->all();
        $checks = $dataPayment['checks'] ?? [];
        $transfers = $dataPayment['transfers'] ?? [];
        $payment = $this->paymentService->create($dataPayment, $checks, $transfers);
        return $this->successResponse($payment,
            'Payment create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDirect(Request $request)
    {
        $this->validate($request, PaymentsRules::createDirectRules(), [], [], true);
        $dataPayment = $request->all();
        $checks = $dataPayment['checks'] ?? [];
        $transfers = $dataPayment['transfers'] ?? [];
        $payment = $this->paymentService->createDirect($dataPayment, $checks, $transfers);
        return $this->successResponse($payment,
            'Payment direct created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, PaymentsRules::showRules());
        $payment = Payment::findOrFail($id);
        return $this->successResponse($payment,
            'Payment returned');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function export(Request $request)
    {
        $this->validate($request, PaymentsRules::exportRules());
        return Excel::download(new PeriodExport(), 'payments.xlsx');
    }
}
