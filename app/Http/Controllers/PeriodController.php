<?php

namespace App\Http\Controllers;

use App\Exports\PeriodExport;
use App\Models\Period;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\PeriodRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class PeriodController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class PeriodController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, PeriodRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            Period::where('consortium_id', $consortiumId)
            ->ordered()
            ->paginate($pageSize)));
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $consortiumPercentageId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $consortiumId, $periodId)
    {
        $request['id'] = $periodId;
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, PeriodRules::editRules());
        $period = Period::findOrFail($periodId);
        $period->fill($request->only(['name', 'description']));
        $period->save();
        return $this->successResponse(
            $period,
            'Period edited',
            200);
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $consortiumPercentageId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $consortiumId, $periodId)
    {
        $request['id'] = $periodId;
        $this->validate($request, PeriodRules::showRules());
        $period = Period::findOrFail($periodId);
        return $this->successResponse($period);
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, PeriodRules::exportRules());
        return Excel::download(new PeriodExport($consortiumId), 'periods.xlsx');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getOpenPeriod(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, PeriodRules::getOpenRules());
        $period = Period::where('consortium_id', $consortiumId)
            ->where('state', 'open')
            ->first();
        return $this->successResponse(
            $period,
            'Open Period returned.'
        );
    }
}
