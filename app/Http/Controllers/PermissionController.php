<?php

namespace App\Http\Controllers;

use App\Exports\PermissionExport;
use App\Http\Traits\ApiResponser;
use App\Models\Permission;
use App\Resources\PaginationResource;
use App\Validators\Rules\PermissionRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;

class PermissionController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, PermissionRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Permission::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $this->validate($request, PermissionRules::showRules());
        $permission = Permission::findOrFail($id);
        return $this->successResponse($permission, 'Ok', Response::HTTP_OK);
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new PermissionExport(), 'permissions.xlsx');
    }

}
