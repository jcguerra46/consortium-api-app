<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\ProfessionalFunction;
use Illuminate\Http\Request;
use App\Validators\Rules\ProfessionalFunctionRules;
use Symfony\Component\HttpFoundation\Response;
use App\Resources\PaginationResource;

class ProfessionalFunctionController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, ProfessionalFunctionRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            ProfessionalFunction::paginate($pageSize)),
            'Professional Function list returned');
    }

    /**
     * Display the specified resource.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $professionalFunction = ProfessionalFunction::findOrFail($id);
        return $this->successResponse($professionalFunction, 'Ok', Response::HTTP_OK);
    }
}
