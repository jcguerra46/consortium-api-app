<?php

namespace App\Http\Controllers;

use App\Exports\ProviderExport;
use App\Models\Provider;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Services\ProviderService;
use App\Validators\Rules\ProviderRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ProviderController extends Controller
{
    use ApiResponser;

    /** @var ProviderService */
    private $providerService;

    /**
     * ProviderController constructor.
     * @param ProviderService $providerService
     */
    public function __construct(ProviderService $providerService)
    {
        $this->providerService = $providerService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, ProviderRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $providers = Provider::fromAdministration()
            ->applyFilters()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($providers),
            'Providers list returned');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ProviderRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;

        $provider = $this->providerService->create($data);

        return $this->successResponse(
            $provider,
            'Provider created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ProviderRules::editRules());
        $data = $request->except('administration_id');

        $provider = $this->providerService->edit($id, $data);

        return $this->successResponse(
            $provider,
            'Provider edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ProviderRules::showRules());
        $provider = Provider::find($id);
        return $this->successResponse($provider);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ProviderRules::showRules());
        return $this->successResponse(new DestroyResource(Provider::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new ProviderExport(), 'providers.xlsx');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProviderServices(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ProviderRules::getProviderServicesRules());
        $pageSize = $request->get('page_size', 15);

        $services = Provider::find($id)
            ->services()
            ->applyFilters()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($services),
            'Provider services returned');
    }
}
