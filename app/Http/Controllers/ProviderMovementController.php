<?php

namespace App\Http\Controllers;

use App\Exports\ProviderMovementExport;
use App\Models\ProviderFinancialMovement;
use App\Validators\Rules\ProviderMovementRules;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProviderMovementController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @param $providerId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $providerId)
    {
        $request['provider_id'] = $providerId;
        $this->validate($request, ProviderMovementRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $providerMovements = ProviderFinancialMovement::where('provider_id', $providerId)
            ->with(['provider', 'consortium'])
            ->addBalanceList($providerId)
            ->addFilters()
            ->ordered()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($providerMovements),
            'Provider Financial Movement pagination');
    }

    /**
     * @param Request $request
     * @param $providerId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $providerId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ProviderMovementRules::showRules());
        $functionalUnitMovement = ProviderFinancialMovement::findOrFail($id);
        return $this->successResponse($functionalUnitMovement,
            'Provider Financial Movement returned');
    }

    /**
     * @param Request $request
     * @param $providerId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function export(Request $request, $providerId)
    {
        $this->validate($request, ProviderMovementRules::exportRules());
        return Excel::download(new ProviderMovementExport($providerId), 'provider_movements.xlsx');
    }

    /**
     * @param Request $request
     * @param $providerId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getBalance(Request $request, $providerId)
    {
        $request['provider_id'] = $providerId;
        $this->validate($request, ProviderMovementRules::getBalanceRules());
        $balance = ProviderFinancialMovement::balance($providerId);
        return $this->successResponse($balance,
            'Provider Balance returned');
    }
}
