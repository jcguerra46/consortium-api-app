<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\Province;
use App\Resources\PaginationResource;
use App\Validators\Rules\ProvinceRules;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $id)
    {
        $this->validate($request, ProvinceRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(
            Province::where('country_id', $id)
                ->paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     * @param Request $request
     * @param $id
     * @param $province_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $id, $province_id)
    {
        $this->validate($request, ProvinceRules::showRules());
        $province = Province::findOrFail($province_id);
        return $this->successResponse($province);
    }

}
