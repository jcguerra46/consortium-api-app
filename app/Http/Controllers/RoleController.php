<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\Role;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use Illuminate\Http\Request;
use App\Validators\Rules\RoleRules;
use Symfony\Component\HttpFoundation\Response;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\RoleExport;

class RoleController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, RoleRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(Role::paginate($pageSize)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, RoleRules::createRules());
        $role = Role::create($request->all());
        $role->permissions()->attach($request->permissions);
        $role->permissions;
        return $this->successResponse(
            $role,
            'Role created',
            Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, RoleRules::showRules());
        $role = Role::findOrFail($id);
        $role->permissions;
        return $this->successResponse($role, 'Ok', Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, RoleRules::editRules());
        $role = Role::findOrFail($id);
        $role->update($request->all());
        $role->permissions()->sync($request->permissions);
        $role->permissions;
        return $this->successResponse(
            $role,
            'Role edited',
            Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, RoleRules::destroyRules($id));
        $role = Role::findOrFail($id);
        $role->permissions()->detach();
        $role->delete();
        return $this->successResponse(new DestroyResource($role));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new RoleExport(), 'roles.xlsx');
    }

    public function assignPermissionsToRole(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->permissions()->attach($request->permissions);
        $role->permissions;
        return $this->successResponse(
            $role,
            'Permissions assigned',
            200);
    }

    public function updateRolePermissions(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->permissions()->sync($request->permissions);
        $role->permissions;
        return $this->successResponse(
            $role,
            'Permissions updated',
            200);
    }

    public function deleteRolePermissions(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $role->permissions()->detach();
        return $this->successResponse(
            $role,
            'Permissions deleted',
            200);
    }
}
