<?php

namespace App\Http\Controllers;

use App\Exports\PeriodExport;
use App\Models\Period;
use App\Models\Salary;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\PeriodRules;
use App\Validators\Rules\SalariesRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class SalariesController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class SalariesController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, SalariesRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $employeeId = $request->get('employee_id');
        return $this->successResponse(new PaginationResource(
            Salary::where('employee_id', $employeeId)
                ->paginate($pageSize)));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SalariesRules::showRules());
        $salary = Salary::with(['concepts', 'contributions'])
            ->findOrFail($id);
        return $this->successResponse(
            $salary,
            'Salary returned');
    }

}
