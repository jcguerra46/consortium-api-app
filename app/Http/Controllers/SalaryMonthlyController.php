<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use App\Services\Salaries\PayslipHtmlGenerator;
use App\Services\Salaries\SalariesService;
use App\Validators\Rules\SalaryMonthlyRules;
use Illuminate\Http\Request;

/**
 * Class SalaryMonthlyController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class SalaryMonthlyController extends Controller
{
    use ApiResponser;

    /**  @var SalariesService */
    protected $salariesService;
    /**  @var PayslipHtmlGenerator */
    protected $payslipHtmlGenerator;

    public function __construct(SalariesService $salariesService,
                                PayslipHtmlGenerator $payslipHtmlGenerator)
    {
        $this->salariesService = $salariesService;
        $this->payslipHtmlGenerator = $payslipHtmlGenerator;
    }

    public function calculate(Request $request)
    {
        $this->validate($request, SalaryMonthlyRules::createRules());
        $salary = $this->salariesService->calculate($request->all(), TypeSalary::MONTHLY);
        return $this->successResponse(
            $salary->toArray(),
            'Monthly Salary returned'
        );
    }

    public function getPayslip(Request $request)
    {
        $this->validate($request, SalaryMonthlyRules::createRules());
        $payslip = $this->salariesService->getPayslip($request->all(), TypeSalary::MONTHLY);

        return $this->successResponse(
            ['html' => $payslip],
            'Monthly Payslip Html returned'
        );
    }

    public function create(Request $request)
    {
        $this->validate($request, SalaryMonthlyRules::createRules());
        $salary = $this->salariesService->create($request->all(), TypeSalary::MONTHLY);

        return $this->successResponse(
            $salary,
            'Monthly Salary created'
        );
    }
}
