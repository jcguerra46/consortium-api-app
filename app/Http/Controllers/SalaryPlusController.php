<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\SalaryPlus;
use App\Resources\PaginationResource;
use App\Validators\Rules\SalaryPlusesRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SalaryPlusController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, SalaryPlusesRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(SalaryPlus::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $salaryPlus = SalaryPlus::findOrFail($id);
        return $this->successResponse($salaryPlus, 'Ok', Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource with paginate.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function paginate(Request $request)
    {
        $this->validate($request, SalaryPlusesRules::paginateRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(SalaryPlus::paginate($pageSize)));
    }
}
