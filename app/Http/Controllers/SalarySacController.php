<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use App\Services\Salaries\PayslipHtmlGenerator;
use App\Services\Salaries\SalariesService;
use App\Validators\Rules\SalarySacRules;
use Illuminate\Http\Request;

/**
 * Class SalarySacController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class SalarySacController extends Controller
{
    use ApiResponser;

    /**  @var SalariesService */
    protected $salariesService;
    /**  @var PayslipHtmlGenerator */
    protected $payslipHtmlGenerator;

    public function __construct(SalariesService $salariesService,
                                PayslipHtmlGenerator $payslipHtmlGenerator)
    {
        $this->salariesService = $salariesService;
        $this->payslipHtmlGenerator = $payslipHtmlGenerator;
    }

    public function calculate(Request $request)
    {
        $this->validate($request, SalarySacRules::createRules());
        $salary = $this->salariesService->calculate($request->all(), TypeSalary::SAC);
        return $this->successResponse(
            $salary->toArray(),
            'SAC Salary returned'
        );
    }

    public function getPayslip(Request $request)
    {
        $this->validate($request, SalarySacRules::createRules());
        $payslip = $this->salariesService->getPayslip($request->all(), TypeSalary::SAC);

        return $this->successResponse(
            ['html' => $payslip],
            'SAC Payslip Html returned'
        );
    }

    public function create(Request $request)
    {
        $this->validate($request, SalarySacRules::createRules());
        $salary = $this->salariesService->create($request->all(), TypeSalary::SAC);

        return $this->successResponse(
            $salary,
            'SAC Salary created'
        );
    }
}
