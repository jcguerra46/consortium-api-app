<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use App\Services\Salaries\PayslipHtmlGenerator;
use App\Services\Salaries\SalariesService;
use App\Validators\Rules\SalaryVacationRules;
use Illuminate\Http\Request;

/**
 * Class SalaryVacationsController
 * @package App\Http\Controllers
 * @author SCOctopus
 */
class SalaryVacationsController extends Controller
{
    use ApiResponser;

    /** @var SalariesService */
    protected $salariesService;
    /** @var PayslipHtmlGenerator */
    protected $payslipHtmlGenerator;

    public function __construct(SalariesService $salariesService,
                                PayslipHtmlGenerator $payslipHtmlGenerator)
    {
        $this->salariesService = $salariesService;
        $this->payslipHtmlGenerator = $payslipHtmlGenerator;
    }

    public function calculate(Request $request)
    {
        $this->validate($request, SalaryVacationRules::createRules());
        $salary = $this->salariesService->calculate($request->all(), TypeSalary::VACATIONS);
        return $this->successResponse(
            $salary->toArray(),
            'Vacation Salary returned'
        );
    }

    public function getPayslip(Request $request)
    {
        $this->validate($request, SalaryVacationRules::createRules());
        $payslip = $this->salariesService->getPayslip($request->all(), TypeSalary::VACATIONS);

        return $this->successResponse(
            ['html' => $payslip],
            'Vacation Payslip Html returned'
        );
    }

    public function create(Request $request)
    {
        $this->validate($request, SalaryIntermediateRules::createRules());
        $salary = $this->salariesService->create($request->all(), TypeSalary::INTERMEDIATE);

        return $this->successResponse(
            $salary,
            'Intermediate Salary created'
        );
    }
}
