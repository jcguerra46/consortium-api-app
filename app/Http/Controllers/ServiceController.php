<?php

namespace App\Http\Controllers;

use App\Exports\ServiceExport;
use App\Models\Service;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\ServiceRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ServiceController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, ServiceRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $services = Service::fromAdministration()
            ->applyFilters()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($services),
            'Services lists returned');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ServiceRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $service = Service::create($data);
        return $this->successResponse(
            $service,
            'Service created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ServiceRules::editRules());
        $service = Service::find($id);
        $service->fill($request->except('administration_id'));
        $service->save();
        return $this->successResponse(
            $service,
            'Service edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ServiceRules::showRules());
        $service = Service::find($id);
        return $this->successResponse($service);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, ServiceRules::destroyRules());
        return $this->successResponse(new DestroyResource(Service::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new ServiceExport(), 'services.xlsx');
    }
}
