<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\SocialWork;
use App\Resources\PaginationResource;
use App\Validators\Rules\SocialWorkRules;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SocialWorkController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request)
    {
        $this->validate($request, SocialWorkRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(SocialWork::paginate($pageSize)));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $socialWork = SocialWork::findOrFail($id);
        return $this->successResponse($socialWork, 'Ok', Response::HTTP_OK);
    }

}
