<?php

namespace App\Http\Controllers;

use App\Exports\SpendingExport;
use App\Models\Spending;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Services\Spendings\SpendingService;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\SpendingRules;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Maatwebsite\Excel\Facades\Excel;

class SpendingController extends Controller
{
    use ApiResponser;

    /** @var SpendingService */
    private $spendingService;

    /**
     * SpendingController constructor.
     * @param SpendingService $spendingService
     */
    public function __construct(SpendingService $spendingService)
    {
        $this->spendingService = $spendingService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, SpendingRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $spendings = Spending::fromAdministration()
            ->applyFilters()
            ->with('installments', 'provider', 'consortium', 'service', 'category')
            ->ordered()
            ->paginate($pageSize);
        return $this->successResponse(new PaginationResource($spendings));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, SpendingRules::createRules(), [],[],true);
        $data = $request->all();
        $dataSpending = Arr::except($data, ['installments', 'percentages']);
        $installments = $data['installments'];
        $percentages = $data['percentages'] ?? [];
        $spending = $this->spendingService->create($dataSpending, $percentages, $installments);
        return $this->successResponse(
            $spending,
            'Spending created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingRules::editRules());
        $spending = Spending::find($id);
        $spending->fill($request->except('administration_id'));
        $spending->save();
        return $this->successResponse(
            $spending,
            'Spending edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingRules::showRules());
        $spending = Spending::with(['installments', 'percentages'])
            ->findOrFail($id);
        return $this->successResponse($spending);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingRules::destroyRules());
        return $this->successResponse(new DestroyResource(Spending::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new SpendingExport(), 'spendings.xlsx');
    }
}
