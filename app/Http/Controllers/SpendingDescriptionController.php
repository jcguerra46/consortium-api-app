<?php

namespace App\Http\Controllers;

use App\Exports\SpendingDescriptionsExport;
use App\Models\SpendingDescription;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use App\Validators\Rules\SpendingDescriptionRules;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class SpendingDescriptionController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, SpendingDescriptionRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $pageSize = $request->get('page_size', 15);

        $spendingsDescriptions = SpendingDescription::fromAdministration()
                                                    ->applyFilters()
                                                    ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($spendingsDescriptions));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, SpendingDescriptionRules::createRules());
        $data = $request->all();
        $data['administration_id'] = Auth::user()->administration_id;
        $spendingDescription = SpendingDescription::create($data);
        return $this->successResponse(
            $spendingDescription,
            'Spending Description created',
            201);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingDescriptionRules::editRules());
        $spendingDescription = SpendingDescription::find($id);
        $spendingDescription->fill($request->except('administration_id'));
        $spendingDescription->save();
        return $this->successResponse(
            $spendingDescription,
            'Spending Description edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingDescriptionRules::showRules());
        $spendingDescription = SpendingDescription::findOrFail($id);
        return $this->successResponse($spendingDescription);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingDescriptionRules::destroyRules());
        return $this->successResponse(new DestroyResource(SpendingDescription::destroy($id)));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new SpendingDescriptionsExport(), 'spending-descriptions.xlsx');
    }
}
