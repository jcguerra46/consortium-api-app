<?php

namespace App\Http\Controllers;

use App\Http\Traits\ApiResponser;
use App\Models\SpendingInstallment;
use App\Resources\PaginationResource;
use App\Validators\Rules\SpendingInstallmentRules;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SpendingInstallmentController extends Controller
{
    use ApiResponser;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, SpendingInstallmentRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        $spendingInstallments = SpendingInstallment::fromAdministration()
            ->with(['spending', 'spending.consortium'])
            ->applyFilters()
            ->ordered()
            ->paginate($pageSize);
        return $this->successResponse(new PaginationResource($spendingInstallments),
            'Spending Installments pagination');
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingInstallmentRules::showRules());
        $spendingInstallment = SpendingInstallment::findOrFail($id);
        return $this->successResponse($spendingInstallment,
            'Spending Installment recovered');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new SpendingInstallmentExport(), 'spendings-installments.xlsx');
    }
}
