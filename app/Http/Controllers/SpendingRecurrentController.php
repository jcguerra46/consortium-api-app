<?php

namespace App\Http\Controllers;

use App\Exports\SpendingRecurrentExport;
use App\Models\SpendingRecurrent;
use App\Validators\Rules\SpendingRecurrentRules;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SpendingRecurrentController extends Controller
{
    use ApiResponser;

    public function index(Request $request)
    {
        $this->validate($request, SpendingRecurrentRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $spendingsRecurrents = SpendingRecurrent::fromAdministration()
            ->with('consortium', 'provider', 'category')
            ->applyFilters()
            ->paginate($pageSize);

        return $this->successResponse(
            new PaginationResource($spendingsRecurrents),
            'Spendings Recurrents list'
        );
    }

    public function create(Request $request)
    {
        $this->validate($request, SpendingRecurrentRules::createRules());
        $data = $request->all();
        $spendingRecurrent = SpendingRecurrent::create($data);
        return $this->successResponse(
            $spendingRecurrent,
            'Spending Recurrent created',
            201);
    }

    public function edit(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingRecurrentRules::editRules());
        $spendingRecurrent = SpendingRecurrent::find($id);
        $spendingRecurrent->fill($request->all());
        $spendingRecurrent->save();
        return $this->successResponse(
            $spendingRecurrent,
            'Spending Recurrent edited',
            200);
    }

    public function show(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingRecurrentRules::showRules());
        $spendingRecurrent = SpendingRecurrent::findOrFail($id);
        return $this->successResponse($spendingRecurrent);
    }

    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, SpendingRecurrentRules::destroyRules());
        return $this->successResponse(new DestroyResource(SpendingRecurrent::destroy($id)));
    }

    public function export()
    {
        return Excel::download(new SpendingRecurrentExport(), 'spendings-recurrents.xlsx');
    }
}
