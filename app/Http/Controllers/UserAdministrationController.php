<?php

namespace App\Http\Controllers;

use App\Exports\UserAdministrationExport;
use App\Models\UserAdministration;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use App\Validators\Rules\UserAdministrationRules;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;

class UserAdministrationController extends Controller
{
    use ApiResponser;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request, UserAdministrationRules::indexRules());
        $pageSize = $request->get('page_size', 15);
        return $this->successResponse(new PaginationResource(UserAdministration::paginate($pageSize)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, UserAdministrationRules::storeRules());
        $request['administration_id'] = Auth::user()->administration_id;
        $userAdministration = UserAdministration::create($request->all());
        $userAdministration->roles()->attach($request->roles);
        $userAdministration->consortiums()->attach($request->consortiums);
        $userAdministration->roles;
        $userAdministration->consortiums;
        return $this->successResponse(
            $userAdministration,
            'User created',
            Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     */
    public function show(Request $request, $id)
    {
        $this->validate($request, UserAdministrationRules::showRules());
        $userAdministration = UserAdministration::findOrFail($id);
        $userAdministration->roles;
        $userAdministration->consortiums;
        return $this->successResponse($userAdministration);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, UserAdministrationRules::updateRules());
        $request['administration_id'] = Auth::user()->administration_id;
        $userAdministration = UserAdministration::findOrFail($id);
        $userAdministration->update($request->all());
        $userAdministration->roles()->sync($request->roles);
        $userAdministration->consortiums()->sync($request->consortiums);
        $userAdministration->roles;
        $userAdministration->consortiums;
        return $this->successResponse(
            $userAdministration,
            'User Administration edited',
            200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $request['id'] = $id;
        $this->validate($request, UserAdministrationRules::destroyRules($id));
        $user = UserAdministration::findOrFail($id);
        $user->roles()->detach();
        $user->consortiums()->detach();
        $user->delete();
        return $this->successResponse(new DestroyResource($user));
    }

    /**
     * Export a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        return Excel::download(new UserAdministrationExport(), 'useradministrations.xlsx');
    }
}
