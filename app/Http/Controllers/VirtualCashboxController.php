<?php

namespace App\Http\Controllers;

use App\Exports\VirtualCashboxExport;
use App\Services\VirtualCashboxMovementService;
use App\Validators\Rules\VirtualCashboxRules;
use App\Models\VirtualCashbox;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use Maatwebsite\Excel\Facades\Excel;

class VirtualCashboxController extends Controller
{
    use ApiResponser;

    /** @var VirtualCashboxMovementService */
    protected $virtualCashboxMovementService;

    /**
     * VirtualCashboxController constructor.
     * @param VirtualCashboxMovementService $virtualCashboxMovementService
     */
    public function __construct(VirtualCashboxMovementService $virtualCashboxMovementService)
    {
        $this->virtualCashboxMovementService = $virtualCashboxMovementService;
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, VirtualCashboxRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $virtualCashboxes = VirtualCashbox::fromConsortium($consortiumId)
            ->applyFilters()
            ->addBalances()
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($virtualCashboxes),
            'Virtual Cashboxes pagination');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, VirtualCashboxRules::createRules());
        $data = $request->all();
        $virtualCashbox = VirtualCashbox::create($data);
        return $this->successResponse(
            $virtualCashbox,
            'Virtual Cashbox created',
            201);
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, VirtualCashboxRules::showRules());
        $virtualCashbox = VirtualCashbox::findOrFail($id);
        return $this->successResponse($virtualCashbox,
            'Virtual Cashbox returned');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, VirtualCashboxRules::editRules());
        $virtualCashbox = VirtualCashbox::findOrFail($id);
        $virtualCashbox->fill($request->except('consortium_id'));
        $virtualCashbox->save();
        return $this->successResponse(
            $virtualCashbox,
            'Virtual Cashbox edited',
            200);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, VirtualCashboxRules::destroyRules());
        return $this->successResponse(new DestroyResource(VirtualCashbox::destroy($id)));
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function export(Request $request, $consortiumId)
    {
        $request['consortium_id'] = $consortiumId;
        $this->validate($request, VirtualCashboxRules::paginateRules());
        return Excel::download(new VirtualCashboxExport($consortiumId), 'virtual-cashboxes.xlsx');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $id
     */
    public function getBalance(Request $request, $consortiumId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, VirtualCashboxRules::getBalancesRules());

        $balances = $this->virtualCashboxMovementService->getBalances($id);
        return $this->successResponse($balances,
            'Virtual Cashbox balances returned');
    }
}
