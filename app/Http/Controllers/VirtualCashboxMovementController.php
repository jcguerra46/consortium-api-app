<?php

namespace App\Http\Controllers;

use App\Exports\VirtualCashboxMovementExport;
use App\Models\VirtualCashboxMovement;
use App\Services\VirtualCashboxMovementService;
use App\Validators\Rules\VirtualCashboxMovementRules;
use App\Http\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Resources\DestroyResource;
use App\Resources\PaginationResource;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class VirtualCashboxMovementController
 * @package App\Http\Controllers
 */
class VirtualCashboxMovementController extends Controller
{
    use ApiResponser;

    /**  @var VirtualCashboxMovementService */
    private $virtualCashboxMovementService;

    /**
     * VirtualCashboxMovementController constructor.
     * @param VirtualCashboxMovementService $virtualCashboxMovementService
     */
    public function __construct(VirtualCashboxMovementService $virtualCashboxMovementService)
    {
        $this->virtualCashboxMovementService = $virtualCashboxMovementService;
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $virtualCashboxId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, $consortiumId, $virtualCashboxId)
    {
        $request['virtual_cashbox_id'] = $virtualCashboxId;
        $this->validate($request, VirtualCashboxMovementRules::indexRules());
        $pageSize = $request->get('page_size', 15);

        $virtualCashboxMovs = VirtualCashboxMovement::fromVirtualCashbox($virtualCashboxId)
            ->applyFilters()    
            ->addBalanceList($virtualCashboxId)
            ->paginate($pageSize);

        return $this->successResponse(new PaginationResource($virtualCashboxMovs),
            'Virtual Cashboxes Movements pagination');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $virtualCashboxId
     * @return \Illuminate\Http\JsonResponse
     */
    public function createInitialBalance(Request $request, $consortiumId, $virtualCashboxId)
    {
        $request['virtual_cashbox_id'] = $virtualCashboxId;
        $this->validate($request, VirtualCashboxMovementRules::createInitialBalanceRules());
        $data = $request->all();
        $checks = $data['checks'] ?? [];
        $bankMovements = $data['bank_movements'] ?? [];

        $virtualCashboxMov = $this->virtualCashboxMovementService->createInitialBalance($data, $checks, $bankMovements);
        return $this->successResponse(
            $virtualCashboxMov,
            'Virtual Cashbox initial balance created',
            201);
    }


    /**
     * @param Request $request
     * @param $consortiumId
     * @param $virtualCashboxId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createIncome(Request $request, $consortiumId, $virtualCashboxId)
    {
        $request['virtual_cashbox_id'] = $virtualCashboxId;
        $this->validate($request, VirtualCashboxMovementRules::createIncomeRules(), [], [], true);
        $data = $request->all();
        $checks = $data['checks'] ?? [];
        $bankMovements = $data['bank_movements'] ?? [];

        $virtualCashboxMovement = $this->virtualCashboxMovementService->createIncome($data, $checks, $bankMovements);
        return $this->successResponse(
            $virtualCashboxMovement,
            'Virtual Cashbox Movement income created',
            201);
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $virtualCashboxId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createOutflow(Request $request, $consortiumId, $virtualCashboxId)
    {
        $request['virtual_cashbox_id'] = $virtualCashboxId;
        $this->validate($request, VirtualCashboxMovementRules::createOutflowRules(), [], [], true);
        $data = $request->all();
        $checks = $data['checks'] ?? [];
        $bankMovements = $data['bank_movements'] ?? [];

        $virtualCashboxMovement = $this->virtualCashboxMovementService->createOutflow($data, $checks, $bankMovements);
        return $this->successResponse(
            $virtualCashboxMovement,
            'Virtual Cashbox Movement outflow created',
            201);
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $virtualCashboxId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function show(Request $request, $consortiumId, $virtualCashboxId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, VirtualCashboxMovementRules::showRules());

        $virtualCashboxMovement = VirtualCashboxMovement::with('bankAccountMovements', 'checks')
            ->findOrFail($id);
        return $this->successResponse($virtualCashboxMovement,
            'Virtual Cashbox Movement returned');
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $virtualCashboxId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request, $consortiumId, $virtualCashboxId, $id)
    {
        $request['id'] = $id;
        $this->validate($request, VirtualCashboxMovementRules::destroyRules());
        return $this->successResponse(new DestroyResource(VirtualCashboxMovement::destroy($id)));
    }

    /**
     * @param Request $request
     * @param $consortiumId
     * @param $virtualCashboxId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function export(Request $request, $consortiumId, $virtualCashboxId)
    {
        $request['virtual_cashbox_id'] = $virtualCashboxId;
        $this->validate($request, VirtualCashboxMovementRules::paginateRules());
        return Excel::download(new VirtualCashboxMovementExport($virtualCashboxId), 'virtual-cashboxes-movements.xlsx');
    }
}
