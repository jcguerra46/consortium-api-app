<?php

namespace App\Http\Middleware;

use App\Exceptions\FunctionalUnitsException;
use App\Models\Consortium;
use App\Models\FunctionalUnit;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthorizationResourcesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       /** ids of Consortium of the user authenticated */
        $consortiumIds = Consortium::getConsortiumsOfUser();

        if(isset($request->route()[2]['id'])) {
            $functional_unit_id = $request->route()[2]['id']; // ID on functional unit
            $resp = Consortium::leftJoin('functional_units', 'functional_units.consortium_id', '=' , 'consortia.id')
                ->where('functional_units.id', $functional_unit_id)
                ->whereIn('functional_units.consortium_id', $consortiumIds)
                ->whereHas('users', function($user) {
                    $user->where('id', Auth::user()->id);
                })->count();
            if ($resp === 0) {
                throw new FunctionalUnitsException();
            }
        }

        // Pre-Middleware Action

        $response = $next($request);

        // Post-Middleware Action

        return $response;
    }
}
