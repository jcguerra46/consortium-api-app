<?php

namespace App\Http\Middleware;

use App\Exceptions\PermissionException;
use App\Exceptions\RoleException;
use App\Models\Permission;
use App\Models\RolePermission;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) // roles, permission = null
    {
        $route = $request->route()[1]['uses'];
        $roles = array_slice(func_get_args(),2);

        if(is_null($request->user())){
            abort(404);
        }

        if($request->user()->isSuperAdmin()){
            return $next($request);
        }

        if(!$request->user()->hasRoles($roles)) {
            throw new RoleException();
        }

        if(!$request->user()->hasPermissions($route)) {
            throw new PermissionException();
        }

        return $next($request);
    }

}
