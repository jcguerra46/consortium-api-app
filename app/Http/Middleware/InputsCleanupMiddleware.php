<?php

namespace App\Http\Middleware;

class InputsCleanupMiddleware
{
    public function handle($request, \Closure $next)
    {
        $inputs = $request->all();
        array_walk_recursive($inputs, function (&$value) {
            if (is_string($value)) {

                //Parse Booleans
                if ($value == 'true' or $value == 'TRUE') {
                    $value = true;
                } elseif ($value == 'false' or $value == 'FALSE') {
                    $value = false;
                }  //Parse Null values
                elseif ($value == 'null' or $value == 'NULL') {
                    $value = null;
                } else {
                    //Remove empty strings
                    $value = trim($value);
                    $value = ($value == "") ? null : $value;
                }
            }
        });

        $request->replace($inputs);
        return $next($request);
    }
}
