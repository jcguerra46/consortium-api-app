<?php

namespace App\Http\Middleware;

use App\Http\Traits\ApiResponser;

class ParseFiltersMiddleware
{
    use ApiResponser;

    public function handle($request, \Closure $next)
    {
        $filters = $request->filters;
        if (isset($filters)) {
            $filters = json_decode($filters, true);

            if (json_last_error() === JSON_ERROR_NONE) {
                $requestData = $request->all();
                $requestData['filters'] = $filters;
                $request->replace($requestData);
            } else {
                return $this->errorResponse(
                    'Filter format validation error. Must  be in JSON format.');
            }
        }

        return $next($request);
    }
}
