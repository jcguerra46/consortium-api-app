<?php

namespace App\Listeners;

use App\Events\CashboxMovementCreatedEvent;
use App\Models\ConsortiumFinancialMovement;
use App\Models\FunctionalUnitMovement;

class CashboxMovementCreatedListener
{
    /**
     * CashboxMovementCreatedListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param CashboxMovementCreatedEvent $event
     */
    public function handle(CashboxMovementCreatedEvent $event)
    {
        $cashboxMovement = $event->cashboxMovement;

        // Cashbox Movement Income
        if ($cashboxMovement->type_movement == 'income') {
            ConsortiumFinancialMovement::create([
                'consortium_id' => $cashboxMovement->cashbox->consortium_id,
                'amount' => $cashboxMovement->total_amount,
                'date' => $cashboxMovement->date,
                'description' => $cashboxMovement->description,
                'type_movement' => 'income',
                'identified' => $cashboxMovement->identified,
                'incomplete' => false,
                'created_from_recurrent' => false,
                'included_in_the_financial_statement' => true,
            ]);

            $functionalUnits = $cashboxMovement->functionalUnits;
            foreach ($functionalUnits as $functionalUnit) {
                FunctionalUnitMovement::create([
                    'functional_unit_id' => $functionalUnit->id,
                    'amount' => $cashboxMovement->total_amount,
                    'date' => $cashboxMovement->date,
                    'description' => $cashboxMovement->description,
                    'type' => 'capital',
                    'type_movement' => 'income',
                    'identified' => true,
                    'accumulate_to_expense' => true,
                ]);
            }
        }
        // Cashbox Movement Outflow
        else {
            ConsortiumFinancialMovement::create([
                'consortium_id' => $cashboxMovement->cashbox->consortium_id,
                'amount' => $cashboxMovement->total_amount,
                'date' => $cashboxMovement->date,
                'description' => $cashboxMovement->description,
                'type_movement' => 'outflow',
                'identified' => $cashboxMovement->identified,
                'incomplete' => false,
                'created_from_recurrent' => false,
                'included_in_the_financial_statement' => true,
            ]);
        }
    }
}
