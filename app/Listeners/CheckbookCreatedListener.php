<?php

namespace App\Listeners;

use App\Events\CheckbookCreatedEvent;
use App\Models\Check;
use Carbon\Carbon;

class CheckbookCreatedListener
{
    /**
     * CheckbookCreatedListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param CheckbookCreatedEvent $event
     */
    public function handle(CheckbookCreatedEvent $event)
    {
        $checkbook = $event->checkbook;
        $checks = [];

        for ($i = $checkbook->first_check;
             $i <= $checkbook->last_check; $i++) {
            $checks[] = [
                'number' => $i,
                'bank_id' => $checkbook->bank_id,
                'administration_id' => $checkbook->administration_id,
                'own' => true,
                'checkbook_id' => $checkbook->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }
        Check::insert($checks);
    }
}
