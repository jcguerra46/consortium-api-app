<?php

namespace App\Listeners;

use App\Events\ConsortiumCreatedEvent;
use App\Models\Cashbox;
use App\Models\ConsortiumProfile;
use App\Models\ConsortiumPortalProfile;
use App\Models\ConsortiumUserAdministration;
use App\Models\Period;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ConsortiumCreatedListener
{
    /**
     * ConsortiumCreatedListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param ConsortiumCreatedEvent $event
     */
    public function handle(ConsortiumCreatedEvent $event)
    {
        $consortium = $event->consortium;
        $consortiumProfile = $event->consortium_profile;

        DB::transaction(function () use ($consortium, $consortiumProfile) {
            ConsortiumProfile::create(
                [
                    'consortium_id' => $consortiumProfile->consortium_id,
                    'first_due_date' => $consortiumProfile->first_due_date
                ]
            );
            ConsortiumPortalProfile::create(
                [
                    'consortium_id' => $consortium->id,
                ]
            );
            Cashbox::create(
                [
                    'name' => 'Caja Única del consorcio ' . $consortium->fancy_name,
                    'consortium_id' => $consortium->id,
                ]
            );
            Period::create([
                'consortium_id' => $consortium->id,
                'name' => strtoupper(Carbon::now()->locale('es_ES')->isoFormat('MMMM-Y')),
                'start_date' => Carbon::now(),
            ]);
        });
    }
}
