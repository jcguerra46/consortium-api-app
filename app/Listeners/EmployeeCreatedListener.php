<?php

namespace App\Listeners;

use App\Events\EmployeeCreatedEvent;
use App\Models\EmployeeAfip;
use App\Models\EmployeeData;
use App\Models\EmployeeProfile;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmployeeCreatedListener
{
    /**
     * EmployeeCreatedListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param EmployeeCreatedEvent $event
     */
    public function handle(EmployeeCreatedEvent $event)
    {
        $employee = $event->employee;

        DB::transaction(function () use ($employee) {
            EmployeeData::create(['employee_id' => $employee->id]);
            EmployeeProfile::create([
                'entry_date' => Carbon::now(),
                'employee_id' => $employee->id]);
            EmployeeAfip::create(['employee_id' => $employee->id]);
        });
    }
}
