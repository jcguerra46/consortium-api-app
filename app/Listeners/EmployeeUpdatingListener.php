<?php

namespace App\Listeners;

use App\Events\EmployeeCreatedEvent;
use App\Events\EmployeeUpdatingEvent;
use App\Models\EmployeeAfip;
use App\Models\EmployeeData;
use App\Models\EmployeeProfile;
use App\Models\EmployeeStatusLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EmployeeUpdatingListener
{
    /**
     * EmployeeUpdatingListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param EmployeeUpdatingEvent $event
     */
    public function handle(EmployeeUpdatingEvent $event)
    {
        $employee = $event->employee;

        if ($employee->isDirty('employee_status_id')) {
            EmployeeStatusLog::create([
                'employee_id' => $employee->id,
                'employee_status_id' => $employee->getOriginal('employee_status_id'),
                'date' => Carbon::now(),
            ]);
        }
    }
}
