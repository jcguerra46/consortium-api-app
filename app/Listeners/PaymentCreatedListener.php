<?php

namespace App\Listeners;

use App\Events\PaymentCreatedEvent;
use App\Models\ProviderFinancialMovement;

class PaymentCreatedListener
{
    /**
     * PaymentCreatedListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param PaymentCreatedEvent $event
     */
    public function handle(PaymentCreatedEvent $event)
    {
        $payment = $event->payment;
        if ($payment->provider_id) {
            ProviderFinancialMovement::create([
                'description' => 'Pago ID ' . $payment->id,
                'provider_id' => $payment->provider_id,
                'date' => $payment->date,
                'amount' => $payment->total_amount,
                'transaction_type' => ProviderFinancialMovement::TRANSACTION_TYPE_DEBIT,
                'payment_id' => $payment->id,
                'consortium_id' => null
            ]);
        }
    }
}
