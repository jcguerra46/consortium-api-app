<?php

namespace App\Listeners;

use App\Events\PeriodCreatedEvent;
use App\Models\SpendingRecurrent;
use App\Services\Spendings\SpendingService;
use Illuminate\Support\Facades\DB;

class PeriodCreatedListener
{
    /**
     * PeriodCreatedListener constructor.
     */
    public function __construct(SpendingService $spendingService)
    {
        $this->spendingService = $spendingService;
    }

    /**
     * @param PeriodCreatedEvent $event
     */
    public function handle(PeriodCreatedEvent $event)
    {
        $period = $event->period;
        DB::transaction(function () use ($period) {
            //Create Spendings from Recurrents
            $spendingsRecurrents = SpendingRecurrent::where('consortium_id', $period->consortium_id)
                ->get();

            foreach ($spendingsRecurrents as $recurrent) {
                $dataSpending = $recurrent->toArray();
                $dataPercentages = [
                    'percentage_consortium_id' => $dataSpending['percentage_consortium_id'],
                    'value' => 100,
                ];
                $this->spendingService->create($dataSpending, $dataPercentages);
            }
        });
    }
}
