<?php

namespace App\Listeners;

use App\Events\SpendingCreatedEvent;
use App\Models\ConsortiumFinancialMovement;
use App\Models\FunctionalUnitMovement;
use App\Models\ProviderFinancialMovement;
use Illuminate\Support\Facades\DB;

class SpendingCreatedListener
{
    /**
     * SpendingCreatedListener constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @param SpendingCreatedEvent $event
     */
    public function handle(SpendingCreatedEvent $event)
    {
        $spending = $event->spending;
        DB::transaction(function () use ($spending) {
            //Create Consortium Financial Movement
            $consortiumFinancialMov = ConsortiumFinancialMovement::create([
                'spending_id' => $spending->id,
                'consortium_id' => $spending->consortium_id,
                'amount' => $spending->amount,
                'date' => $spending->date,
                'description' => $spending->description,
                'type_movement' => 'outflow',
                'type' => 'spending',
                'identified' => true,
                'incomplete' => false,
                'created_from_recurrent' => false,
                'included_in_the_financial_statement' => $spending->affect_financial_section,
            ]);

            //Create Functional Unit Financial Movement
            if ($spending->functional_unit_id) {
                FunctionalUnitMovement::create([
                    'functional_unit_id' => $spending->functional_unit_id,
                    'amount' => $spending->amount,
                    'date' => $spending->date,
                    'description' => $spending->description,
                    'type' => 'capital',
                    'type_movement' => 'outflow',
                    'identified' => true,
                    'accumulate_to_expense' => false,
                    'spending_id' => $spending->id,
                ]);
            }

            //Create Provider Movement
            if ($spending->provider_id) {
                $installments = $spending->installments;
                foreach ($installments as $i) {
                    ProviderFinancialMovement::create([
                        'provider_id' => $spending->provider_id,
                        'amount' => $i->amount,
                        'date' => $spending->date,
                        'description' => $spending->description,
                        'transaction_type' => ProviderFinancialMovement::TRANSACTION_TYPE_CREDIT,
                        'spending_installment_id' => $i->id,
                        'consortium_id' => $spending->consortium_id
                    ]);
                }
            }
        });
    }
}
