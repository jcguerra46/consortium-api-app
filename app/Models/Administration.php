<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Administration extends Model
{
    protected $table = 'administrations';

    protected $fillable = [
        'name',
        'business_name',
        'postal_code',
        'email',
        'cuit',
        'fiscal_situation',
        'image',
        'signature',
        'address_street',
        'address_number',
        'address_floor',
        'address_department',
        'responsible',
        'unregistered_payment',
        'active',
        'province_id',
        'contact',
    ];

    /**
     * Relation HasMany with UserAdministration Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(UserAdministration::class);
    }

    /**
     * Relation HasMany with Consortium Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function consortium()
    {
        return $this->hasMany(Consortium::class);
    }

    /**
     * Relation BelongsTo with Province Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        $address = $this->address_street . ' ' . $this->address_number . '.';
        if ($this->address_floor) {
            $address = $address . ' Piso ' . $this->address_floor . '.';
        }
        if ($this->address_department) {
            $address = $address . ' Dpto: ' . $this->address_department . '.';
        }
        return $address;
    }
}
