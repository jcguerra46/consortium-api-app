<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AfipModoContratacion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'afip_modos_contratacion';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'description'
    ];
}
