<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AfipProvincia extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'afip_provincias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'description'
    ];
}
