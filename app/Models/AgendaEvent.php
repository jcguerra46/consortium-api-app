<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AgendaEvent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'agenda_events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administration_id',
        'consortium_id',
        'eventable',
        'title',
        'description',
        'date',
        'trans',
        'service_class_name',
        'route_class_name'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'datetime:d-m-Y'
    ];

    /**
     * Scope a query to only include all agenda events of the administration.
     *
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }
}
