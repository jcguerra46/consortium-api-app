<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Amenity extends Model
{
    protected $table = 'amenities';

    protected $fillable = [
        'consortium_id',
        'name',
        'location',
        'enabled',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('consortia','consortia.id','=', 'amenities.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('amenities.*');
    }
}
