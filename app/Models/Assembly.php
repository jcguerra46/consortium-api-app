<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Assembly extends Model
{
    protected $table = 'assemblies';

    protected $fillable = [
        'consortium_id',
        'date',
        'time',
        'second_time',
        'place',
        'issues',
        'send'
    ];

    protected $dates = [
        'date',
    ];

    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('consortia','consortia.id','=', 'assemblies.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('assemblies.*');
    }
}
