<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $table = 'bank_accounts';

    protected $fillable = [
        'bank_id',
        'account_number',
        'cbu',
        'alias',
        'cuit',
        'type',
        'branch_office',
        'owner',
        'show_data_in_expense',
        'email',
        'signatorie_1',
        'signatorie_2',
        'signatorie_3',
        'consortium_id',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank');
    }

    public function scopeAddBalances($query)
    {
        return $query->select('*')
            ->addSelect(DB::raw('
                    (
                    SELECT
                        COALESCE((SUM(CASE WHEN type = \'income\' THEN amount ELSE -amount END)),0) AS balance
                    FROM bank_account_movements bam
                    WHERE bam.bank_account_id = bank_accounts.id
                    )'));
    }

    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }

    /**
     * Scope a query to filter bank accounts data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query;
    }
}
