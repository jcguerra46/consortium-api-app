<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BankAccountMovement extends Model
{
    /**
     * Table associated to the model
     *
     * @var string
     */
    protected $table = 'bank_account_movements';

    /**
     * Fields available to complete on creation or update
     *
     * @var string[]
     */
    protected $fillable = [
        'bank_account_id',
        'description',
        'date',
        'amount',
        'type',
        'type_movement',
        'cashbox_movement_id',
    ];

    protected $dates = [
        'date'
    ];

    /**
     * Custom casts for date fields
     *
     * @var string[]
     */
    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public static function createForPaymentOrderPayment($data, PaymentOrder $paymentOrder)
    {
        $data['description'] = 'Egreso orden de pago N°: ' . $paymentOrder->id;
        $data['type'] = 'outflow';
        $data['type_movement'] = 'transfer';
        return self::create($data);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('date', 'DESC');
    }

    public static function scopeAddBalanceLists($query, $bankAccountId)
    {
        return $query
            ->select('*')
            ->addSelect(
                DB::raw(
                    '
                (SELECT bam.balance
                FROM (
                         SELECT id,
                                SUM(
                                CASE
                                    WHEN type = \'income\' THEN amount
                                    ELSE (-amount)
                                    END
                                    ) OVER (ORDER BY (date, id) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS balance
                         FROM  bank_account_movements
                         WHERE bank_account_id = '.$bankAccountId.'
                     ) AS bam
                WHERE bank_account_movements.id = bam.id)'
                )
            );
    }

    public function scopeBalance($query, $bankAccountId)
    {
        return $query->where('bank_account_id', $bankAccountId)
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type = \'income\' THEN amount ELSE -amount END)),0) AS balance'))
            ->first();
    }

    /**
     * Scope a query to filter bank accounts movements data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if(!empty($filters['date_from'])){
            $from = Carbon::parse($filters['date_from']);
            $query->where('date', '>=' ,$from);
        }

        if(!empty($filters['date_to'])){
            $to = Carbon::parse($filters['date_to']);
            $query->where('date', '<=' ,$to);
        }

        if(isset($filters['amount_from'])){
            $query->where('amount', '>=',  $filters['amount_from']);
        }

        if(isset($filters['amount_to'])){
            $query->where('amount', '<=',  $filters['amount_to']);
        }

        if (!empty($filters['type'])) {
            $query->where('type', $filters['type']);
        }

        return $query;
    }
}
