<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Cashbox extends Model
{
    protected $table = 'cashboxes';

    protected $fillable = [
        'name',
        'consortium_id',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeFromAdministration($query)
    {
        $query->leftJoin('consortia', 'consortia.id', '=', 'cashboxes.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('cashboxes.*');
    }

    public static function fromConsortium($consortiumId)
    {
        return self::where('consortium_id', $consortiumId)
            ->first();
    }
}
