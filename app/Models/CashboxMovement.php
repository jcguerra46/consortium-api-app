<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class CashboxMovement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cashbox_movements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cashbox_id',
        'date',
        'description',
        'identified',
        'total_amount',
        'cash_amount',
        'type_movement',
        'payment_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * @param $data
     * @return mixed
     */
    public static function createIncome($data)
    {
        $data['type_movement'] = 'income';
        return self::create($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    public static function createOutflow($data)
    {
        $data['type_movement'] = 'outflow';
        $data['identified'] = true;
        return self::create($data);
    }

    /**
     * Relation belongsTo with Cashbox model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cashbox()
    {
        return $this->belongsTo('App\Models\Cashbox');
    }

    /**
     * Relation belongsToMany with FunctionalUnit model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function functionalUnits()
    {
        return $this->belongsToMany('App\Models\FunctionalUnit', 'cashbox_movement_functional_units');
    }

    /**
     * Relation hasMany with BankAccountMovement model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transfers()
    {
        return $this->hasMany('App\Models\BankAccountMovement');
    }

    /**
     * Relation hasMany with Check model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checksIncome()
    {
        return $this->hasMany('App\Models\Check', 'cashbox_movement_income_id', 'id');
    }

    /**
     * Relation hasMany with Check model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checksOutflow()
    {
        return $this->hasMany('App\Models\Check', 'cashbox_movement_outflow_id', 'id');
    }

    /**
     * @param $query
     * @param $cashboxId
     * @return float
     */
    public function scopeBalance($query, $cashboxId)
    {
        return (float)$query->where('cashbox_id', $cashboxId)
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN total_amount ELSE -total_amount END)),0) AS balance'))
            ->first('balance')->balance;
    }

    /**
     * @param $query
     * @param $cashboxId
     * @return float
     */
    public function scopeBalanceCashAmount($query, $cashboxId)
    {
        return (float)$query->where('cashbox_id', $cashboxId)
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN cash_amount ELSE -cash_amount END)),0) AS cash_balance'))
            ->first()->cash_balance;
    }


    /**
     * @param $query
     * @return mixed
     */
    public static function scopeAddBalanceLists($query, $cashboxId)
    {
        return $query->select('*')->addSelect(
            DB::raw(
                '
                (SELECT cm.balance
                FROM (
                         SELECT id,
                                SUM(
                                CASE
                                    WHEN type_movement = \'income\' THEN total_amount
                                    ELSE (-total_amount)
                                    END
                                    ) OVER (ORDER BY (date, id) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS balance
                         FROM  cashbox_movements
                         WHERE cashbox_id = ' . $cashboxId . '
                     ) AS cm
                WHERE cashbox_movements.id = cm.id)'
            )
        );
    }

    /**
     * Scope a query to filter cashbox movements data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters(Builder $query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['date_from'])) {
            $from = Carbon::parse($filters['date_from']);
            $query->where('date', '>=', $from);
        }

        if (!empty($filters['date_to'])) {
            $to = Carbon::parse($filters['date_to']);
            $query->where('date', '<=', $to);
        }

        if (!empty($filters['description'])) {
            $query->where('description', 'LIKE', '%' . $filters['description'] . '%');
        }

        if (isset($filters['amount_from'])) {
            $query->where('amount', '>=',  $filters['amount_from']);
        }

        if (isset($filters['amount_to'])) {
            $query->where('amount', '<=',  $filters['amount_to']);
        }

        if (Arr::has($filters, 'identified')) {
            $query->where('identified', $filters['identified']);
        }

        if (!empty($filters['type_movement'])) {
            $query->where('type_movement', $filters['type_movement']);
        }

        return $query;
    }

    /**
     * @param $cashboxId
     * @return mixed
     */
    public static function balanceChecks($cashboxId)
    {
        $incomes = self::where('cashbox_id', $cashboxId)
            ->where('type_movement', 'income')
            ->leftJoin('checks', 'checks.cashbox_movement_income_id', '=', 'cashbox_movements.id')
            ->sum('checks.amount');

        $outflows = self::where('cashbox_movements.cashbox_id', $cashboxId)
            ->where('type_movement', 'outflow')
            ->leftJoin('checks', 'checks.cashbox_movement_outflow_id', '=', 'cashbox_movements.id')
            ->sum('checks.amount');

        return $incomes - $outflows;
    }

    /**
     * @param $cashboxId
     * @return mixed
     */
    public static function balanceTransfers($cashboxId)
    {
        $incomes = self::where('cashbox_movements.cashbox_id', $cashboxId)
            ->where('cashbox_movements.type_movement', 'income')
            ->leftJoin('bank_account_movements', 'bank_account_movements.cashbox_movement_id', '=', 'cashbox_movements.id')
            ->sum('bank_account_movements.amount');

        $outflows = self::where('cashbox_movements.cashbox_id', $cashboxId)
            ->where('cashbox_movements.type_movement', 'outflow')
            ->leftJoin('bank_account_movements', 'bank_account_movements.cashbox_movement_id', '=', 'cashbox_movements.id')
            ->sum('bank_account_movements.amount');

        return $incomes - $outflows;
    }
}
