<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashboxMovementFunctionalUnit extends Model
{
    protected $table = 'cashbox_movement_functional_units';

    protected $fillable = [
        'cashbox_movement_id',
        'functional_unit_id',
        'amount',
    ];

    public $timestamps = false;
}
