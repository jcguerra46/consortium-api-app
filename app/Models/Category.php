<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'name',
        'position',
        'hidden',
        'salaries',
        'administration_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation BelongsTo with Administration model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administration()
    {
        return $this->belongsTo(Administration::class);
    }

    /**
     * Scope a query to only include categories of the administration.
     *
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', 1);
//        return $query->where('administration_id', Auth::user()->administration_id);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['name'])) {
            $query->where('name', 'ILIKE', '%' . $filters['name'] . '%');
        }

        return $query;
    }
}
