<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Check extends Model
{
    protected $table = 'checks';

    protected $fillable = [
        'number',
        'deposit_date',
        'issuance_date',
        'due_date',
        'amount',
        'crossed',
        'is_to_the_order',
        'endorsements',
        'own',
        'cashbox_movement_income_id',
        'cashbox_movement_outflow_id',
        'bank_id',
        'checkbook_id',
        'consortium_id',
        'administration_id',
    ];

    protected $dates = [
        'deposit_date',
        'issuance_date'
    ];

    protected $casts = [
        'deposit_date' => 'date:d-m-Y',
        'issuance_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation BelongsTo with Bank model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }

    /**
     * Scope a query to filter check data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters(Builder $query)
    {

        $filters = \request()->get('filters');

        if (isset($filters['consortium_id'])) {
            $query->where('consortium_id', $filters['consortium_id']);
        }
        if (array_key_exists('cashbox_movement_income_id', $filters)) {
            $query->where('cashbox_movement_income_id', $filters['cashbox_movement_income_id']);
        }
        if (array_key_exists('cashbox_movement_outflow_id', $filters)) {
            $query->where('cashbox_movement_outflow_id', $filters['cashbox_movement_outflow_id']);
        }


        if (isset($filters['own'])) {
            $query->where('own', $filters['own']);
        }
        return $query;
    }
}
