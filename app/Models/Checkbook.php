<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Checkbook extends Model
{
    protected $table = 'checkbooks';

    protected $fillable = [
        'administration_id',
        'bank_id',
        'number',
        'first_check',
        'last_check',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $dispatchesEvents = [
        'created' => \App\Events\CheckbookCreatedEvent::class,
    ];

    public function checks()
    {
        return $this->hasMany('App\Models\Check');
    }

    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }
}
