<?php

namespace App\Models;

use App\Models\UserPortal;
use App\Models\FunctionalUnit;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Claim extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'claims';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'functional_unit_id',
        'user_portal_id',
        'title',
        'body',
        'state'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation BelongsTo with Functional unit model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function functionalUnit()
    {
        return $this->belongsTo(FunctionalUnit::class);
    }

    /**
     * Relation BelongsTo with User Portal Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userPortal()
    {
        return $this->belongsTo(UserPortal::class);
    }

    /**
     * Relatin HasMany with Claim Comment Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function claimComments()
    {
        return $this->hasMany(ClaimComment::class);
    }

    /**
     * Scope a query to only include all functional unit of the administration.
     *
     * @param $query
     * @return mixed
     */
    public function scopeFromFunctionalUnit($query)
    {
        return $query->leftJoin('functional_units', 'functional_units.id', '=', 'claims.functional_unit_id')
            ->leftJoin('consortia', 'consortia.id', '=', 'functional_units.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('claims.*','consortia.administration_id');

    }
}
