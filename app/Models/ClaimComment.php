<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClaimComment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'claim_comments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'claim_id',
        'user_portal_id',
        'title',
        'body',
        'viewed'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation BelongsTo with Claim Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function claim()
    {
        return $this->belongsTo(Claim::class);
    }

    /**
     * Relation BelongsTo with User Portal Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userPortal()
    {
        return $this->belongsTo(UserPortal::class);
    }
}
