<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Collection extends Model
{
    protected $table = 'collections_functional_units';

    protected $fillable = [
        'description',
        'date',
        'total_amount',
        'cash_amount',
        'amount_checks',
        'amount_transfers',
        'cashbox_movement_id',
        'period_id',
        'functional_unit_id',
    ];

    protected $dates = [
        'date',
    ];

    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('functional_units', 'functional_units.id', '=', 'collections_functional_units.functional_unit_id')
                    ->leftJoin('consortia', 'consortia.id', '=', 'functional_units.consortium_id')
                    ->where('consortia.administration_id', Auth::user()->administration_id)
                    ->select('collections_functional_units.*');
    }
}
