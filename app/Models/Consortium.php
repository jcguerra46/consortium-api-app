<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Consortium extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'consortia';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administration_id',
        'order',
        'code',
        'fancy_name',
        'business_name',
        'address_number',
        'address_street',
        'country_id',
        'province_id',
        'location',
        'postal_code',
        'cuit',
        'start_date',
        'payment_method_id',
        'suterh_code',
        'siro_code',
        'pmc_code',
        'pme_code',
        'ep_code',
        'pf_code',
        'interfast_code',
        'siro_verification_number',
        'count_floors',
        'count_functional_units',
        'type_of_building',
        'state',
        'category'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation hasOne with ConsortiumProfile Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('App\Models\ConsortiumProfile');
    }

    /**
     * Relation hasOne with Cashbox Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cashbox()
    {
        return $this->hasOne('App\Models\Cashbox');
    }

    /**
     * Relation hasMany with UserAdministration model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(UserAdministration::class, 'consortia_user_administration')
            ->withPivot('consortium_id');
    }

    /**
     * Relation BelongsTo with Administration Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administration()
    {
        return $this->belongsTo(Administration::class);
    }

    /**
     * Relatin hasMany with FunctionalUnit Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function functionalunits()
    {
        return $this->hasMany(FunctionalUnit::class);
    }

    /**
     * Relation hasOne with PortalProfile model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function portalProfile()
    {
        return $this->hasOne('App\Models\ConsortiumPortalProfile');
    }

    /**
     * Scope to bring the consortia of the
     * administrations assigned to the user
     *
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministrationIndex($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id)
            ->whereHas('users', function($user) {
                $user->where('id', Auth::user()->id);
            });
    }

    /**
     * Scope to bring the consortia assigned to the user
     *
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministrationShow($query, $id)
    {
        return $query->where('id', $id)
            ->whereHas('users', function($user) {
                $user->where('id', Auth::user()->id);
            });
    }

    /**
     * Scope a query to filter consortium data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters(Builder $query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['business_name'])) {
            $query->where('business_name', 'ILIKE', '%' . $filters['business_name'] . '%');
        }

        if (!empty($filters['order'])) {
            $query->where('order', $filters['order']);
        }

        if (!empty($filters['fancy_name'])) {
            $query->where('fancy_name', 'ILIKE', '%' . $filters['fancy_name'] . '%');
        }

        return $query;
    }

    /**
     * Scope used on functionalUnitController@index
     *
     * @param $query
     * @return mixed
     */
    public function scopeIndexFunctionalUnits($query, $consortium_id)
    {
        return $query->leftJoin('functional_units', 'functional_units.consortium_id', '=' , 'consortia.id')
            ->where('functional_units.consortium_id', $consortium_id)
            ->whereHas('users', function($user) {
                $user->where('id', Auth::user()->id);
            })->select('functional_units.*');
    }

    /**
     * Scope a query to order data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyOrder(Builder $query)
    {
        $sortBy = \request()->get('sort_by', 'order');
        $sortDir =  \request()->get('sort_dir', 'asc');

        return $query->orderBy($sortBy, $sortDir);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address_street . ' ' . $this->address_number;
    }

    /**
     * @return mixed
     */
    public function getOpenPeriod()
    {
        return Period::open($this->id)
            ->first();
    }

    /**
     * Get ids of consortiums of the user authenticated
     *
     * @return mixed
     */
    public static function getConsortiumsOfUser()
    {
        foreach(Auth::user()->consortiums as $consortium) {
            $consortiumIds[] = $consortium->id;
        }
        return $consortiumIds;
    }

}
