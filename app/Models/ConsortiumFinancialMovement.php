<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ConsortiumFinancialMovement extends Model
{
    protected $table = 'consortium_financial_movements';

    protected $fillable = [
        'consortium_id',
        'spending_id',
        'amount',
        'date',
        'description',
        'type_movement',
        'type',
        'expense_id',
        'identified',
        'incomplete',
        'created_from_recurrent',
        'included_in_the_financial_statement',
    ];

    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $dates = [
        'date',
    ];

    public function scopeOrdered($query)
    {
        return $query->orderBy('date', 'DESC')
            ->orderBy('id', 'DESC');
    }

    public function scopeBalance($query, $consortiumId)
    {
        return $query->where('consortium_id', $consortiumId)
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type = \'income\' THEN amount ELSE -amount END)),0) AS balance'))
            ->first();
    }

    public static function scopeAddBalanceLists($query, $consortiumId)
    {
        return $query
            ->select('*')
            ->addSelect(
                DB::raw(
                    '
                (SELECT cm.balance
                FROM (
                         SELECT id,
                                SUM(
                                CASE
                                    WHEN type_movement = \'income\' THEN amount
                                                        ELSE (-amount)
                                    END
                                                        ) OVER (ORDER BY (date, id) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS balance
                         FROM  consortium_financial_movements WHERE consortium_id = '.$consortiumId.'
                     ) AS cm
                WHERE consortium_financial_movements.id = cm.id) AS balance'
                )
            );
    }

    /**
     * Scope a query to filter cashbox movements data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters(Builder $query)
    {
        $filters = \request()->get('filters');

        if(!empty($filters['date_from'])){
            $from = Carbon::parse($filters['date_from']);
            $query->where('date', '>=' ,$from);
        }

        if(!empty($filters['date_to'])){
            $to = Carbon::parse($filters['date_to']);
            $query->where('date', '<=' ,$to);
        }

        if (!empty($filters['description'])) {
            $query->where('description', 'LIKE', '%' . $filters['description'] . '%');
        }

        if (isset($filters['amount_from'])) {
            $query->where('amount', '>=',  $filters['amount_from']);
        }

        if (isset($filters['amount_to'])) {
            $query->where('amount', '<=',  $filters['amount_to']);
        }

        if (Arr::has($filters, 'identified')) {
            $query->where('identified', $filters['identified']);
        }

        if (!empty($filters['type_movement'])) {
            $query->where('type_movement', $filters['type_movement']);
        }

        return $query;
    }


}
