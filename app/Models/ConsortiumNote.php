<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ConsortiumNote extends Model
{
    protected $table = 'consortium_notes';

    protected $fillable = [
        'consortium_id',
        'note_id',
        'position'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeOrdered($query)
    {
        return $query->orderBy('position', 'ASC');
    }
}
