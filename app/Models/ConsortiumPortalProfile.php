<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsortiumPortalProfile extends Model
{
    protected $table = 'consortium_portal_profiles';

    protected $fillable = [
        'consortium_id',
        'show_claims',
        'show_ammenities',
        'need_ammenities_approval',
        'show_consortium_current_account',
        'show_consortium_bank_account',
        'show_money_in_debt',
        'show_spendings'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeFromConsortium($consortiumId)
    {
        return self::where('consortium_id', $consortiumId)
            ->first();
    }
}
