<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsortiumProfile extends Model
{
    protected $table = 'consortium_profiles';

    protected $fillable = [
        'consortium_id',
        'bank_account_id',
        'escrow',
        'email',
        'license',
        'liquidation',
        'rounding_type',
        'first_due_date',
        'second_due_date',
        'second_due_date_interests',
        'penalty_interests',
        'penalty_interests_mode',
        'rounding_payslips',
        'garage_amount',
        'art_amount',
        'art_percentage',
        'collect_interest_outside_due_date',
        'independent_cashbox',
        'notes',
        'next_payment_receipt_number',
        'payslips_details',
        'next_moving_sheet_number',
        'private_spendings_title',
        'self_managment',
        'early_payment',
        'early_payment_discount_days',
        'early_payment_discount',
        'show_bank_movements',
        'show_patrimonial_status',
        'show_provider_data',
        'legends_my_expenses',
        'show_judgments_section',
        'auto_billing',
        'package_id',
        'expense_header_id',
        'observations',
        'waste_removal_ammount', 
        'provider_spending_location',
        'show_previous_balance',
        'show_payments'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeFromConsortium($consortiumId)
    {
        return self::where('consortium_id', $consortiumId)
            ->first();
    }
}
