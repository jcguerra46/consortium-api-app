<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConsortiumUserAdministration extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "consortia_user_administration";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_administration_id',
        'consortium_id'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
}
