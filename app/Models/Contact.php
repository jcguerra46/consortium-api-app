<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Contact extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administration_id',
        'first_name',
        'last_name',
        'address_street',
        'postal_code',
        'dni',
        'birth_date',
        'phone_personal',
        'phone_work',
        'phone_cel',
        'email',
        'expenses_by_email',
        'council_member',
        'observations'
    ];

    /**
     * @var string[]
     */
    protected $dates = [
        'birth_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'birth_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation belongsToMany with Functinal Unit Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function functionalunit()
    {
        return $this->belongsToMany(FunctionalUnit::class, 'functional_unit_contacts')
            ->withPivot('contact_id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }
}
