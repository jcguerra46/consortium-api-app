<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use App\Events\EmployeeCreatedEvent;
use App\Events\EmployeeUpdatingEvent;

class Employee extends Model
{
    use SoftDeletes;

    protected $table = 'employees';

    protected $fillable = [
        'professional_function_id',
        'first_name',
        'last_name',
        'birth_date',
        'type_dni',
        'dni',
        'cuil',
        'number_docket',
        'active',
        'employee_status_id',
        'consortium_id',
    ];

    protected $casts = [
        'birth_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $dates = [
        'birth_date',
    ];

    protected $dispatchesEvents = [
        'created' => EmployeeCreatedEvent::class,
        'updating' => EmployeeUpdatingEvent::class,
    ];

    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('consortia', 'consortia.id', '=', 'employees.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('employees.*');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\EmployeeProfile');
    }

    public function professionalFunction()
    {
        return $this->belongsTo('App\Models\ProfessionalFunction');
    }

    public function consortium()
    {
        return $this->belongsTo('App\Models\Consortium');
    }

    public function adjustments()
    {
        return $this->hasMany('App\Models\EmployeeAdjustment');
    }

    public function pluses()
    {
        return $this->belongsToMany('App\Models\SalaryPlus', 'employee_pluses');
    }

    /**
     *
     * @param $query
     * @return mixed
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['consortium_id'])) {
            $query->where('consortium_id', $filters['consortium_id']);
        }

        if (!empty($filters['professional_function_id'])) {
            $query->where('professional_function_id', $filters['professional_function_id']);
        }

        if (!empty($filters['cuil'])) {
            $query->where('cuil', 'ILIKE', '%' . $filters['cuil'] . '%');
        }

        if (!empty($filters['first_name'])) {
            $query->where('first_name', 'ILIKE', '%' . $filters['first_name'] . '%');
        }

        if (!empty($filters['last_name'])) {
            $query->where('last_name', 'ILIKE', '%' . $filters['last_name'] . '%');
        }

        if (isset($filters['active'])) {
            $query->where('active', $filters['active']);
        }

        return $query;
    }
}
