<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeAdjustment extends Model
{
    protected $table = 'employee_adjustments';

    protected $fillable = [
        'employee_id',
        'type',
        'haber_descuento',
        'description',
        'value',
        'used',
        'suma_sueldo_jornal',
        'recurrent',
        'es_remunerativo',
        'unit',
        'percentage',
        'percentage_sobre_base',
        'active',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
