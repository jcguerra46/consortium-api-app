<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeAfip extends Model
{
    protected $table = 'employee_afips';

    protected $fillable = [
        'employee_id',
        'conyuge',
        'seguro_colectivo_vida',
        'trabajador_convencionado',
        'adicional_obra_sociales',
        'maternidad',
        'situacion',
        'condicion',
        'actividad',
        'zona',
        'adicional_seguridad_social',
        'adicionales',
        'cantidad_hijos',
        'cantidad_adherentes',
        'modalidad_contratacion',
        'provincia_localidad',
        'siniestrado',
        'importe_adicional_obra_social',
        'revista1',
        'revista2',
        'revista3',
        'dia_revista1',
        'dia_revista2',
        'dia_revista3',
        'premios',
        'conceptos_no_remunerativos',
        'rectificacion_remuneracion',
        'contribucion_tarea_diferencial',
    ];

    protected $dates = [
        'birth_date',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];
}
