<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeBeneficiary extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employee_beneficiaries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'cuil',
        'first_name',
        'last_name',
        'document_type',
        'dni',
        'relationship',
        'porcentage'
    ];

}
