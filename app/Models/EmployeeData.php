<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeData extends Model
{
    protected $table = 'employee_datas';

    protected $fillable = [
        'employee_id',
        'sex',
        'civil_status_code',
        'address_street',
        'address_number',
        'address_floor',
        'address_departament',
        'postal_code',
        'phone',
        'email',
        'nationality_code',
        'nationality',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
