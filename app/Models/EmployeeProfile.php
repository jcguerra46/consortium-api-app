<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeProfile extends Model
{
    protected $table = 'employee_profiles';

    protected $fillable = [
        'employee_id',
        'social_work_id',
        'retired',
        'adicional_obra_social',
        'jornalizado_hours_per_day',
        'entry_date',
        'departure date',
        'antiquity',
        'afiliado_sindicato',
        'afiliado_f_m_v_d_d',
        'descuento_c_p_f',
        'd_g_d_y_p_c',
        'calculate_obra_social_by_law',
        'extra_hours_50',
        'extra_hours_saturday_50',
        'extra_hours_saturday_100',
        'extra_hours_sunday_100',
        'extra_hours_holiday_100',
        'law27430',
    ];

    protected $dates = [
        'entry_date',
        'departure date',
    ];

    protected $casts = [
        'entry_date' => 'date:d-m-Y',
        'departure_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
