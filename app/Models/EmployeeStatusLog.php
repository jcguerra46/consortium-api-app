<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatusLog extends Model
{
    protected $table = 'employee_status_logs';

    protected $fillable = [
        'employee_id',
        'employee_status_id',
        'date',
    ];

    protected $dates = [
        'date',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
