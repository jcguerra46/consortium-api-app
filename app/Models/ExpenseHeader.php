<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ExpenseHeader extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'expense_headers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administration_id',
        'name_header',
        'cuit',
        'name',
        'address_street',
        'address_number',
        'email',
        'phone',
        'fiscal_situation',
        'postal_code',
        'rpa'
    ];

    /**
     * Scope a query to only include all expense headers of the administration.
     *
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }
}
