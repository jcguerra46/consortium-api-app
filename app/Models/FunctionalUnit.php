<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FunctionalUnit extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'functional_units';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'consortium_id',
        'package_id',
        'functional_unit_type_id',
        'owner_first_name',
        'owner_last_name',
        'floor',
        'department',
        'functional_unit_number',
        'initial_balance',
        'type_forgive_interest',
        'legal_state',
        'm2',
        'order',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation BelongsTo with Consortium Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consortium()
    {
        return $this->belongsTo(Consortium::class);
    }

    /**
     * Relation HasMany with Claim model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function claims()
    {
        return $this->hasMany(Claim::class);
    }

    /**
     * Relation BelongsTo with Functional Unit Type Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function functionalUnitType()
    {
        return $this->belongsTo(FunctionalUnitType::class);
    }

    /**
     * Relation belongsToMany with Contact model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contacts()
    {
        return $this->belongsToMany(Contact::class, 'functional_unit_contacts');
    }

    /**
     * Scope to filter a listing of the resource.
     *
     * @param $query
     * @return mixed
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['functional_unit_number'])) {
            $query->where('functional_unit_number', 'ILIKE', '%' . $filters['functional_unit_number'] . '%');
        }

        return $query;
    }

    /**
     * Scope a query to only include functional units
     * of consortiums assigned to a user authenticated.
     *
     * @param $query
     * @return mixed
     */
    public function scopeFromConsortiums($query)
    {
        /** ids of Consortium of the user authenticated */
        $consortiumIds = Consortium::getConsortiumsOfUser();

        return $query->whereIn('consortium_id', $consortiumIds);
    }

    /**
     * Scope a query to get functional units by consortium.
     *
     * @param $query
     * @param $consortium_id
     * @return mixed
     */
    public function scopeByConsortium($query, $consortium_id)
    {
        return $query->where('consortium_id', $consortium_id);

    }

}
