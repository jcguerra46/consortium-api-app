<?php

namespace App\Models;

use App\Models\Claim;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FunctionalUnitContact extends Model
{
    protected $table = 'functional_unit_contacts';

    protected $fillable = [
        'functional_unit_id',
        'contact_id',
        'contact_type',
        'primary',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Scope to get list of contacts from functional unit.
     *
     * @param $query
     * @param $functional_unit_id
     * @return mixed
     */
    public function scopeContactsFromFunctionalUnit($query, $functional_unit_id)
    {
        return $query->leftJoin('functional_units', 'functional_unit_contacts.functional_unit_id', '=', 'functional_units.id')
            ->leftJoin('contacts', 'functional_unit_contacts.contact_id', '=', 'contacts.id')
            ->where('functional_units.id', $functional_unit_id)
            ->select('functional_unit_contacts.id as functional_unit_contacts_id', 'functional_unit_contacts.contact_type', 'contacts.*');
    }

    public function scopeFromAdministration($query, $functional_unit_id)
    {
        return $query->leftJoin('functional_units', 'functional_units.id', '=', 'functional_unit_contacts.functional_unit_id')
            ->leftJoin('consortia', 'consortia.id', '=', 'functional_units.consortium_id')
            ->where('functional_units.id', $functional_unit_id)
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('functional_unit_contacts.*');
    }

    /**
     * Scope a query to filter consortium data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters(Builder $query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['searchString'])) {
            $query->where('first_name', 'ILIKE', '%' . $filters['searchString'] . '%');
            $query->orwhere('last_name', 'ILIKE', '%' . $filters['searchString'] . '%');
            $query->orwhere('email', 'ILIKE', '%' . $filters['searchString'] . '%');
            $query->orwhere('phone_personal', 'ILIKE', '%' . $filters['searchString'] . '%');
            $query->orwhere('phone_work', 'ILIKE', '%' . $filters['searchString'] . '%');
            $query->orwhere('phone_cel', 'ILIKE', '%' . $filters['searchString'] . '%');
        }

        return $query;
    }


}
