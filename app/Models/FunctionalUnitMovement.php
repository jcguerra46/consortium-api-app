<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FunctionalUnitMovement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'functional_unit_movements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'functional_unit_id',
        'amount',
        'date',
        'description',
        'type',
        'type_movement',
        'identified',
        'accumulate_to_expense',
        'expense_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date',
    ];

    public function scopeOrdered($query)
    {
        return $query->orderBy('date', 'DESC')->orderBy('id', 'DESC');
    }

    /**
     * Scope retrieve balance of functional unit current account
     *
     * @param $query
     * @param $functionalUnitId
     * @return mixed
     */
    public function scopeBalance($query, $functionalUnitId)
    {
        return $query->where('functional_unit_id', $functionalUnitId)
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN amount ELSE -amount END)),0) AS balance'))
            ->first();
    }

    /**
     * Scope retrieve accumulated interest of functional unit
     *
     * @param $query
     * @param $functionalUnitId
     * @return mixed
     */
    public function scopeAccumulatedInterest($query, $functionalUnitId)
    {
        return $query->whereIn('type', ['month_interest', 'accumulated_interest', 'expiration_interest'])
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN amount ELSE -amount END)),0) AS interest'));
    }

    /**
     * Scope retrieve capital debt of functional unit
     *
     * @param $query
     * @param $functionalUnitId
     * @return mixed
     */
    public function scopeAccumulatedCapital($query, $functionalUnitId)
    {
        return $query->whereIn('type', ['initial_balance', 'accumulated_capital', 'early_payment_discount', 'capital', 'debt_capital'])
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN amount ELSE -amount END)),0) AS capital'));
    }

    /**
     * Scope to send balance per movement.
     *
     *
     * @param $query
     * @param $functionalUnitId
     * @return mixed
     */
    public function scopeAddBalanceList($query, $functionalUnitId)
    {
        return $query->select('*')
            ->addSelect(
                DB::raw(
                    '
            (SELECT fum.balance
            FROM (
                     SELECT id,
                            SUM(
                            CASE
                                WHEN type_movement = \'income\' THEN amount
                                ELSE (-amount)
                                END
                                ) OVER (ORDER BY (date, id) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS balance
                     FROM  functional_unit_movements
                     WHERE functional_unit_id = ' . $functionalUnitId . '
                 ) AS fum
            WHERE functional_unit_movements.id = fum.id)'
                )
            );
    }

    /**
     * Scope to filter movement per functional unit.
     *
     * @param $query
     * @param $functional_unit_id
     * @return mixed
     */
    public function scopeByFunctionalUnit($query, $functional_unit_id)
    {
        return $query->where('functional_unit_id', $functional_unit_id);
    }
}
