<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FunctionalUnitType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'functional_unit_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'active'
    ];

    /**
     * Relatin HasMany with Functional Unit Type Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function functionalUnitTypes()
    {
        return $this->hasMany(FunctionalUnitType::class);
    }
}
