<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'provider_id',
        'invoice_number',
        'description',
        'date',
        'amount',
        'administration_id',
    ];

    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $dates = [
        'date',
    ];

    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }

    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
}
