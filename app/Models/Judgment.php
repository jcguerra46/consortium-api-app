<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Judgment extends Model
{
    protected $table = 'judgments';

    protected $fillable = [
        'consortium_id',
        'cover',
        'number_expedient',
        'judged',
        'objet',
        'state',
        'reclaimed_amount',
        'active',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('consortia','consortia.id','=', 'judgments.consortium_id')
                    ->where('consortia.administration_id', Auth::user()->administration_id)
                    ->select('judgments.*');
    }
}
