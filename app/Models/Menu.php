<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data',
        'active'
    ];

    /**
     * Scope a query to only include menu active.
     *
     * @param $query
     * @return mixed
     */
    public function scopeMenuActive($query)
    {
        return $query->where('active', true)->first();
    }

}
