<?php

namespace App\Models;

use App\Events\PaymentCreatedEvent;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * Fields available to complete on creation or update
     *
     * @var string[]
     */
    protected $fillable = [
        'date',
        'total_amount',
        'provider_id',
    ];

    /**
     * Custom casts for date fields
     *
     * @var string[]
     */
    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $dates = [
        'date',
    ];

    protected $dispatchesEvents = [
        'created' => PaymentCreatedEvent::class,
    ];
}
