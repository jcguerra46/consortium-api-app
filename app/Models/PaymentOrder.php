<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class PaymentOrder extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model
     *
     * @var string
     */
    protected $table = 'payment_orders';

    /**
     * Fields available to complete on creation or update
     *
     * @var string[]
     */
    protected $fillable = [
        'provider_id',
        'administration_id',
        'amount',
        'description',
        'status',
        'date',
    ];

    /**
     * Custom casts for date fields
     *
     * @var string[]
     */
    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $dates = [
        'date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function installments()
    {
        return $this->belongsToMany(SpendingInstallment::class, 'payment_order_spending_installments');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administration()
    {
        return $this->belongsTo('App\Models\Administration');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    /**
     *
     * @param $query
     * @return mixed
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['consortium_id'])) {
            $query->where('consortium_id', $filters['consortium_id']);
        }

        if (!empty($filters['provider_id'])) {
            $query->where('provider_id', $filters['provider_id']);
        }

        if (!empty($filters['status'])) {
            $query->where('status', $filters['status']);
        }

        if (!empty($filters['description'])) {
            $query->where('description', 'ILIKE', '%' . $filters['description'] . '%');
        }

        if (isset($filters['date_from'])) {
            $from = Carbon::parse($filters['date_from']);
            $query->where('date', '>=', $from);
        }

        if (isset($filters['date_to'])) {
            $to = Carbon::parse($filters['date_to']);
            $query->where('date', '<=', $to);
        }

        return $query;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('providers', 'providers.id', '=', 'payment_orders.provider_id')
            ->where('payment_orders.administration_id', Auth::user()->administration_id)
            ->select('providers.*', 'payment_orders.*')
            ->orderby('payment_orders.date', 'desc');
    }
}
