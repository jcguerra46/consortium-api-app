<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentOrderSpendingInstallments extends Model
{
    protected $table = 'payment_order_spending_installments';
}
