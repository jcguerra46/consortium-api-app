<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PercentageConsortium extends Model
{
    use SoftDeletes;

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'percentages_consortium';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'consortium_id',
        'name',
        'second_line_name',
        'description',
        'type',
        'position',
        'last_fixed_amount',
        'particular_percentage',
        'hidden_expense_spending',
        'hidden_in_proration',
        'hidden_percentage_value',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation hasMany with PercentageFunctionalUnit model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function percentagesFunctionalUnits()
    {
        return $this->hasMany('App\Models\PercentageFunctionalUnit');
    }

    /**
     * Relation belongsToMany with Spending Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function spending() {
        return $this->belongsToMany(Spending::class, 'spending_consortium_percentages')
            ->withPivot('percentage_consortium_id');
    }

    /**
     * Scope to filter by consortium.
     *
     * @param $query
     * @param $Consortium_id
     * @return mixed
     */
    public function scopeByConsortium($query, $Consortium_id)
    {
        return $query->where('consortium_id', $Consortium_id);
    }

    /**
     * Scope to send percentages data ordered.
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('position', 'ASC');
    }

    /**
     * Scope to apply filters
     *
     * @param $query
     * @return mixed
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['name'])) {
            $query->where('name', 'ILIKE', '%' . $filters['name'] . '%');
        }

        return $query;
    }
}
