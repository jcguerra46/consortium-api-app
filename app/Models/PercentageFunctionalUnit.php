<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PercentageFunctionalUnit extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'percentages_functional_units';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'functional_unit_id',
        'percentage_consortium_id',
        'value',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * Relation belongsTo with PercentageConsortium model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function percentageConsortium()
    {
        return $this->belongsTo('App\Models\PercentageConsortium');
    }

    /**
     * Scope to filter by percentage consortium.
     *
     * @param $query
     * @param $percentage_consortium_id
     * @return mixed
     */
    public function scopeByPercentageConsortium($query, $percentage_consortium_id)
    {
        return $query->where('percentage_consortium_id', $percentage_consortium_id);
    }

}
