<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $table = 'periods';

    protected $fillable = [
        'consortium_id',
        'name',
        'description',
        'start_date',
        'end_date',
        'state',
    ];

    protected $casts = [
        'start_date' => 'date:d-m-Y',
        'end_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    protected $dispatchesEvents = [
        'created' => \App\Events\PeriodCreatedEvent::class,
    ];

    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    public function scopeOpen($query, $consortiumId)
    {
        return $query->where('consortium_id', $consortiumId)
            ->where('state', 'open');
    }
}
