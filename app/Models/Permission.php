<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'resource',
        'controller',
        'method',
        'display_name',
        'description'
    ];

    /**
     * Relation belongsToMany with UserAdministration Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(UserAdministration::class,'assigned_permissions_to_user_administration')
            ->withPivot('permission_id');
    }

    /**
     * Relation belongsToMany with Role Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'assigned_permissions_to_roles')
            ->withPivot('permission_id');
    }
}
