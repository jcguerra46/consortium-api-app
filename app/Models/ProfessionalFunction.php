<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfessionalFunction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'professional_functions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'has_department',
        'jornalizado',
        'surrogate',
        'type_seniority',
        'part_time'
    ];
}
