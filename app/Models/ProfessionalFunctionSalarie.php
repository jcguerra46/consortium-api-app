<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ProfessionalFunctionSalarie extends Model
{
    /**
     * @var string
     */
    protected $table = 'professional_function_salaries';

    /**
     * @var string[]
     */
    protected $fillable = [
        'consortium_category_id',
        'professional_function_id',
        'value',
        'period',
    ];

    public static function getBasicSalary(
        $professionalFunctionId,
        $consortiumCategoryId,
        $month,
        $year)
    {
        $liquidationDate = Carbon::create($year, $month, 1)->format('Y-m-d');
        return self::where('professional_function_id', $professionalFunctionId)
            ->where('consortium_category_id', $consortiumCategoryId)
            ->where('period', '<=', $liquidationDate)
            ->orderBy('period', 'DESC')
            ->first();
    }
}
