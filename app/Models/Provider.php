<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Provider extends Model
{
    use SoftDeletes;

    protected $table = 'providers';

    protected $fillable = [
        'business_name',
        'name',
        'address',
        'location',
        'province_id',
        'postal_code',
        'cuit',
        'email',
        'phone',
        'note',
        'license',
        'attention_hours',
        'fiscal_situation',
        'administration_id',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function consortium()
    {
        return $this->belongsTo('App\Models\Consortium');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service', 'provider_services');
    }

    public function scopeFromAdministration($query)
    {
        return $query->where('administration_id', Auth::user()->administration_id);
    }

    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['business_name'])) {
            $query->where('business_name', 'ILIKE', '%' . $filters['business_name'] . '%');
        }

        if (!empty($filters['name'])) {
            $query->where('name', 'ILIKE', '%' . $filters['name'] . '%');
        }

        if (!empty($filters['cuit'])) {
            $query->where('cuit', 'ILIKE', '%' . $filters['cuit'] . '%');
        }

        if (!empty($filters['email'])) {
            $query->where('email', 'ILIKE', '%' . $filters['email'] . '%');
        }

        if (!empty($filters['phone'])) {
            $query->where('phone', 'ILIKE', '%' . $filters['phone'] . '%');
        }

        return $query;
    }
}
