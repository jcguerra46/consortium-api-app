<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProviderFinancialMovement extends Model
{
    protected $table = 'provider_financial_movements';

    const TRANSACTION_TYPE_DEBIT = 'debit';
    const TRANSACTION_TYPE_CREDIT = 'credit';

    protected $fillable = [
        'provider_id',
        'amount',
        'date',
        'description',
        'transaction_type',
        'spending_installment_id',
        'payment_id',
        'consortium_id'
    ];

    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consortium()
    {
        return $this->belongsTo('App\Models\Consortium');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('date', 'DESC');
    }

    /**
     * @param $query
     * @param $providerId
     * @return mixed
     */
    public function scopeAddBalanceList($query, $providerId)
    {
        return $query
            ->select('*')
            ->addSelect(
                DB::raw(
                    '
                (SELECT pfm.balance
                FROM (
                         SELECT id,
                                SUM(
                                CASE
                                    WHEN transaction_type = \'debit\' THEN amount
                                    ELSE (-amount)
                                    END
                                    ) OVER (ORDER BY (date, id) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS balance
                         FROM  provider_financial_movements
                         WHERE provider_id = '.$providerId.'
                     ) AS pfm
                WHERE provider_financial_movements.id = pfm.id)'
                )
            );
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAddFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['consortium_id'])) {
            $query->where('consortium_id', $filters['consortium_id']);
        }

        if (!empty($filters['description'])) {
            $query->where('description', 'ILIKE', '%' . $filters['description'] . '%');
        }

        if(!empty($filters['date_from'])){
            $from = Carbon::parse($filters['date_from']);
            $query->where('date', '>=' ,$from);
        }

        if(!empty($filters['date_to'])){
            $to = Carbon::parse($filters['date_to']);
            $query->where('date', '<=' ,$to);
        }

        if(!empty($filters['amount_to'])){
            $query->where('amount', '<=' ,$filters['amount_to']);
        }

        if(!empty($filters['amount_from'])){
            $query->where('amount', '>=' ,$filters['amount_from']);
        }

        return $query;
    }
}
