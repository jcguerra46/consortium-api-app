<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderProfile extends Model
{
    protected $table = 'provider_profiles';

    protected $fillable = [
        'provider_id',
        'email',
        'phone',
        'note',
        'license',
        'attention_hours',
        'fiscal_situation',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
