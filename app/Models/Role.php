<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'display_name',
        'description',
        'active'
    ];

    /**
     * Relation hasMany with UserAdministration model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(UserAdministration::class, 'assigned_roles_to_user_administration')
            ->withPivot('role_id');
    }

    /**
     * Relation belongsToMany with Permission Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'assigned_permissions_to_roles');
    }
}
