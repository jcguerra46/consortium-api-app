<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table = 'salaries';

    protected $fillable = [
        'employee_id',
        'spending_id',
        'type',
        'period',
        'basico',
        'jornal',
        'neto',
        'bruto',
        'value_porcentual_art',
        'value_fijo_art',
        'extra_hours_worked',
        'value_extra_hours_worked',
        'vacation_days',
        'not_vacation_days',
        'contributions_period',
        'contributions_deposit_date',
        'contributions_bank',
        'contributions_employee_total',
        'contributions_employer_total',
        'inputs',
    ];

    protected $casts = [
        'period' => 'date:m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    public function concepts()
    {
        return $this->hasMany('App\Models\SalaryConcept');
    }

    public function contributions()
    {
        return $this->hasMany('App\Models\SalaryConceptContribution');
    }

}
