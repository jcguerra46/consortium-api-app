<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryAdditional extends Model
{
    protected $table = 'salary_additionals';

    protected $fillable = [
        'salary_plus_id',
        'period',
        'type',
        'description',
        'value',
        'quantity',
        'extra_value',
    ];

    protected $casts = [
        'period' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];
}
