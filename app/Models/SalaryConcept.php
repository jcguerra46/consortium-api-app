<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryConcept extends Model
{
    protected $table = 'salary_concepts';
    public $timestamps = false;

    protected $fillable = [
        'salary_id',
        'employee_adjustment_id',
        'value',
        'description',
        'type',
        'unit',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

}
