<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryConceptContribution extends Model
{
    protected $table = 'salary_concepts_contributions';
    public $timestamps = false;

    protected $fillable = [
        'salary_id',
        'value',
        'type_contribution',
        'description',
        'tipo_boleta',
        'concept',
        'spending_contribution_id',
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

}
