<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryPlus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'salary_pluses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'value',
        'internal_class',
    ];
}
