<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialWork extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'social_works';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code'
    ];
}
