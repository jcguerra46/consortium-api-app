<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Spending extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'spendings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number',
        'amount',
        'date',
        'prorate_in_expenses',
        'affect_financial_section',
        'description',
        'type',
        'category_id',
        'spending_recurrent_id',
        'consortium_id',
        'functional_unit_id',
        'provider_id',
        'service_id',
    ];

    protected $dates = [
        'date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];


    /**
     * Relation hasMany with SpendingInstallment model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function installments()
    {
        return $this->hasMany('App\Models\SpendingInstallment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consortium()
    {
        return $this->belongsTo('App\Models\Consortium');
    }

    /**
     * Relation hasMany with SpendingConsortiumPercentage model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function percentages()
    {
        return $this->hasMany('App\Models\SpendingConsortiumPercentage');
    }

    /**
     * Relation BelongsToMany with Percentage Consortium Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function percentageConsortium()
    {
        return $this->belongsToMany(PercentageConsortium::class, 'spending_consortium_percentages');
    }

    /**
     * Scope to send spending data ordered.
     *
     * @param $query
     * @return mixed
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }

    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['consortium_id'])) {
            $query->where('consortium_id', $filters['consortium_id']);
        }

        if (!empty($filters['provider_id'])) {
            $query->where('provider_id', $filters['provider_id']);
        }

        if (!empty($filters['category_id'])) {
            $query->where('category_id', $filters['category_id']);
        }

        if (isset($filters['amount_from'])) {
            $query->where('amount', '>=',  $filters['amount_from']);
        }

        if (isset($filters['amount_to'])) {
            $query->where('amount', '<=',  $filters['amount_to']);
        }

        if (!empty($filters['description'])) {
            $query->where('description', 'ILIKE', '%' . $filters['description'] . '%');
        }

        return $query;
    }

    /**
     * Scope to send spending data by administration.
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('consortia', 'consortia.id', '=', 'spendings.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('spendings.*');
    }
}
