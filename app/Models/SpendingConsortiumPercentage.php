<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpendingConsortiumPercentage extends Model
{
    protected $table = 'spending_consortium_percentages';

    protected $fillable = [
        'spending_id',
        'percentage_consortium_id',
        'value',
    ];

}
