<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SpendingInstallment extends Model
{
    /**
     * Table associated to the model
     *
     * @var string
     */
    protected $table = 'spending_installments';

    /**
     * Fields available to complete on creation or update
     *
     * @var string[]
     */
    protected $fillable = [
        'number',
        'amount',
        'date_pay',
        'payed',
        'spending_id',
        'payment_id',
        'invoice_number',
        'payment_order_id',
        'period_id',
    ];

    /**
     * Custom casts for date fields
     *
     * @var string[]
     */
    protected $casts = [
        'date_pay'   => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOrdered($query)
    {
        return $query->orderBy('spending_installments.created_at', 'DESC');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('spendings', 'spendings.id', '=', 'spending_installments.spending_id')
            ->leftJoin('consortia', 'consortia.id', '=', 'spendings.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('spending_installments.*');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function spending()
    {
        return $this->belongsTo('App\Models\Spending');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['consortium_id'])) {
            $query->where('consortium_id', $filters['consortium_id']);
        }

        if (!empty($filters['provider_id'])) {
            $query->where('provider_id', $filters['provider_id']);
        }

        if (!empty($filters['not_in_payment_order'])) {
            $query->leftJoin(
                'payment_order_spending_installments',
                'payment_order_spending_installments.spending_installment_id',
                '=',
                'spending_installments.id'
            )->where('payment_order_spending_installments.spending_installment_id', null)
                ->orWhere('payment_order_spending_installments.payment_order_id', $filters['not_in_payment_order']);
        }

        return $query;
    }
}
