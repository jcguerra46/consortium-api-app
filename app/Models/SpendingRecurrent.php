<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpendingRecurrent extends Model
{
    use SoftDeletes;

    protected $table = 'spendings_recurrents';

    protected $fillable = [
        'consortium_id',
        'provider_id',
        'percentage_consortium_id',
        'category_id',
        'description',
        'amount',
        'type',
        'periodicity',
        'start_date',
        'due_date',
    ];

    protected $dates = [
        'start_date',
        'due_date'
    ];

    protected $casts = [
        'start_date' => 'date:d-m-Y',
        'due_date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s',
        'deleted_at' => 'datetime:d-m-Y H:i:s',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consortium()
    {
        return $this->belongsTo(Consortium::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        if (!empty($filters['consortium_id'])) {
            $query->where('consortium_id', $filters['consortium_id']);
        }

        if (!empty($filters['provider_id'])) {
            $query->where('provider_id', $filters['provider_id']);
        }

        if (!empty($filters['type'])) {
            $query->where('type', $filters['type']);
        }

        return $query;
    }

    public function scopeFromAdministration($query)
    {
        return $query->leftJoin('consortia', 'consortia.id', '=', 'spendings_recurrents.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->select('spendings_recurrents.*');
    }
}
