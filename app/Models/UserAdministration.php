<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;

class UserAdministration extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    const VERIFIED_USER = true;

    protected $table = 'users_administrations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'administration_id',
        'principal',
        'first_name',
        'last_name',
        'email',
        'password',
        'active',
        'menu'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function isVerified($active)
    {
        return $active == UserAdministration::VERIFIED_USER;
    }

    /**
     * Generate token
     *
     * @param $number
     * @return string
     */
    public static function generateToken($number)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return $randomString;
    }

    /**
     * Relation BelongsTo with Administration Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function administration()
    {
        return $this->belongsTo(Administration::class);
    }

    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'assigned_roles_to_user_administration'
        );
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(
            Permission::class,
            'assigned_permissions_to_user_administration'
        );
    }

    /**
     * Relation BelongsToMany with consortium model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function consortiums()
    {
        return $this->belongsToMany(Consortium::class, 'consortia_user_administration');
    }

    /**
     * Verifying role
     *
     * @param array $roles
     * @return bool
     */
    public function hasRole(array $roles)
    {
        return (bool) $this->roles->pluck('name')->intersect($roles)->count();
    }

    /**
     * Check superadmin role
     *
     * @return mixed
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(['superadmin']);
    }

    /**
     * Verifying user role
     *
     * @param $roles
     * @return bool
     */
    public function hasRoles($roles)
    {
        return $this->hasRole($roles);
    }

    /**
     * Check if user has permission
     *
     * @param $route
     * @return bool
     */
    public function hasPermissions($route)
    {
        $url = explode('\\', $route);
        $data = explode('@', $url[3]);
        $controller = $data[0];
        $method = $data[1];

        return $this->handleRole($controller, $method);
    }

    /**
     * @param $controller
     * @param $method
     * @return bool
     */
    private function handleRole($controller, $method)
    {
        $user = Auth::user();
        foreach($user->roles as $role){
            $hasPermission = $this->handlePermission($role->id, $role->permissions->toArray(), $controller, $method);
            if($hasPermission){
                return true;
            }
        }
        return false;
    }

    /**
     * @param $role_id
     * @param $permissions
     * @param $controller
     * @param $method
     * @return bool
     */
    private function handlePermission($role_id, $permissions, $controller, $method)
    {
        foreach($permissions as $myPermission){
            $myPermissions[] = $myPermission['method'];
        }
        $permissionData = Permission::where('controller', $controller)->where('method', $method)->first();
        if($permissionData){
            return (bool) RolePermission::where('role_id', $role_id)->where('permission_id', $permissionData->id)->count();
        }
        return false;
    }

}

