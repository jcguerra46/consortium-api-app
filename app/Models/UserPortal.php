<?php

namespace App\Models;

use App\Models\Claim;
use App\Models\ClaimComment;
use Illuminate\Database\Eloquent\Model;

class UserPortal extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_portal';

    /**
     * Relation HasMany with claim model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function claims()
    {
        return $this->hasMany(Claim::class);
    }

    /**
     * Relatin HasMany with Claim Comment Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function claimComments()
    {
        return $this->hasMany(ClaimComment::class);
    }
}
