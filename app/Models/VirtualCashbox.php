<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VirtualCashbox extends Model
{
    protected $table = 'virtual_cashboxes';

    protected $fillable = [
        'consortium_id',
        'name',
        'description',
        'show_in_expense',
        'independent'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function consortium()
    {
        return $this->belongsTo('App\Models\Consortium');
    }

    /**
     * @param $query
     * @param $consortiumId
     * @return mixed
     */
    public function scopeFromConsortium($query, $consortiumId)
    {
        return $query->where('consortium_id', $consortiumId);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAddBalances($query)
    {
        return $query->select('*')
            ->addSelect(DB::raw('
                    (
                    SELECT
                        COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN total_amount ELSE -total_amount END)),0) AS balance
                    FROM virtual_cashboxes_movements vcm
                    WHERE vcm.virtual_cashbox_id = virtual_cashboxes.id
                    )'));
    }

    /**
     * Scope a query to filter virtual cashbox data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if (!empty($filters['id'])) {
            $query->where('id', $filters['id']);
        }

        return $query;
    }
}
