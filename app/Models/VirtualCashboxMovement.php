<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class VirtualCashboxMovement extends Model
{
    const TYPE_INCOME = 'income';
    const TYPE_OUTFLOW = 'outflow';

    protected $table = 'virtual_cashboxes_movements';

    protected $fillable = [
        'virtual_cashbox_id',
        'description',
        'date',
        'total_amount',
        'cash_amount',
        'type_movement',
    ];

    protected $casts = [
        'date' => 'date:d-m-Y',
        'created_at' => 'datetime:d-m-Y H:i:s',
        'updated_at' => 'datetime:d-m-Y H:i:s'
    ];

    public function virtualCashbox()
    {
        return $this->belongsTo('App\Models\VirtualCashbox');
    }

    public function bankAccountMovements()
    {
        return $this->hasMany('App\Models\BankAccountMovement');
    }

    public function checks()
    {
        return $this->hasMany('App\Models\Check');
    }

    /**
     * @param $query
     * @param $virtualCashboxId
     * @return mixed
     */
    public function scopeFromVirtualCashbox($query, $virtualCashboxId)
    {
        return $query->where('virtual_cashbox_id', $virtualCashboxId);
    }

    /**
     * @param $query
     * @param $virtualCashboxId
     * @return mixed
     */
    public function scopeAddBalanceList($query, $virtualCashboxId)
    {
        return $query
            ->select('*')
            ->addSelect(
                DB::raw(
                    '
                (SELECT vcm.balance
                FROM (
                         SELECT id,
                                SUM(
                                CASE
                                    WHEN type_movement = \'income\' THEN total_amount
                                    ELSE (-total_amount)
                                    END
                                    ) OVER (ORDER BY (date, id) ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS balance
                         FROM  virtual_cashboxes_movements
                         WHERE virtual_cashbox_id = ' . $virtualCashboxId . '
                     ) AS vcm
                WHERE virtual_cashboxes_movements.id = vcm.id)'
                )
            );
    }

    /**
     * @param $query
     * @param $virtualCashboxId
     * @return float
     */
    public function scopeBalance($query, $virtualCashboxId)
    {
        return (float)$query->where('virtual_cashbox_id', $virtualCashboxId)
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN total_amount ELSE -total_amount END)),0) AS balance'))
            ->first('balance')->balance;
    }

    /**
     * @param $query
     * @param $virtualCashboxId
     * @return float
     */
    public function scopeBalanceCashAmount($query, $virtualCashboxId)
    {
        return (float)$query->where('virtual_cashbox_id', $virtualCashboxId)
            ->select(DB::raw('COALESCE((SUM(CASE WHEN type_movement = \'income\' THEN cash_amount ELSE -cash_amount END)),0) AS cash_balance'))
            ->first()->cash_balance;
    }

    /**
     * @param int $virtualCashboxId
     * @return mixed
     */
    public static function balanceChecks(int $virtualCashboxId)
    {
        $incomes = self::where('virtual_cashboxes_movements.virtual_cashbox_id', $virtualCashboxId)
            ->where('type_movement', 'income')
            ->leftJoin('checks', 'checks.virtual_cashbox_movement_id', '=', 'virtual_cashboxes_movements.id')
            ->sum('checks.amount');

        $outflows = self::where('virtual_cashboxes_movements.virtual_cashbox_id', $virtualCashboxId)
            ->where('type_movement', 'outflow')
            ->leftJoin('checks', 'checks.virtual_cashbox_movement_id', '=', 'virtual_cashboxes_movements.id')
            ->sum('checks.amount');

        return $incomes - $outflows;
    }

    /**
     * @param int $virtualCashboxId
     * @return mixed
     */
    public static function balanceBankAccountsMovements(int $virtualCashboxId)
    {
        $incomes = self::where('virtual_cashboxes_movements.virtual_cashbox_id', $virtualCashboxId)
            ->where('virtual_cashboxes_movements.type_movement', 'income')
            ->leftJoin('bank_account_movements', 'bank_account_movements.virtual_cashbox_movement_id', '=', 'virtual_cashboxes_movements.id')
            ->sum('bank_account_movements.amount');

        $outflows = self::where('virtual_cashboxes_movements.virtual_cashbox_id', $virtualCashboxId)
            ->where('virtual_cashboxes_movements.type_movement', 'outflow')
            ->leftJoin('bank_account_movements', 'bank_account_movements.virtual_cashbox_movement_id', '=', 'virtual_cashboxes_movements.id')
            ->sum('bank_account_movements.amount');

        return $incomes - $outflows;
    }

    /**
     * Scope a query to filter bank accounts movements data.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeApplyFilters($query)
    {
        $filters = \request()->get('filters');

        if(!empty($filters['date_from'])){
            $from = Carbon::parse($filters['date_from']);
            $query->where('date', '>=' ,$from);
        }

        if(!empty($filters['date_to'])){
            $to = Carbon::parse($filters['date_to']);
            $query->where('date', '<=' ,$to);
        }

        if(isset($filters['amount_from'])){
            $query->where('total_amount', '>=',  $filters['amount_from']);
        }

        if(isset($filters['amount_to'])){
            $query->where('total_amount', '<=',  $filters['amount_to']);
        }

        if (!empty($filters['description'])) {
            $query->where('description', 'LIKE', '%' . $filters['description'] . '%');
        }

        if (!empty($filters['type_movement'])) {
            $query->where('type_movement', $filters['type_movement']);
        }

        return $query;
    }
}
