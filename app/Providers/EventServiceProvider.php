<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ProviderCreatedEvent' => [
            'App\Listeners\ProviderCreatedListener',
        ],

        'App\Events\ConsortiumCreatedEvent' => [
            'App\Listeners\ConsortiumCreatedListener',
        ],

        'App\Events\EmployeeCreatedEvent' => [
            'App\Listeners\EmployeeCreatedListener'
        ],

        'App\Events\EmployeeUpdatingEvent' => [
            'App\Listeners\EmployeeUpdatingListener'
        ],

        'App\Events\SpendingCreatedEvent' => [
            'App\Listeners\SpendingCreatedListener'
        ],

        'App\Events\CheckbookCreatedEvent' => [
            'App\Listeners\CheckbookCreatedListener'
        ],

        'App\Events\PaymentCreatedEvent' => [
            'App\Listeners\PaymentCreatedListener'
        ],

        'App\Events\CashboxMovementCreatedEvent' => [
            'App\Listeners\CashboxMovementCreatedListener'
        ],
    ];
}
