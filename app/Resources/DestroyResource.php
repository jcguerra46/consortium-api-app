<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DestroyResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'rows_deleted' => $this->resource,
        ];
    }
}
