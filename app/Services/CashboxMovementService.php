<?php


namespace App\Services;

use App\Events\CashboxIncomeCreatedEvent;
use App\Events\CashboxMovementCreatedEvent;
use App\Events\CashboxOutflowCreatedEvent;
use Illuminate\Support\Facades\Event;
use App\Models\BankAccountMovement;
use App\Models\CashboxMovement;
use App\Models\CashboxMovementFunctionalUnit;
use App\Models\Check;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Mixed_;

class CashboxMovementService
{
    /**
     * @param $movementData
     * @param $checks
     * @param $transfers
     * @param mixed $functionalUnits
     * @return mixed
     */
    public function createIncome($movementData, $checks, $transfers, $functionalUnits)
    {
        return DB::transaction(function () use ($movementData, $checks, $transfers, $functionalUnits) {

            $cashboxMovement = CashboxMovement::createIncome($movementData);

            if ($cashboxMovement->identified && !empty($functionalUnits)) {
                foreach ($functionalUnits as $fu) {
                    CashboxMovementFunctionalUnit::create([
                        'cashbox_movement_id' => $cashboxMovement->id,
                        'functional_unit_id' => $fu['functional_unit_id'],
                        'amount' => $fu['amount'],
                    ]);
                }
            }

            $amountChecks = 0;
            foreach ($checks as $id) {
                $check = Check::findOrFail($id);
                $amountChecks += $check->amount;
                $check->cashbox_movement_income_id = $cashboxMovement->id;
                $check->save();
            }

            $amountTransfers = 0;
            foreach ($transfers as $id) {
                $bankAccountMov = BankAccountMovement::findOrFail($id);
                $amountTransfers += $bankAccountMov->amount;
                $bankAccountMov->cashbox_movement_id = $cashboxMovement->id;
                $bankAccountMov->save();
            }

            $cashboxMovement->total_amount = $cashboxMovement->cash_amount + $amountTransfers + $amountChecks;
            $cashboxMovement->save();

            Event::dispatch(new CashboxMovementCreatedEvent($cashboxMovement));
            return $cashboxMovement;
        });
    }

    /**
     * @param $movementData
     * @param $checks
     * @param $transfers
     * @return mixed
     */
    public function createOutflow($movementData, $checks, $transfers)
    {
        return DB::transaction(function () use ($movementData, $checks, $transfers) {
            $cashboxMovement = CashboxMovement::createOutflow($movementData);

            $amountChecks = 0;
            foreach ($checks as $id) {
                $check = Check::findOrFail($id);
                $amountChecks += $check->amount;
                $check->cashbox_movement_outflow_id = $cashboxMovement->id;
                $check->save();
            }

            $amountTransfers = 0;
            foreach ($transfers as $id) {
                $bankAccountMov = BankAccountMovement::findOrFail($id);
                $amountTransfers += $bankAccountMov->amount;
                $bankAccountMov->cashbox_movement_id = $cashboxMovement->id;
                $bankAccountMov->save();
            }

            $cashboxMovement->total_amount = $cashboxMovement->cash_amount + $amountTransfers + $amountChecks;
            $cashboxMovement->save();

            Event::dispatch(new CashboxMovementCreatedEvent($cashboxMovement));
            return $cashboxMovement;
        });
    }

    public function createInitialBalance()
    {
    }

    public function getBalances($cashboxId)
    {
        $balance = CashboxMovement::balance($cashboxId);
        $balanceCashAmount = CashboxMovement::balanceCashAmount($cashboxId);
        $balanceChecks = CashboxMovement::balanceChecks($cashboxId);
        $balanceTransfers = CashboxMovement::balanceTransfers($cashboxId);

        return [
            'balance' => $balance,
            'balance_cash_amount' => $balanceCashAmount,
            'balance_checks' => $balanceChecks,
            'balance_transfers' => $balanceTransfers,
        ];
    }
}
