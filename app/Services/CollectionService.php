<?php


namespace App\Services;


use App\Models\Collection;
use App\Models\FunctionalUnit;
use Illuminate\Support\Facades\DB;

class CollectionService
{
    /** @var CashboxMovementService */
    private $cashboxMovementService;

    public function __construct(CashboxMovementService $cashboxMovementService)
    {
        $this->cashboxMovementService = $cashboxMovementService;
    }

    public function create(array $data, $checks, $transfers)
    {
        return DB::transaction(function () use ($data, $checks, $transfers) {

            $dataCashboxMov = $data;
            $functionalUnitId = $data['functional_unit_id'];


            $consortium = FunctionalUnit::find($functionalUnitId)->consortium;
            $period = $consortium->getOpenPeriod();
            $functionalUnitsMov = [[
                'functional_unit_id' => $functionalUnitId,
                'amount' => $data['total_amount']
            ]];
            $dataCashboxMov['cashbox_id'] = $consortium->cashbox->id;
            $dataCashboxMov['description'] = isset($dataCashboxMov['description']) ?? 'Cobro Unidad Funcional';
            $dataCashboxMov['identified'] = true;

            $cashboxMovement = $this->cashboxMovementService->createIncome($dataCashboxMov, $checks, $transfers, $functionalUnitsMov);

            return Collection::create([
                'description' => $data['description'] ?? null,
                'date' => $data['date'],
                'total_amount' => $data['total_amount'],
                'cashbox_movement_id' => $cashboxMovement->id,
                'period_id' => $period->id,
                'functional_unit_id' => $functionalUnitId,
            ]);
        });
    }

}
