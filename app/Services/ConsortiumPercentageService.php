<?php

namespace App\Services;

use App\Models\FunctionalUnit;
use App\Models\PercentageConsortium;
use App\Models\PercentageFunctionalUnit;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ConsortiumPercentageService
{
    public function create($data)
    {
        return DB::transaction(function () use ($data) {
            $percentageConsortium = PercentageConsortium::create($data);
            $equalParts = $data['equal_parts'];
            $functionalUnits = FunctionalUnit::where('consortium_id', $data['consortium_id'])->get();
            $countFunctionalUnits = $functionalUnits->count();

            $percentagesFunctionalUnits = [];
            if ($equalParts) {
                foreach ($functionalUnits as $fu) {
                    $percentagesFunctionalUnits[] = [
                        'functional_unit_id' => $fu->id,
                        'percentage_consortium_id' => $percentageConsortium->id,
                        'value' => round(100 / $countFunctionalUnits, 4),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }
            } else {
                foreach ($functionalUnits as $fu) {
                    $percentagesFunctionalUnits[] = [
                        'functional_unit_id' => $fu->id,
                        'percentage_consortium_id' => $percentageConsortium->id,
                        'value' => 0,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ];
                }
            }

            PercentageFunctionalUnit::insert($percentagesFunctionalUnits);
            return $percentageConsortium;
        });
    }


    public function duplicate($data)
    {
        return DB::transaction(function () use ($data) {
            $percentageConsortium = PercentageConsortium::findOrFail($data['id']);
            $newPercentageConsortium = $percentageConsortium->replicate()->fill([ 'name' => $data['name'] ]);
            $newPercentageConsortium->save();

            $percentagesFunctionalUnits = $percentageConsortium->percentagesFunctionalUnits->toArray();
            foreach ($percentagesFunctionalUnits as $key => $pfu) {
                unset($percentagesFunctionalUnits[$key]['id']);
                $percentagesFunctionalUnits[$key]['percentage_consortium_id'] = $newPercentageConsortium->id;
                $percentagesFunctionalUnits[$key]['created_at'] = Carbon::now()->format('Y-m-d H:m:s');
                $percentagesFunctionalUnits[$key]['updated_at'] = Carbon::now()->format('Y-m-d H:m:s');
            }
            PercentageFunctionalUnit::insert($percentagesFunctionalUnits);

            return $newPercentageConsortium;
        });
    }

    /**
     * @param int $consortiumId
     * @param array $percentages
     * @return mixed
     */
    public function massCreate(int $consortiumId, array $percentages)
    {
        return DB::transaction(function () use ($consortiumId, $percentages) {
            $percentagesInserted = [];

            foreach ($percentages as $percentage) {
                $percentage['consortium_id'] = $consortiumId;
                $percentagesInserted[] = $this->create($percentage);
            }
            return $percentagesInserted;
        });
    }

    /**
     * @param int $percentageId
     * @param array $percentageValues
     * @return mixed
     */
    public function massUpdateFunctionalUnitsValue(int $percentageId, array $percentageValues)
    {
        return DB::transaction(function () use ($percentageId, $percentageValues) {
            $functionalUnitsPercentages = [];
            foreach ($percentageValues as $percentageValue) {
                $percentageFU = PercentageFunctionalUnit::where([
                    'functional_unit_id' => $percentageValue['functional_unit_id'],
                    'percentage_consortium_id' => $percentageId
                ])->first();
                if (isset($percentageFU)) {
                    $percentageFU->value = $percentageValue['value'];
                    $percentageFU->save();
                    $functionalUnitsPercentages[] = $percentageFU;
                }
            }
            return $functionalUnitsPercentages;
        });
    }

}
