<?php

namespace App\Services\FunctionalUnitMovements;

use App\Models\FunctionalUnitMovement;
use Carbon\Carbon;

class FunctionalUnitMovementService
{

    /**
     * Prepare a newly resource to create.
     *
     * @param $data
     * @return array
     */
    public function storeService($data)
    {
        $movement = [
            'functional_unit_id' => $data['functional_unit_id'],
            'description' => $data['description'],
            'type_movement' => $data['type_movement'],
            'identified' => isset($data['identified']) ? $data['identified'] : false,
            'accumulate_to_expense' => isset($data['accumulate_to_expense']) ? $data['accumulate_to_expense'] : false,
            'date' => Carbon::now()
        ];

        $functional_unit_movement = [];

        /** Capital storage */
        if (isset($data['capital'])) {
            $movement['amount'] = $data['capital'];
            $movement['type'] = 'debt_capital';
            $functional_unit_movement[] = $this->storeFunctionalUnitMovement($movement);
        }

        /** Interest storage */
        if (isset($data['interest'])) {
            $movement['amount'] = $data['interest'];
            $movement['type'] = 'accumulated_interest';
            $functional_unit_movement[] = $this->storeFunctionalUnitMovement($movement);
        }

        return $functional_unit_movement;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $movement
     * @return mixed
     */
    public function storeFunctionalUnitMovement($movement) {
        return FunctionalUnitMovement::create($movement);
    }

}
