<?php


namespace App\Services;


use App\Models\PercentageFunctionalUnit;
use Illuminate\Support\Facades\DB;

/**
 * Class FunctionalUnitPercentageService
 * @package App\Services
 */
class FunctionalUnitPercentageService
{
    /**
     * @param array $percentages
     * @return mixed
     */
    public function massUpdate(array $percentages)
    {
        return DB::transaction(function () use ($percentages) {
            $percentagesUpdated = [];
            foreach ($percentages as $percentage) {
                $percentageFU = PercentageFunctionalUnit::findOrFail($percentage['id']);
                $percentageFU->value = $percentage['value'];
                $percentageFU->save();
                $percentagesUpdated[] = $percentageFU;
            }
            return $percentagesUpdated;
        });
    }
}
