<?php


namespace App\Services;


use App\Models\BankAccountMovement;
use App\Models\Cashbox;
use App\Models\Payment;
use App\Models\PaymentOrder;
use App\Models\SpendingInstallment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Throwable;

class PaymentOrderService
{
    /** @var CashboxMovementService */
    private $cashboxMovementService;

    /**
     * PaymentOrderService constructor.
     * @param CashboxMovementService $cashboxMovementService
     */
    public function __construct(CashboxMovementService $cashboxMovementService)
    {
        $this->cashboxMovementService = $cashboxMovementService;
    }

    /**
     * @param $paymentOrderData
     * @return mixed
     */
    public function create($paymentOrderData)
    {
        return DB::transaction(function () use ($paymentOrderData) {
            $amountTotal = 0;
            $paymentOrderData['administration_id'] = Auth::user()->administration_id;
            $paymentOrderData['date'] = Carbon::now();
            $paymentOrderData['amount'] = $amountTotal;

            /** @var PaymentOrder $paymentOrder */
            $paymentOrder = PaymentOrder::create($paymentOrderData);

            foreach ($paymentOrderData['installments'] as $spendingInstallmentData) {
                $spendingInstallment = SpendingInstallment::findOrFail($spendingInstallmentData)->first();
                $amountTotal += $spendingInstallment->amount;
                $paymentOrder->installments()->save($spendingInstallment);
            }

            $paymentOrder->update(['amount' => $amountTotal]);

            return $paymentOrder;
        });
    }

    /**
     * @param $id
     * @param $paymentOrderData
     * @return mixed
     */
    public function edit($id, $paymentOrderData)
    {
        return DB::transaction(function () use ($id, $paymentOrderData) {
            /** @var PaymentOrder $paymentOrder */
            $paymentOrder = PaymentOrder::findOrFail($id);
            $paymentOrder->fill($paymentOrderData);
            $paymentOrder->installments()->detach();
            $amountTotal = 0;

            foreach ($paymentOrderData['installments'] as $spendingInstallmentData) {
                $spendingInstallment = SpendingInstallment::findOrFail($spendingInstallmentData)->first();
                $amountTotal += $spendingInstallment->amount;
                $paymentOrder->installments()->save($spendingInstallment);
            }

            $paymentOrder->amount = $amountTotal;
            $paymentOrder->save();
            $paymentOrder->load('installments');
            return $paymentOrder;
        });
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function duplicate(int $id)
    {
        return DB::transaction(function () use ($id) {
            $paymentOrder = PaymentOrder::with('invoices')->findOrFail($id);
            $duplicatePO = $paymentOrder->replicate();
            $duplicatePO->save();

            $duplicatePO->invoices()->saveMany($paymentOrder->invoices);
            return $duplicatePO;
        });
    }

    /**
     * @param PaymentOrder $paymentOrder
     * @return array|string
     * @throws Throwable
     */
    public function generateHtml(PaymentOrder $paymentOrder)
    {
        $administration = $paymentOrder->administration;
        return view('payment-order.print', [
            'paymentOrder' => $paymentOrder,
            'administration' => $administration
        ])->render();
    }

    /**
     * @param Request $request
     * @param PaymentOrder $paymentOrder
     * @return mixed
     */
    public function doPayment(Request $request, PaymentOrder $paymentOrder)
    {
        return DB::transaction(function () use ($request, $paymentOrder) {
            $cashboxMovements = [];
            $totalAmount = 0;
            foreach ($request['cashboxes'] as $cashbox) {
                $transfers = [];
                foreach ($cashbox['transfers'] as $transfer) {
                    $transfer['date'] = $cashbox['date'];
                    $bankAccountMovement = BankAccountMovement::createForPaymentOrderPayment($transfer, $paymentOrder);
                    array_push($transfers, $bankAccountMovement->id);
                }

                $cashboxFromConsortium = Cashbox::fromConsortium($cashbox['consortium_id']);
                $cashbox['cashbox_id'] = $cashboxFromConsortium->id;
                $cashbox['description'] = 'Pago de orden de pago N°: ' . $paymentOrder->id;
                $cashbox['total_amount'] = 0;
                $cashboxOutflow = $this->cashboxMovementService->createOutflow(
                    $cashbox,
                    $cashbox['checks'],
                    $transfers
                );

                $totalAmount += $cashboxOutflow->total_amount;
                array_push($cashboxMovements, $cashboxOutflow);
            }

            $dataPayment['date'] = $request['cashboxes'][0]['date'];
            $dataPayment['total_amount'] = $totalAmount;
            $dataPayment['provider_id'] = $paymentOrder->provider_id;
            $payment = Payment::create($dataPayment);

            foreach ($cashboxMovements as $cashboxMovement) {
                $cashboxMovement->payment_id = $payment->id;
                $cashboxMovement->save();
            }

            $paymentOrder->installments()->update(['payment_id' => $payment->id]);
            $paymentOrder->update(['status' => 'paid']);

            return $paymentOrder;
        });
    }
}
