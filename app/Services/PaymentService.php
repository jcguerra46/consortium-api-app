<?php


namespace App\Services;

use App\Models\Cashbox;
use App\Models\Payment;
use App\Models\SpendingInstallment;
use Illuminate\Support\Facades\DB;

class PaymentService
{
    /** @var CashboxMovementService */
    private $cashboxMovementService;

    /**
     * PaymentService constructor.
     * @param CashboxMovementService $cashboxMovementService
     */
    public function __construct(CashboxMovementService $cashboxMovementService)
    {
        $this->cashboxMovementService = $cashboxMovementService;
    }

    /**
     * @param $dataPayment
     * @param $checks
     * @param $transfers
     * @return mixed
     */
    public function create($dataPayment, $checks, $transfers)
    {
        return DB::transaction(function () use ($dataPayment, $checks, $transfers) {

        });
    }

    /**
     * @param $dataPayment
     * @param $checks
     * @param $transfers
     * @return mixed
     */
    public function createDirect($dataPayment, $checks, $transfers)
    {
        return DB::transaction(function () use ($dataPayment, $checks, $transfers) {
            $spendingInstallment = SpendingInstallment::findOrFail($dataPayment['spending_installment_id']);

            $spending = $spendingInstallment->spending;
            $consortiumId = $spending->consortium_id;
            $cashbox = Cashbox::fromConsortium($consortiumId);

            $dataPayment['provider_id'] = $spending->provider_id;
            $payment = Payment::create($dataPayment);

            $spendingInstallment->update([
                    'payment_id' => $payment->id,
                    'payed' => true
                ]
            );

            $dataPayment['cashbox_id'] = $cashbox->id;
            $dataPayment['payment_id'] = $payment->id;
            $dataPayment['description'] = 'Pago Directo Gasto ID ' . $spending->id;

            $this->cashboxMovementService->createOutflow($dataPayment, $checks, $transfers);

            return $payment;
        });
    }
}
