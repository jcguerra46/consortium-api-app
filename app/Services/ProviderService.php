<?php


namespace App\Services;


use App\Models\Provider;
use Illuminate\Support\Facades\DB;

/**
 * Class ProviderService
 * @package App\Services
 */
class ProviderService
{
    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return DB::transaction(function () use ($data) {
            $provider = Provider::create($data);
            $services = $data['services'] ?? [];
            $provider->services()->attach($services);
            $provider->refresh();

            return $provider;
        });
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function edit(int $id, array $data)
    {
        return DB::transaction(function () use ($id, $data) {
            $provider = Provider::find($id);
            $provider->fill($data);
            $provider->save();

            $services = $data['services'] ?? [];
            $provider->services()->sync($services);
            $provider->refresh();

            return $provider;
        });
    }
}
