<?php


namespace App\Services\Salaries\Calculators;


use App\Services\Salaries\Calculators\DTOs\SalaryDTO;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;

abstract class CalculatorAbstract
{
    protected $employee;
    protected $basicSalary = 0;
    protected $jornalSalary = 0;
    protected $brutoSalary = 0;
    protected $netoSalary = 0;
    protected $noRemunerativo = 0;
    protected $rounding = 0;
    protected $workedHours = 0;
    protected $employeeAdjustments = [];
    protected $concepts = [];
    protected $employeeContributions = [];
    protected $employerContributions = [];
    protected $employeeContributionsTotal = 0;
    protected $employerContributionsTotal = 0;

    abstract function calculate(array $data): SalaryDTO;

    public function addBasicSalary(float $value)
    {
        $this->basicSalary += $value;
    }

    public function addJornalSalary(float $value)
    {
        $this->jornalSalary += $value;
    }

    public function addBrutoSalary(float $value)
    {
        $this->brutoSalary += $value;
    }

    protected function addNoRemunerativo(float $value)
    {
        $this->noRemunerativo += $value;
    }

    public function addNetoSalary(float $value)
    {
        $this->netoSalary += $value;
    }

    public function addConcept(ConceptSalary $concept)
    {
        $this->concepts[] = $concept;
    }

    public function calculateRounding()
    {
        if ($this->consortium->profile->rounding_payslips) {
            $diff = round(round($this->netoSalary) - $this->netoSalary, 2);
            $this->rounding = $diff;

            if ($diff == 0) {
                return;
            }

            if ($diff > 0) {
                $type = HaberDescuento::HABER;
            } else {
                $type = HaberDescuento::DESCUENTO;
            }

            $concept = new ConceptSalary(
                abs($diff),
                trans('payslip.concept_rounding'),
                $type
            );
            $this->addConcept($concept);
            $this->addNetoSalary($diff);
        }
    }

    public function calculateNetoSalary()
    {
        $discounts = 0;

        foreach ($this->concepts as $concept) {
            if ($concept->type == HaberDescuento::DESCUENTO) {
                if ($concept->value != 0) {
                    $discounts += $concept->value;
                }
            }
        }

        foreach ($this->employeeContributions as $employeeContribution) {
            $discounts += $employeeContribution->value;
        }

        $value = $this->brutoSalary - $discounts + $this->noRemunerativo;
        $haber = 0;
        $discounts2 = 0;


        foreach ($this->employeeAdjustments as $employeeAdjustment) {
            if (!is_null($employeeAdjustment->percentage)) {
                if ($employeeAdjustment->percentage_sobre_base == trans('payslip.total_neto')) {
                    if ($employeeAdjustment->haber_descuento == HaberDescuento::HABER) {
                        $total = $value / 100 * $employeeAdjustment->percentage;
                        if ($employeeAdjustment->suma_sueldo_jornal && $employeeAdjustment->es_remunerativo) {
                            $this->addJornalSalary($total);
                        }
                        if ($employeeAdjustment->haber_descuento == HaberDescuento::HABER && $employeeAdjustment->es_remunerativo) {
                            $this->addBrutoSalary($total);
                        } elseif ($employeeAdjustment->haber_descuento == HaberDescuento::HABER && !$employeeAdjustment->es_remunerativo) {
                            $this->addNoRemunerativo($total);
                        }
                        $haber += $total;
                    } elseif ($employeeAdjustment->haber_descuento == HaberDescuento::DESCUENTO) {
                        $discounts2 += ($value + $haber) / 100 * $employeeAdjustment->percentage;
                    }
                }
            }
        }

        $this->addNetoSalary($this->brutoSalary - $discounts + $this->noRemunerativo + $haber - $discounts2);
    }

    public function calculateContributionsTotals()
    {
        foreach ($this->employeeContributions as $employeeContribution) {
            $this->employeeContributionsTotal += $employeeContribution->value;
        }

        foreach ($this->employerContributions as $employerContribution) {
            $this->employerContributionsTotal += $employerContribution->value;
        }
    }
}
