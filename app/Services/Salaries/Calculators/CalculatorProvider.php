<?php


namespace App\Services\Salaries\Calculators;


class CalculatorProvider
{
    /** @var MonthlyCalculator */
    protected $monthlyCalculator;
    /** @var IntermediateCalculator */
    protected $intermediateCalculator;
    /** @var SacCalculator */
    protected $sacCalculator;
    /** @var VacationsCalculator */
    protected $vacationsCalculator;

    public function __construct(MonthlyCalculator $monthlyCalculator,
                                SacCalculator $sacCalculator,
                                VacationsCalculator $vacationsCalculator,
                                IntermediateCalculator $intermediateCalculator)
    {
        $this->intermediateCalculator = $intermediateCalculator;
        $this->vacationsCalculator = $vacationsCalculator;
        $this->monthlyCalculator = $monthlyCalculator;
        $this->sacCalculator = $sacCalculator;
    }

    public function getCalculator(string $type): CalculatorAbstract
    {
        $calculator = null;
        switch ($type) {
            case 'monthly':
                $calculator = $this->monthlyCalculator;
                break;
            case 'intermediate':
                $calculator = $this->intermediateCalculator;
                break;
            case 'sac':
                $calculator = $this->sacCalculator;
                break;
            case 'vacations':
                $calculator = $this->vacationsCalculator;
                break;
        }
        return $calculator;
    }
}
