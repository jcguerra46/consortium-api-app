<?php


namespace App\Services\Salaries\Calculators\DTOs;


use App\Models\Employee;
use Carbon\Carbon;

class SalaryDTO
{
    protected $type;
    protected $period;
    protected $employee;
    protected $basicSalary;
    protected $brutoSalary;
    protected $jornalSalary;
    protected $netoSalary;
    protected $artFijo;
    protected $artPercentage = 0;
    protected $workedHours;
    protected $pluses = [];
    protected $adjustments = [];
    protected $concepts = [];
    protected $contributionsEmployee = [];
    protected $contributionsEmployer = [];
    protected $contributionsEmployeeTotal;
    protected $contributionsEmployerTotal;
    protected $contributionsPeriod;
    protected $contributionsDepositDate;
    protected $contributionsBank;

    /**
     * SalaryDTO constructor.
     * @param string $type
     * @param Carbon $period
     * @param Employee $employee
     * @param float $basicSalary
     * @param float $brutoSalary
     * @param float $jornalSalary
     * @param float $netoSalary
     * @param $artFijo
     * @param $artPercentage
     * @param $workedHours
     * @param $pluses
     * @param $adjustments
     * @param $concepts
     * @param $contributionsEmployee
     * @param $contributionsEmployer
     * @param null $contributionsEmployeeTotal
     * @param null $contributionsEmployerTotal
     * @param string|null $contributionsBank
     */
    public function __construct(string $type,
                                Carbon $period,
                                Employee $employee,
                                float $basicSalary,
                                float $brutoSalary,
                                float $jornalSalary,
                                float $netoSalary,
                                $artFijo,
                                $artPercentage,
                                $workedHours,
                                $pluses,
                                $adjustments,
                                $concepts,
                                $contributionsEmployee,
                                $contributionsEmployer,
                                $contributionsEmployeeTotal = null,
                                $contributionsEmployerTotal = null,
                                Carbon $contributionsPeriod = null,
                                Carbon $contributionsDepositDate = null,
                                string $contributionsBank = null)
    {
        $this->type = $type;
        $this->period = $period;
        $this->employee = $employee;
        $this->basicSalary = $basicSalary;
        $this->brutoSalary = $brutoSalary;
        $this->jornalSalary = $jornalSalary;
        $this->netoSalary = $netoSalary;
        $this->artFijo = $artFijo;
        $this->artPercentage = $artPercentage;
        $this->workedHours = $workedHours;
        $this->pluses = $pluses;
        $this->adjustments = $adjustments;
        $this->concepts = $concepts;
        $this->contributionsEmployee = $contributionsEmployee;
        $this->contributionsEmployer = $contributionsEmployer;
        $this->contributionsEmployeeTotal = $contributionsEmployeeTotal;
        $this->contributionsEmployerTotal = $contributionsEmployerTotal;
        $this->contributionsPeriod = $contributionsPeriod;
        $this->contributionsDepositDate = $contributionsDepositDate;
        $this->contributionsBank = $contributionsBank;
    }


    public function toArray()
    {
        return [
            'type' => $this->type,
            'period' => $this->period,
            'employee' => $this->employee,
            'basicSalary' => $this->basicSalary,
            'brutoSalary' => $this->brutoSalary,
            'jornalSalary' => $this->jornalSalary,
            'netoSalary' => $this->netoSalary,
            'artFijo' => $this->artFijo,
            'artPercentage' => $this->artPercentage,
            'workedHours' => $this->workedHours,
            'pluses' => $this->pluses,
            'adjustments' => $this->adjustments,
            'concepts' => $this->concepts,
            'contributionsEmployee' => $this->contributionsEmployee,
            'contributionsEmployer' => $this->contributionsEmployer,
            'contributionsPeriod' => $this->contributionsPeriod,
            'contributionsDepositDate' => $this->contributionsDepositDate,
            'contributionsBank' => $this->contributionsBank,
        ];
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Carbon
     */
    public function getPeriod(): Carbon
    {
        return $this->period;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @return float
     */
    public function getBasicSalary(): float
    {
        return $this->basicSalary;
    }

    /**
     * @return float
     */
    public function getBrutoSalary(): float
    {
        return $this->brutoSalary;
    }

    /**
     * @return float
     */
    public function getJornalSalary(): float
    {
        return $this->jornalSalary;
    }

    /**
     * @return float
     */
    public function getNetoSalary(): float
    {
        return $this->netoSalary;
    }

    /**
     * @return mixed
     */
    public function getArtFijo()
    {
        return $this->artFijo;
    }

    /**
     * @return mixed
     */
    public function getArtPercentage()
    {
        return $this->artPercentage;
    }

    /**
     * @return mixed
     */
    public function getWorkedHours()
    {
        return $this->workedHours;
    }

    /**
     * @return array
     */
    public function getPluses(): array
    {
        return $this->pluses;
    }

    /**
     * @return array
     */
    public function getAdjustments(): array
    {
        return $this->adjustments;
    }

    /**
     * @return array
     */
    public function getConcepts(): array
    {
        return $this->concepts;
    }

    /**
     * @return array
     */
    public function getContributionsEmployee(): array
    {
        return $this->contributionsEmployee;
    }

    /**
     * @return array
     */
    public function getContributionsEmployer(): array
    {
        return $this->contributionsEmployer;
    }

    /**
     * @return null
     */
    public function getContributionsEmployeeTotal()
    {
        return $this->contributionsEmployeeTotal;
    }

    /**
     * @return null
     */
    public function getContributionsEmployerTotal()
    {
        return $this->contributionsEmployerTotal;
    }

    /**
     * @return string|null
     */
    public function getContributionsBank(): ?string
    {
        return $this->contributionsBank;
    }

    /**
     * @param string|null $contributionsBank
     */
    public function setContributionsBank(?string $contributionsBank): void
    {
        $this->contributionsBank = $contributionsBank;
    }

    /**
     * @return Carbon|null
     */
    public function getContributionsPeriod(): ?Carbon
    {
        return $this->contributionsPeriod;
    }

    /**
     * @param Carbon|null $contributionsPeriod
     */
    public function setContributionsPeriod(?Carbon $contributionsPeriod): void
    {
        $this->contributionsPeriod = $contributionsPeriod;
    }

    /**
     * @return Carbon|null
     */
    public function getContributionsDepositDate(): ?Carbon
    {
        return $this->contributionsDepositDate;
    }

    /**
     * @param Carbon|null $contributionsDepositDate
     */
    public function setContributionsDepositDate(?Carbon $contributionsDepositDate): void
    {
        $this->contributionsDepositDate = $contributionsDepositDate;
    }



}
