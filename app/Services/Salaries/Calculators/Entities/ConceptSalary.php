<?php


namespace App\Services\Salaries\Calculators\Entities;


class ConceptSalary
{
    public $value;
    public $description;
    public $type;
    public $unit;
    public $employeeAdjustmentId;

    /**
     * ConceptSalary constructor.
     * @param $value
     * @param $description
     * @param $type
     * @param $unit
     * @param null $employeeAdjustmentId
     */
    public function __construct($value, $description, $type, $unit, $employeeAdjustmentId = null)
    {
        $this->value = $value;
        $this->description = $description;
        $this->type = $type;
        $this->unit = $unit;
        $this->employeeAdjustmentId = $employeeAdjustmentId;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'value' => $this->value,
            'description' => $this->description,
            'type' => $this->type,
            'unit' => $this->unit,
            'employeeAdjustmentId' => $this->employeeAdjustmentId
        ];
    }
}
