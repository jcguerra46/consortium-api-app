<?php


namespace App\Services\Salaries\Calculators;


use App\Models\Employee;
use App\Services\Salaries\Calculators\DTOs\SalaryDTO;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\EmployeeContributionsService;
use App\Services\Salaries\Calculators\Utils\EmployerContributionsService;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use Carbon\Carbon;

class IntermediateCalculator extends CalculatorAbstract
{
    protected $type = TypeSalary::INTERMEDIATE;

    public function __construct(
        EmployeeContributionsService $employeeContributionsService,
        EmployerContributionsService $employerContributionsService)
    {
        $this->employeeContributionsService = $employeeContributionsService;
        $this->employerContributionsService = $employerContributionsService;
    }

    public function calculate(array $data): SalaryDTO
    {
        $this->employee = Employee::with('profile')->find($data['employee_id']);
        $this->consortium = $this->employee->consortium;
        $this->professionalFunction = $this->employee->professionalFunction;
        $this->employeeAdjustments = $this->employee->adjustments;
        $this->month = $data['month'];
        $this->year = $data['year'];
        $data['start_date'] = Carbon::createFromDate($data['year'], $data['month'], 1);
        $this->data = $data;

        $this->calculateIntermediateConcepts($data);

        return new SalaryDTO(
            $this->type,
            $data['start_date'],
            $this->employee,
            $this->basicSalary,
            $this->brutoSalary,
            $this->jornalSalary,
            $this->netoSalary,
            $this->artFijo,
            $this->artPercentage,
            $this->workedHours,
            null,
            $this->employeeAdjustments,
            $this->concepts,
            $this->employeeContributions,
            $this->employerContributions,
            $this->employeeContributionsTotal,
            $this->employerContributionsTotal,
            Carbon::createFromDate($data['year'], $data['month'], 1),
            Carbon::createFromFormat('d-m-Y',$data['payslip_deposit_date']),
            $data['payslip_contributions_bank']
        );
    }

    private function calculateIntermediateConcepts(array $data)
    {
        if (isset($data['extra_hours'])) {
            $this->addIntermediateConcepts($data['extra_hours']);
        }
        $this->calculateContributionsEmployee();
        $this->calculateContributionsEmployer();
        $this->calculateContributionsTotals();
        $this->calculateNetoSalary();
        $this->calculateRounding();
    }


    private function addIntermediateConcepts(array $extraHours)
    {
        foreach ($extraHours as $extraHour) {
            $concept = new ConceptSalary(
                $extraHour['value'],
                $extraHour['concept'],
                HaberDescuento::HABER,
                null
            );

            $this->addBrutoSalary($extraHour['value']);
            $this->addJornalSalary($extraHour['value']);
            $this->addConcept($concept);
        }
    }

    private function calculateContributionsEmployee()
    {
        $this->employeeContributions = $this->employeeContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->type,
            $this->month,
            $this->year
        );
    }

    private function calculateContributionsEmployer()
    {
        $dataContributions = $this->employerContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->month,
            $this->year,
            $this->type,
            $this->employeeAdjustments,
            $this->workedHours,
            true
        );

        $this->employerContributions = $dataContributions['contributions'];
        $this->artFijo = $dataContributions['artFijo'];
        $this->artPercentage = $dataContributions['artPercentage'];
    }
}
