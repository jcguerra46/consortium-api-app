<?php


namespace App\Services\Salaries\Calculators;


use App\Models\Consortium;
use App\Models\Employee;
use App\Models\EmployeeAdjustment;
use App\Services\Salaries\Calculators\DTOs\SalaryDTO;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\AdjustmentsEmployeeService;
use App\Services\Salaries\Calculators\Utils\EmployeeContributionsService;
use App\Services\Salaries\Calculators\Utils\EmployerContributionsService;
use App\Services\Salaries\Calculators\Utils\PlusesService;
use App\Services\Salaries\Calculators\Utils\ProfessionalFunctionsService;
use App\Services\Salaries\Calculators\Utils\SalaryAdditionalService;
use App\Services\Salaries\Calculators\Utils\SalarySeniorityService;
use App\Services\Salaries\Calculators\Utils\SueldoBasicoService;
use App\Services\Salaries\Calculators\Utils\VacationService;
use App\Services\Salaries\Calculators\ValueObjects\DefinitionRules;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use Carbon\Carbon;

class MonthlyCalculator extends CalculatorAbstract
{
    protected $type = TypeSalary::MONTHLY;
    protected $employee;
    protected $consortium;
    protected $professionalFunction;
    protected $workedHours;
    protected $employeeAdjustments = [];
    protected $month;
    protected $year;
    protected $usedPluses = [];
    protected $usedAdjustments = [];
    protected $extraHoursWorked = 0;
    protected $extraHoursWorkedValue;

    public function __construct(
        EmployeeContributionsService $employeeContributionsService,
        EmployerContributionsService $employerContributionsService,
        AdjustmentsEmployeeService $adjustmentsEmployeeService,
        SalarySeniorityService $salarySeniorityService,
        PlusesService $plusesService,
        SalaryAdditionalService $salaryAdditionalService,
        SueldoBasicoService $sueldoBasicoService,
        VacationService $vacationService,
        ProfessionalFunctionsService $professionalFunctionsService
    )
    {
        $this->employeeContributionsService = $employeeContributionsService;
        $this->employerContributionsService = $employerContributionsService;
        $this->plusesService = $plusesService;
        $this->salaryAdditionalService = $salaryAdditionalService;
        $this->salarySeniorityService = $salarySeniorityService;
        $this->adjustemntsEmployeeService = $adjustmentsEmployeeService;
        $this->sueldoBasicoService = $sueldoBasicoService;
        $this->vacationService = $vacationService;
        $this->professionalFunctionsService = $professionalFunctionsService;
    }

    public function calculate(array $data): SalaryDTO
    {
        $this->employee = Employee::with('profile')->find($data['employee_id']);
        $this->consortium = Consortium::with('profile')->find($this->employee->consortium_id);
        $this->professionalFunction = $this->employee->professionalFunction;
        $this->employeeAdjustments = $this->adjustemntsEmployeeService->getAdjustmentsEmployee($this->employee->id);
        $this->month = $data['month'];
        $this->year = $data['year'];
        $data['start_date'] = Carbon::createFromDate($data['year'], $data['month'], 1)->format('d-m-Y');
        $data['until_date'] = Carbon::createFromDate($data['year'], $data['month'], 1)->lastOfMonth();

        //Calculate de Monthly Salary
        $this->calculateWorkedHours($data);
        $this->calculateSueldoBasico();
        $this->calculateConcepts($data, $this->month, $this->year);

        return new SalaryDTO(
            $this->type,
            Carbon::createFromDate($data['year'], $data['month'], 1),
            $this->employee,
            $this->basicSalary,
            $this->brutoSalary,
            $this->jornalSalary,
            $this->netoSalary,
            $this->artFijo,
            $this->artPercentage,
            $this->workedHours,
            $this->usedPluses,
            $this->employeeAdjustments,
            $this->concepts,
            $this->employeeContributions,
            $this->employerContributions,
            $this->employeeContributionsTotal,
            $this->employerContributionsTotal,
            Carbon::createFromDate($data['year'], $data['month'], 1),
            Carbon::createFromFormat('d-m-Y', $data['payslip_deposit_date']),
            $data['payslip_contributions_bank']
        );
    }

    private function calculateWorkedHours(array $data)
    {
        $workedHours = isset($data['horas_trabajadas']) ? $data['horas_trabajadas'] : 0;
        $this->workedHours = $workedHours;
    }

    private function calculateSueldoBasico()
    {
        $basicSalary = $this->sueldoBasicoService->getSueldoBasico(
            $this->professionalFunction,
            $this->consortium,
            $this->workedHours,
            $this->month,
            $this->year
        );
        $this->addBasicSalary($basicSalary);
    }

    private function calculateConcepts(array $data, $month, $year)
    {
        $this->addJornalSalary($this->basicSalary);
        $this->addBrutoSalary($this->basicSalary);
        $this->addConceptBasicSalary($data['payslip_concept']);
        $this->addConceptSeniority($data['until_date'], $month, $year);
        $this->addDepartment();
        $this->addPluses();
        $this->removeApartment();
        $this->addAdjustments();
        $this->addAdjustmentsByPercentage();
        $this->addLicenseDays($data['license_days'], $data['license_days_justified']);
        if (isset($data['extra_hours'])) {
            $this->calculateExtraHours($data['extra_hours']);
        }
        if (isset($data['vacation_days'])) {
            $this->addVacations($data['vacation_days'], $data['not_vacation_days']);
        }
        $this->calculateEmployeeContributions();
        $this->calculateEmployerContributions();
        $this->calculateContributionsTotals();
        $this->calculateNetoSalary();
        $this->calculateRounding();
    }

    private function addConceptBasicSalary($basicSalaryLabel)
    {
        $unitConcept = '';
        if ($this->professionalFunction->jornalizado || $this->professionalFunction->substituto) {
            $unitConcept = $this->workedHours . ' HS';
        }

        $concept = new ConceptSalary(
            $this->basicSalary,
            $basicSalaryLabel,
            HaberDescuento::HABER,
            $unitConcept
        );
        $this->addConcept($concept);
    }

    private function addConceptSeniority(Carbon $untilDate, $month, $year)
    {
        $concept = $this->salarySeniorityService->calculateConcept(
            $this->employee->profile,
            $this->professionalFunction,
            $untilDate,
            $month,
            $year
        );

        $this->addJornalSalary($concept->value);
        $this->addBrutoSalary($concept->value);
        $this->addConcept($concept);
    }

    private function addDepartment()
    {
        if ($this->professionalFunction->has_department) {
            $additionalSalary = $this->salaryAdditionalService->getSalaryAdditional(
                DefinitionRules::HOUSING_VALUE,
                null,
                $this->month,
                $this->year
            );

            $this->addJornalSalary($additionalSalary->value);
            $this->addBrutoSalary($additionalSalary->value);
            $concept = new ConceptSalary(
                $additionalSalary->value,
                trans('payslip.apartment'),
                HaberDescuento::HABER,
                null
            );
            $this->addConcept($concept);
        }
    }

    private function addPluses()
    {
        $pluses = $this->plusesService->getPlusesEmployee(
            $this->employee,
            $this->basicSalary,
            $this->month,
            $this->year
        );

        foreach ($pluses['concepts'] as $concept) {
            $this->addJornalSalary($concept->value);
            $this->addBrutoSalary($concept->value);
        }
        $this->usedPluses = $pluses['pluses'];
    }

    private function removeApartment()
    {
        if ($this->professionalFunction->has_department) {
            $additionalSalary = $this->salaryAdditionalService->getSalaryAdditional(
                DefinitionRules::HOUSING_VALUE,
                null,
                $this->month,
                $this->year
            );

            $value = $additionalSalary->value;
            $concept = new ConceptSalary(
                $value,
                trans('payslip.apartment'),
                HaberDescuento::DESCUENTO,
                null
            );
            $this->addConcept($concept);
        }
    }

    private function addAdjustments()
    {
        foreach ($this->employeeAdjustments as $adjustment) {
            if ($adjustment->type == 'value') {
                $value = $adjustment->value;

                if ($adjustment->suma_sueldo_jornal && $adjustment->es_remunerativo) {
                    $this->addJornalSalary($value);
                }

                if ($adjustment->haber_descuento == 'haber' && $adjustment->es_remunerativo) {
                    $this->addBrutoSalary($value);
                } elseif ($adjustment->haber_descuento == 'haber' && $adjustment->es_remunerativo) {
                    $this->addNoRemunerativo($value);
                }

                $concept = new ConceptSalary(
                    $value,
                    $adjustment->description,
                    $adjustment->haber_descuento,
                    $adjustment->unit,
                    $adjustment->id
                );
                $this->addConcept($concept);
                $this->addAdjustment($adjustment);
            }
        }
    }

    private function addAdjustmentsByPercentage()
    {
        $value = 0;
        foreach ($this->employeeAdjustments as $adjustment) {
            if ($adjustment->type == 'percentage') {
                switch ($adjustment->percentage_sobre_base) {
                    case trans('salaries.jornal_salary'):
                        $value = $this->jornalSalary / 100 * $adjustment->percentage;
                        break;
                    case trans('salaries.total_bruto'):
                        $value = $this->brutoSalary / 100 * $adjustment->percentage;
                        break;
                    case trans('salaries.basic_salary'):
                        $value = $this->basicSalary / 100 * $adjustment->percentage;
                        break;
                }

                if ($adjustment->suma_sueldo_jornal && $adjustment->es_remunerativo) {
                    $this->addJornalSalary($value);
                } elseif ($adjustment->haber_descuent == 'haber' && $adjustment->es_remunerativo) {
                    $this->addNoRemunerativo($value);
                }

                $concept = new ConceptSalary(
                    $value,
                    $adjustment->description,
                    $adjustment->haber_descuento,
                    $adjustment->unit
                );
                $this->addConcept($concept);
                $this->addAdjustment($adjustment);
            }
        }
    }

    private function addAdjustment(EmployeeAdjustment $adjustment)
    {
        $this->usedAdjustments[] = $adjustment;
    }

    private function addLicenseDays($licenseDays, $licenseDaysJustified)
    {
        if ($licenseDaysJustified == 0) {
            $value = $licenseDays * ($this->jornalSalary / 30);
            $description = $licenseDaysJustified ? trans('payslip.licence_days_justified_label') : trans('payslip.licence_days_unjustified_label');

            $concept = new ConceptSalary(
                $value * -1,
                $description,
                HaberDescuento::HABER,
                $licenseDays . 'Días'
            );
            $this->addBrutoSalary($value * -1);
            $this->addConcept($concept);
        }

        if ($licenseDays == 0) {
            $value = $licenseDaysJustified * ($this->jornalSalary / 30);
            $description = trans('payslip.licence_days_justified_label');

            $concept = new ConceptSalary(
                $value,
                $description,
                HaberDescuento::HABER,
                $licenseDaysJustified . ' Días'
            );
            $this->addConcept($concept);
            $this->addBrutoSalary($value);

            $value = $licenseDaysJustified * ($this->jornalSalary / 30);
            $description = trans('payslip.licence_days_justified');

            $concept = new ConceptSalary(
                $value,
                $description,
                HaberDescuento::HABER,
                $licenseDaysJustified . ' Días'
            );
            $this->addConcept($concept);
            $this->addBrutoSalary($value * -1);
        }

        if ($licenseDaysJustified != 0 && $licenseDays != 0) {
            $value = $licenseDays * ($this->jornalSalary / 30);
            $description = trans('payslip.licence_days_unjustified_label');
            $concept = new ConceptSalary(
                $value * -1,
                $description,
                HaberDescuento::HABER,
                $licenseDays . ' Días'
            );
            $this->addConcept($concept);
            $this->addBrutoSalary($value * -1);

            $value = $licenseDaysJustified * ($this->jornalSalary / 30);
            $description = trans('payslip.licence_days_justified_label');
            $concept = new ConceptSalary(
                $value,
                $description,
                HaberDescuento::HABER,
                $licenseDays . ' Días'
            );
            $this->addConcept($concept);
            $this->addBrutoSalary($value);

            $description = trans('payslip.licence_days_justified');
            $concept = new ConceptSalary(
                $value * -1,
                $description,
                HaberDescuento::HABER,
                $licenseDays . ' Días'
            );
            $this->addConcept($concept);
            $this->addBrutoSalary($value * -1);
        }
    }

    private function calculateExtraHours(array $extraHours)
    {
        foreach ($extraHours as $extraHour) {
            if ($extraHour['percentage'] > 0) {
                // Jornalizado
                if ($this->professionalFunction->jornalizado) {
                    $basicSalary = $this->professionalFunctionsService->getSalarioBasico(
                        $this->professionalFunction->id,
                        null,
                        $this->month,
                        $this->year
                    );
                    $valueHour = $basicSalary;
                } // Substituto
                elseif ($this->professionalFunction->substituto) {
                    $basicSalary = $this->professionalFunctionsService->getSalarioBasico(
                        $this->professionalFunction->id,
                        null,
                        $this->month,
                        $this->year
                    );
                    $valueHour = $basicSalary / 8;
                } // Normal
                else {
                    $hours = 8;

                    if ($this->professionalFunction->part_time) {
                        $hours = 4;
                    }
                    if ($this->professionalFunction->id == 15) {
                        $hours = 7;
                    }

                    $valueHour = $this->jornalSalary / 25 / $hours;
                }

                $value = round((($valueHour * ($extraHour['percentage'] + 100)) / 100) * $extraHour['quantity'],
                    2);
                $concept = new ConceptSalary(
                    $value,
                    $extraHour['description'],
                    HaberDescuento::HABER,
                    $extraHour['quantity'] . ' hs.'
                );
                $this->addConcept($concept);
                $this->addBrutoSalary($value);
                $this->addExtraHoursWorked($extraHour['quantity']);
                $this->addExtraHoursWorkedValue($value);
            }
        }
    }

    private function addExtraHoursWorked($extraHours)
    {
        $this->extraHoursWorked += $extraHours;
    }

    private function addExtraHoursWorkedValue($extraHoursValue)
    {
        $this->extraHoursWorkedValue += $extraHoursValue;
    }

    private function addVacations($vacationDays, $notVacationDays)
    {
        if (!is_null($vacationDays)) {
            //sueldo jornal + promedio de las horas extras de los 6 meses liquidados
            $init = Carbon::create($this->year, $this->month, 1);

            $extraHoursSixMonths = $this->vacationService->sumExtraHoursVacations($this->employee, $init);
            $hours = ($extraHoursSixMonths + $this->extraHoursWorkedValue) / 6;
            $valueUnitVacations = ($hours + $this->jornalSalary) / 25;

            $valueVacations = $vacationDays * $valueUnitVacations;
            $description = trans('employee.days_vacations');
            $conceptVac = new ConceptSalary(
                $valueVacations,
                $description,
                HaberDescuento::HABER,
                $vacationDays . ' Días.'
            );
            $this->addConcept($conceptVac);

            $valueNotVacations = $notVacationDays * ($this->jornalSalary / 30);
            $descriptionNoVac = trans('employee.days_no_worked_vacations');
            $conceptNoVac = new ConceptSalary(
                $vacationDays * -1,
                $descriptionNoVac,
                HaberDescuento::HABER,
                $notVacationDays . ' Días'
            );
            $this->addConcept($conceptNoVac);

            $newValue = $valueVacations - $valueNotVacations;
            $this->addBrutoSalary($newValue);
        }
    }

    private function calculateEmployeeContributions()
    {
        $this->employeeContributions = $this->employeeContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->type,
            $this->month,
            $this->year
        );
    }

    private function calculateEmployerContributions($addART = true)
    {
        $dataContributions = $this->employerContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->month,
            $this->year,
            $this->type,
            $this->employeeAdjustments,
            $this->workedHours,
            $addART
        );

        $this->employerContributions = $dataContributions['contributions'];
        $this->artFijo = $dataContributions['artFijo'];
        $this->artPercentage = $dataContributions['artPercentage'];
    }


}
