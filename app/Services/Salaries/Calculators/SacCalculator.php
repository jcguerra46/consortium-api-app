<?php


namespace App\Services\Salaries\Calculators;


use App\Models\Employee;
use App\Services\Salaries\Calculators\DTOs\SalaryDTO;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\EmployeeContributionsService;
use App\Services\Salaries\Calculators\Utils\EmployerContributionsService;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use Carbon\Carbon;

class SacCalculator extends CalculatorAbstract
{
    protected $type = TypeSalary::SAC;

    public function __construct(
        EmployeeContributionsService $employeeContributionsService,
        EmployerContributionsService $employerContributionsService)
    {
        $this->employeeContributionsService = $employeeContributionsService;
        $this->employerContributionsService = $employerContributionsService;
    }

    public function calculate(array $data): SalaryDTO
    {
        $this->employee = Employee::with('profile')->find($data['employee_id']);
        $this->consortium = $this->employee->consortium;
        $this->professionalFunction = $this->employee->professionalFunction;
        $this->employeeAdjustments = $this->employee->adjustments;
        $this->month = $data['month'];
        $this->year = $data['year'];
        $data['start_date'] = Carbon::createFromDate($data['year'], $data['month'], 1);
        $data['end_date'] = Carbon::createFromDate($data['year'], $data['month'], 1)->lastOfMonth();
        $this->data = $data;

        $this->calculateSacConcepts();

        return new SalaryDTO(
            $this->type,
            $data['start_date'],
            $this->employee,
            $this->basicSalary,
            $this->brutoSalary,
            $this->jornalSalary,
            $this->netoSalary,
            $this->artFijo,
            $this->artPercentage,
            $this->workedHours,
            null,
            $this->employeeAdjustments,
            $this->concepts,
            $this->employeeContributions,
            $this->employerContributions,
            $this->employeeContributionsTotal,
            $this->employerContributionsTotal,
            Carbon::createFromDate($data['year'], $data['month'], 1),
            Carbon::createFromFormat('d-m-Y',$data['payslip_deposit_date']),
            $data['payslip_contributions_bank']
        );
    }

    private function calculateSacConcepts()
    {
        $value = $this->addConceptsSac(
            ($this->data['best_bruto_salary'] - ($this->getValue27430($this->type)) / 2),
            $this->employee,
            $this->data['end_date']
        );
        $this->addBrutoSalary($value);
        $this->addJornalSalary($value);
        $this->calculateContributionsEmployee();
        $this->calculateContributionsEmployer();
        $this->calculateContributionsTotals();
        $this->calculateNetoSalary();
        $this->calculateRounding();
    }

    private function getValue27430(string $type): float
    {
        $value = 0;
        if ($this->employee->profile->law27430) {
            if ($this->funcionProfesionalEmpleado['jornalizado'] || $this->funcionProfesionalEmpleado['substituto']) {
                $value = 0;
            } elseif ($this->funcionProfesionalEmpleado['part_time']) {
                $value = 3501.84;
            } else {
                $value = 7003.68;
            }
        }
        if ($type === TypeSalary::SAC) {
            $value = 0;
        }
        return (float)$value;
    }

    private function addConceptsSac(float $bestBrutoSalary, Employee $employee, Carbon $endDate)
    {
        $unit = '';
        $value = $this->getSacValue($bestBrutoSalary, $employee, $endDate);

        $concept = new ConceptSalary(
            $value,
            'Sueldo Anual Complementario',
            HaberDescuento::HABER,
            $unit
        );
        $this->addConcept($concept);
        return $value;
    }

    private function getSacValue(float $bestBrutoSalary, Employee $employee, Carbon $endDate)
    {
        $periodSac = $this->getSacPeriod($endDate);

        $employeeInitDate = Carbon::parse($employee->profile->entry_date);
        if ($employeeInitDate->between($periodSac['init_semestre'], $periodSac['end_semestre'])) {
            $days = (30 - $employeeInitDate->day + 1) + ($periodSac['end_semestre']->diffInMonths($employee->profile->entry_date) * 30);
            $value = $bestBrutoSalary / 360 * $days;
        } else {
            $value = $bestBrutoSalary / 2;
        }
        return $value;
    }

    private function getSacPeriod(Carbon $endDate)
    {
        $endSemestre = $endDate->startOfDay();

        if ($endSemestre->month != 6 && $endSemestre->month != 12) {
            if ($endSemestre->month < 6) {
                $endSemestre = Carbon::create($endSemestre->year, 6, 30);
            } else {
                $endSemestre = Carbon::create($endSemestre->year, 12, 31);
            }
        } else {
            $endSemestre = $endSemestre->endOfMonth();
        }

        $initSemestre = clone $endSemestre;
        $initSemestre = $initSemestre->subMonths(5)->firstOfMonth();

        return [
            'init_semestre' => $initSemestre,
            'end_semestre' => $endSemestre
        ];
    }

    private function calculateContributionsEmployee()
    {
        $this->employeeContributions = $this->employeeContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->type,
            $this->month,
            $this->year
        );
    }

    private function calculateContributionsEmployer()
    {
        $dataContributions = $this->employerContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->month,
            $this->year,
            $this->type,
            $this->employeeAdjustments,
            $this->workedHours,
            true
        );

        $this->employerContributions = $dataContributions['contributions'];
        $this->artFijo = $dataContributions['artFijo'];
        $this->artPercentage = $dataContributions['artPercentage'];
    }

}
