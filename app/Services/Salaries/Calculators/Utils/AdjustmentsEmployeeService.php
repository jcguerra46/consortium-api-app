<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\EmployeeAdjustment;

class AdjustmentsEmployeeService
{
    public function getAdjustmentsEmployee($employeeId)
    {
        return EmployeeAdjustment::where('employee_id', $employeeId)
            ->where(function ($query) {
                $query->where([
                    ['active', true],
                    ['recurrent', true]
                ])
                    ->orWhere([
                        ['recurrent', false],
                        ['used', false]
                    ]);
            })
            ->get();
    }


}
