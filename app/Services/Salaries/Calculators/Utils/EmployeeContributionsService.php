<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\Consortium;
use App\Models\Employee;
use App\Models\EmployeeProfile;
use App\Models\ProfessionalFunction;
use App\Services\Salaries\Calculators\ValueObjects\ContributionConcept;
use App\Services\Salaries\Calculators\ValueObjects\DefinitionRules;
use App\Services\Salaries\Calculators\ValueObjects\PayslipContributionConcept;
use App\Services\Salaries\Calculators\ValueObjects\TipoBoleta;
use App\Services\Salaries\Calculators\ValueObjects\TypeContribution;
use App\Utils\MathHelper;

class EmployeeContributionsService
{
    protected $employee;
    protected $professionalFunction;
    protected $consortium;
    protected $month;
    protected $year;
    protected $typeSalary;
    protected $brutoSalary;
    protected $conceptsContributions = [];

    public function __construct(ObraSocialService $obraSocialService)
    {
        $this->obraSocialService = $obraSocialService;
    }

    public function calculate($brutoSalary,
                              Employee $employee,
                              ProfessionalFunction $professionalFunction,
                              Consortium $consortium,
                              $typeSalary,
                              $month,
                              $year
    )
    {
        if (!$employee->profile->retired) {

            $this->employee = $employee;
            $this->professionalFunction = $professionalFunction;
            $this->consortium = $consortium;
            $this->brutoSalary = $brutoSalary;
            $this->typeSalary = $typeSalary;
            $this->month = $month;
            $this->year = $year;

            $this->calculateJubilacion($this->brutoSalary);
            $this->calculateLey19032($this->brutoSalary, $this->employee->profile);
            $this->calculateObraSocial();
            $this->calculateAdicionalObraSocial();
            $this->calculateCPF($this->brutoSalary, $employee);
            $this->calculateFateryh($this->brutoSalary, $employee);
            $this->calculateCuotaSindical($this->brutoSalary, $employee);

            return $this->conceptsContributions;
        }
    }

    private function calculateJubilacion(float $brutoSalary)
    {
        $value = MathHelper::calculateValuePercentage($brutoSalary, DefinitionRules::CONTRIBUTION_JUBILATORIO);
        $concept = new PayslipContributionConcept(
            $value,
            ContributionConcept::APORTE_JUBILATORIO,
            'Jubilación',
            TypeContribution::EMPLOYEE,
            TipoBoleta::AFIP
        );
        $this->addConceptContribution($concept);
    }

    private function addConceptContribution(PayslipContributionConcept $concept)
    {
        $this->conceptsContributions[] = $concept;
    }

    private function calculateLey19032(float $brutoSalary, EmployeeProfile $profileEmployee)
    {
        if (!$profileEmployee->retired) {
            $value = MathHelper::calculateValuePercentage($brutoSalary, DefinitionRules::CONTRIBUTION_LEY_19032);
            $concept = new PayslipContributionConcept(
                $value,
                ContributionConcept::LEY_19032,
                'Ley 19032',
                TypeContribution::EMPLOYEE,
                TipoBoleta::AFIP
            );
            $this->addConceptContribution($concept);
        }
    }

    private function calculateObraSocial()
    {
        $conceptOS = $this->obraSocialService->calculateEmployee(
            $this->employee->profile,
            $this->professionalFunction,
            $this->consortium,
            $this->typeSalary,
            $this->month,
            $this->year,
            $this->brutoSalary
        );

        if ($conceptOS) {
            $this->addConceptContribution($conceptOS);
        }
    }

    private function calculateAdicionalObraSocial()
    {
        $concept = $this->obraSocialService->calculateEmployeeAdicional(
            $this->employee,
            $this->typeSalary
        );

        if ($concept) {
            $this->addConceptContribution($concept);
        }
    }

    private function calculateCPF(float $brutoSalary, Employee $employee)
    {
        if ($employee->profile->descuento_c_p_f) {
            $value = MathHelper::calculateValuePercentage($brutoSalary, DefinitionRules::CONTRIBUTION_ADICIONAL_CPF);

            $concept = new PayslipContributionConcept(
                $value,
                ContributionConcept::CPF,
                ContributionConcept::CPF,
                TypeContribution::EMPLOYEE,
                TipoBoleta::SUTERH
            );
            $this->addConceptContribution($concept);
        }
    }

    private function calculateFateryh(float $brutoSalary, Employee $employee)
    {
        $contribution = $employee->profile->afiliado_f_m_v_d_d ? DefinitionRules::CONTRIBUTION_ADICIONAL_FATERYH : DefinitionRules::CONTRIBUTION_ADICIONAL_FATERYH_ALTERNATE;
        $value = MathHelper::calculateValuePercentage($brutoSalary, $contribution);

        $payslipContribution = new PayslipContributionConcept(
            $value,
            ContributionConcept::FATERYH,
            ContributionConcept::FATERYH,
            TypeContribution::EMPLOYEE,
            TipoBoleta::SUTERH
        );
        $this->addConceptContribution($payslipContribution);
    }

    private function calculateCuotaSindical(float $brutoSalary, Employee $employee)
    {
        if ($employee->profile->afiliado_sindicato) {
            $value = MathHelper::calculateValuePercentage($brutoSalary, DefinitionRules::CONTRIBUTION_ADICIONAL_CUOTA_SINDICAL);

            $contribution = new PayslipContributionConcept(
                $value,
                ContributionConcept::CUOTA_SINDICAL_SUTERH,
                ContributionConcept::CUOTA_SINDICAL_SUTERH,
                TypeContribution::EMPLOYEE,
                TipoBoleta::SUTERH
            );

            $this->addConceptContribution($contribution);
        }
    }
}
