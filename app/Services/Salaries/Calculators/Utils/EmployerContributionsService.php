<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\Consortium;
use App\Models\Employee;
use App\Models\ProfessionalFunction;
use App\Services\Salaries\Calculators\ValueObjects\ContributionConcept;
use App\Services\Salaries\Calculators\ValueObjects\DefinitionRules;
use App\Services\Salaries\Calculators\ValueObjects\PayslipContributionConcept;
use App\Services\Salaries\Calculators\ValueObjects\TipoBoleta;
use App\Services\Salaries\Calculators\ValueObjects\TypeContribution;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use App\Utils\MathHelper;

class EmployerContributionsService
{
    /**  @var array */
    public $conceptsContributions = [];

    public function __construct(ObraSocialService $obraSocialService)
    {
        $this->obraSocialService = $obraSocialService;
    }

    public function calculate(
        float $brutoSalary,
        Employee $employee,
        ProfessionalFunction $professionalFunction,
        Consortium $consortium,
        $month,
        $year,
        string $typeSalary,
        $adjustments,
        $workedHours,
        $addART = false
    )
    {
        $this->brutoSalary = $brutoSalary;
        $this->employee = $employee;
        $this->professionalFunction = $professionalFunction;
        $this->consortium = $consortium;
        $this->month = $month;
        $this->year = $year;
        $this->typeSalary = $typeSalary;
        $this->adjustments = $adjustments;
        $this->workedHours = $workedHours;

        $this->calculateEmployerJubilacion();
        $this->calculateEmployer19032();
        $this->calculateEmployerCajaSubsidio();
        $this->calculateEmployerObraSocial();

        if ($addART) {
            $this->calculateEmployerARTFijo();
        }

        if ($this->typeSalary == TypeSalary::MONTHLY) {
            $this->calculateEmployerSeguroVida();
        }

        $this->calculateEmployerATRPercentage();
        $this->calculateEmployerCajaProteccionFamilia();
        $this->calculateEmployerFATERYH();

        if ($this->typeSalary == TypeSalary::MONTHLY) {
            $this->calculateEmployerContribucionSolidaria();
        }

        $this->calculateEmployerSERACARH();

        return [
            'contributions' => $this->conceptsContributions,
            'artFijo' => $this->artFijo,
            'artPercentage' => $this->artPercentage,
        ];
    }

    private function calculateEmployerJubilacion()
    {
        $value = MathHelper::calculateValuePercentage($this->brutoSalary - $this->getValue27430(), DefinitionRules::CONTRIBUTION_JUBILATORIO_EMPLOYER);
        $contribution = new PayslipContributionConcept(
            $value,
            ContributionConcept::APORTE_JUBILATORIO,
            ContributionConcept::APORTE_JUBILATORIO,
            TypeContribution::EMPLOYER,
            TipoBoleta::AFIP
        );
        $this->addConceptContribution($contribution);
    }

    private function getValue27430($type = null): float
    {
        $value = 0;
        if ($this->employee->profile->law27430) {
            if ($this->professionalFunction->jornalizado || $this->professionalFunction->surrogate) {
                $value = 0;
            } elseif ($this->professionalFunction->part_time) {
                $value = 3501.84;
            } else {
                $value = 7003.68;
            }
        }

        if ($type = TypeSalary::SAC) {
            $value = 0;
        }
        return (float)$value;
    }

    private function calculateEmployer19032()
    {
        if (!$this->employee->profile->retired) {
            $value = MathHelper::calculateValuePercentage($this->brutoSalary - $this->getValue27430(), DefinitionRules::CONTRIBUTION_LEY_19032_EMPLOYER);
            $concept = new PayslipContributionConcept(
                $value,
                ContributionConcept::LEY_19032,
                ContributionConcept::LEY_19032,
                TypeContribution::EMPLOYER,
                TipoBoleta::AFIP
            );
            $this->addConceptContribution($concept);
        }
    }

    private function calculateEmployerCajaSubsidio()
    {
        if ($this->employee->profile->retired) {
            $value = MathHelper::calculateValuePercentage($this->brutoSalary - $this->getValue27430(), DefinitionRules::CONTRIBUTION_CAJA_SUBSIDIOS_EMPLOYER);
            $concept = new PayslipContributionConcept(
                $value,
                ContributionConcept::CAJA_SUBSIDIOS_FAMILIARES,
                ContributionConcept::CAJA_SUBSIDIOS_FAMILIARES,
                TypeContribution::EMPLOYER,
                TipoBoleta::AFIP
            );
            $this->addConceptContribution($concept);
        }
    }

    private function calculateEmployerObraSocial()
    {
        $contribution = $this->obraSocialService->calculateEmployerObraSocial(
            $this->employee->profile,
            $this->consortium,
            $this->typeSalary,
            $this->professionalFunction,
            $this->brutoSalary,
            $this->month,
            $this->year
        );

        if ($contribution) {
            $this->addConceptContribution($contribution);
        }
    }

    private function calculateEmployerARTFijo()
    {
        $value = $this->consortium->profile->art_amount ?: 0;

        $concept = new PayslipContributionConcept(
            $value,
            ContributionConcept::ART_FIJO,
            ContributionConcept::ART_FIJO,
            TypeContribution::EMPLOYER,
            TipoBoleta::AFIP
        );

        $this->artFijo = $value;
        $this->addConceptContribution($concept);
    }

    private function addConceptContribution(PayslipContributionConcept $concept)
    {
        $this->conceptsContributions[] = $concept;
    }

    private function calculateEmployerSeguroVida()
    {
        $value = DefinitionRules::CONTRIBUTION_SEGURO_VIDA_EMPLOYER;
        $contribution = new PayslipContributionConcept(
            $value,
            ContributionConcept::SEGURO_VIDA,
            ContributionConcept::SEGURO_VIDA,
            TypeContribution::EMPLOYER,
            TipoBoleta::AFIP
        );
        $this->addConceptContribution($contribution);
    }

    private function calculateEmployerATRPercentage()
    {
        $montoNoRemunerativo = 0;

        foreach ($this->adjustments as $adjustment) {
            if (!$adjustment->es_remunerativo) {
                if ($adjustment->percentage) {
                    $montoNoRemunerativo += MathHelper::calculateValuePercentage($this->brutoSalary, $adjustment->percentage);
                } else {
                    $montoNoRemunerativo += $adjustment->value;
                }
            }
        }

        $value = MathHelper::calculateValuePercentage($this->brutoSalary + $montoNoRemunerativo, $this->consortium->profile->art_percentage);

        $contribution = new PayslipContributionConcept(
            $value,
            ContributionConcept::ART_PORCENTAJE,
            ContributionConcept::ART_PORCENTAJE,
            TypeContribution::EMPLOYER,
            TipoBoleta::AFIP
        );

        $this->artPercentage = $this->consortium->profile->art_percentage;
        $this->addConceptContribution($contribution);
    }

    private function calculateEmployerCajaProteccionFamilia()
    {
        $value = MathHelper::calculateValuePercentage($this->brutoSalary, DefinitionRules::CONTRIBUTION_CAJA_PROTECCION_FAMILIA_EMPLOYER);

        $contribution = new PayslipContributionConcept(
            $value,
            ContributionConcept::CAJA_PROTECCION_FAMILIA,
            ContributionConcept::CAJA_PROTECCION_FAMILIA,
            TypeContribution::EMPLOYER,
            TipoBoleta::SUTERH
        );

        $this->addConceptContribution($contribution);
    }

    private function calculateEmployerFATERYH()
    {
        $value = MathHelper::calculateValuePercentage($this->brutoSalary, DefinitionRules::CONTRIBUTION_FATERYH_EMPLOYER);

        $contribution = new PayslipContributionConcept(
            $value,
            ContributionConcept::FATERYH,
            ContributionConcept::FATERYH,
            TypeContribution::EMPLOYER,
            TipoBoleta::FATERYH
        );

        $this->addConceptContribution($contribution);
    }

    private function calculateEmployerContribucionSolidaria()
    {
        if ($this->professionalFunction->jornalizado || $this->professionalFunction->surrogate) {
            $value = DefinitionRules::CONTRIBUTION_CONTRIBUCION_SOLIDARIA_BY_HOUR * $this->workedHours;
            if ($this->month == 12 || $this->month == 6) {
                $valorAgregado = $value / 2;
                $value += $valorAgregado;
            }
        } else {
            if ($this->professionalFunction->part_time) {
                $value = DefinitionRules::CONTRIBUTION_CONTRIBUCION_SOLIDARIA_PART_TIME;
                if ($this->month == 12 || $this->month == 6) {
                    $valorAgregado = $value / 2;
                    $value += $valorAgregado;
                }
            } else {
                $value = DefinitionRules::CONTRIBUTION_CONTRIBUCION_SOLIDARIA_FULL_TIME;
                if ($this->month == 12 || $this->month == 6) {
                    $valorAgregado = $value / 2;
                    $value += $valorAgregado;
                }
            }
        }

        $contribution = new PayslipContributionConcept(
            $value,
            ContributionConcept::CONTRIBUCION_SOLIDARIA,
            ContributionConcept::CONTRIBUCION_SOLIDARIA,
            TypeContribution::EMPLOYER,
            TipoBoleta::FATERYH
        );

        $this->addConceptContribution($contribution);
    }

    private function calculateEmployerSERACARH()
    {
        $value = MathHelper::calculateValuePercentage($this->brutoSalary, DefinitionRules::CONTRIBUTION_SERACARH_EMPLOYER);

        $concept = new PayslipContributionConcept(
            $value,
            ContributionConcept::SERACARH,
            ContributionConcept::SERACARH,
            TypeContribution::EMPLOYER,
            TipoBoleta::SERACARH
        );
        $this->addConceptContribution($concept);
    }
}
