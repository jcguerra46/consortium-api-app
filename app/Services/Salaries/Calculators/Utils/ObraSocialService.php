<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\Consortium;
use App\Models\Employee;
use App\Models\EmployeeProfile;
use App\Models\Salary;
use App\Models\ProfessionalFunction;
use App\Services\Salaries\Calculators\Entities\ConceptContribution;
use App\Services\Salaries\Calculators\ValueObjects\ContributionConcept;
use App\Services\Salaries\Calculators\ValueObjects\DefinitionRules;
use App\Services\Salaries\Calculators\ValueObjects\ObraSocialDiffLogType;
use App\Services\Salaries\Calculators\ValueObjects\PayslipContributionConcept;
use App\Services\Salaries\Calculators\ValueObjects\TipoBoleta;
use App\Services\Salaries\Calculators\ValueObjects\TypeContribution;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use App\Utils\MathHelper;
use Carbon\Carbon;

class ObraSocialService
{
    protected $employeeProfile;
    protected $professionalFunction;
    protected $consortium;
    protected $brutoSalary;
    protected $month;
    protected $year;

    public function __construct(
        ProfessionalFunctionsService $professionalFunctionsService
    )
    {
        $this->professionalFunctionService = $professionalFunctionsService;
    }

    public function calculateEmployee(
        EmployeeProfile $employeeProfile,
        ProfessionalFunction $professionalFunction,
        Consortium $consortium,
        $type,
        $month,
        $year,
        $brutoSalary
    ): PayslipContributionConcept
    {
        if (!$employeeProfile->retired) {
            $this->employeeProfile = $employeeProfile;
            $this->professionalFunction = $professionalFunction;
            $this->consortium = $consortium;
            $this->brutoSalary = $brutoSalary;

            $percentage = DefinitionRules::CONTRIBUTION_OBRA_SOCIAL;
            $diffType = ObraSocialDiffLogType::EMPLOYEE_OBRA_SOCIAL;
            $obraSocialValue = $this->getValueObraSocial($type, $percentage, $diffType);

            $conceptContribution = ContributionConcept::OBRA_SOCIAL;

            if ($this->mustCalculateMinObraSocial()) {
                $conceptContribution = ContributionConcept::OBRA_SOCIAL_2;
            }

            $contribution = new PayslipContributionConcept(
                $obraSocialValue,
                $conceptContribution,
                $conceptContribution,
                TypeContribution::EMPLOYEE,
                TipoBoleta::AFIP
            );

            return $contribution;
        }
    }

    public function getValueObraSocial($type, $percentage, $diffType): float
    {
        switch ($type) {
            case TypeSalary::MONTHLY:
                return $this->getMonthlyValueObraSocial($percentage, $diffType);
                break;

            // para liq parciales siempre es el sueldo bruto
            case TypeSalary::SAC:
            case TypeSalary::VACATIONS:
            case TypeSalary::INTERMEDIATE:
            default:
                return $this->getObraSocialPartialsPeriods($diffType);
                break;
        }
    }

    private function getMonthlyValueObraSocial($percentage, $diffType): float
    {
        if ($this->mustCalculateMinObraSocial()) {
            if ($this->hasPartialsLiquidations()) {

            } else {

            }
        } else {
            return MathHelper::calculateValuePercentage($this->brutoSalary, $percentage);
        }

    }

    private function mustCalculateMinObraSocial(): bool
    {
        return (
            $this->employeeProfile->calculate_obra_social_by_law
            &&
            ($this->professionalFunction->part_time || $this->professionalFunction->surrogate || $this->professionalFunction->jornalizado)
            &&
            ($this->getMinBrutoSalaryObraSocial() > $this->brutoSalary)
        );
    }

    private function hasPartialsLiquidations(): bool
    {
        return Salary::where('employee_id', $this->employeeProfile->employee_id)
            ->where('type', TypeSalary::MONTHLY)
            ->get();
    }

    private function getMinBrutoSalaryObraSocial(): float
    {
        return $this->professionalFunctionService->getSalarioBasico(
            2,
            $this->consortium->category,
            $this->month,
            $this->year
        );
    }

    public function calculateEmployeeAdicional(Employee $employee, $type)
    {
        if (!$employee->profile->retired && $employee->profile->adicional_obra_social) {

            $percentage = DefinitionRules::CONTRIBUTION_ADICIONAL_OBRA_SOCIAL;
            $diffType = ObraSocialDiffLogType::EMPLOYEE_ADC_OBRA_SOCIAL;
            $obraSocialValue = $this->getValueObraSocial($type, $percentage, $diffType);

            $conceptContribution = ContributionConcept::ADICIONAL_OBRA_SOCIAL;

            if ($this->mustCalculateMinObraSocial()) {
                $conceptContribution = ContributionConcept::ADICIONAL_OBRA_SOCIAL_2;
            }

            return new PayslipContributionConcept(
                $obraSocialValue,
                $conceptContribution,
                $conceptContribution,
                TypeContribution::EMPLOYEE,
                TipoBoleta::AFIP
            );
        }
    }

    public function calculateEmployerObraSocial(
        EmployeeProfile $employeeProfile,
        Consortium $consortium,
        string $type,
        ProfessionalFunction $professionalFunction,
        float $brutoSalary,
        $month,
        $year
    ): PayslipContributionConcept
    {
        $this->employeeProfile = $employeeProfile;
        $this->consortium = $consortium;
        $this->type = $type;
        $this->professionalFunction = $professionalFunction;
        $this->brutoSalary = $brutoSalary;
        $this->month = $month;
        $this->year = $year;

        if (!$this->employeeProfile->retired) {
            $percentage = DefinitionRules::CONTRIBUTION_OBRA_SOCIAL_EMPLOYER;
            $diffType = ObraSocialDiffLogType::EMPLOYER_OBRA_SOCIAL;
            $obraSocialValue = $this->getValueObraSocial($this->type, $percentage, $diffType);
            $conceptContribution = ContributionConcept::OBRA_SOCIAL;

            if ($this->mustCalculateMinObraSocial()) {
                $conceptContribution = ContributionConcept::OBRA_SOCIAL_2;
            }

            return new PayslipContributionConcept(
                $obraSocialValue,
                $conceptContribution,
                $conceptContribution,
                TypeContribution::EMPLOYER,
                TipoBoleta::AFIP
            );
        }
    }

    public function getObraSocialPartialsPeriods($diffType)
    {
        $value = 0;
        foreach ($this->getPartialsLiquidations() as $liquidation) {
            $value += 321;
        }
        return $value;
    }

    private function getPartialsLiquidations()
    {
        $datePeriod = Carbon::create($this->year, $this->month, 1);
        $liquidations = Salary::where('employee_id', $this->employeeProfile->employee_id)
            ->where('period', '>=', $datePeriod->format('m-d-Y'))
            ->where('type', '!=', TypeSalary::MONTHLY)
            ->orderBy('period', 'DESC')
            ->get();
        return $liquidations;
    }
}
