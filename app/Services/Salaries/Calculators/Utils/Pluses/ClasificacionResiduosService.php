<?php


namespace App\Services\Salaries\Calculators\Utils\Pluses;


use App\Models\Employee;
use App\Models\FunctionalUnit;
use App\Models\SalaryPlus;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\SalaryAdditionalService;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;

class ClasificacionResiduosService implements IPlus
{
    public function __construct()
    {
        $this->salaryAdditionalsService = new SalaryAdditionalService();
    }

    public function calculate(Employee $employee, $month, $year, SalaryPlus $plus = null, $basicSalary = null): ConceptSalary
    {
        $additionalSalary = $this->salaryAdditionalsService->getSalaryAdditional(
            $plus->internal_class,
            $plus,
            $month,
            $year
        );

        $functionalUnitsCount = FunctionalUnit::where('consortium_id', $employee->id)->count();
        $unitConcept = $functionalUnitsCount . ' UFs';

        if ($functionalUnitsCount <= $additionalSalary->quantity) {
            $value = $additionalSalary->value;
        } else {
            $functionalUnitsCount = $functionalUnitsCount - $additionalSalary->quantity;
            $multiplier = ceil($functionalUnitsCount / $additionalSalary->quantity);
            $value = (($additionalSalary->value /3) * $multiplier) + $additionalSalary->value;
        }

        return new ConceptSalary(
            $value,
            $plus->name,
            HaberDescuento::HABER,
            $unitConcept
        );
    }
}
