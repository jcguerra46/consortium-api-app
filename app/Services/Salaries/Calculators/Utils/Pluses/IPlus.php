<?php


namespace App\Services\Salaries\Calculators\Utils\Pluses;


use App\Models\Employee;
use App\Models\SalaryPlus;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;

interface IPlus
{
    public function calculate(Employee $employee, $month, $year, SalaryPlus $plus = null, $basicSalary = null): ConceptSalary;
}
