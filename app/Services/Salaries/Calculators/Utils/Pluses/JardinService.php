<?php


namespace App\Services\Salaries\Calculators\Utils\Pluses;


use App\Models\Employee;
use App\Models\FunctionalUnit;
use App\Models\SalaryPlus;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\SalaryAdditionalService;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;

class JardinService implements IPlus
{
    public function __construct()
    {
        $this->salaryAdditionalsService = new SalaryAdditionalService();
    }

    public function calculate(Employee $employee, $month, $year, SalaryPlus $plus = null, $basicSalary = null): ConceptSalary
    {
        $additionalSalary = $this->salaryAdditionalsService->getSalaryAdditional(
            $plus->internal_class,
            $plus,
            $month,
            $year
        );

        return new ConceptSalary(
            $additionalSalary->value,
            $plus->name,
            HaberDescuento::HABER,
            null
        );
    }
}
