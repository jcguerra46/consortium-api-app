<?php


namespace App\Services\Salaries\Calculators\Utils\Pluses;


use App\Models\ConsortiumProfile;
use App\Models\Employee;
use App\Models\FunctionalUnit;
use App\Models\SalaryPlus;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\SalaryAdditionalService;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;

class MovimientoCochesService implements IPlus
{
    public function __construct()
    {
        $this->salaryAdditionalsService = new SalaryAdditionalService();
    }

    public function calculate(Employee $employee, $month, $year, SalaryPlus $plus = null, $basicSalary = null): ConceptSalary
    {
        $additionalSalary = $this->salaryAdditionalsService->getSalaryAdditional(
            $plus->internal_class,
            $plus,
            $month,
            $year
        );

        $value = $plus->value;
        $consortiumProfile = ConsortiumProfile::fromConsortium($employee->consortium_id);
        $garageAmount = $consortiumProfile->garage_amount;

        if ($garageAmount) {
            $unitConcept = $garageAmount . ' UFs.';
        } else {
            $unitConcept = '';
        }

        if ($garageAmount > $additionalSalary->quantity) {
            // si hay mas de 20 autos, por cada auto se agregan 22.45 extra
            $value += ($garageAmount - $additionalSalary->quantity) * $additionalSalary->extra_value;
        }

        return new ConceptSalary(
            $value,
            $plus->name,
            HaberDescuento::HABER,
            $unitConcept
        );
    }
}
