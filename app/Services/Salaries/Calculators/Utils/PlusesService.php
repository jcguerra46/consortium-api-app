<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\Employee;
use App\Models\SalaryPlus;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\Pluses\CapacitacionSeguridadHigieneService;
use App\Services\Salaries\Calculators\Utils\Pluses\ClasificacionResiduosService;
use App\Services\Salaries\Calculators\Utils\Pluses\JardinService;
use App\Services\Salaries\Calculators\Utils\Pluses\LimpiezaCocherasService;
use App\Services\Salaries\Calculators\Utils\Pluses\LimpiezaPiletasService;
use App\Services\Salaries\Calculators\Utils\Pluses\MovimientoCochesService;
use App\Services\Salaries\Calculators\Utils\Pluses\RetiroResiduosService;
use App\Services\Salaries\Calculators\Utils\Pluses\TituloEncargadoService;
use App\Services\Salaries\Calculators\Utils\Pluses\ZonaDesfavorableService;

class PlusesService
{
    private $concepts = [];
    private $pluses = [];

    public function getPlusesEmployee(
        Employee $employee,
        $basicSalary,
        $month,
        $year
    )
    {
        $employeePluses = $employee->pluses;

        foreach ($employeePluses as $plus) {
            switch ($plus->internal_class) {
                case 'RetiroResiduos':
                    $service = new RetiroResiduosService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'ClasificacionResiduos':
                    $service = new ClasificacionResiduosService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'LimpiezaCocheras':
                    $service = new LimpiezaCocherasService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'MovimientoCoches':
                    $service = new MovimientoCochesService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'Jardin':
                    $service = new JardinService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'ZonaDesfavorable':
                    $service = new ZonaDesfavorableService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'TituloEncargado':
                    $service = new TituloEncargadoService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'LimpiezaPiletas':
                    $service = new LimpiezaPiletasService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
                case 'CapacitacionSeguridadHigiene':
                    $service = new CapacitacionSeguridadHigieneService();
                    $this->addConcept($service->calculate($employee, $month, $year, $plus, $basicSalary));
                    $this->addPlus($plus);
                    break;
            }
        }

        return [
            'concepts' => $this->concepts,
            'pluses' => $this->pluses
        ];
    }

    private function addConcept(ConceptSalary $concept)
    {
        $this->concepts[] = $concept;
    }

    private function addPlus(SalaryPlus $plus)
    {
        $this->pluses[] = $plus;
    }
}
