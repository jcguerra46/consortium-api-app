<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\ProfessionalFunctionSalarie;

class ProfessionalFunctionsService
{
    public function getSalarioBasico(
        $professionalFunctionId,
        $consortiumCategory,
        $month,
        $year
    )
    {
        $basicSalary = ProfessionalFunctionSalarie::getBasicSalary(
            $professionalFunctionId,
            $consortiumCategory,
            $month,
            $year
        );
        return isset($basicSalary->value) ? $basicSalary->value : 0;
    }
}
