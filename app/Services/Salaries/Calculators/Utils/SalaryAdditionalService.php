<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\SalaryAdditional;
use App\Models\SalaryPlus;
use Carbon\Carbon;

class SalaryAdditionalService
{
    /**
     * @param null $type
     * @param SalaryPlus|null $plus
     * @param $month
     * @param $year
     */
    public function getSalaryAdditional(
        $type = null,
        SalaryPlus $plus = null,
        $month,
        $year
    )
    {
        $period = Carbon::createFromDate($year, $month, 1)->format('Y-m-d');
        $queryBuilder = SalaryAdditional::where('period', '<=', $period);

        if ($plus) {
            $queryBuilder = $queryBuilder->where('salary_plus_id', $plus->id);
        } else {
            $queryBuilder = $queryBuilder->whereNull('salary_plus_id');
        }
        if ($type) {
            $queryBuilder = $queryBuilder->where('type', $type);
        }

        $salaryAdditional = $queryBuilder->orderBy('period', 'DESC')
            ->first();
        return $salaryAdditional;
    }

}
