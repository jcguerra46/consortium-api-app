<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\EmployeeProfile;
use App\Models\ProfessionalFunction;
use App\Services\Salaries\Calculators\ValueObjects\DefinitionRules;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;
use App\Services\Salaries\Calculators\ValueObjects\ProfessionalFunction as ProfessionalFunctionValueObject;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use Carbon\Carbon;

class SalarySeniorityService
{
    public function __construct(SalaryAdditionalService $salaryAdditionalService)
    {
        $this->salaryAdditionalService = $salaryAdditionalService;
    }

    /**
     * @param EmployeeProfile $employeeProfile
     * @param ProfessionalFunction $professionalFunction
     * @param Carbon $untilDate
     * @param $month
     * @param $year
     * @return ConceptSalary
     */
    public function calculateConcept(
        EmployeeProfile $employeeProfile,
        ProfessionalFunction $professionalFunction,
        Carbon $untilDate,
        $month,
        $year
    ): ConceptSalary
    {
        $workedYears = $this->calculateWorkedYears($employeeProfile->entry_date, $untilDate);
        $value = $this->calculateValue($employeeProfile, $professionalFunction, $workedYears, $month, $year);
        $conceptYearsText = ($workedYears != 1) ? trans('payslip.seniority_concept_years') : trans('payslip.seniority_concept_year');
        $conceptText = trans('payslip.seniority_concept', ['percentage' => $this->getPercentage($professionalFunction, $employeeProfile)]);
        $payslipConceptUnit = $workedYears . ' ' . $conceptYearsText;

        return new ConceptSalary(
            $value,
            $conceptText,
            HaberDescuento::HABER,
            $payslipConceptUnit
        );
    }

    private function calculateWorkedYears(Carbon $entryDate, Carbon $untilDate)
    {
        return $entryDate->diffInYears($untilDate);
    }

    private function calculateValue(EmployeeProfile $employeeProfile,
                                    ProfessionalFunction $professionalFunction,
                                    $workedYears,
                                    $month,
                                    $year)
    {
        $additional = $this->getValueConcept($employeeProfile, $professionalFunction, $month, $year);
        return $additional->value * $workedYears;
    }

    private function getValueConcept(EmployeeProfile $employeeProfile,
                                     ProfessionalFunction $professionalFunction,
                                     $month,
                                     $year)
    {
        if ($professionalFunction->type_seniority == ProfessionalFunctionValueObject::SENIORITY_TYPE_1
            and $employeeProfile->antiquity) {

            $additional = $this->salaryAdditionalService->getSalaryAdditional(
                DefinitionRules::ANTIGUEDAD_V1,
                null,
                $month,
                $year
            );
            return $additional;
        } else {

            $additional = $this->salaryAdditionalService->getSalaryAdditional(
                DefinitionRules::ANTIGUEDAD_V2,
                null,
                $month,
                $year
            );
            return $additional;
        }
    }

    private function getPercentage(ProfessionalFunction $professionalFunction,
                                   EmployeeProfile $employeeProfile)
    {
        if ($professionalFunction->type_seniority == ProfessionalFunctionValueObject::SENIORITY_TYPE_1
            and $employeeProfile->antiquity) {
            return 1;
        }
        return 2;
    }
}
