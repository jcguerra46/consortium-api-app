<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\Consortium;
use App\Models\ProfessionalFunction;

class SueldoBasicoService
{
    public function __construct(ProfessionalFunctionsService $professionalFunctionsService)
    {
        $this->professionalFunctionService = $professionalFunctionsService;
    }

    /**
     * @param ProfessionalFunction $professionalFunction
     * @param Consortium $consortium
     * @param $workedHours
     * @param $month
     * @param $year
     * @return float|int
     */
    public function getSueldoBasico(
        ProfessionalFunction $professionalFunction,
        Consortium $consortium,
        $workedHours,
        $month,
        $year
    )
    {
        if ($professionalFunction->jornalizado) {
            $basicSalary = $this->professionalFunctionService->getSalarioBasico(
                $professionalFunction->id,
                null,
                $month,
                $year
            );
            return $this->calculateBasicSalaryJornalizadoSubstituto($workedHours, $basicSalary);
        } elseif ($professionalFunction->substituto) {
            $basicSalary = $this->professionalFunctionService->getSalarioBasico(
                $professionalFunction->id,
                null,
                $month,
                $year
            );
            $proportionalSalary = $basicSalary / 8;
            return $this->calculateBasicSalaryJornalizadoSubstituto($workedHours, $proportionalSalary);
        } else {
            $basicSalary = $this->professionalFunctionService->getSalarioBasico(
                $professionalFunction->id,
                $consortium->category,
                $month,
                $year
            );
            return $basicSalary;
        }
    }

    /**
     * @param $workedHours
     * @param $basicSalary
     * @return float|int
     */
    private function calculateBasicSalaryJornalizadoSubstituto(
        $workedHours,
        $basicSalary
    )
    {
        return $workedHours * $basicSalary;
    }
}
