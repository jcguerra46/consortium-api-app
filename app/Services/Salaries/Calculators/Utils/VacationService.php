<?php


namespace App\Services\Salaries\Calculators\Utils;


use App\Models\Employee;
use App\Models\Salary;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use Carbon\Carbon;

class VacationService
{

    public function sumExtraHoursVacations(
        Employee $employee,
        Carbon $date)
    {
        $previusDate = $date->subMonths(5);

        return Salary::where('type', TypeSalary::MONTHLY)
            ->where('employee_id', $employee->id)
            ->where('period', '<=', $date)
            ->where('period', '>=', $previusDate)
            ->sum('value_extra_hours_worked');
    }

}
