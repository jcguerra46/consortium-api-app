<?php


namespace App\Services\Salaries\Calculators;


use App\Models\Employee;
use App\Services\Salaries\Calculators\DTOs\SalaryDTO;
use App\Services\Salaries\Calculators\Entities\ConceptSalary;
use App\Services\Salaries\Calculators\Utils\EmployeeContributionsService;
use App\Services\Salaries\Calculators\Utils\EmployerContributionsService;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;
use App\Services\Salaries\Calculators\ValueObjects\TypeSalary;
use Carbon\Carbon;

class VacationsCalculator extends CalculatorAbstract
{
    protected $type = TypeSalary::VACATIONS;

    public function __construct(
        EmployeeContributionsService $employeeContributionsService,
        EmployerContributionsService $employerContributionsService)
    {
        $this->employeeContributionsService = $employeeContributionsService;
        $this->employerContributionsService = $employerContributionsService;
    }

    public function calculate(array $data): SalaryDTO
    {
        $this->employee = Employee::with('profile')->find($data['employee_id']);
        $this->consortium = $this->employee->consortium;
        $this->professionalFunction = $this->employee->professionalFunction;
        $this->month = $data['month'];
        $this->year = $data['year'];
        $data['start_date'] = Carbon::createFromDate($data['year'], $data['month'], 1);
        $this->data = $data;

        $this->calculateVacationsConcepts($data);

        return new SalaryDTO(
            $this->type,
            $data['start_date'],
            $this->employee,
            $this->basicSalary,
            $this->brutoSalary,
            $this->jornalSalary,
            $this->netoSalary,
            $this->artFijo,
            $this->artPercentage,
            $this->workedHours,
            null,
            null,
            $this->concepts,
            $this->employeeContributions,
            $this->employerContributions,
            $this->employeeContributionsTotal,
            $this->employerContributionsTotal,
            Carbon::createFromDate($data['year'], $data['month'], 1),
            Carbon::createFromFormat('d-m-Y',$data['payslip_deposit_date']),
            $data['payslip_contributions_bank']
        );
    }

    private function calculateVacationsConcepts(array $data)
    {
        $value = $this->addConceptsVacationsDays($data['vacations_days'], $data['vacations_day_value']);
        $this->addBrutoSalary($value);
        $this->addJornalSalary($value);
        $this->calculateContributionsEmployee();
        $this->calculateContributionsEmployer();
        $this->calculateContributionsTotals();
        $this->calculateNetoSalary();
        $this->calculateRounding();
    }

    private function addConceptsVacationsDays(int $daysVacations, float $valueVacations): float
    {
        $unit = $daysVacations . ' Días';
        $value = $daysVacations * $valueVacations;

        $concept = new ConceptSalary(
            $value,
            'Adelanto por Vacaciones',
            HaberDescuento::HABER,
            $unit
        );
        $this->addConcept($concept);
        return $value;
    }

    private function calculateContributionsEmployee()
    {
        $this->employeeContributions = $this->employeeContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->type,
            $this->month,
            $this->year
        );
    }

    private function calculateContributionsEmployer()
    {
        $dataContributions = $this->employerContributionsService->calculate(
            $this->brutoSalary,
            $this->employee,
            $this->professionalFunction,
            $this->consortium,
            $this->month,
            $this->year,
            $this->type,
            $this->employeeAdjustments,
            $this->workedHours,
            true
        );

        $this->employerContributions = $dataContributions['contributions'];
        $this->artFijo = $dataContributions['artFijo'];
        $this->artPercentage = $dataContributions['artPercentage'];
    }
}
