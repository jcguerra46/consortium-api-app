<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


class ContributionConcept
{
    const APORTE_JUBILATORIO = 'aporte_jubilatorio';
    const LEY_19032 = 'ley_19032';
    const LEY_27430 = 'ley_27430';
    const OBRA_SOCIAL = 'obra_social';
    const OBRA_SOCIAL_2 = 'obra_social_2';
    const ADICIONAL_OBRA_SOCIAL = 'adicional_obra_social';
    const ADICIONAL_OBRA_SOCIAL_2 = 'adicional_obra_social_2';
    const CPF = 'cfp';
    const CUOTA_SINDICAL_SUTERH = 'cuota_sindical_suterh';
    const CAJA_SUBSIDIOS_FAMILIARES = 'caja_subsidios_familiares';
    const ART_FIJO = 'art_fijo';
    const ART_PORCENTAJE = 'art_porcentaje';
    const SEGURO_VIDA = 'seguro_vida';
    const CAJA_PROTECCION_FAMILIA = 'caja_proteccion_familia';
    const CONTRIBUCION_SOLIDARIA = 'contribucion_solidaria';
    const AFIP = 'afip931';
    const SUTERH = 'suterh';
    const SERACARH = 'seracarh';
    const FATERYH = 'fateryh';
}
