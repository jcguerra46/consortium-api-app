<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


use Carbon\Carbon;

class DefinitionRules
{
    const ANTIGUEDAD_V1 = 'plus_antiguedad_1';
    const ANTIGUEDAD_V2 = 'plus_antiguedad_2';
    const HOUSING_VALUE = 'valorVivienda';
    const CONTRIBUTION_JUBILATORIO = 11;
    const CONTRIBUTION_LEY_19032 = 3;
    const CONTRIBUTION_OBRA_SOCIAL = 3;
    const CONTRIBUTION_ADICIONAL_OBRA_SOCIAL = 1.5;
    const CONTRIBUTION_ADICIONAL_CPF = 1;
    const CONTRIBUTION_ADICIONAL_FATERYH = 1.75;
    const CONTRIBUTION_ADICIONAL_FATERYH_ALTERNATE = 0.75; // Sin el 1% de fateryh pero con el 0.75 obligatorio
    const CONTRIBUTION_ADICIONAL_CUOTA_SINDICAL = 2;
    const CONTRIBUTION_JUBILATORIO_EMPLOYER = 10.77;
    const CONTRIBUTION_LEY_19032_EMPLOYER = 1.59;
    const CONTRIBUTION_CAJA_SUBSIDIOS_EMPLOYER = 5.64;
    const CONTRIBUTION_OBRA_SOCIAL_EMPLOYER = 6;
    const CONTRIBUTION_SEGURO_VIDA_EMPLOYER = 14.09;
    const CONTRIBUTION_CAJA_PROTECCION_FAMILIA_EMPLOYER = 1.5;
    const CONTRIBUTION_CONTRIBUCION_SOLIDARIA_FULL_TIME = 250;
    const CONTRIBUTION_CONTRIBUCION_SOLIDARIA_PART_TIME = 125;
    const CONTRIBUTION_CONTRIBUCION_SOLIDARIA_BY_HOUR = 1.25;
    const CONTRIBUTION_SERACARH_EMPLOYER = 0.5;
    const CONTRIBUTION_FATERYH_EMPLOYER = 4.75;
    const CONTRIBUTION_ADICIONAL_FATERYH_JORNALIZADO = 150;
    const CONTRIBUTION_ADICIONAL_FATERYH_PART_TIME = 75;
    const CONTRIBUTION_ADICIONAL_FATERYH_SUBSTITUTE = 0.75;
    const CONTRIBUTION_VALOR_OBRA_SOCIAL = 25;

    /**
     * @deprecated OR-1654: Quitar validacion por fecha
     * @return \Carbon\Carbon
     */
    public static function getAntiguedadDate()
    {
        return Carbon::create(2009, 5, 1);
    }

}
