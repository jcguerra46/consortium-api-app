<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


class ObraSocialDiffLogType
{
    const EMPLOYEE_OBRA_SOCIAL = "empleado_obra_social";
    const EMPLOYEE_ADC_OBRA_SOCIAL = "empleado_adc_obra_social";
    const EMPLOYER_OBRA_SOCIAL = "empleador_obra_social";
}
