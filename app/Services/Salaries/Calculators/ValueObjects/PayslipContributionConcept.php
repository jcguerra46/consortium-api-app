<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


class PayslipContributionConcept
{
    public $value;
    public $contributionConcept;
    public $description;
    public $contributionType;
    public $tipoBoleta;
    public $code;

    public function __construct($value, $contributionConcept, $description, $contributionType, $tipoBoleta, $code = '')
    {
        $this->value = $value;
        $this->contributionConcept = $contributionConcept;
        $this->description = $description;
        $this->contributionType = $contributionType;
        $this->tipoBoleta = $tipoBoleta;
        $this->code = $code;
    }

}
