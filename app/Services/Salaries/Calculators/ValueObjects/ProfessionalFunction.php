<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


class ProfessionalFunction
{
    const SENIORITY_TYPE_1 = '1';
    const SENIORITY_TYPE_2 = '2';
}
