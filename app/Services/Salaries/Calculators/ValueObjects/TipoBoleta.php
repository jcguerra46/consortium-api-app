<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


class TipoBoleta
{
    const AFIP = 'afip931';
    const SUTERH = 'SUTERH';
    const FATERYH = 'FATERYH';
    const SERACARH = 'SERACARH';
}
