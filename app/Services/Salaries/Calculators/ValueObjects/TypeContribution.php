<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


class TypeContribution
{
    const EMPLOYEE = 'employee';
    const EMPLOYER = 'employer';

    private $value;

    public function __construct($value)
    {
        $this->value = $value;

        $this->validate();
    }

    public function validate()
    {
        if (!in_array($this->value, self::getData())) {
            throw new \Exception('Invalid Contribution Type ' . $this->value . '.');
        }
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return trans('contribution_type.' . $this->value);
    }

    public function isEmployer()
    {
        return $this->value == self::EMPLOYER;
    }

    public function isEmployee()
    {
        return $this->value == self::EMPLOYEE;
    }

    /**
     * @return Array
     */
    public static function listValues()
    {
        return self::getData();
    }


    protected static function getData()
    {
        return [
            self::EMPLOYER => trans('contribution_type.' . self::EMPLOYER),
            self::EMPLOYEE => trans('contribution_type.' . self::EMPLOYEE),
        ];
    }
}
