<?php


namespace App\Services\Salaries\Calculators\ValueObjects;


class TypeSalary
{
    const MONTHLY = 'monthly';
    const SAC = 'sac';
    const INTERMEDIATE = 'intermediate';
    const VACATIONS = 'vacations';
}
