<?php


namespace App\Services\Salaries;


use App\Services\Salaries\Calculators\DTOs\SalaryDTO;
use App\Services\Salaries\Calculators\ValueObjects\HaberDescuento;
use App\Utils\CurrencyHelper;
use App\Utils\NumberToText;
use Illuminate\Support\Facades\View;

class PayslipHtmlGenerator
{
    const ITEMS_PER_PAGE = 18;
    private $paginateConcepts;
    private $totalConceptsPages;
    private $totalHaberes = 0;
    private $totalDescuentos = 0;

    /**
     * @param SalaryDTO $salary
     * @param bool $watermark
     * @param bool $skeletonPrint
     * @return string
     */
    public function generateHtml(SalaryDTO $salary, bool $watermark = false, bool $skeletonPrint = true)
    {
        $consortium = $salary->getEmployee()->consortium;
        $this->paginateConcepts($salary);

        return View::make('payslip.payslip-liquidation', [
            'salary' => $salary,
            'employee' => $salary->getEmployee(),
            'professionalFunction' => $salary->getEmployee()->professionalFunction,
            'consortium' => $consortium,
            'paginateConcepts' => $this->paginateConcepts,
            'totalConceptsPages' => $this->totalConceptsPages,
            'totalHaberes' => $this->totalHaberes,
            'totalDescuentos' => $this->totalDescuentos,
            'netoSalaryText' => $this->netoSalaryText($salary->getNetoSalary()),
            'watermark' => $watermark,
            'skeletonPrint' => $skeletonPrint
        ])->render();
    }

    /**
     * @param float $netoSalary
     * @return string
     */
    private function netoSalaryText(float $netoSalary): string
    {
        $numberToText = new NumberToText();
        $netoSalaryText = $numberToText->numberToText(str_replace('.', ',', $netoSalary));
        $cents = round($netoSalary - intval($netoSalary), 2) * 100;

        return $netoSalaryText . ' con ' . $cents . '/100';
    }

    /**
     * @param SalaryDTO $salary
     */
    private function paginateConcepts(SalaryDTO $salary)
    {
        $haberes = [];
        $descuentos = [];

        foreach ($salary->getConcepts() as $concept) {
            if ($concept->type == HaberDescuento::HABER) {
                $haberes[] = $concept;
            } else {
                $descuentos[] = $concept;
            }
        }

        foreach ($salary->getContributionsEmployee() as $contributionEmployee) {
            $contributionEmployee->type = HaberDescuento::DESCUENTO;
            $descuentos[] = $contributionEmployee;
        }

        foreach ($salary->getContributionsEmployer() as $contributionEmployer) {
            $contributionEmployer->type = HaberDescuento::DESCUENTO;
            $descuentos[] = $contributionEmployer;
        }

        $concepts = array_merge($haberes, $descuentos);

        $this->paginateConcepts = array_chunk($concepts, self::ITEMS_PER_PAGE);
        $this->totalConceptsPages = count($this->paginateConcepts);
        $tempPaginatedConcepts = [];

        foreach ($this->paginateConcepts as $concepts) {
            $transHaberes = 0;
            $transDiscounts = 0;

            foreach ($concepts as $concept) {
                if ($concept->type == HaberDescuento::HABER) {
                    $transHaberes += $concept->value;
                } else {
                    $transDiscounts += $concept->value;
                }
            }

            $data = [
                'transHaberes' => CurrencyHelper::formatWithoutCurrency($transHaberes),
                'transDiscounts' => CurrencyHelper::formatWithoutCurrency($transDiscounts)
            ];

            $tempPaginatedConcepts[] = [
                'data' => $data,
                'concepts' => $concepts
            ];

            //Sum totales
            $this->totalHaberes += $transHaberes;
            $this->totalDescuentos += $transDiscounts;
        }

        $this->paginateConcepts = $tempPaginatedConcepts;
        $this->totalHaberes = CurrencyHelper::formatWithoutCurrency($this->totalHaberes);
        $this->totalDescuentos = CurrencyHelper::formatWithoutCurrency($this->totalDescuentos);
    }
}
