<?php


namespace App\Services\Salaries;

use App\Models\Salary;
use App\Models\SalaryConcept;
use App\Models\SalaryConceptContribution;
use App\Services\Salaries\Calculators\CalculatorProvider;
use App\Services\Salaries\Calculators\DTOs\SalaryDTO;
use Illuminate\Support\Facades\DB;

/**
 * Class SalariesService
 * @package App\Services\Salaries
 * @author SCabral
 */
class SalariesService
{
    /**
     * SalariesService constructor.
     * @param CalculatorProvider $calculatorProvider
     * @param PayslipHtmlGenerator $payslipHtmlGenerator
     */
    public function __construct(CalculatorProvider $calculatorProvider,
                                PayslipHtmlGenerator $payslipHtmlGenerator)
    {
        $this->calculatorProvider = $calculatorProvider;
        $this->payslipHtmlGenerator = $payslipHtmlGenerator;
    }

    /**
     * @param array $data
     * @param string $type
     * @return SalaryDTO
     */
    public function calculate(array $data, string $type): SalaryDTO
    {
        $calculator = $this->calculatorProvider->getCalculator($type);
        return $calculator->calculate($data);
    }

    /**
     * @param array $data
     * @param string $type
     * @return string
     */
    public function getPayslip(array $data, string $type): string
    {
        $salary = $this->calculate($data, $type);
        return $this->payslipHtmlGenerator->generateHtml($salary);
    }

    /**
     * @param array $data
     * @param string $type
     * @return Salary
     */
    public function create(array $data, string $type): Salary
    {
        $salaryCalculated = $this->calculate($data, $type);

        return DB::transaction(function () use ($salaryCalculated) {

            $salary = Salary::create([
                'employee_id' => $salaryCalculated->getEmployee()->id,
                'type' => $salaryCalculated->getType(),
                'period' => $salaryCalculated->getPeriod(),
                'basico' => $salaryCalculated->getBasicSalary(),
                'jornal' => $salaryCalculated->getJornalSalary(),
                'neto' => $salaryCalculated->getNetoSalary(),
                'bruto' => $salaryCalculated->getBrutoSalary(),
                'value_porcentual_art' => 0,
                'value_fijo_art' => $salaryCalculated->getArtFijo(),
                'extra_hours_worked' => 0,
                'value_extra_hours_worked' => 0,
                'vacation_days' => 0,
                'not_vacation_days' => 0,
                'contributions_period' => $salaryCalculated->getContributionsPeriod(),
                'contributions_deposit_date' => $salaryCalculated->getContributionsDepositDate(),
                'contributions_bank' => $salaryCalculated->getContributionsBank(),
                'contributions_employee_total' => $salaryCalculated->getContributionsEmployeeTotal(),
                'contributions_employer_total' => $salaryCalculated->getContributionsEmployerTotal(),
            ]);

            foreach ($salaryCalculated->getConcepts() as $concept) {
                SalaryConcept::create([
                    'salary_id' => $salary->id,
                    'employee_adjustment_id' => $concept->employeeAdjustmentId,
                    'value' => $concept->value,
                    'description' => $concept->description,
                    'type' => $concept->type,
                    'unit' => $concept->unit
                ]);
            }

            $contributions = [];
            foreach ($salaryCalculated->getContributionsEmployer() as $contribution) {
                $contributions[] = [
                    'salary_id' => $salary->id,
                    'value' => $contribution->value,
                    'type_contribution' => $contribution->contributionType,
                    'description' => $contribution->description,
                    'tipo_boleta' => $contribution->tipoBoleta,
                    'concept' => $contribution->contributionConcept,
                ];
            }

            foreach ($salaryCalculated->getContributionsEmployee() as $contribution) {
                $contributions[] = [
                    'salary_id' => $salary->id,
                    'value' => $contribution->value,
                    'type_contribution' => $contribution->contributionType,
                    'description' => $contribution->description,
                    'tipo_boleta' => $contribution->tipoBoleta,
                    'concept' => $contribution->contributionConcept,
                ];
            }

            SalaryConceptContribution::insert($contributions);

            return $salary;
        });
    }

    public function getPayslipFromSalary(Salary $salary): string
    {

    }
}
