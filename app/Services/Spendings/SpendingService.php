<?php


namespace App\Services\Spendings;


use App\Events\SpendingCreatedEvent;
use App\Models\Period;
use App\Models\Spending;
use App\Models\SpendingConsortiumPercentage;
use App\Models\SpendingInstallment;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

class SpendingService
{

    public function create(array $dataSpending, array $percentages, array $installments = null)
    {
        return DB::transaction(function () use ($dataSpending, $percentages, $installments) {
            //Create Spending
            $spending = Spending::create($dataSpending);

            //Create Installments
            $openPeriod = Period::where('consortium_id', $spending->consortium_id)
                            ->where('state', 'open')->first();

            if (!empty($installments)) {
                foreach ($installments as $installment) {
                    $installment['spending_id'] = $spending->id;
                    $installment['period_id'] = ($installment['include_open_period'] ? $openPeriod->id : null);
                    SpendingInstallment::create($installment);
                }
            } else {
                SpendingInstallment::create([
                    'number' => 1,
                    'amount' => $spending->amount,
                    'date_pay' => Carbon::now(),
                    'spending_id' => $spending->id,
                    'period_id' => $openPeriod->id,
                    'invoice_number' => isset($dataSpending['invoice_number']) ? $dataSpending['invoice_number'] : null
                ]);
            }

            //Create Percentages
            if (!isset($dataSpending['functional_unit_id'])) {
                foreach ($percentages as $key => $percentage) {
                    $percentages[$key]['spending_id'] = $spending->id;
                }
                SpendingConsortiumPercentage::insert($percentages);
            }

            $spending->refresh();
            $spending->load(['installments', 'percentages']);
            Event::dispatch(new SpendingCreatedEvent($spending));

            return $spending;
        });
    }

}
