<?php


namespace App\Services;


use App\Models\BankAccountMovement;
use App\Models\Check;
use App\Models\VirtualCashboxMovement;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * Class VirtualCashboxMovementService
 * @package App\Services
 */
class VirtualCashboxMovementService
{
    /** @var CashboxMovementService */
    protected $cashboxMovementService;

    /**
     * VirtualCashboxMovementService constructor.
     * @param CashboxMovementService $cashboxMovementService
     */
    public function __construct(CashboxMovementService $cashboxMovementService)
    {
        $this->cashboxMovementService = $cashboxMovementService;
    }

    /**
     * @param $dataMovement
     * @param $checksIds
     * @param $bankMovsIds
     * @return mixed
     */
    public function createIncome($dataMovement, $checksIds, $bankMovsIds)
    {
        return DB::transaction(function () use ($dataMovement, $checksIds, $bankMovsIds) {

            $dataMovement['type_movement'] = VirtualCashboxMovement::TYPE_INCOME;
            $virtualCashboxMov = VirtualCashboxMovement::create($dataMovement);

            $bankAccountMovements = BankAccountMovement::whereIn('id', $bankMovsIds);
            foreach ($bankAccountMovements as $bankAccountMovement) {
                $bankAccountMovement->virtual_cashbox_movement_id = $virtualCashboxMov->id;
                $bankAccountMovement->save();
            }

            $checks = Check::whereIn('id', $checksIds);
            foreach ($checks as $check) {
                $check->virtual_cashbox_movement_id = $virtualCashboxMov->id;
                $check->save();
            }

            if (!$virtualCashboxMov->virtualCashbox->independient) {
                $dataMovement['identified'] = false;
                $dataMovement['cashbox_id'] = $virtualCashboxMov->virtualCashbox->consortium->cashbox->id;
                $this->cashboxMovementService->createIncome($dataMovement, $checksIds, $bankMovsIds, null);
            }

            return $virtualCashboxMov;
        });
    }

    /**
     * @param $dataMovement
     * @param $checksIds
     * @param $bankMovsIds
     * @return mixed
     */
    public function createOutflow($dataMovement, $checksIds, $bankMovsIds)
    {
        return DB::transaction(function () use ($dataMovement, $checksIds, $bankMovsIds) {

            $dataMovement['type_movement'] = VirtualCashboxMovement::TYPE_OUTFLOW;
            $virtualCashboxMov = VirtualCashboxMovement::create($dataMovement);

            $bankAccountMovements = BankAccountMovement::whereIn('id', $bankMovsIds);
            foreach ($bankAccountMovements as $bankAccountMovement) {
                $bankAccountMovement->virtual_cashbox_movement_id = $virtualCashboxMov->id;
                $bankAccountMovement->save();
            }

            $checks = Check::whereIn('id', $checksIds);
            foreach ($checks as $check) {
                $check->virtual_cashbox_movement_id = $virtualCashboxMov->id;
                $check->save();
            }

            if (!$virtualCashboxMov->virtualCashbox->independent) {
                $dataMovement['identified'] = false;
                $dataMovement['cashbox_id'] = $virtualCashboxMov->virtualCashbox->consortium->cashbox->id;
                $this->cashboxMovementService->createOutflow($dataMovement, $checksIds, $bankMovsIds);
            }

            return $virtualCashboxMov;
        });
    }

    /**
     * @param $dataMovement
     * @param $checkIds
     * @param $bankMovsIds
     */
    public function createInitialBalance($dataMovement, $checkIds, $bankMovsIds)
    {
        $dataMovement['description'] = 'Balance Inicial';
        $dataMovement['date'] = Carbon::now();

        $virtualCashboxMovement = $this->createIncome($dataMovement, $checkIds, $bankMovsIds);
        return $virtualCashboxMovement;
    }

    /**
     * @param int $virtualCashboxId
     * @return array
     */
    public function getBalances(int $virtualCashboxId)
    {
        $balance = VirtualCashboxMovement::balance($virtualCashboxId);
        $balanceCashAmount = VirtualCashboxMovement::balanceCashAmount($virtualCashboxId);
        $balanceChecks = VirtualCashboxMovement::balanceChecks($virtualCashboxId);
        $balanceBankAccountMovements = VirtualCashboxMovement::balanceBankAccountsMovements($virtualCashboxId);

        return [
            'balance' => $balance,
            'balance_cash_amount' => $balanceCashAmount,
            'balance_checks' => $balanceChecks,
            'balance_bank_account_movements' => $balanceBankAccountMovements,
        ];
    }
}
