<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/afip-actividades",
 *     tags={"Afip Actividades"},
 *     summary="Get Afip Actividades list",
 *     description="Returns list of Afip Actividades.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Afip Actividades overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/afip-actividades/{id}",
 *     tags={"Afip Actividades"},
 *     summary="Get a Afip Actividad info",
 *     description="Returns a Afip Actividad info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Afip Actividad ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Afip Actividad overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Afip Actividad not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/afip-actividades/paginate",
 *     tags={"Afip Actividades"},
 *     summary="Get Afip Actividades list with paginate",
 *     description="Returns list of Afip Actividades with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Afip Actividades overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
