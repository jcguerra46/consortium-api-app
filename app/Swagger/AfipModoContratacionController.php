<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/afip-modos-contratacion",
 *     tags={"Afip Modos Contratacion"},
 *     summary="Get Afip Modos Contratacion list",
 *     description="Returns list of Afip Modos Contratacion.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Afip Modos Contratacion overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/afip-modos-contratacion/{id}",
 *     tags={"Afip Modos Contratacion"},
 *     summary="Get a Afip Modo Contratacion info",
 *     description="Returns a Afip Modo Contratacion info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Afip Modo Contratacion ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Afip Modo Contratacion overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Afip Modo Contratacion not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/afip-modos-contratacion/paginate",
 *     tags={"Afip Modos Contratacion"},
 *     summary="Get Afip Modos Contratacion list with paginate",
 *     description="Returns list of Afip Modos Contratacion with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Afip Modos Contratacion overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
