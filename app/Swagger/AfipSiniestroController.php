<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/afip-siniestros",
 *     tags={"Afip Siniestros"},
 *     summary="Get Afip Siniestros list",
 *     description="Returns list of Afip Siniestros.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Afip Siniestros overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/afip-siniestros/{id}",
 *     tags={"Afip Siniestros"},
 *     summary="Get a Afip Siniestro info",
 *     description="Returns a Afip Siniestro info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Afip Siniestro ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Afip Siniestro overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Afip Siniestro not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/afip-siniestros/paginate",
 *     tags={"Afip Siniestros"},
 *     summary="Get Afip Siniestros list with paginate",
 *     description="Returns list of Afip Siniestros with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Afip Siniestros overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
