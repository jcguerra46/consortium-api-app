<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/amenity",
 *     tags={"Amenities"},
 *     summary="Get amenity list",
 *     description="Returns list of Amenities.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Amenities overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/amenity",
 *     tags={"Amenities"},
 *     summary="Create amenity",
 *     description="Create amenity.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortium_id",
 *         in="query",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="location",
 *         in="query",
 *         description="Location",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="enabled",
 *         in="query",
 *         description="Enabled",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Amenities overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/amenity/{id}",
 *     tags={"Amenities"},
 *     summary="Update amenity",
 *     description="Update amenity.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Amenity id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortium_id",
 *         in="query",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="location",
 *         in="query",
 *         description="Location",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="enabled",
 *         in="query",
 *         description="Enabled",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Amenities overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/amenity/{id}",
 *     tags={"Amenities"},
 *     summary="Get a amenity info",
 *     description="Returns a amenity info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Amenity ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Amenity overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Amenity not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/amenity/{id}",
 *     tags={"Amenities"},
 *     summary="Delete a amenity",
 *     description="Delete amenity.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Amenity ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Amenities deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="COnsortium not found.",
 *     )
 * )
 */


/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/amenity/paginate",
 *     tags={"Amenities"},
 *     summary="Get Amenities list with paginate",
 *     description="Returns list of Amenities with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Amenities overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/amenity/export",
 *     tags={"Amenities"},
 *     summary="Export Amenities list",
 *     description="Export list of Amenities.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Amenities overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
