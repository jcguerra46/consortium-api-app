<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account",
 *     tags={"Bank Accounts"},
 *     summary="Get consortium Bank Accounts list",
 *     description="Returns list of Bank Accounts.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Bank Accounts overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/consortium/{consortiumID}/bank-account",
 *     tags={"Bank Accounts"},
 *     summary="Create consortium Bank Accounts",
 *     description="Create consortium Bank Accounts.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"bank_id", "account_number", "cbu", "cuit", "type", "branch_office", "owner", "show_data_in_expense"},
 *    				 @OA\Property(property="bank_id",
 *    					type="string",
 *    					example="1",
 *    					description="Bank ID"
 *    				),
 *    				 @OA\Property(property="account_number",
 *    					type="string",
 *    					example="2132132",
 *    					description="Account Number"
 *    				),
 *                  @OA\Property(property="cbu",
 *    					type="string",
 *    					example="1321654651",
 *    					description="CBU"
 *    				),
 *                  @OA\Property(property="alias",
 *    					type="string",
 *    					example="alias-ejemplo",
 *    					description="Alias"
 *    				),
 *                  @OA\Property(property="cuit",
 *    					type="string",
 *    					example="20-35226046-1",
 *    					description="Cuit"
 *    				),
 *                  @OA\Property(property="type",
 *    					type="string",
 *    					enum= {"savings_box", "current_account", "single_account"},
 *    					description="Type of account"
 *    				),
 *                  @OA\Property(property="branch_office",
 *    					type="string",
 *    					example="Sucursal EJEMPLO",
 *    					description="branch_office"
 *    				),
 *                  @OA\Property(property="owner",
 *    					type="string",
 *    					example="Juan Perez",
 *    					description="Owner"
 *    				),
 *                  @OA\Property(property="show_data_in_expense",
 *    					type="boolean",
 *    					description="Show data in expense"
 *    				),
 *                  @OA\Property(property="email",
 *    					type="string",
 *                      example="ejemplo@octopus.com.ar",
 *    					description="Email"
 *    				),
 *                  @OA\Property(property="signatorie_1",
 *    					type="string",
 *                      example="Firmante 1",
 *    					description="Email"
 *    				),
 *                  @OA\Property(property="signatorie_2",
 *    					type="string",
 *                      example="Firmante 2",
 *    					description="Firmante 2"
 *    				),
 *                  @OA\Property(property="signatorie_3",
 *    					type="string",
 *                      example="Firmante 3",
 *    					description="Firmante 3"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{consortiumID}/bank-account/{id}",
 *     tags={"Bank Accounts"},
 *     summary="Update consortium Bank Accounts",
 *     description="Update consortium Bank Accounts.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Bank Account id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"bank_id", "account_number", "cbu", "cuit", "type", "branch_office", "owner", "show_data_in_expense"},
 *    				 @OA\Property(property="bank_id",
 *    					type="string",
 *    					example="1",
 *    					description="Bank ID"
 *    				),
 *    				 @OA\Property(property="account_number",
 *    					type="string",
 *    					example="2132132",
 *    					description="Account Number"
 *    				),
 *                  @OA\Property(property="cbu",
 *    					type="string",
 *    					example="1321654651",
 *    					description="CBU"
 *    				),
 *                  @OA\Property(property="alias",
 *    					type="string",
 *    					example="alias-ejemplo",
 *    					description="Alias"
 *    				),
 *                  @OA\Property(property="cuit",
 *    					type="string",
 *    					example="20-35226046-1",
 *    					description="Cuit"
 *    				),
 *                  @OA\Property(property="type",
 *    					type="string",
 *    					enum= {"savings_box", "current_account", "single_account"},
 *    					description="Type of account"
 *    				),
 *                  @OA\Property(property="branch_office",
 *    					type="string",
 *    					example="Sucursal EJEMPLO",
 *    					description="branch_office"
 *    				),
 *                  @OA\Property(property="owner",
 *    					type="string",
 *    					example="Juan Perez",
 *    					description="Owner"
 *    				),
 *                  @OA\Property(property="show_data_in_expense",
 *    					type="boolean",
 *    					description="Show data in expense"
 *    				),
 *                  @OA\Property(property="email",
 *    					type="string",
 *                      example="ejemplo@octopus.com.ar",
 *    					description="Email"
 *    				),
 *                  @OA\Property(property="signatorie_1",
 *    					type="string",
 *                      example="Firmante 1",
 *    					description="Email"
 *    				),
 *                  @OA\Property(property="signatorie_2",
 *    					type="string",
 *                      example="Firmante 2",
 *    					description="Firmante 2"
 *    				),
 *                  @OA\Property(property="signatorie_3",
 *    					type="string",
 *                      example="Firmante 3",
 *    					description="Firmante 3"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Accounts overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/{id}",
 *     tags={"Bank Accounts"},
 *     summary="Get a consortium Bank Accounts info",
 *     description="Returns a consortium Bank Accounts info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Bank Account not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/consortium/{consortiumID}/bank-account/{id}",
 *     tags={"Bank Accounts"},
 *     summary="Delete a consortium Bank Accounts",
 *     description="Delete consortium Bank Accounts.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Accounts deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Consortium not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/paginate",
 *     tags={"Bank Accounts"},
 *     summary="Get Bank Accounts list with paginate",
 *     description="Returns list of Bank Accounts with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Accounts overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/export",
 *     tags={"Bank Accounts"},
 *     summary="Export Bank Accounts list",
 *     description="Export list of Bank Accounts.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Bank Accounts overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
