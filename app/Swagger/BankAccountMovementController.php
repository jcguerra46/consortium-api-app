<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/movement",
 *     tags={"Bank Account Movements"},
 *     summary="Get consortium Bank Account Movements list",
 *     description="Returns list of Bank Account Movements.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/movement",
 *     tags={"Bank Account Movements"},
 *     summary="Get a consortium Bank Account Movements info",
 *     description="Returns a consortium Bank Account Movements info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="description",
 *    					type="string",
 *    					example="Example..",
 *    					description="Description"
 *    				),
 *    				 @OA\Property(property="date",
 *    					type="string",
 *    					example="15-12-2000",
 *    					description="date"
 *    				),
 *                  @OA\Property(property="amount",
 *    					type="numeric",
 *    					example="500",
 *    					description="Amount"
 *    				),
 *                  @OA\Property(property="type",
 *    					type="string",
 *    					enum= {"income", "outflow"},
 *    					description="Type"
 *    				),
 *                  @OA\Property(property="type_movement",
 *    					type="string",
 *    					enum= {"deposit", "transfer", "extraction"},
 *    					description="Type"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Bank Account not found.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/movement/{id}",
 *     tags={"Bank Account Movements"},
 *     summary="Get a consortium Bank Account Movements info",
 *     description="Returns a consortium Bank Account Movements info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Movement ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="description",
 *    					type="string",
 *    					example="Example..",
 *    					description="Description"
 *    				),
 *    				 @OA\Property(property="date",
 *    					type="string",
 *    					example="15-12-2000",
 *    					description="date"
 *    				),
 *                  @OA\Property(property="amount",
 *    					type="numeric",
 *    					example="500",
 *    					description="Amount"
 *    				),
 *                  @OA\Property(property="type",
 *    					type="string",
 *    					enum= {"income", "outflow"},
 *    					description="Type"
 *    				),
 *                  @OA\Property(property="type_movement",
 *    					type="string",
 *    					enum= {"deposit", "transfer", "extraction"},
 *    					description="Type"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Bank Account not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/movement/{id}",
 *     tags={"Bank Account Movements"},
 *     summary="Get a consortium Bank Account Movements info",
 *     description="Returns a consortium Bank Account Movements info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="ID",
 *         in="path",
 *         description="Movement ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Bank Account not found.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/movement/{id}",
 *     tags={"Bank Account Movements"},
 *     summary="Get a consortium Bank Account Movements info",
 *     description="Returns a consortium Bank Account Movements info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Bank Account not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/movement/paginate",
 *     tags={"Bank Account Movements"},
 *     summary="Get Bank Account Movements list with paginate",
 *     description="Returns list of Bank Account Movements with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/movement/export",
 *     tags={"Bank Account Movements"},
 *     summary="Export Bank Account Movements list",
 *     description="Export list of Bank Account Movements.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Export Bank Account Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Get Balance Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/bank-account/{bankAccountID}/balance",
 *     tags={"Bank Account Movements"},
 *     summary="Get a consortium Bank Account balance info",
 *     description="Returns Bank Account Balance info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="bankAccountID",
 *         in="path",
 *         description="Bank Account ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Bank Account Balance overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Bank Account not found.",
 *     )
 * )
 */
