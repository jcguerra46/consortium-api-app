<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/categories",
 *     tags={"Categories"},
 *     summary="Get categories list",
 *     description="Returns list of Categories.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Categories overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/categories",
 *     tags={"Categories"},
 *     summary="Create category",
 *     description="Create category.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="number",
 *         in="query",
 *         description="Number",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="position",
 *         in="query",
 *         description="Position",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden",
 *         in="query",
 *         description="Hidden",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="salaries",
 *         in="query",
 *         description="Salaries",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="administration_id",
 *         in="query",
 *         description="Administration Id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Category overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/categories/{id}",
 *     tags={"Categories"},
 *     summary="Update category",
 *     description="Update category.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Category id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="number",
 *         in="query",
 *         description="Number",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="position",
 *         in="query",
 *         description="Position",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden",
 *         in="query",
 *         description="Hidden",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="salaries",
 *         in="query",
 *         description="Salaries",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="administration_id",
 *         in="query",
 *         description="Administration Id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Category overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/categories/{id}",
 *     tags={"Categories"},
 *     summary="Get a category info",
 *     description="Returns a category info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Category ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Category overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Category not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/categories/{id}",
 *     tags={"Categories"},
 *     summary="Delete a category",
 *     description="Delete category.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Category ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Category deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Category not found.",
 *     )
 * )
 */
