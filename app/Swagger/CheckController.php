<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/check",
 *     tags={"Checks"},
 *     summary="Get checks list",
 *     description="Returns list of Checks.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Checks overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/check",
 *     tags={"Checks"},
 *     summary="Create check",
 *     description="Create check.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="number",
 *    					type="integer",
 *    					example="1234",
 *    					description="Number of check"
 *    				),
 *    				 @OA\Property(property="deposit_date",
 *    					type="string",
 *    					example="10-12-2000",
 *    					description="deposit_date"
 *    				),
 *                  @OA\Property(property="issuance_date",
 *    					type="string",
 *    					example="10-12-2000",
 *    					description="issuance_date"
 *    				),
 *                  @OA\Property(property="due_date",
 *    					type="string",
 *    					example="31-12-2020",
 *    					description="due_date"
 *    				),
 *                  @OA\Property(property="amount",
 *    					type="numeric",
 *    					example="1265",
 *    					description="amount"
 *    				),
 *                  @OA\Property(property="crossed",
 *    					type="boolean",
 *    					description="Crossed"
 *    				),
 *                  @OA\Property(property="is_to_the_order",
 *    					type="boolean",
 *    					description="is_to_the_order"
 *    				),
 *                  @OA\Property(property="endorsements",
 *    					type="boolean",
 *    					description="Endorsements"
 *    				),
 *                  @OA\Property(property="own",
 *    					type="boolean",
 *    					description="Own"
 *    				),
 *                  @OA\Property(property="bank_id",
 *    					type="integer",
 *    					description="Bank ID"
 *    				),
 *                  @OA\Property(property="consortium_id",
 *    					type="integer",
 *    					description="Consortium ID"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Checks overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/check/{id}",
 *     tags={"Checks"},
 *     summary="Update check",
 *     description="Update check.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Check id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="number",
 *    					type="integer",
 *    					example="1234",
 *    					description="Number of check"
 *    				),
 *    				 @OA\Property(property="deposit_date",
 *    					type="string",
 *    					example="10-12-2000",
 *    					description="deposit_date"
 *    				),
 *                  @OA\Property(property="issuance_date",
 *    					type="string",
 *    					example="10-12-2000",
 *    					description="issuance_date"
 *    				),
 *                  @OA\Property(property="due_date",
 *    					type="string",
 *    					example="31-12-2020",
 *    					description="due_date"
 *    				),
 *                  @OA\Property(property="amount",
 *    					type="numeric",
 *    					example="1265",
 *    					description="amount"
 *    				),
 *                  @OA\Property(property="crossed",
 *    					type="boolean",
 *    					description="Crossed"
 *    				),
 *                  @OA\Property(property="is_to_the_order",
 *    					type="boolean",
 *    					description="is_to_the_order"
 *    				),
 *                  @OA\Property(property="endorsements",
 *    					type="boolean",
 *    					description="Endorsements"
 *    				),
 *                  @OA\Property(property="own",
 *    					type="boolean",
 *    					description="Own"
 *    				),
 *                  @OA\Property(property="bank_id",
 *    					type="integer",
 *    					description="Bank ID"
 *    				),
 *                  @OA\Property(property="consortium_id",
 *    					type="integer",
 *    					description="Consortium ID"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Check overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/check/{id}",
 *     tags={"Checks"},
 *     summary="Get a check info",
 *     description="Returns a check info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Check ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Check overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Check not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/check/{id}",
 *     tags={"Checks"},
 *     summary="Delete a check",
 *     description="Delete check.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Check ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Checks deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Check not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/check/paginate",
 *     tags={"Checks"},
 *     summary="Get Checks list with paginate",
 *     description="Returns list of Checks with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Checks overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/check/export",
 *     tags={"Checks"},
 *     summary="Export Checks list",
 *     description="Export list of Check.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Checks overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
