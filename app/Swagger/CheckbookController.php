<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/checkbook",
 *     tags={"Checkbooks"},
 *     summary="Get checkbooks list",
 *     description="Returns list of Checkbooks.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Checkbooks overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/checkbook",
 *     tags={"Checkbooks"},
 *     summary="Create check",
 *     description="Create checkbook.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="number",
 *    					type="numeric",
 *    					example="1234",
 *    					description="Number of checkbook"
 *    				),
 *    				 @OA\Property(property="first_check",
 *    					type="integer",
 *    					example="1",
 *    					description="first_check"
 *    				),
 *                  @OA\Property(property="last_check",
 *    					type="integer",
 *    					example="50",
 *    					description="last_check"
 *    				),
 *                  @OA\Property(property="bank_id",
 *    					type="integer",
 *    					description="Bank ID"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Checkbooks overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/checkbook/{id}",
 *     tags={"Checkbooks"},
 *     summary="Update check",
 *     description="Update checkbook.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Checkbook id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="number",
 *    					type="numeric",
 *    					example="1234",
 *    					description="Number of checkbook"
 *    				),
 *    				 @OA\Property(property="first_check",
 *    					type="integer",
 *    					example="1",
 *    					description="first_check"
 *    				),
 *                  @OA\Property(property="last_check",
 *    					type="integer",
 *    					example="50",
 *    					description="last_check"
 *    				),
 *                  @OA\Property(property="bank_id",
 *    					type="integer",
 *    					description="Bank ID"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Checkbook overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/checkbook/{id}",
 *     tags={"Checkbooks"},
 *     summary="Get a checkbook info",
 *     description="Returns a checkbook info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Checkbook ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Checkbook overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Checkbook not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/checkbook/{id}",
 *     tags={"Checkbooks"},
 *     summary="Delete a check",
 *     description="Delete checkbook.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Checkbook ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Checkbooks deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Checkbook not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/checkbook/paginate",
 *     tags={"Checkbooks"},
 *     summary="Get Checkbooks list with paginate",
 *     description="Returns list of Checkbooks with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Checkbooks overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/checkbook/export",
 *     tags={"Checkbooks"},
 *     summary="Export Checkbooks list",
 *     description="Export list of Check.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Checkbooks overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
