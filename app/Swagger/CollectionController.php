<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/collection",
 *     tags={"Collections"},
 *     summary="Get collections list",
 *     description="Returns list of Collections.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="period_id",
 *         in="query",
 *         description="Period ID",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="limit",
 *         in="query",
 *         description="Limit",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Collections overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/collection",
 *     tags={"Collections"},
 *     summary="Create collection",
 *     description="Create collection.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="description",
 *    					type="string",
 *    					example="Example",
 *    					description="Description"
 *    				),
 *    				 @OA\Property(property="date",
 *    					type="string",
 *    					example="10-12-2000",
 *    					description="date"
 *    				),
 *                  @OA\Property(property="total_amount",
 *    					type="numeric",
 *    					example="500",
 *    					description="Total Amount"
 *    				),
 *                  @OA\Property(property="cash_amount",
 *    					type="numeric",
 *    					example="500",
 *    					description="Cash Amount"
 *    				),
 *                  @OA\Property(property="checks",
 *    					type="array",
 *    					description="Checks",
 *                      @OA\Items(type="integer")
 *    				),
 *                  @OA\Property(property="transfers",
 *    					type="array",
 *    					description="Transfers",
 *                      @OA\Items(type="integer")
 *    				),
 *                  @OA\Property(property="functional_unit_id",
 *    					type="integer",
 *    					example="500",
 *    					description="Functional Unit ID"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Collections overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/collection/{id}",
 *     tags={"Collections"},
 *     summary="Get a collection info",
 *     description="Returns a collection info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Collection ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Collection overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Collection not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/collection/paginate",
 *     tags={"Collections"},
 *     summary="Get Collections list with paginate",
 *     description="Returns list of Collections with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="period_id",
 *         in="query",
 *         description="Period ID",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Collections overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/collection/export",
 *     tags={"Collections"},
 *     summary="Export Collections list",
 *     description="Export list of Collection.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Collections overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
