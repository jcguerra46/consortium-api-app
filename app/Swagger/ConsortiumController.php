<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium",
 *     tags={"Consortia"},
 *     summary="Get consortium list",
 *     description="Returns list of Consortia.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Consortia overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Store Swagger Annotation
 * @OA\Post(
 *     path="/consortium",
 *     tags={"Consortia"},
 *     summary="Create consortium",
 *     description="Create consortium.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="code",
 *         in="query",
 *         description="Code",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="order",
 *         in="query",
 *         description="Order",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="fancy_name",
 *         in="query",
 *         description="Fancy name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="business_name",
 *         in="query",
 *         description="Business name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="address_number",
 *         in="query",
 *         description="Address number",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="address_street",
 *         in="query",
 *         description="Address Street",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="country_id",
 *         in="query",
 *         description="Country ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="province_id",
 *         in="query",
 *         description="Province ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="postal_code",
 *         in="query",
 *         description="Postal code",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cuit",
 *         in="query",
 *         description="CUIT",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         description="Start date",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="payment_method_id",
 *         in="query",
 *         description="Payment Method Id",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="suterh_code",
 *         in="query",
 *         description="Suterh_code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="siro_code",
 *         in="query",
 *         description="Siro code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="pmc_code",
 *         in="query",
 *         description="PMC code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="ep_code",
 *         in="query",
 *         description="EP code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="siro_verification_number",
 *         in="query",
 *         description="Siro verification number",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="count_floors",
 *         in="query",
 *         description="Count floors",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="count_functional_units",
 *         in="query",
 *         description="Count functional units",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="type_of_building",
 *         in="query",
 *         description="Type of building",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="state",
 *         in="query",
 *         description="State",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="category",
 *         in="query",
 *         description="Category",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="first_due_date",
 *         in="query",
 *         description="First due date",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortiums overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Update Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{id}",
 *     tags={"Consortia"},
 *     summary="Update consortium",
 *     description="Update consortium.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="code",
 *         in="query",
 *         description="Code",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="order",
 *         in="query",
 *         description="Order",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="fancy_name",
 *         in="query",
 *         description="Fancy name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="business_name",
 *         in="query",
 *         description="Business name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="address_number",
 *         in="query",
 *         description="Address number",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="address_street",
 *         in="query",
 *         description="Address Street",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="country_id",
 *         in="query",
 *         description="Country ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="province_id",
 *         in="query",
 *         description="Province ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="postal_code",
 *         in="query",
 *         description="Postal code",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cuit",
 *         in="query",
 *         description="CUIT",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="start_date",
 *         in="query",
 *         description="Start date",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="payment_method_id",
 *         in="query",
 *         description="Payment Method Id",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="suterh_code",
 *         in="query",
 *         description="Suterh_code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="siro_code",
 *         in="query",
 *         description="Siro code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="pmc_code",
 *         in="query",
 *         description="PMC code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="ep_code",
 *         in="query",
 *         description="EP code",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="siro_verification_number",
 *         in="query",
 *         description="Siro verification number",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="count_floors",
 *         in="query",
 *         description="Count floors",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="count_functional_units",
 *         in="query",
 *         description="Count functional units",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="type_of_building",
 *         in="query",
 *         description="Type of building",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="state",
 *         in="query",
 *         description="State",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="category",
 *         in="query",
 *         description="Category",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="first_due_date",
 *         in="query",
 *         description="First due date",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortiums overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{id}",
 *     tags={"Consortia"},
 *     summary="Get a consortium info",
 *     description="Returns a consortium info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Consortium not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/consortium/{id}",
 *     tags={"Consortia"},
 *     summary="Delete a consortium",
 *     description="Delete consortium.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortiums deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="COnsortium not found.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/export",
 *     tags={"Consortia"},
 *     summary="Export Consortia list",
 *     description="Export list of Consortia.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Consortia overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * getFunctionalUnits Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{id}/functional-units",
 *     tags={"Consortia"},
 *     summary="Get functional units of the consortium",
 *     description="Returns functional units of the consortium.",
 *     operationId="getFunctionalUnits",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Consortium not found.",
 *     )
 * )
 */

/**
 * getChecks Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{id}/checks",
 *     tags={"Consortia"},
 *     summary="Get checks of the consortium",
 *     description="Returns checks of the consortium.",
 *     operationId="getChecks",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Consortium not found.",
 *     )
 * )
 */

/**
 * getCashbox Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{id}/cashbox",
 *     tags={"Consortia"},
 *     summary="Get cashbox of the consortium",
 *     description="Returns cashbox of the consortium.",
 *     operationId="getCashbox",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Consortium not found.",
 *     )
 * )
 */
