<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/movement",
 *     tags={"Consortium Current Account"},
 *     summary="Get consortium Financial Movements list",
 *     description="Returns list of Notes.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Notes overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/movement/{id}",
 *     tags={"Consortium Current Account"},
 *     summary="Get a consortium Financial Movements info",
 *     description="Returns a consortium Financial Movements info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Note ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Note overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Note not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/movement/paginate",
 *     tags={"Consortium Current Account"},
 *     summary="Get Consortium Financial Movements list with paginate",
 *     description="Returns list of Consortium Financial Movements with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium Financial Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/movement/export",
 *     tags={"Consortium Current Account"},
 *     summary="Export Consortium Financial Movements list",
 *     description="Export list of Consortium Financial Movements.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Consortium Financial Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Balance Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/movement/balance",
 *     tags={"Consortium Current Account"},
 *     summary="Get Consortium Financial Movements balance",
 *     description="Returns balance of Consortium Financial Movements.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium Financial Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
