<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/percentage",
 *     tags={"Consortium Percentages"},
 *     summary="Get consortium Percentages list",
 *     description="Returns list of Notes.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Percentages overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Store Swagger Annotation
 * @OA\Post(
 *     path="/consortium/{consortiumID}/percentage",
 *     tags={"Consortium Percentages"},
 *     summary="Create consortium Percentages",
 *     description="Create consortium Percentages.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="second_line_name",
 *         in="query",
 *         description="Second Line Name",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Description",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="type",
 *         in="query",
 *         description="Type",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             enum={"spendings", "fixed", "forced_fixed"},
 *             default="spendings",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="position",
 *         in="query",
 *         description="Position",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_fixed_amount",
 *         in="query",
 *         description="Last Fixed Amount",
 *         required=false,
 *         @OA\Schema(
 *             type="double"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="particular_percentage",
 *         in="query",
 *         description="Particular Percentage",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_expense_spending",
 *         in="query",
 *         description="Hidden Expense Spending",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_in_proration",
 *         in="query",
 *         description="Hidden in proration",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_percentage_value",
 *         in="query",
 *         description="Hidden percentage value",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="equal_parts",
 *         in="query",
 *         description="Equal Parts",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium Percentage overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/percentage/{percentageId}",
 *     tags={"Consortium Percentages"},
 *     summary="Get a consortium Percentages info",
 *     description="Returns a consortium Percentages info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="percentageId",
 *         in="path",
 *         description="Consortium Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Percentages overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Percentages not found.",
 *     )
 * )
 */

/**
 * Update Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{consortiumID}/percentage/{percentageId}",
 *     tags={"Consortium Percentages"},
 *     summary="Update consortium Percentages",
 *     description="Update consortium Percentages.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="percentageId",
 *         in="path",
 *         description="Consortium Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="second_line_name",
 *         in="query",
 *         description="Second Line Name",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Description",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="type",
 *         in="query",
 *         description="Type",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             enum={"spendings", "fixed", "forced_fixed"},
 *             default="spendings",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="position",
 *         in="query",
 *         description="Position",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_fixed_amount",
 *         in="query",
 *         description="Last Fixed Amount",
 *         required=false,
 *         @OA\Schema(
 *             type="double"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="particular_percentage",
 *         in="query",
 *         description="Particular Percentage",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_expense_spending",
 *         in="query",
 *         description="Hidden Expense Spending",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_in_proration",
 *         in="query",
 *         description="Hidden in proration",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_percentage_value",
 *         in="query",
 *         description="Hidden percentage value",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Percentages overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/consortium/{consortiumID}/percentage/{percentageId}",
 *     tags={"Consortium Percentages"},
 *     summary="Delete a consortium Percentages",
 *     description="Delete consortium Percentages.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="percentageId",
 *         in="path",
 *         description="Consortium Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Percentage deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Percentage not found.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/percentage/export",
 *     tags={"Consortium Percentages"},
 *     summary="Export Consortium Percentages list",
 *     description="Export list of Consortium Percentages.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Export Consortium Percentages overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Duplicate Swagger Annotation
 * @OA\Post(
 *     path="/consortium/{consortiumID}/percentage/{id}/duplicate",
 *     tags={"Consortium Percentages"},
 *     summary="Duplicate consortium Percentages",
 *     description="Duplicate consortium Percentages.",
 *     operationId="duplicate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium Percentage overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * massStore Swagger Annotation
 * @OA\POST(
 *     path="/consortium/{consortiumID}/percentage/batch",
 *     tags={"Consortium Percentages"},
 *     summary="Store a batch of Consortium Percentage.",
 *     description="Store a batch of Consortium Percentage.",
 *     operationId="massStore",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *        required = true,
 *        description = "Store a batch of Consortium Percentage.",
 *        @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                property="consortiumID",
 *                type="integer",
 *                example="1",
 *                description=""
 *             ),
 *             @OA\Property(
 *                property="percentages",
 *                type="array",
 *                example={{
 *                  "name": "Expensa Test 1",
 *                  "second_line_name": "Consequatur et quis dolorum autem. Sit praesentium consequatur enim inventore. Odio ab placeat est.",
 *                  "description": "Mock Turtle in a deep voice, 'What are tarts made of?' Alice asked in a furious passion",
 *                  "type": "spendings",
 *                  "position": "1",
 *                  "last_fixed_amount": "3250",
 *                  "particular_percentage": "false",
 *                  "hidden_expense_spending": "false",
 *                  "hidden_in_proration": "false",
 *                  "hidden_percentage_value": "false",
 *                  "equal_parts": "false",
 *                  "created_at": "2020-09-01 12:15:00",
 *                  "updated_at": "2020-09-01 12:15:00"
 *                }, {
 *                  "name": "",
 *                  "second_line_name": "",
 *                  "description": "",
 *                  "type": "",
 *                  "position": "",
 *                  "last_fixed_amount": "",
 *                  "particular_percentage": "",
 *                  "hidden_expense_spending": "",
 *                  "hidden_in_proration": "",
 *                  "hidden_percentage_value": "",
 *                  "equal_parts": "",
 *                  "created_at": "",
 *                  "updated_at": ""
 *                }},
 *                @OA\Items(
 *                      @OA\Property(
 *                         property="name",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="second_line_name",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="description",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="type",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="position",
 *                         type="number",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="last_fixed_amount",
 *                         type="float",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="particular_percentage",
 *                         type="boolean",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="hidden_expense_spending",
 *                         type="boolean",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="hidden_in_proration",
 *                         type="boolean",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="hidden_percentage_value",
 *                         type="boolean",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="equal_parts",
 *                         type="boolean",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="created_at",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="updated_at",
 *                         type="string",
 *                         example=""
 *                      ),
 *                ),
 *             ),
 *        ),
 *     ),
 *
 *
 *     @OA\Response(
 *        response="200",
 *        description="Successful response",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Not Found.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Unprocessable Entity.",
 *     ),
 * )
 */

/**
 * getFunctionalUnitsPercentages Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/percentage/{percentageId}/functional-units-percentages",
 *     tags={"Consortium Percentages"},
 *     summary="Get a consortium Percentages info",
 *     description="Returns a consortium Percentages info.",
 *     operationId="getFunctionalUnitsPercentages",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="percentageId",
 *         in="path",
 *         description="Consortium Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Percentage not found.",
 *     )
 * )
 */

/**
 * massUpdateFunctionalUnitsPercentages Swagger Annotation
 * @OA\PUT(
 *     path="/consortium/{consortiumID}/percentage/{percentageId}/functional-units-percentages/batch",
 *     tags={"Consortium Percentages"},
 *     summary="Store a batch of Consortium Percentage.",
 *     description="Store a batch of Consortium Percentage.",
 *     operationId="massUpdateFunctionalUnitsPercentages",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *        required = true,
 *        description = "Store a batch of Consortium Percentage.",
 *        @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                property="consortiumID",
 *                type="integer",
 *                example="1",
 *                description=""
 *             ),
 *             @OA\Property(
 *                property="percentageId",
 *                type="integer",
 *                example="51",
 *                description=""
 *             ),
 *             @OA\Property(
 *                property="functional_units",
 *                type="array",
 *                example={{
 *                  "functional_unit_id": 8,
 *                  "percentage_consortium_id": 51,
 *                  "value": 25.50
 *                }, {
 *                  "functional_unit_id": "",
 *                  "percentage_consortium_id": "",
 *                  "value": ""
 *                }},
 *                @OA\Items(
 *                      @OA\Property(
 *                         property="functional_unit_id",
 *                         type="integer",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="percentage_consortium_id",
 *                         type="integer",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="value",
 *                         type="float",
 *                         example=""
 *                      )
 *                ),
 *             ),
 *        ),
 *     ),
 *
 *
 *     @OA\Response(
 *        response="200",
 *        description="Successful response",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Not Found.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description="Unprocessable Entity.",
 *     ),
 * )
 */
