<?php

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{id}/profile",
 *     tags={"Consortium Profiles"},
 *     summary="Update employee profile",
 *     description="Update employee.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="social_work_id",
 *         in="query",
 *         description="Social Work ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="retired",
 *         in="query",
 *         description="Retired",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="adicional_obra_social",
 *         in="query",
 *         description="Adicional Obra Social",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="jornalizado_hours_per_day",
 *         in="query",
 *         description="Jornalizado hours per day",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="entry_date",
 *         in="query",
 *         description="Entry Date",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="departure_date",
 *         in="query",
 *         description="Departure Date",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="antiquity",
 *         in="query",
 *         description="Antiquity",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="active",
 *         in="query",
 *         description="Active",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="afiliado_sindicato",
 *         in="query",
 *         description="Afiliado Sindicato",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="afiliado_f_m_v_d_d",
 *         in="query",
 *         description="afiliado_f_m_v_d_d",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="descuento_c_p_f",
 *         in="query",
 *         description="descuento_c_p_f",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="d_g_d_y_p_c",
 *         in="query",
 *         description="d_g_d_y_p_c",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="calculate_obra_social_by_law",
 *         in="query",
 *         description="calculate_obra_social_by_law",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="extra_hours_50",
 *         in="query",
 *         description="extra_hours_50",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="extra_hours_saturday_50",
 *         in="query",
 *         description="extra_hours_saturday_50",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="extra_hours_saturday_100",
 *         in="query",
 *         description="extra_hours_saturday_100",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="extra_hours_sunday_100",
 *         in="query",
 *         description="extra_hours_sunday_100",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="extra_hours_holiday_100",
 *         in="query",
 *         description="extra_hours_holiday_100",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="law27430",
 *         in="query",
 *         description="law27430",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="employees overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{id}/profile",
 *     tags={"Consortium Profiles"},
 *     summary="Get a employee profile data",
 *     description="Returns a employee profile info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="employee overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="employee not found.",
 *     )
 * )
 */
