<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/adjustments",
 *     tags={"Employee Adjustments"},
 *     summary="Get employee adjustments list",
 *     description="Returns list of employee adjustments.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee adjustments overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/employee/{id}/adjustments",
 *     tags={"Employee Adjustments"},
 *     summary="Create employee adjustments",
 *     description="Create employee adjustments.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                   required={"type", "haber_descuento", "description", "suma_sueldo_jornal", "recurrent", "active"},
 *    				 @OA\Property(property="type",
 *    					type="string",
 *    					example="value|percentage",
 *    					description="Type"
 *    				),
 *    				 @OA\Property(property="haber_descuento",
 *    					type="string",
 *    					example="haber|descuento",
 *    					description="Haber-Descuento"
 *    				),
 *                  @OA\Property(property="description",
 *    					type="string",
 *    					example="Text Example...",
 *    					description="Description"
 *    				),
 *                  @OA\Property(property="value",
 *    					type="string",
 *    					example="100",
 *    					description="Value"
 *    				),
 *                  @OA\Property(property="suma_sueldo_jornal",
 *    					type="boolean",
 *    					description="Suma sueldo jornal"
 *    				),
 *                  @OA\Property(property="recurrent",
 *    					type="boolean",
 *    					description="Recurrent"
 *    				),
 *                  @OA\Property(property="es_remunerativo",
 *    					type="boolean",
 *    					description="Es remunerativo"
 *    				),
 *                  @OA\Property(property="unit",
 *    					type="string",
 *    					description="Unit"
 *    				),
 *                  @OA\Property(property="percentage",
 *    					type="string",
 *    					description="Percentage"
 *    				),
 *                  @OA\Property(property="percentage_sobre_base",
 *    					type="string",
 *    					description="Percentage sobre base"
 *    				),
 *                  @OA\Property(property="active",
 *    					type="boolean",
 *    					description="Active"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Employee adjustments overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/employee/{id}/adjustments/{beneficiary_id}",
 *     tags={"Employee Adjustments"},
 *     summary="Update employee adjustments",
 *     description="Update employee adjustments.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                   required={"type", "haber_descuento", "description", "suma_sueldo_jornal", "recurrent", "active"},
 *    				 @OA\Property(property="type",
 *    					type="string",
 *    					example="value|percentage",
 *    					description="Type"
 *    				),
 *    				 @OA\Property(property="haber_descuento",
 *    					type="string",
 *    					example="haber|descuento",
 *    					description="Haber-Descuento"
 *    				),
 *                  @OA\Property(property="description",
 *    					type="string",
 *    					example="Text Example...",
 *    					description="Description"
 *    				),
 *                  @OA\Property(property="value",
 *    					type="string",
 *    					example="100",
 *    					description="Value"
 *    				),
 *                  @OA\Property(property="suma_sueldo_jornal",
 *    					type="boolean",
 *    					description="Suma sueldo jornal"
 *    				),
 *                  @OA\Property(property="recurrent",
 *    					type="boolean",
 *    					description="Recurrent"
 *    				),
 *                  @OA\Property(property="es_remunerativo",
 *    					type="boolean",
 *    					description="Es remunerativo"
 *    				),
 *                  @OA\Property(property="unit",
 *    					type="string",
 *    					description="Unit"
 *    				),
 *                  @OA\Property(property="percentage",
 *    					type="string",
 *    					description="Percentage"
 *    				),
 *                  @OA\Property(property="percentage_sobre_base",
 *    					type="string",
 *    					description="Percentage sobre base"
 *    				),
 *                  @OA\Property(property="active",
 *    					type="boolean",
 *    					description="Active"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Employee adjustments overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/adjustments/{adjustment_id}",
 *     tags={"Employee Adjustments"},
 *     summary="Get a employee adjustments info",
 *     description="Returns a employee adjustments info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="adjustment_id",
 *         in="path",
 *         description="Adjustment ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee adjustment returned."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Employee adjustments not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/employee/{id}/adjustments/{adjustment_id}",
 *     tags={"Employee Adjustments"},
 *     summary="Delete a employee adjustments",
 *     description="Delete employee adjustments.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="adjustment_id",
 *         in="path",
 *         description="Adjustment Id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee adjustment deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Employee adjustments not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/adjustments/paginate",
 *     tags={"Employee Adjustments"},
 *     summary="Get employee adjustments list with paginate",
 *     description="Returns list of employee adjustments with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee adjustments overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/adjustments/export",
 *     tags={"Employee Adjustments"},
 *     summary="Get employee adjustments export",
 *     description="Returns export of employee adjustments.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee adjustments overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
