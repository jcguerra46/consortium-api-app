<?php

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/employee/{id}/afip",
 *     tags={"Employees AFIP"},
 *     summary="Update employee AFIP data",
 *     description="Update employee.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="conyuge",
 *         in="query",
 *         description="Conyuge",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="seguro_colectivo_vida",
 *         in="query",
 *         description="Seguro Colectivo vida",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="trabajador_convencionado",
 *         in="query",
 *         description="Trabajador Convencionado",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="adicional_obra_sociales",
 *         in="query",
 *         description="Adicional Obra Sociales",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="maternidad",
 *         in="query",
 *         description="Maternidad",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="situacion",
 *         in="query",
 *         description="Situacion",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="condicion",
 *         in="query",
 *         description="Condicion",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="actividad",
 *         in="query",
 *         description="Actividad",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="zona",
 *         in="query",
 *         description="Zona",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="adicional_seguridad_social",
 *         in="query",
 *         description="Adicional Seguridad Social",
 *         required=true,
 *         @OA\Schema(
 *             type="numeric",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="adicionales",
 *         in="query",
 *         description="Adicionales",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cantidad_hijos",
 *         in="query",
 *         description="Cantidad de hijos",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cantidad_adherentes",
 *         in="query",
 *         description="Cantidad de adherentes",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="modalidad_contratacion",
 *         in="query",
 *         description="Modalidad de contratacion",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="provincia_localidad",
 *         in="query",
 *         description="Provincia Localidad",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="siniestrado",
 *         in="query",
 *         description="Siniestrado",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="importe_adicional_obra_social",
 *         in="query",
 *         description="Importe adicional OS",
 *         required=true,
 *         @OA\Schema(
 *             type="numeric",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="revista1",
 *         in="query",
 *         description="Revista 1",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="revista2",
 *         in="query",
 *         description="Revista 2",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="revista3",
 *         in="query",
 *         description="Revista 3",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="premios",
 *         in="query",
 *         description="Premios",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="conceptos_no_remunerativos",
 *         in="query",
 *         description="Conceptos No Remunerativos",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="rectificacion_remuneracion",
 *         in="query",
 *         description="Rectificación Remuneración",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="contribucion_tarea_diferencial",
 *         in="query",
 *         description="Contribucion tarea diferencial",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="employees overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/afip",
 *     tags={"Employees AFIP"},
 *     summary="Get a employee AFIP data",
 *     description="Returns a employee AFIP info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="employee overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="employee not found.",
 *     )
 * )
 */
