<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/beneficiaries",
 *     tags={"Employee Beneficiaries"},
 *     summary="Get employee beneficiaries list",
 *     description="Returns list of employee beneficiaries.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee beneficiaries overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/employee/{id}/beneficiaries",
 *     tags={"Employee Beneficiaries"},
 *     summary="Create employee beneficiaries",
 *     description="Create employee beneficiaries.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cuil",
 *         in="query",
 *         description="Cuil",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="first_name",
 *         in="query",
 *         description="First name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_name",
 *         in="query",
 *         description="Last name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="document_type",
 *         in="query",
 *         description="Document type",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             enum={"le", "lc", "dni"},
 *             default="dni",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="dni",
 *         in="query",
 *         description="DNI",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="relationship",
 *         in="query",
 *         description="Relationship",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="porcentage",
 *         in="query",
 *         description="Porcentage",
 *         required=true,
 *         @OA\Schema(
 *             type="float",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee beneficiaries overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/employee/{id}/beneficiaries/{beneficiary_id}",
 *     tags={"Employee Beneficiaries"},
 *     summary="Update employee beneficiaries",
 *     description="Update employee beneficiaries.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="beneficiary_id",
 *         in="path",
 *         description="Beneficiary id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="cuil",
 *         in="query",
 *         description="Cuil",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="first_name",
 *         in="query",
 *         description="First name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_name",
 *         in="query",
 *         description="Last name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="document_type",
 *         in="query",
 *         description="Document type",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             enum={"le", "lc", "dni"},
 *             default="dni",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="dni",
 *         in="query",
 *         description="DNI",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="relationship",
 *         in="query",
 *         description="Relationship",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="porcentage",
 *         in="query",
 *         description="Porcentage",
 *         required=true,
 *         @OA\Schema(
 *             type="float",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee beneficiaries overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/beneficiaries/{beneficiary_id}",
 *     tags={"Employee Beneficiaries"},
 *     summary="Get a employee beneficiaries info",
 *     description="Returns a employee beneficiaries info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="beneficiary_id",
 *         in="path",
 *         description="Beneficiary id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee beneficiaries overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Employee beneficiaries not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/employee/{id}/beneficiaries/{beneficiary_id}",
 *     tags={"Employee Beneficiaries"},
 *     summary="Delete a employee beneficiaries",
 *     description="Delete employee beneficiaries.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="beneficiary_id",
 *         in="path",
 *         description="Beneficiary id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee beneficiaries deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Employee beneficiaries not found.",
 *     )
 * )
 */


/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/employee/{id}/beneficiaries/paginate",
 *     tags={"Employee Beneficiaries"},
 *     summary="Get employee beneficiaries list with paginate",
 *     description="Returns list of employee beneficiaries with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Employee id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Employee beneficiaries overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
