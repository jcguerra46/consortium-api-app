<?php

/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{id}/contact",
 *     tags={"Functional Units Contacts"},
 *     summary="Get Functional Units Contacts list",
 *     description="Returns list of Functional Units Contacts.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Functional Units Contacts overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/functional-unit/{id}/contact",
 *     tags={"Functional Units Contacts"},
 *     summary="Create Functional Unit Contact",
 *     description="Create Functional Unit Contact.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="contact_id",
 *         in="query",
 *         description="Contact ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="contact_type",
 *         in="query",
 *         description="Contact Type",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             enum={"PROPIETARIO", "INQUILINO", "OTRO"},
 *             default="PROPIETARIO",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="primary",
 *         in="query",
 *         description="Primary",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium Note overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/functional-unit/{id}/contact/{functionalUnitContactId}",
 *     tags={"Functional Units Contacts"},
 *     summary="Update Functional Unit Contact",
 *     description="Update Functional Unit Contact.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="functionalUnitContactId",
 *         in="path",
 *         description="Functional Unit Contact ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="contact_id",
 *         in="query",
 *         description="Contact ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="contact_type",
 *         in="query",
 *         description="Contact Type",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             enum={"PROPIETARIO", "INQUILINO", "OTRO"},
 *             default="PROPIETARIO",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="primary",
 *         in="query",
 *         description="Primary",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Consortium Note overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{id}/contact/{functionalUnitContactId}",
 *     tags={"Functional Units Contacts"},
 *     summary="Get a Functional Unit Contact info",
 *     description="Returns a Functional Unit Contact info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="functionalUnitContactId",
 *         in="path",
 *         description="Functional Unit Contact ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Contact overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional Unit Contact not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/functional-unit/{id}/contact/{functionalUnitContactId}",
 *     tags={"Functional Units Contacts"},
 *     summary="Delete a Functional Unit Contact",
 *     description="Delete Functional Unit Contact.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="functionalUnitContactId",
 *         in="path",
 *         description="Functional Unit Contact ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Contact deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional Unit Contact not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{id}/contact/paginate",
 *     tags={"Functional Units Contacts"},
 *     summary="Get Functional Units Contacts list with paginate",
 *     description="Returns list of Functional Units Contacts with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Contact overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
