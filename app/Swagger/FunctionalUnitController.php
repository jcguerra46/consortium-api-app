<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/functional-units",
 *     tags={"Functional Units"},
 *     summary="Get functional units list",
 *     description="Returns list of Functional Units.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Functional Units overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Store Swagger Annotation
 * @OA\Post(
 *     path="/functional-units",
 *     tags={"Functional Units"},
 *     summary="Create functional unit",
 *     description="Create functional unit.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortium_id",
 *         in="query",
 *         description="Consortium id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="package_id",
 *         in="query",
 *         description="Package id",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="floor",
 *         in="query",
 *         description="Floor",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="department",
 *         in="query",
 *         description="Department",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="number",
 *         in="query",
 *         description="Number",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="type_forgive_interest",
 *         in="query",
 *         description="Type forgive interest",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="legal_state",
 *         in="query",
 *         description="Legal state",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="m2",
 *         in="query",
 *         description="CUIT",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="order",
 *         in="query",
 *         description="Order",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional unit overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Update Swagger Annotation
 * @OA\Put(
 *     path="/functional-units/{id}",
 *     tags={"Functional Units"},
 *     summary="Update Functional Unit",
 *     description="Update functional unit.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional unit id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortium_id",
 *         in="query",
 *         description="Consortium id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="package_id",
 *         in="query",
 *         description="Package id",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="floor",
 *         in="query",
 *         description="Floor",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="department",
 *         in="query",
 *         description="Department",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="number",
 *         in="query",
 *         description="Number",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="type_forgive_interest",
 *         in="query",
 *         description="Type forgive interest",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="legal_state",
 *         in="query",
 *         description="Legal state",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="m2",
 *         in="query",
 *         description="CUIT",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="order",
 *         in="query",
 *         description="Order",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional unit overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/functional-units/{id}",
 *     tags={"Functional Units"},
 *     summary="Get a functional unit info",
 *     description="Returns a functional unit info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional unit overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional unit not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/functional-units/{id}",
 *     tags={"Functional Units"},
 *     summary="Delete a functional unit",
 *     description="Delete functional unit.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional unit deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional unit not found.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/functional-units/export",
 *     tags={"Functional Units"},
 *     summary="Export Functional Units list",
 *     description="Export list of Functional Units.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Functional Units overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Store a batch Swagger Annotation
 * @OA\POST(
 *     path="/functional-units/batch",
 *     tags={"Functional Units"},
 *     summary="Create a batch of Functional Units.",
 *     description="Create a batch of Functional Units.",
 *     operationId="createBatchOfFunctionalUnits",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *        required = true,
 *        description = "Create a batch of Functional Units.",
 *        @OA\JsonContent(
 *             type="object",
 *             @OA\Property(
 *                property="consortium_id",
 *                type="integer",
 *                example="1",
 *                description=""
 *             ),
 *             @OA\Property(
 *                property="functional_units",
 *                type="array",
 *                example={{
 *                  "package_id": 1,
 *                  "functional_unit_type_id": 1,
 *                  "owner_first_name": "John",
 *                  "owner_last_name": "War",
 *                  "floor": "5",
 *                  "functional_unit_number": "5E",
 *                  "initial_balance": "8080",
 *                  "type_forgive_interest": "until_further_notice",
 *                  "legal_state": "trial",
 *                  "m2": "808",
 *                  "order": "1",
 *                  "created_at": "2020-07-15 12:15:00",
 *                  "updated_at": "2020-07-15 12:15:00"
 *                }, {
 *                  "package_id": "",
 *                  "functional_unit_type_id": "",
 *                  "owner_first_name": "",
 *                  "owner_last_name": "",
 *                  "floor": "",
 *                  "functional_unit_number": "",
 *                  "initial_balance": "",
 *                  "type_forgive_interest": "",
 *                  "legal_state": "",
 *                  "m2": "",
 *                  "order": "",
 *                  "created_at": "",
 *                  "updated_at": ""
 *                }},
 *                @OA\Items(
 *                      @OA\Property(
 *                         property="firstName",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="lastName",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="companyId",
 *                         type="string",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="accountNumber",
 *                         type="number",
 *                         example=""
 *                      ),
 *                      @OA\Property(
 *                         property="netPay",
 *                         type="money",
 *                         example=""
 *                      ),
 *                ),
 *             ),
 *        ),
 *     ),
 *
 *
 *     @OA\Response(
 *        response="200",
 *        description="Successful response",
 *     ),
 * )
 */
