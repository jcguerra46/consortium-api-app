<?php

/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/movement",
 *     tags={"Functional Units Current Account"},
 *     summary="Get Functional Units Movements list",
 *     description="Returns list of Functional Units Movements.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="functionalUnitId",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Units Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/movement/{id}",
 *     tags={"Functional Units Current Account"},
 *     summary="Get a Functional Unit Percentage info",
 *     description="Returns a Functional Unit Percentage info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="functionalUnitId",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional Unit Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional Unit Percentage not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/movement/paginate",
 *     tags={"Functional Units Current Account"},
 *     summary="Get Functional Units Movements list with paginate",
 *     description="Returns list of Functional Units Movements with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/movement/export",
 *     tags={"Functional Units Current Account"},
 *     summary="Get Functional Units Movements list export file",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="functionalUnitId",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Balance Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/movement/balance",
 *     tags={"Functional Units Current Account"},
 *     summary="Get Functional Units Movements balance",
 *     description="Returns balance of Functional Units Movements.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="functionalUnitId",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Units Movements balance."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */
