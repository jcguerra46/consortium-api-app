<?php

/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/percentage",
 *     tags={"Functional Units Percentages"},
 *     summary="Get Functional Units Percentages list",
 *     description="Returns list of Functional Units Percentages.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="functionalUnitId",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Units Percentages overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/functional-unit/{functionalUnitId}/percentage/{id}",
 *     tags={"Functional Units Percentages"},
 *     summary="Update Functional Unit Percentage",
 *     description="Update Functional Unit Percentage.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="functionalUnitId",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional Unit Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="value",
 *         in="query",
 *         description="Value",
 *         required=true,
 *         @OA\Schema(
 *             type="number"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/percentage/{id}",
 *     tags={"Functional Units Percentages"},
 *     summary="Get a Functional Unit Percentage info",
 *     description="Returns a Functional Unit Percentage info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="functionalUnitId",
 *         in="path",
 *         description="Functional Unit ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional Unit Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional Unit Percentage not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit/{functionalUnitId}/percentage/paginate",
 *     tags={"Functional Units Percentages"},
 *     summary="Get Functional Units Percentages list with paginate",
 *     description="Returns list of Functional Units Percentages with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
