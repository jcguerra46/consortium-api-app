<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit-types",
 *     tags={"Functional Unit Types"},
 *     summary="Get Functional Unit Types list",
 *     description="Returns list of Functional Unit Types.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Types overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/functional-unit-types",
 *     tags={"Functional Unit Types"},
 *     summary="Create Functional Unit Type",
 *     description="Create Functional Unit Type.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Description",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="active",
 *         in="query",
 *         description="Active",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             enum={"true", "false"},
 *             default="true",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Types overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/functional-unit-types/{id}",
 *     tags={"Functional Unit Types"},
 *     summary="Update Functional Unit Type",
 *     description="Update Functional Unit Type.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional Unit Type id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Description",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="active",
 *         in="query",
 *         description="Active",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             enum={"true", "false"},
 *             default="true",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Type overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit-types/{id}",
 *     tags={"Functional Unit Types"},
 *     summary="Get a Functional Unit Type info",
 *     description="Returns a Functional Unit Type info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional Unit Type ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Type overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional Unit Type not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/functional-unit-types/{id}",
 *     tags={"Functional Unit Types"},
 *     summary="Delete a Functional Unit Type",
 *     description="Delete Functional Unit Type.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Functional Unit Type ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Type deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Functional Unit Type not found.",
 *     )
 * )
 */


/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit-types/paginate",
 *     tags={"Functional Unit Types"},
 *     summary="Get Functional Unit Types list with paginate",
 *     description="Returns list of Functional Unit Types with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Functional Unit Types overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/functional-unit-types/export",
 *     tags={"Functional Unit Types"},
 *     summary="Export Functional Unit Types list",
 *     description="Export list of Functional Unit Types.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Functional Unit Types overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
