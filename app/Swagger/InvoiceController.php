<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/invoice",
 *     tags={"Invoices"},
 *     summary="Get invoices list",
 *     description="Returns list of Invoices.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Invoices overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/invoice",
 *     tags={"Invoices"},
 *     summary="Create invoices",
 *     description="Create invoices.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="provider_id",
 *    					type="integer",
 *    					example="1",
 *    					description="Provider ID"
 *    				),
 *    				 @OA\Property(property="invoice_number",
 *    					type="string",
 *    					example="2213",
 *    					description="Invoice Number"
 *    				),
 *                  @OA\Property(property="description",
 *    					type="string",
 *    					example="Text Example...",
 *    					description="Description"
 *    				),
 *                  @OA\Property(property="date",
 *    					type="string",
 *    					example="20-12-2010",
 *    					description="Date"
 *    				),
 *                  @OA\Property(property="amount",
 *    					type="numeric",
 *    					example="199.99",
 *    					description="Amount"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Invoices overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/invoice/{id}",
 *     tags={"Invoices"},
 *     summary="Update invoices",
 *     description="Update invoices.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Invoice id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="provider_id",
 *    					type="integer",
 *    					example="1",
 *    					description="Provider ID"
 *    				),
 *    				 @OA\Property(property="invoice_number",
 *    					type="string",
 *    					example="2213",
 *    					description="Invoice Number"
 *    				),
 *                  @OA\Property(property="description",
 *    					type="string",
 *    					example="Text Example...",
 *    					description="Description"
 *    				),
 *                  @OA\Property(property="date",
 *    					type="string",
 *    					example="20-12-2010",
 *    					description="Date"
 *    				),
 *                  @OA\Property(property="amount",
 *    					type="numeric",
 *    					example="199.99",
 *    					description="Amount"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Invoices overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/invoice/{id}",
 *     tags={"Invoices"},
 *     summary="Get a invoices info",
 *     description="Returns a invoices info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Invoice ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Invoice overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Invoice not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/invoice/{id}",
 *     tags={"Invoices"},
 *     summary="Delete a invoices",
 *     description="Delete invoices.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Invoice ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Invoices deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Invoice not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/invoice/paginate",
 *     tags={"Invoices"},
 *     summary="Get Invoices list with paginate",
 *     description="Returns list of Invoices with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Invoices overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/invoice/export",
 *     tags={"Invoices"},
 *     summary="Export Invoices list",
 *     description="Export list of Invoices.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Invoices overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
