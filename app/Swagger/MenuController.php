<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/menus",
 *     tags={"Menus"},
 *     summary="Get menu",
 *     description="Returns menu.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Menus overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/menus",
 *     tags={"Menus"},
 *     summary="Update menu",
 *     description="Update menu.",
 *     operationId="setUserMenu",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="menu",
 *         in="query",
 *         description="Menu",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Menu overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/menus/default",
 *     tags={"Menus"},
 *     summary="Get menu default",
 *     description="Returns menu default.",
 *     operationId="getMenuDefault",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Menu default overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/menus/default",
 *     tags={"Menus"},
 *     summary="Update menu default",
 *     description="Update menu default.",
 *     operationId="setMenuDefault",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Menu default overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
