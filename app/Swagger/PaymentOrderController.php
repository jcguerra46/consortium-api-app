<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/payment-order",
 *     tags={"Payment Orders"},
 *     summary="Get payment orders list",
 *     description="Returns list of Payment Orders.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/payment-order",
 *     tags={"Payment Orders"},
 *     summary="Create payment orders",
 *     description="Create payment orders.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"provider_id", "description", "invoices"},
 *    				 @OA\Property(property="provider_id",
 *    					type="integer",
 *    					example="1",
 *    					description="Provider ID"
 *    				),
 *    				 @OA\Property(property="description",
 *    					type="string",
 *    					example="Example...",
 *    					description="Payment Order description"
 *    				),
 *                  @OA\Property(property="invoices",
 *    					type="array",
 *    					example="Text Example...",
 *    					description="Description",
 *                      @OA\Items(type="integer")
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/payment-order/{id}",
 *     tags={"Payment Orders"},
 *     summary="Update payment orders",
 *     description="Update payment orders.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"description", "invoices"},
 *    				 @OA\Property(property="description",
 *    					type="string",
 *    					example="Example...",
 *    					description="Payment Order description"
 *    				),
 *                  @OA\Property(property="invoices",
 *    					type="array",
 *    					example="Text Example...",
 *    					description="Description",
 *                      @OA\Items(type="integer")
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/payment-order/{id}",
 *     tags={"Payment Orders"},
 *     summary="Get a payment orders info",
 *     description="Returns a payment orders info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Order overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Payment Order not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/payment-order/{id}",
 *     tags={"Payment Orders"},
 *     summary="Delete a payment orders",
 *     description="Delete payment orders.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Payment Order not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/payment-order/paginate",
 *     tags={"Payment Orders"},
 *     summary="Get Payment Orders list with paginate",
 *     description="Returns list of Payment Orders with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/payment-order/export",
 *     tags={"Payment Orders"},
 *     summary="Export Payment Orders list",
 *     description="Export list of Payment Orders.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Duplicate Swagger Annotation
 * @OA\Post(
 *     path="/payment-order/{id}/duplicate",
 *     tags={"Payment Orders"},
 *     summary="Duplicate payment orders",
 *     description="Duplicate payment orders.",
 *     operationId="duplicate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Annular Swagger Annotation
 * @OA\Put(
 *     path="/payment-order/{id}/annular",
 *     tags={"Payment Orders"},
 *     summary="Annular payment orders",
 *     description="Annular payment orders.",
 *     operationId="annular",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Print Swagger Annotation
 * @OA\Get(
 *     path="/payment-order/{id}/print",
 *     tags={"Payment Orders"},
 *     summary="Print payment orders",
 *     description="Print in HTML payment order.",
 *     operationId="printOrder",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Print Swagger Annotation
 * @OA\Post(
 *     path="/payment-order/{id}/do-payment",
 *     tags={"Payment Orders"},
 *     summary="Do payment of payment order",
 *     description="Creates necessary movements to complete the payment of a payment order",
 *     operationId="doPayment",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment Orders overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
