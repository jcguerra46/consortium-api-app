<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/payments",
 *     tags={"Payments"},
 *     summary="Get payment  list",
 *     description="Returns list of Payment Orders.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="provider_id",
 *         in="query",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/payments",
 *     tags={"Payments"},
 *     summary="Create payment ",
 *     description="Create payment .",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *                mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"provider_id", "description", "invoices"},
 *    				 @OA\Property(property="provider_id",
 *                        type="integer",
 *                        example="1",
 *                        description="Provider ID"
 *                    ),
 *    				 @OA\Property(property="description",
 *                        type="string",
 *                        example="Example...",
 *                        description="Payment Order description"
 *                    ),
 *                  @OA\Property(property="invoices",
 *                        type="array",
 *                        example="Text Example...",
 *                        description="Description",
 *                        @OA\Items(type="integer")
 *                    ),
 *                ),
 *            ),
 *        ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Create Direct Swagger Annotation
 * @OA\Post(
 *     path="/payments/direct",
 *     tags={"Payments"},
 *     summary="Create payment direct",
 *     description="Create payment .",
 *     operationId="createDirect",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *                mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"spending_installment_id", "description", "invoices"},
 *    				 @OA\Property(property="spending_installment_id",
 *                        type="integer",
 *                        example="1",
 *                        description="Spending Installment ID"
 *                    ),
 *    				 @OA\Property(property="date",
 *                        type="string",
 *                        example="10-12-2000",
 *                        description="Date"
 *                    ),
 *                  @OA\Property(property="cash_amount",
 *                        type="numeric",
 *                        example="500",
 *                        description="Cash Amount",
 *                    ),
 *                  @OA\Property(property="checks",
 *                        type="array",
 *                        description="Checks",
 *                        @OA\Items(type="integer")
 *                    ),
 *                  @OA\Property(property="transfers",
 *                        type="array",
 *                        description="Transfers",
 *                        @OA\Items(type="integer")
 *                    ),
 *                  @OA\Property(property="total_amount",
 *                        type="numeric",
 *                        example="500",
 *                        description="Total Amount",
 *                    ),
 *                ),
 *            ),
 *        ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/payments/{id}",
 *     tags={"Payments"},
 *     summary="Get a payment  info",
 *     description="Returns a payment  info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Payment Order ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Payment not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/payments/paginate",
 *     tags={"Payments"},
 *     summary="Get Payment list with paginate",
 *     description="Returns list of Payment with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Payment overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/payments/export",
 *     tags={"Payments"},
 *     summary="Export Payment list",
 *     description="Export list of Payment Orders.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="provider_id",
 *         in="query",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Export Payment overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

