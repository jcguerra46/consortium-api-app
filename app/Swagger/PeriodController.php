<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/period",
 *     tags={"Periods"},
 *     summary="Get periods list",
 *     description="Returns list of Periods.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Periods overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{consortiumID}/period/{id}",
 *     tags={"Periods"},
 *     summary="Update periods",
 *     description="Update periods.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Period ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="name",
 *         in="query",
 *         description="Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="second_line_name",
 *         in="query",
 *         description="Second Line Name",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Description",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="type",
 *         in="query",
 *         description="Type",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="position",
 *         in="query",
 *         description="Position",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_fixed_amount",
 *         in="query",
 *         description="Last Fixed Amount",
 *         required=false,
 *         @OA\Schema(
 *             type="double"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="particular_period",
 *         in="query",
 *         description="Particular Percentage",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_expense_spending",
 *         in="query",
 *         description="Hidden Expense Spending",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_in_proration",
 *         in="query",
 *         description="Hidden in proration",
 *         required=true,
 *         @OA\Schema(
 *             type="double"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="hidden_period_value",
 *         in="query",
 *         description="Hidden period value",
 *         required=true,
 *         @OA\Schema(
 *             type="double"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Periods overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/period/{id}",
 *     tags={"Periods"},
 *     summary="Get a periods info",
 *     description="Returns a periods info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Period ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Note overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Note not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/period/paginate",
 *     tags={"Periods"},
 *     summary="Get Periods list with paginate",
 *     description="Returns list of Periods with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Periods overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/period/export",
 *     tags={"Periods"},
 *     summary="Export Periods list",
 *     description="Export list of Periods.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Periods overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Get Open Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumID}/period/open",
 *     tags={"Periods"},
 *     summary="Get open period from Consortium",
 *     description="Returns a open period info.",
 *     operationId="getOpenPeriod",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumID",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Open Period overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 * )
 */
