<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/provider",
 *     tags={"Providers"},
 *     summary="Get providers list",
 *     description="Returns list of Providers.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Providers overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/provider",
 *     tags={"Providers"},
 *     summary="Create providers",
 *     description="Create providers.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"business_name", "address", "cuit"},
 *    				 @OA\Property(property="business_name",
 *    					type="string",
 *    					example="Example..",
 *    					description="Business name"
 *    				),
 *    				 @OA\Property(property="name",
 *    					type="string",
 *    					example="Example",
 *    					description="Name"
 *    				),
 *                  @OA\Property(property="address",
 *    					type="string",
 *    					example="Example",
 *    					description="address"
 *    				),
 *                  @OA\Property(property="location",
 *    					type="string",
 *    					example="Buenos Aires",
 *    					description="location"
 *    				),
 *                  @OA\Property(property="province_id",
 *    					type="integer",
 *    					description="Province ID"
 *    				),
 *                  @OA\Property(property="postal_code",
 *    					type="string",
 *    					description="Postal Code"
 *    				),
 *                  @OA\Property(property="cuit",
 *    					type="string",
 *    					description="CUIT"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Providers overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/provider/{id}",
 *     tags={"Providers"},
 *     summary="Update providers",
 *     description="Update providers.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Provider id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"business_name", "address", "cuit"},
 *    				 @OA\Property(property="business_name",
 *    					type="string",
 *    					example="Example..",
 *    					description="Business name"
 *    				),
 *    				 @OA\Property(property="name",
 *    					type="string",
 *    					example="Example",
 *    					description="Name"
 *    				),
 *                  @OA\Property(property="address",
 *    					type="string",
 *    					example="Example",
 *    					description="address"
 *    				),
 *                  @OA\Property(property="location",
 *    					type="string",
 *    					example="Buenos Aires",
 *    					description="location"
 *    				),
 *                  @OA\Property(property="province_id",
 *    					type="integer",
 *    					description="Province ID"
 *    				),
 *                  @OA\Property(property="postal_code",
 *    					type="string",
 *    					description="Postal Code"
 *    				),
 *                  @OA\Property(property="cuit",
 *    					type="string",
 *    					description="CUIT"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Providers overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/provider/{id}",
 *     tags={"Providers"},
 *     summary="Get a providers info",
 *     description="Returns a providers info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Provider overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Provider not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/provider/{id}",
 *     tags={"Providers"},
 *     summary="Delete a providers",
 *     description="Delete providers.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Providers deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="COnsortium not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/provider/paginate",
 *     tags={"Providers"},
 *     summary="Get Providers list with paginate",
 *     description="Returns list of Providers with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Providers overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/provider/export",
 *     tags={"Providers"},
 *     summary="Export Providers list",
 *     description="Export list of Providers.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Providers overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
