<?php

/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/provider/{providerId}/movement",
 *     tags={"Providers Current Account"},
 *     summary="Get Providers Movements list",
 *     description="Returns list of Providers Movements.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="providerId",
 *         in="path",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Providers Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/provider/{providerId}/movement/{id}",
 *     tags={"Providers Current Account"},
 *     summary="Get a Provider Percentage info",
 *     description="Returns a Provider Movement info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="providerId",
 *         in="path",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Provider Percentage ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Provider Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Provider Percentage not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/provider/{providerId}/movement/paginate",
 *     tags={"Providers Current Account"},
 *     summary="Get Providers Movements list with paginate",
 *     description="Returns list of Providers Movements with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Provider Percentage overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/provider/{providerId}/movement/export",
 *     tags={"Providers Current Account"},
 *     summary="Get Providers Movements list export file",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="providerId",
 *         in="path",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Provider Movements overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Balance Swagger Annotation
 * @OA\Get(
 *     path="/provider/{providerId}/movement/balance",
 *     tags={"Providers Current Account"},
 *     summary="Get Providers Movements balance",
 *     description="Returns balance of Providers Movements.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="providerId",
 *         in="path",
 *         description="Provider ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Providers Movements balance."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */
