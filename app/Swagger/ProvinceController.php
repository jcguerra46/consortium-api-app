<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/countries/{id}/provinces",
 *     tags={"Provinces"},
 *     summary="Get provinces list",
 *     description="Returns list of provinces.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="country id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Provinces overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/countries/{id}/provinces/{province_id}",
 *     tags={"Provinces"},
 *     summary="Get a province info",
 *     description="Returns a province info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Country ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="province_id",
 *         in="path",
 *         description="Province id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="province overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Province not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/countries/{id}/provinces/paginate",
 *     tags={"Provinces"},
 *     summary="Get provinces list with paginate",
 *     description="Returns list of provinces with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Province id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Provinces overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
