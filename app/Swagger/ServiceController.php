<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/service",
 *     tags={"Services"},
 *     summary="Get services list",
 *     description="Returns list of Services.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Services overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/service",
 *     tags={"Services"},
 *     summary="Create services",
 *     description="Create services.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Description",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Services overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/service/{id}",
 *     tags={"Services"},
 *     summary="Update services",
 *     description="Update services.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Spending Description id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="description",
 *         in="query",
 *         description="Description",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Services overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/service/{id}",
 *     tags={"Services"},
 *     summary="Get a services info",
 *     description="Returns a services info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Spending Description ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Spending Description overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Spending Description not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/service/{id}",
 *     tags={"Services"},
 *     summary="Delete a services",
 *     description="Delete services.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Spending Description ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Services deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="COnsortium not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/service/paginate",
 *     tags={"Services"},
 *     summary="Get Services list with paginate",
 *     description="Returns list of Services with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Services overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/service/export",
 *     tags={"Services"},
 *     summary="Export Services list",
 *     description="Export list of Services.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Services overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
