<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/spending",
 *     tags={"Spendings"},
 *     summary="Get spending list",
 *     description="Returns list of Spendings.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Spendings overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/spending",
 *     tags={"Spendings"},
 *     summary="Create spending",
 *     description="Create spending.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *    				 @OA\Property(property="amount",
 *    					type="number",
 *    					example="500",
 *    					description="Amount"
 *    				),
 *    				 @OA\Property(property="date",
 *    					type="string",
 *    					example="15-12-2010",
 *    					description="Date"
 *    				),
 *                  @OA\Property(property="description",
 *    					type="string",
 *    					example="Example",
 *    					description="Description"
 *    				),
 *                  @OA\Property(property="prevision",
 *    					type="boolean",
 *    					description="Prevision"
 *    				),
 *                  @OA\Property(property="affect_financial_section",
 *    					type="boolean",
 *    					description="Affect financial section"
 *    				),
 *                  @OA\Property(property="prorate_in_expenses",
 *    					type="boolean",
 *    					description="Prorate in expenses"
 *    				),
 *                  @OA\Property(property="category_id",
 *    					type="integer",
 *    					description="Category ID"
 *    				),
 *                  @OA\Property(property="provider_id",
 *    					type="integer",
 *    					description="Provider ID"
 *    				),
 *                  @OA\Property(property="consortium_id",
 *    					type="integer",
 *    					description="Consortium ID"
 *    				),
 *                  @OA\Property(property="functional_unit_id",
 *    					type="integer",
 *    					description="Functional Unit ID"
 *    				),
 *                  @OA\Property(property="installments",
 *    					type="array",
 *    					description="Installments",
 *                      @OA\Items(type="string")
 *    				),
 *                  @OA\Property(property="percentages",
 *    					type="array",
 *    					description="Percentages",
 *                      @OA\Items(type="string")
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Consortiums overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/spending/{id}",
 *     tags={"Spendings"},
 *     summary="Get a spending info",
 *     description="Returns a spending info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Spending ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Spending overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Spending not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/spending/paginate",
 *     tags={"Spendings"},
 *     summary="Get Spendings list with paginate",
 *     description="Returns list of Spendings with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Spendings overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/spending/export",
 *     tags={"Spendings"},
 *     summary="Export Spendings list",
 *     description="Export list of Spendings.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export Spendings overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
