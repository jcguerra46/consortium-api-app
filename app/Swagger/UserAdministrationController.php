<?php

/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/user-administrations",
 *     tags={"User Administrations"},
 *     summary="Get user administrations list",
 *     description="Returns list of User Administrations.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="User Administrations overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/user-administrations",
 *     tags={"User Administrations"},
 *     summary="Create user administration",
 *     description="Create user administration.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="principal",
 *         in="query",
 *         description="Principal",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=true,
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="first_name",
 *         in="query",
 *         description="First Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_name",
 *         in="query",
 *         description="Last Name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="email",
 *         in="query",
 *         description="Email",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password",
 *         in="query",
 *         description="Password",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password_confirmation",
 *         in="query",
 *         description="Password Confirmation",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *      *     @OA\Parameter(
 *         name="active",
 *         in="query",
 *         description="Active",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=true,
 *             format=""
 *         )
 *     ),
 *      *     @OA\Parameter(
 *         name="menu",
 *         in="query",
 *         description="Menu",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User Administration overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Update Swagger Annotation
 * @OA\Put(
 *     path="/user-administrations",
 *     tags={"User Administrations"},
 *     summary="Update user administration",
 *     description="Update user administration.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="principal",
 *         in="query",
 *         description="Principal",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=true,
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="first_name",
 *         in="query",
 *         description="First Name",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="last_name",
 *         in="query",
 *         description="Last Name",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="email",
 *         in="query",
 *         description="Email",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password",
 *         in="query",
 *         description="Password",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *      *     @OA\Parameter(
 *         name="active",
 *         in="query",
 *         description="Active",
 *         required=false,
 *         @OA\Schema(
 *             type="boolean",
 *             enum={true, false},
 *             default=true,
 *             format=""
 *         )
 *     ),
 *      *     @OA\Parameter(
 *         name="menu",
 *         in="query",
 *         description="Menu",
 *         required=false,
 *         @OA\Schema(
 *             type="string",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User Administration overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/user-administrations/{id}",
 *     tags={"User Administrations"},
 *     summary="Get a user administration info",
 *     description="Returns a user administration info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="User Administration ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User Administration overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="User Administration not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/user-administrations/{id}",
 *     tags={"User Administrations"},
 *     summary="Delete a user administration",
 *     description="Delete user administration.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="User Administration ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User Administration deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="User Administration not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/user-administrations/paginate",
 *     tags={"User Administrations"},
 *     summary="Get user administrations list with paginate",
 *     description="Returns list of user administrations with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="User Administrations overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/user-administrations/export",
 *     tags={"User Administrations"},
 *     summary="Export user administrations list",
 *     description="Export list of user administrations.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Response(
 *         response=200,
 *         description="Export User Administrations overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
