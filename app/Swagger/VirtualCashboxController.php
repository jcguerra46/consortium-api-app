<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox",
 *     tags={"Virtual Cashboxes"},
 *     summary="Get virtual cashbox list",
 *     description="Returns list of Virtual Cashboxes.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/consortium/{consortiumId}/virtual-cashbox",
 *     tags={"Virtual Cashboxes"},
 *     summary="Create virtual cashbox",
 *     description="Create virtual cashbox.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortium_id",
 *         in="query",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="date",
 *         in="query",
 *         description="Date",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="time",
 *         in="query",
 *         description="Time",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="second_time",
 *         in="query",
 *         description="Second Time",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="place",
 *         in="query",
 *         description="Place",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="issues",
 *         in="query",
 *         description="Issues",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="send",
 *         in="query",
 *         description="Send",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{id}",
 *     tags={"Virtual Cashboxes"},
 *     summary="Update virtual cashbox",
 *     description="Update virtual cashbox.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Virtual Cashbox id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortium_id",
 *         in="query",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="date",
 *         in="query",
 *         description="Date",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="time",
 *         in="query",
 *         description="Time",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="second_time",
 *         in="query",
 *         description="Second Time",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="place",
 *         in="query",
 *         description="Place",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="issues",
 *         in="query",
 *         description="Issues",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="send",
 *         in="query",
 *         description="Send",
 *         required=true,
 *         @OA\Schema(
 *             type="boolean",
 *             format=""
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{id}",
 *     tags={"Virtual Cashboxes"},
 *     summary="Get a virtual cashbox info",
 *     description="Returns a virtual cashbox info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Virtual Cashbox ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashbox overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Virtual Cashbox not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{id}",
 *     tags={"Virtual Cashboxes"},
 *     summary="Delete a virtual cashbox",
 *     description="Delete virtual cashbox.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Virtual Cashbox ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="COnsortium not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox/paginate",
 *     tags={"Virtual Cashboxes"},
 *     summary="Get Virtual Cashboxes list with paginate",
 *     description="Returns list of Virtual Cashboxes with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox/export",
 *     tags={"Virtual Cashboxes"},
 *     summary="Export Virtual Cashboxes list",
 *     description="Export list of Virtual Cashboxes.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Export Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
