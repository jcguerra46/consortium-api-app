<?php
/**
 * Index Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{virtualCashboxId}/movement",
 *     tags={"Virtual Cashboxes Movements"},
 *     summary="Get virtual cashbox movements list",
 *     description="Returns list of Virtual Cashboxes Movements.",
 *     operationId="index",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="virtualCashboxId",
 *         in="path",
 *         description="Virtual Cashbox ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 *
 */

/**
 * Create Swagger Annotation
 * @OA\Post(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{virtualCashboxId}/movement",
 *     tags={"Virtual Cashboxes Movements"},
 *     summary="Create virtual cashbox",
 *     description="Create virtual cashbox.",
 *     operationId="create",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="virtualCashboxId",
 *         in="path",
 *         description="Virtual Cashbox ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="query",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="string"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"amount", "date", "description", "type"},
 *    				 @OA\Property(property="amount",
 *    					type="amount",
 *    					example="15000",
 *    					description="Amount"
 *    				),
 *    				 @OA\Property(property="date",
 *    					type="string",
 *    					example="15-12-2000",
 *    					description="Date"
 *    				),
 *                  @OA\Property(property="description",
 *    					type="string",
 *    					example="Example...",
 *    					description="Description"
 *    				),
 *                  @OA\Property(property="type",
 *    					type="string",
 *    					enum= {"income", "outflow"},
 *    					description="Type of movement"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes Movements overview.",
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Edit Swagger Annotation
 * @OA\Put(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{virtualCashboxId}/movement/{id}",
 *     tags={"Virtual Cashboxes Movements"},
 *     summary="Update virtual cashbox",
 *     description="Update virtual cashbox.",
 *     operationId="update",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="virtualCashboxId",
 *         in="path",
 *         description="Virtual Cashbox id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\RequestBody(
 *    		@OA\MediaType(
 *    			mediaType="application/x-www-form-urlencoded",
 *    			@OA\Schema(
 *                  required={"amount", "date", "description", "type"},
 *    				 @OA\Property(property="amount",
 *    					type="amount",
 *    					example="15000",
 *    					description="Amount"
 *    				),
 *    				 @OA\Property(property="date",
 *    					type="string",
 *    					example="15-12-2000",
 *    					description="Date"
 *    				),
 *                  @OA\Property(property="description",
 *    					type="string",
 *    					example="Example...",
 *    					description="Description"
 *    				),
 *                  @OA\Property(property="type",
 *    					type="string",
 *    					enum= {"income", "outflow"},
 *    					description="Type of movement"
 *    				),
 *    			),
 *    		),
 *    	),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=422,
 *         description=" Unprocessable Entity.",
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Show Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{virtualCashboxId}/movement/{id}",
 *     tags={"Virtual Cashboxes Movements"},
 *     summary="Get a virtual cashbox info",
 *     description="Returns a virtual cashbox info.",
 *     operationId="show",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="virtualCashboxId",
 *         in="path",
 *         description="Virtual Cashbox ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Movement ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashbox overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="Virtual Cashbox not found.",
 *     )
 * )
 */

/**
 * Delete Swagger Annotation
 * @OA\Delete(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{virtualCashboxId}/movement/{id}",
 *     tags={"Virtual Cashboxes Movements"},
 *     summary="Delete a virtual cashbox",
 *     description="Delete virtual cashbox.",
 *     operationId="destroy",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="Virtual Cashbox ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes deleted."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     ),
 *     @OA\Response(
 *         response=404,
 *         description="COnsortium not found.",
 *     )
 * )
 */

/**
 * Paginate Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{virtualCashboxId}/movement/paginate",
 *     tags={"Virtual Cashboxes Movements"},
 *     summary="Get Virtual Cashboxes list with paginate",
 *     description="Returns list of Virtual Cashboxes with paginate.",
 *     operationId="paginate",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page",
 *         in="query",
 *         description="Page",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="page_size",
 *         in="query",
 *         description="Page size",
 *         required=false,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */

/**
 * Export Swagger Annotation
 * @OA\Get(
 *     path="/consortium/{consortiumId}/virtual-cashbox/{virtualCashboxId}/movement/export",
 *     tags={"Virtual Cashboxes Movements"},
 *     summary="Export Virtual Cashboxes list",
 *     description="Export list of Virtual Cashboxes.",
 *     operationId="export",
 *     security={{ "BearerAuth": {} }},
 *     @OA\Parameter(
 *         name="consortiumId",
 *         in="path",
 *         description="Consortium ID",
 *         required=true,
 *         @OA\Schema(
 *             type="integer"
 *         )
 *     ),
 *     @OA\Response(
 *         response=200,
 *         description="Export Virtual Cashboxes overview."
 *     ),
 *     @OA\Response(
 *         response=401,
 *         description="Unauthorized action.",
 *     )
 * )
 */
