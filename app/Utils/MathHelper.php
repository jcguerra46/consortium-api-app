<?php


namespace App\Utils;


class MathHelper
{
    public static function calculateValuePercentage($value, $porcentage)
    {
        return round(($value * $porcentage) / 100, 2);
    }
}
