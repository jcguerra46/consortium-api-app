<?php
/**
 * Created by PhpStorm.
 * User: sergio
 * Date: 04/09/19
 * Time: 12:17
 */

namespace App\Utils;


class NumberToText
{
    function doUnits($num)
    {
        switch ($num) {
            case 1:
                return 'UNO';
            case 2:
                return 'DOS';
            case 3:
                return 'TRES';
            case 4:
                return 'CUATRO';
            case 5:
                return 'CINCO';
            case 6:
                return 'SEIS';
            case 7:
                return 'SIETE';
            case 8:
                return 'OCHO';
            case 9:
                return 'NUEVE';
        }

        return '';
    }

    function doTens($num)
    {
        $ten = floor($num / 10);
        $unit = $num - ($ten * 10);

        switch ($ten) {
            case 1:
                switch ($unit) {
                    case 0:
                        return 'DIEZ';
                    case 1:
                        return 'ONCE';
                    case 2:
                        return 'DOCE';
                    case 3:
                        return 'TRECE';
                    case 4:
                        return 'CATORCE';
                    case 5:
                        return 'QUINCE';
                    default:
                        return 'DIECI' . $this->doUnits($unit);
                }
            case 2:
                switch ($unit) {
                    case 0:
                        return 'VEINTE';
                    default:
                        return 'VEINTI' . $this->doUnits($unit);
                }
            case 3:
                return $this->doTensY('TREINTA', $unit);
            case 4:
                return $this->doTensY('CUARENTA', $unit);
            case 5:
                return $this->doTensY('CINCUENTA', $unit);
            case 6:
                return $this->doTensY('SESENTA', $unit);
            case 7:
                return $this->doTensY('SETENTA', $unit);
            case 8:
                return $this->doTensY('OCHENTA', $unit);
            case 9:
                return $this->doTensY('NOVENTA', $unit);
            case 0:
                return $this->doUnits($unit);
        }
    }

    function doTensY($strSin, $numUnits)
    {
        if ($numUnits > 0) {
            return $strSin . ' Y ' . $this->doUnits($numUnits);
        }

        return $strSin;
    }

    function doHundreds($num)
    {
        $hundreds = floor($num / 100);
        $tens = $num - ($hundreds * 100);

        switch ($hundreds) {
            case 1:
                if ($tens > 0) {
                    return 'CIENTO ' . $this->doTens($tens);
                }
                return 'CIEN';
            case 2:
                return 'DOSCIENTOS ' . $this->doTens($tens);
            case 3:
                return 'TRESCIENTOS ' . $this->doTens($tens);
            case 4:
                return 'CUATROCIENTOS ' . $this->doTens($tens);
            case 5:
                return 'QUINIENTOS ' . $this->doTens($tens);
            case 6:
                return 'SEISCIENTOS ' . $this->doTens($tens);
            case 7:
                return 'SETECIENTOS ' . $this->doTens($tens);
            case 8:
                return 'OCHOCIENTOS ' . $this->doTens($tens);
            case 9:
                return 'NOVECIENTOS ' . $this->doTens($tens);
        }

        return $this->doTens($tens);
    }

    function doSection($num, $divider, $strSingular, $strPlural)
    {
        $hundreds = floor($num / $divider);
        $rest = $num - ($hundreds * $divider);
        $letters = '';

        if ($hundreds > 0) {
            if ($hundreds > 1) {
                $letters = $this->doHundreds($hundreds) . ' ' . $strPlural;
            } else {
                $letters = $strSingular;
            }
        }

        if ($rest > 0) {
            $letters .= '';
        }

        return $letters;
    }

    function doThousands($num)
    {
        $divider = 1000;
        $hundreds = floor($num / $divider);
        $rest = $num - ($hundreds * $divider);
        $strThousands = $this->doSection($num, $divider, 'MIL', 'MIL');
        $strHundreds = $this->doHundreds($rest);

        if ($strThousands == '') {
            return $strHundreds;
        }

        $returnTxt = $strThousands;

        if ($strHundreds != '') {
            $returnTxt .= ' ' . $strHundreds;
        }

        return $returnTxt;
    }

    function doMillions($num)
    {
        $divider = 1000000;
        $hundreds = floor($num / $divider);
        $rest = $num - ($hundreds * $divider);
        $strMillions = $this->doSection($num, $divider, 'UN MILLON', 'MILLONES');
        $strThousands = $this->doThousands($rest);

        if ($strMillions == '') {
            return $strThousands;
        }

        $returnTxt = $strMillions;

        if ($strThousands != '') {
            $returnTxt .= ' ' . $strThousands;
        }

        return $returnTxt;
    }

    function numberToText($num)
    {
        $num = (float)abs($num);
        $data = [
            'integers' => floor($num),
            'cents' => (((round($num * 100)) - (floor($num) * 100))),
            'lettersCents' => '',
            'lettersCentsPlural' => 'CENTAVOS',
            'lettersCentsSingular' => 'CENTAVO'
        ];

        if ($data['cents'] > 0) {
            $cents = (function ($data) {
                $returnTxt = $this->doMillions($data['cents']);

                if ($data['cents'] == 1) {
                    $returnTxt = 'UN ' . $data['lettersCentsSingular'];
                } else {
                    $returnTxt .= ' ' . $data['lettersCentsPlural'];
                }

                return $returnTxt;
            })($data);

            $data['lettersCents'] = 'CON ' . $cents;
        }

        $returnTxt = '';

        if ($data['integers'] == 0) {
            $returnTxt = 'CERO';
        } else {
            $returnTxt = $this->doMillions($data['integers']);
        }

        if ($data['lettersCents'] != '') {
            $returnTxt .= ' ' . $data['lettersCents'];
        }

        return $returnTxt;
    }
}
