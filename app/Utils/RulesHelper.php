<?php


namespace App\Utils;

/**
 * Class RulesHelper
 * @package App\Utils
 * @author SergioC
 */
class RulesHelper
{
    /**
     * @param array $rules
     * @return array
     */
    public static function formatEditRules(array $rules): array
    {
        foreach ($rules as $key => $rule) {

            if (is_string($rule)) {
                if (strpos($rule, 'required') !== false) {
                    $rules[$key] = 'sometimes|' . $rule;
                }
            } elseif (is_array($rule)) {
                if (in_array('required', $rule)) {
                    $rules[$key][] = 'sometimes';
                }
            }
        }
        return $rules;
    }
}
