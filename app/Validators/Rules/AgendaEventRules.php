<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ConsortiumFromAdministration;

class AgendaEventRules extends BaseRules
{
    public static function storeRules()
    {
        return [
            'consortium_id' => [
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'eventable' => 'integer',
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'date' => 'required|date_format:Y-m-d',
            'trans' => 'required|string|max:255',
            'service_class_name' => 'string|max:255',
            'route_class_name' => 'required|string|max:255'
        ];
    }

    public static function showRules()
    {
        return [

        ];
    }

    public static function updateRules()
    {
        return [
            'consortium_id' => [
                'sometimes',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'eventable' => 'sometimes|integer',
            'title' => 'sometimes|string|max:255',
            'description' => 'sometimes|string',
            'date' => 'sometimes|date_format:Y-m-d',
            'trans' => 'sometimes|string|max:255',
            'service_class_name' => 'sometimes|string|max:255',
            'route_class_name' => 'sometimes|string|max:255'
        ];
    }

    public static function destroyRules()
    {
        return [

        ];
    }

}
