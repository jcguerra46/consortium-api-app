<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\AmenityFromAdministration;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;

class AmenityRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration(),
                'bail'
            ],
            'name' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'enabled' => 'required|boolean',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:amenities,id',
                new AmenityFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:amenities,id',
                new AmenityFromAdministration(),
                'bail'
            ]
        ];
    }
}
