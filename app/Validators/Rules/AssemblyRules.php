<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\AssemblyFromAdministration;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;

class AssemblyRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'date' => 'required|date_format:d-m-Y',
            'time' => 'required|date_format:H:i:s',
            'second_time' => 'required|date_format:H:i:s',
            'place' => 'required|string|max:255',
            'issues' => 'required|string',
            'send' => 'required|boolean',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:assemblies,id',
                new AssemblyFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:assemblies,id',
                new AssemblyFromAdministration()
            ]
        ];
    }
}
