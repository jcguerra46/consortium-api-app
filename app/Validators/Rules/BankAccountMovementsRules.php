<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\BankAccountFromAdministration;
use App\Validators\Rules\Customs\BankAccountMovementFromAdministration;
use App\Validators\Rules\Customs\BankAccountMovementWithoutCashbox;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class BankAccountMovementsRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'bank_account_id' => [
                    'required',
                    'integer',
                    'max:2147483647',
                    new BankAccountFromAdministration(),
                    'bail'
                ]
            ] + parent::indexRules();
    }

    public static function createRules()
    {
        return [
            'bank_account_id' => [
                'required',
                'integer',
                'max:2147483647',
                new BankAccountFromAdministration(),
                'bail'
            ],
            'description' => 'string|max:255',
            'date' => 'required|date_format:d-m-Y',
            'amount' => 'required|numeric|gt:0',
            'type' => [
                'required',
                'string',
                Rule::in(['income', 'outflow'])
            ],
            'type_movement' => [
                'string',
                Rule::in(['deposit', 'transfer', 'extraction'])
            ],
        ];
    }

    public static function editRules()
    {
        return [
                'id' => [
                    'required',
                    'integer',
                    'max:2147483647',
                    new BankAccountMovementFromAdministration(),
                    new BankAccountMovementWithoutCashbox(),
                    'bail'
                ]
            ] + Arr::except(self::createRules(), ['bank_account_id']);
    }

    public static function deleteRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new BankAccountMovementFromAdministration(),
                new BankAccountMovementWithoutCashbox(),
                'bail'
            ]
        ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new BankAccountMovementFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function exportRules()
    {
        return [
            'bank_account_id' => [
                'required',
                'integer',
                'max:2147483647',
                new BankAccountFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function getBalance()
    {
        return [
            'bank_account_id' => [
                'required',
                'integer',
                'max:2147483647',
                new BankAccountFromAdministration(),
                'bail'
            ]
        ];
    }
}
