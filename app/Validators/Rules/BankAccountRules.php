<?php


namespace App\Validators\Rules;

use App\Utils\RulesHelper;
use App\Validators\Rules\Customs\BankAccountFromAdministration;
use App\Validators\Rules\Customs\BankAccountWithoutMovements;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\CuitValidator;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class BankAccountRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'consortium_id' => [
                    'required',
                    'integer',
                    'exists:consortia,id',
                    'max:2147483647',
                    new ConsortiumFromAdministration(),
                    'bail'
                ]
            ] + parent::indexRules();
    }

    public static function createRules()
    {
        return [
            'bank_id' => [
                'nullable',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:banks,id'
            ],
            'account_number' => 'required|string|max:255',
            'cbu' => 'nullable|string|max:255',
            'alias' => 'nullable|string|max:255',
            'cuit' => [
                'nullable',
                'string',
                'max:255',
                new CuitValidator(),
            ],
            'type' => [
                'nullable',
                'string',
                Rule::in(['savings_box', 'current_account', 'single_account'])
            ],
            'branch_office' => 'nullable|string|max:255',
            'owner' => 'nullable|string|max:255',
            'show_data_in_expense' => 'boolean',
            'email' => 'nullable|string|max:255|email',
            'signatorie_1' => 'nullable|string|max:255',
            'signatorie_2' => 'nullable|string|max:255',
            'signatorie_3' => 'nullable|string|max:255',
            'consortium_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new ConsortiumFromAdministration()
            ]
        ];
    }

    public static function editRules()
    {
        return self::showRules() + RulesHelper::formatEditRules(Arr::except(self::createRules(), 'consortium_id'));
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new BankAccountFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new BankAccountFromAdministration(),
                new BankAccountWithoutMovements(),
                'bail'
            ]
        ];
    }

    public static function exportRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                'max:2147483647',
                new ConsortiumFromAdministration(),
                'bail'
            ]
        ];
    }

}
