<?php


namespace App\Validators\Rules;


class BaseRules
{
    public static function indexRules()
    {
        return [
            'page' => 'integer',
            'page_size' => 'integer|max:30',
            'filters' => 'array',
            'sort_by' => 'string|max:255',
            'sort_direction' => 'string|in:asc,desc'
        ];
    }
}
