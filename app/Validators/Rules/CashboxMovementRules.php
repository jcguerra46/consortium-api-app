<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;
use App\Validators\Rules\Customs\BankAccountFromAdministration;
use App\Validators\Rules\Customs\BankAccountMovementFromAdministration;
use App\Validators\Rules\Customs\BankAccountMovementIsOutflow;
use App\Validators\Rules\Customs\BankAccountMovementWithoutCashbox;
use App\Validators\Rules\Customs\CashboxFromAdministration;
use App\Validators\Rules\Customs\CashboxMovementFromAdministration;
use App\Validators\Rules\Customs\CheckFromAdministration;
use App\Validators\Rules\Customs\CheckTotalAmountIncome;
use App\Validators\Rules\Customs\CheckTotalFUsIncome;
use App\Validators\Rules\Customs\FunctionalUnitFromAdministration;

class CashboxMovementRules extends BaseRules
{

    /**
     * @return array[]|string[]
     */
    public static function editRules()
    {
        return self::showRules() + [
                'name' => 'required|string|max:255',
            ];
    }

    /**
     * @return array[]
     */
    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:cashbox_movements,id',
                new CashboxMovementFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function createIncomeRules()
    {
        return [
            'cashbox_id' => [
                'required',
                'integer',
                'exists:cashboxes,id',
                new CashboxFromAdministration(),
                'bail'
            ],
            'description' => 'required|string|max:255',
            'date' => 'required|date_format:d-m-Y',
            'total_amount' => [
                'required',
                'numeric',
                'gt:0',
                new CheckTotalAmountIncome(),
                'bail'
            ],
            'cash_amount' => 'required|numeric|gte:0',
            'identified' => 'required|boolean',
            'functional_units' => [
                'nullable',
                'array',
                new CheckTotalFUsIncome(),
                'bail',
            ],
            'functional_units.*.functional_unit_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new FunctionalUnitFromAdministration(),
                'bail',
            ],
            'functional_units.*.amount' => 'required|numeric|gt:0',
            'checks' => 'nullable|array',
            'checks.*' => 'integer|gt:0',
            'bank_account_movements' => 'nullable|array',
            'bank_account_movements.*' => 'integer|gt:0',
        ];
    }

    public static function createOutflowRules()
    {
        return [
            'cashbox_id' => [
                'required',
                'integer',
                'exists:cashboxes,id',
                new CashboxFromAdministration(),
                'bail'
            ],
            'description' => 'required|string|max:255',
            'date' => 'required|date_format:d-m-Y',
            'cash_amount' => 'required|numeric|gte:0',
            'checks' => 'nullable|array',
            'checks.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:checks,id',
                new CheckFromAdministration(),
                'bail',
            ],
            'bank_account_movements' => 'nullable|array',
            'bank_account_movements.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:bank_account_movements,id',
                new BankAccountMovementFromAdministration(),
                new BankAccountMovementWithoutCashbox(),
                new BankAccountMovementIsOutflow(),
                'bail'
            ],
            'total_amount' => [
                'required',
                'numeric',
                'gt:0',
                new CheckTotalAmountIncome(),
                'bail'
            ],
        ];
    }

    public static function getBalancesRules()
    {
        return [
            'cashbox_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new CashboxFromAdministration(),
                'bail'
            ]
        ];
    }
}
