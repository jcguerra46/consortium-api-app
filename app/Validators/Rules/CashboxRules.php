<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\CashboxFromAdministration;

class CashboxRules extends BaseRules
{

    /**
     * @return array[]|string[]
     */
    public static function editRules()
    {
        return self::showRules() + [
                'name' => 'required|string|max:255',
            ];
    }

    /**
     * @return array[]
     */
    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:cashboxes,id',
                new CashboxFromAdministration()
            ]
        ];
    }

}
