<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;

class CategoryRules extends BaseRules {

    /**
     * Rules for creating a category
     * @return array
     */
    public static function storeRules()
    {
        return [
            'number' => 'required|integer',
            'name' => 'required|string|max:255',
            'position' => 'required|integer',
            'hidden' => 'required|boolean',
            'salaries' => 'required|boolean',
            'administration_id' => 'required|integer|exists:administrations,id',
        ];
    }

    /**
     * Rules for update a category
     * @return array
     */
    public static function updateRules()
    {
        return [
            'number' => 'sometimes|required|integer',
            'name' => 'sometimes|required|string|max:255',
            'position' => 'sometimes|required|integer',
            'hidden' => 'sometimes|required|boolean',
            'salaries' => 'sometimes|required|boolean',
            'administration_id' => 'sometimes|required|integer|exists:administrations,id',
        ];
    }

    /**
     * Rules for show a category
     * @return array
     */
    public static function showRules()
    {
        return [

        ];
    }

    /**
     * Rules for delete a category
     * @return array
     */
    public static function destroyRules()
    {
        return [

        ];
    }
}
