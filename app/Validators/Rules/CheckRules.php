<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\CheckbookFromAdministration;
use App\Validators\Rules\Customs\CheckFromAdministration;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\NotExists;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class CheckRules extends BaseRules
{

    public static function indexRules()
    {
        return parent::indexRules() + [
            'filters.cashbox_movement_income_id ' => 'nullable|integer|gt:0|max:2147483647',
            'filters.cashbox_movement_outflow_id ' => 'nullable|integer|gt:0|max:2147483647',
            'filters.own ' => 'nullable|boolean',
        ];
    }

    public static function createRules()
    {
        return [
            'number' => 'required|integer|gt:0|max:2147483647',
            'deposit_date' => 'nullable|date_format:d-m-Y',
            'issuance_date' => 'nullable|date_format:d-m-Y',
            'due_date' => 'nullable|date_format:d-m-Y',
            'amount' => 'nullable|numeric|gt:0',
            'crossed' => 'required|boolean',
            'is_to_the_order' => 'required|boolean',
            'endorsements' => 'required|boolean',
            'own' => 'required|boolean',
            'bank_id' => 'required|integer|gt:0|max:2147483647|exists:banks,id|bail',
            'consortium_id' => [
                'nullable',
                'integer',
                'gt:0',
                'max:2147483647',
                new ConsortiumFromAdministration(),
                ],
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new CheckFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
