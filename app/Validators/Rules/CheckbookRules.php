<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\CheckbookFromAdministration;
use App\Validators\Rules\Customs\NotExists;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class CheckbookRules extends BaseRules
{
    /**
     * @return array
     */
    public static function createRules()
    {
        return [
            'bank_id' => 'required|integer|gt:0|max:2147483647|exists:banks,id',
            'number' => 'required|integer|gt:0|max:2147483647',
            'first_check' => 'required|integer|gt:0|max:2147483647',
            'last_check' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'gt:first_check',
                'bail'
            ],
        ];
    }

    /**
     * @return array|array[]
     */
    public static function editRules()
    {
        return self::showRules() + Arr::only(self::createRules(), ['number']);
    }

    /**
     * @return array[]
     */
    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new CheckbookFromAdministration(),
                'bail'
            ]
        ];
    }

    /**
     * @return array[]
     */
    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new CheckbookFromAdministration(),
                new NotExists('checks', 'checkbook_id'),
                'bail'
            ]
        ];
    }
}
