<?php

namespace App\Validators\Rules;

class ClaimCommentRules extends BaseRules {

    /**
     * Rules for creating a claim comment
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'user_portal_id' => 'required|integer|exists:users_portal,id',
            'title' => 'required|string|max:255',
            'body' => 'required|string',
            'viewed' => 'required|boolean'
        ];
    }

    /**
     * Rules for update a claim comment
     *
     * @return array
     */
    public static function updateRules()
    {
        return [
            'claim_id' => 'sometimes|required|integer|exists:claims,id',
            'user_portal_id' => 'sometimes|required|integer|exists:users_portal,id',
            'title' => 'sometimes|required|string|max:255',
            'body' => 'sometimes|required|string',
            'viewed' => 'sometimes|required|boolean'
        ];
    }

    /**
     * Rules for show a claim comment
     *
     * @return array
     */
    public static function showRules()
    {
        return [

        ];
    }

    /**
     * Rules for delete a claim comment
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [

        ];
    }
}
