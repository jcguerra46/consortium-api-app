<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;

class ClaimRules extends BaseRules {

    /**
     * Rules for creating a claim
     *
     * @return array
     */
    public static function createRules()
    {
        return [
            'functional_unit_id' => 'required|integer|exists:functional_units,id',
            'user_portal_id' => 'required|integer|exists:users_portal,id',
            'title' => 'required|string|max:255',
            'body' => 'required|string',
            'state' => 'required|string|max:255'
        ];
    }

    /**
     * Rules for editing a claim
     *
     * @return array
     */
    public static function editRules()
    {
        return [
            'functional_unit_id' => 'sometimes|required|integer|exists:functional_units,id',
            'user_portal_id' => 'sometimes|required|integer|exists:users_portal,id',
            'title' => 'sometimes|required|string|max:255',
            'body' => 'sometimes|required|string',
            'state' => 'sometimes|required|string|max:255'
        ];
    }

    /**
     * Rules for show a claim
     *
     * @return array
     */
    public static function showRules()
    {
        return [

        ];
    }

    /**
     * Rules for delete a claim
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [

        ];
    }
}
