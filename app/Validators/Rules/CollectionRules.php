<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\CollectionFromAdministration;
use App\Validators\Rules\Customs\FunctionalUnitFromAdministration;

class CollectionRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'description' => 'string|max:255',
            'date' => 'required|date_format:d-m-Y|before_or_equal:today',
            'total_amount' => 'required|numeric|gt:0',
            'cash_amount' => 'required|numeric|gte:0|bail',
            'checks' => 'nullable|array',
            'checks.*' => [
                'integer' .
                'gt:0',
                'max:2147483647',
                'exists:checks,id',
                'bail'
            ],
            'transfers' => 'nullable|array',
            'transfers.*' => [
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:bank_account_movements,id',
                'bail'
            ],
            'functional_unit_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new FunctionalUnitFromAdministration(),
                'bail'
            ],
        ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new CollectionFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
