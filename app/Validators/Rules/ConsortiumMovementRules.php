<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\NoteFromAdministration;

class ConsortiumMovementRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'consortium_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    new ConsortiumFromAdministration(),
                    'bail',
                ]
            ] + parent::indexRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:consortium_notes,id',
            ]
        ];
    }

    public static function getBalanceRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new ConsortiumFromAdministration(),
                'bail',
            ]
        ];
    }
}
