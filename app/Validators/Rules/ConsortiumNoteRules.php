<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\NoteFromAdministration;

class ConsortiumNoteRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'note_id' => [
                'required',
                'integer',
                'exists:notes,id',
                new NoteFromAdministration()
            ],
            'position' => 'required|integer|gt:0'
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:consortium_notes,id',
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
