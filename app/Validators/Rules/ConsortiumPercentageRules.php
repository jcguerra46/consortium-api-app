<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\FunctionalUnitFromAdministration;
use App\Validators\Rules\Customs\MaxCountPercentages;
use App\Validators\Rules\Customs\PercentageConsortiumFromAdministration;
use App\Validators\Rules\Customs\SpendingFromPercentageConsortium;
use Illuminate\Validation\Rule;

class ConsortiumPercentageRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'consortium_id' => [
                    'required',
                    'integer',
                    'exists:consortia,id',
                    new ConsortiumFromAdministration()
                ]
            ] + parent::indexRules();
    }


    public static function createRules($consortiumId)
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                new ConsortiumFromAdministration(),
                new MaxCountPercentages(8),
            ],
            'name' => 'required|string|max:255',
            'second_line_name' => 'string|max:255',
            'type' => [
                'string',
                Rule::in(['spendings', 'fixed', 'forced_fixed'])
            ],
            'position' => 'required|integer|gt:0|unique:percentages_consortium,position,NULL,id,consortium_id,' . $consortiumId,
            'last_fixed_amount' => 'numeric',
            'particular_percentage' => 'boolean',
            'hidden_expense_spending' => 'boolean',
            'hidden_in_proration' => 'boolean',
            'hidden_percentage_value' => 'boolean',

            //Para el calculo de Percentages FUs
            'equal_parts' => 'required|boolean'
        ];
    }

    public static function editRules($consortiumId, $percentageId)
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new PercentageConsortiumFromAdministration(),
                'bail'
            ],
            'name' => 'required|string|max:255',
            'second_line_name' => 'string|max:255',
            'type' => [
                'string',
                Rule::in(['spendings', 'fixed', 'forced_fixed'])
            ],
            'position' => 'required|integer|gt:0|unique:percentages_consortium,position,' . $percentageId . ',id,consortium_id,' . $consortiumId,
            'last_fixed_amount' => 'numeric',
            'particular_percentage' => 'boolean',
            'hidden_expense_spending' => 'boolean',
            'hidden_in_proration' => 'boolean',
            'hidden_percentage_value' => 'boolean'
        ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new PercentageConsortiumFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new PercentageConsortiumFromAdministration(),
                new SpendingFromPercentageConsortium(),
                'bail'
            ]
        ];
    }

    public static function duplicateRules()
    {
        return self::showRules() + [
                'consortium_id' => [
                    'required',
                    'integer',
                    'max:2147483647',
                    new ConsortiumFromAdministration(),
                    new MaxCountPercentages(8),
                    'bail'
                ],
                'name' => 'required|string|max:255',
            ];
    }

    public static function massCreate()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'max:2147483647',
                new ConsortiumFromAdministration(),
                new MaxCountPercentages(8),
                'bail'
            ],
            'percentages' => 'required|array|max:8',
            'percentages.*.name' => 'required|string|max:255',
            'percentages.*.second_line_name' => 'string|max:255',
            'percentages.*.description' => 'string',
            'percentages.*.type' => [
                'required',
                'string',
                Rule::in(['spendings', 'fixed', 'forced_fixed'])
            ],
            'percentages.*.position' => 'required|integer|gt:0',
            'percentages.*.last_fixed_amount' => 'numeric',
            'percentages.*.particular_percentage' => 'required|boolean',
            'percentages.*.hidden_expense_spending' => 'required|boolean',
            'percentages.*.hidden_in_proration' => 'required|boolean',
            'percentages.*.hidden_percentage_value' => 'required|boolean',

            //Para el calculo de Percentages FUs
            'percentages.*.equal_parts' => 'required|boolean',
        ];
    }

    public static function getFunctionalUnitsPercentagesRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new PercentageConsortiumFromAdministration(),
                'bail'
            ],
            'consortium_id' => [
                'required',
                'integer',
                'max:2147483647',
                new ConsortiumFromAdministration(),
                'bail'
            ],
        ];
    }

    public static function paginateFunctionalUnitsPercentagesRules()
    {
        return self::indexRules() + self::getFunctionalUnitsPercentagesRules();
    }

    public static function massUpdateFunctionalUnitsPercentagesRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new PercentageConsortiumFromAdministration(),
                'bail'
            ],
            'functional_units' => 'required|array',
            'functional_units.*.functional_unit_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new FunctionalUnitFromAdministration(),
                'bail'
            ],
            'functional_units.*.value' => [
                'required',
                'numeric',
                'gte:0',
                'max:100',
            ]
        ];
    }
}
