<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use Illuminate\Validation\Rule;
use App\Validators\Rules\Customs\CuitValidator;

class ConsortiumRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'start_date' => 'date_format:d-m-Y',
            'business_name' => 'required|string|max:255',
            'fancy_name' => 'nullable|string|max:255',
            'code' => 'nullable|string|max:255',
            'order' => 'required|integer|gt:0|bail',
            'address_number' => 'required|string|max:255',
            'address_street' => 'required|string|max:255',
            'country_id' => 'required|integer|gt:0|exists:countries,id|bail',
            'province_id' => 'required|integer|gt:0|exists:provinces,id|bail',
            'location' => 'nullable|string|max:255',
            'postal_code' => 'required|string|max:8',
            'cuit' => [
                'required',
                'string',
                'max:255',
                new CuitValidator(),
            ],
            'payment_method_id' => '',
            'suterh_code' => 'nullable|string|max:255',
            'siro_code' => 'nullable|string|max:255',
            'pmc_code' => 'nullable|string|max:255',
            'pme_code' => 'nullable|string|max:255',
            'ep_code' => 'nullable|string|max:255',
            'pf_code' => 'nullable|string|max:255',
            'interfast_code' => 'nullable|string|max:255',
            'siro_verification_number' => 'nullable|integer|max:32767',
            'count_floors' => 'required|integer|max:1000',
            'count_functional_units' => 'required|integer|max:1000',
            'type_of_building' => [
                'string',
                'max:255',
                Rule::in(['H', 'R'])
            ],
            'state' => [
                'string',
                'max:255',
                Rule::in(['PENDIENTE', 'INACTIVO', 'ACTIVO', 'BORRADO']),
                'bail'
            ],
            'category' => [
                'required',
                'integer',
                Rule::in(['1', '2', '3', '4']),
                'bail'
            ],
            "first_due_date" => 'required|integer|between:1,31'
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ]
        ];
    }

    public static function editProfileRules()
    {
        return self::showRules() + [
                'bank_account_id' => [
                    'nullable',
                    'integer',
                    'exists:bank_accounts,id'
                ],
                'escrow' => 'nullable|boolean',
                'email' => 'nullable|string|max:255|email',
                'license' => 'nullable|string|max:255',
                'liquidation' => [
                    'nullable',
                    'string',
                    Rule::in(['vencido', 'adelantado'])
                ],
                'rounding_type' => [
                    'nullable',
                    'string',
                    Rule::in(['fu', 'yes', 'no'])
                ],
                'first_due_date' => 'required|integer|gt:0',
                'second_due_date' => 'nullable|integer|gt:0',
                'second_due_date_interests' => 'nullable|numeric|gt:0',
                'penalty_interests' => 'nullable||gt:0',
                'penalty_interests_mode' => [
                    'nullable',
                    'string',
                    Rule::in('capital', 'interest')
                ],
                'rounding_payslips' => 'nullable|boolean',
                'garage_amount' => 'nullable|integer',
                'art_amount' => 'nullable|numeric',
                'art_percentage' => 'nullable|numeric',
                'collect_interest_outside_due_date' => 'nullable|boolean',
                'independent_cashbox' => 'nullable|boolean',
                'notes' => 'nullable|string',
                'next_payment_receipt_number' => 'integer',
                'payslips_details' => 'boolean',
                'next_moving_sheet_number' => 'integer',
                'private_spendings_title' => 'string|max:255',
                'self_managment' => 'nullable|boolean',
                'early_payment' => 'boolean',
                'early_payment_discount_days' => 'integer',
                'early_payment_discount' => 'numeric',
                'show_bank_movements' => 'boolean',
                'show_patrimonial_status' => 'boolean',
                'show_provider_data' => 'boolean',
                'legends_my_expenses' => 'boolean',
                'show_judgments_section' => 'boolean',
                'auto_billing' => 'boolean',
                'package_id' => [
                    'nullable',
                    'integer',
                    'exists:packages,id'
                ],
                'expense_header_id' => [
                    'nullable',
                    'integer',
                    'exists:expense_headers,id'
                ],
                'observations' => 'nullable|string',
                'waste_removal_ammount' => 'numeric',
                'provider_spending_location' => [
                    'nullable',
                    'string',
                    Rule::in('delante', 'detras')
                ],
                'show_previous_balance' => 'boolean',
                'show_payments' => 'boolean',
            ];
    }

    public static function editPortalProfileRules()
    {
        return self::showRules() + [
                'consortium_id' => 'boolean',
                'show_claims' => 'boolean',
                'show_ammenities' => 'boolean',
                'need_ammenities_approval' => 'boolean',
                'show_consortium_current_account' => 'boolean',
                'show_consortium_bank_account' => 'boolean',
                'show_money_in_debt' => 'boolean',
                'show_spendings' => 'boolean',
            ];
    }


    public static function getFunctionalUnitsRules()
    {
        return self::showRules() + parent::indexRules();
    }

    public static function getCashboxRules()
    {
        return self::showRules();
    }

    public static function getChecksRules()
    {
        return self::showRules();
    }
}
