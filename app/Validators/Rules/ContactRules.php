<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ContactFromAdministration;

class ContactRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'required|string|max:255',
            'address_street' => 'nullable|string|max:255',
            'postal_code' => 'nullable|string|max:255',
            'document_number' => 'nullable|numeric|max:999999999',
            'birth_date' => 'nullable|date_format:d-m-Y',
            'phone_personal' => 'nullable|string|max:255',
            'phone_work' => 'nullable|string|max:255',
            'phone_cel' => 'nullable|string|max:255',
            'email' => 'required|string|max:255|email',
            'expenses_by_email' => 'boolean',
            'council_member' => 'boolean',
            'observations' => 'nullable|string'
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
          'id' => [
              'required',
              'integer',
              'exists:contacts,id',
              new ContactFromAdministration()
          ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
