<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ProviderFromAdministration;
use App\Validators\Rules\Customs\ServiceFromAdministration;

class CurrentAccountProviderRules extends BaseRules
{
    public static function indexRules()
    {
        return [
            'provider_id' => [
                'required',
                'integer',
                'exists:providers,id',
                new ProviderFromAdministration()
            ]
        ];
    }

    public static function showRules()
    {
        return [
            'movement_id' => [
                'required',
                'integer',
                'exists:services,id',
                new ServiceFromAdministration()
            ]
        ];
    }

    public static function getBalance()
    {
        return self::indexRules();
    }
}
