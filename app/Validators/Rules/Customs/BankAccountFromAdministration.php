<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BankAccountFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('bank_accounts')
            ->leftJoin('consortia', 'consortia.id', '=', 'bank_accounts.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->where('bank_accounts.id', $value)
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Bank Account does not correspond to the administration';
    }

}
