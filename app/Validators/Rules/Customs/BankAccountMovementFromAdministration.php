<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BankAccountMovementFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('bank_account_movements')
            ->leftJoin('bank_accounts', 'bank_accounts.id', '=', 'bank_account_movements.bank_account_id')
            ->leftJoin('consortia', 'consortia.id', '=', 'bank_accounts.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->where('bank_account_movements.id', $value)
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Bank Account Movement does not correspond to the administration';
    }

}
