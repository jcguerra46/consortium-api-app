<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class BankAccountMovementWithoutCashbox implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('bank_account_movements')
            ->where('bank_account_movements.id', $value)
            ->whereNull('bank_account_movements.cashbox_movement_id')
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Bank Account Movement has a cash movement';
    }

}
