<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class BankAccountWithoutMovements implements Rule
{
    /**
     * Determine if the validation rule passes.
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('bank_account_movements')
            ->where('bank_account_movements.bank_account_id', $value)
            ->count();
        return $resultCount == 0;
    }

    /**
     * Get the validation error message.
     * @return string
     */
    public function message()
    {
        return 'The Bank Account has associated movements';
    }

}
