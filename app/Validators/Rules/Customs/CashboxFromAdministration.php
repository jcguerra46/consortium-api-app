<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CashboxFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('cashboxes')
            ->leftJoin('consortia', 'consortia.id', '=', 'cashboxes.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->where('cashboxes.id', $value)
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Cashbox does not correspond to the administration';
    }

}
