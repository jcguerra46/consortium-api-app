<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CashboxMovementFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('cashbox_movements')
            ->leftJoin('cashboxes', 'cashboxes.id', '=', 'cashbox_movements.cashbox_id')
            ->leftJoin('consortia', 'consortia.id', '=', 'cashboxes.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->where('cashbox_movements.id', $value)
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Cashbox Movement does not correspond to the administration';
    }

}
