<?php


namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class CheckSumInstallments implements Rule
{
    private $amount;

    public function __construct()
    {
        $this->amount = app('request')->get('amount');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!empty($value)) {
            if ($this->amount) {
                $values = collect($value);
                return $this->amount == $values->sum('amount');
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The sum of the installments values must be equal to the spending total.';
    }
}
