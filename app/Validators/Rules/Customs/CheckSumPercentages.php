<?php


namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;

class CheckSumPercentages implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $values = collect($value);
        return $values->sum('value') == 100;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The sum of the values must be equal to 100';
    }
}
