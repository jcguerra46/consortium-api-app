<?php


namespace App\Validators\Rules\Customs;

use App\Models\BankAccountMovement;
use App\Models\Check;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class CheckTotalAmount implements Rule
{
    private $checks;
    private $bankAccountMovs;
    private $cash_amount;

    public function __construct()
    {
        $this->checks = app('request')->input('checks') ?? [];
        $this->bankAccountMovs = app('request')->input('bank_account_movements') ?? [];
        $this->cash_amount = app('request')->input('cash_amount') ?? 0;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $checks = Check::whereIn('id', $this->checks)->get();
        $amountChecks = $checks->sum('amount');

        $bankAccountMovs = BankAccountMovement::whereIn('id', $this->bankAccountMovs)->get();
        $amountBankMovs = $bankAccountMovs->sum('amount');

        return ($amountChecks + $amountBankMovs + $this->cash_amount) == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The sum of the cash amount, checks and bank account movements must be equal to the Total Amount';
    }
}
