<?php


namespace App\Validators\Rules\Customs;

use App\Models\BankAccountMovement;
use App\Models\Check;
use App\Models\SpendingInstallment;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class CheckTotalAmountEqualSpending implements Rule
{
    private $spendingAmount;

    public function __construct()
    {
        $spendingInstallmentId = app('request')->input('spending_installment_id');
        $this->spendingAmount = SpendingInstallment::find($spendingInstallmentId)->amount;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->spendingAmount == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Spending Installment amount must be equal to the Total Amount';
    }
}
