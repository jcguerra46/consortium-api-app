<?php

namespace App\Validators\Rules\Customs;

use App\Models\BankAccountMovement;
use App\Models\Check;
use Illuminate\Contracts\Validation\Rule;

class CheckTotalAmountIncome implements Rule
{
    private $cashAmount;
    private $checksAmount;
    private $transfersAmount;

    public function __construct()
    {
        $this->cashAmount = app('request')->get('cash_amount');

        $checks = app('request')->get('checks') ?? [];
        $this->checksAmount = Check::whereIn('id', $checks)->sum('amount');

        $transfers = app('request')->get('bank_account_movements') ?? [];
        $this->transfersAmount = BankAccountMovement::whereIn('id', $transfers)->sum('amount');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ($this->cashAmount + $this->checksAmount + $this->transfersAmount) == $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The sum of the cash amount, checks and bank account movements must be equal to the total amount.';
    }
}
