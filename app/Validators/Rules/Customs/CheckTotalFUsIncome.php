<?php


namespace App\Validators\Rules\Customs;

use App\Models\BankAccountMovement;
use App\Models\Check;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class CheckTotalFUsIncome implements Rule
{
    private $totalAmount;
    private $identified;

    public function __construct()
    {
        $this->totalAmount = app('request')->get('total_amount');
        $this->identified = app('request')->get('identified');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->identified) {
            $fuTotal = 0;
            foreach ($value as $fu) {
                $fuTotal += $fu['amount'];
            }
            return $fuTotal == $this->totalAmount;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The sum of the amount of the functional units must be equal to the total amount.';
    }
}
