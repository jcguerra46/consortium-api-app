<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CollectionFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('collections_functional_units')
            ->leftJoin('functional_units', 'functional_units.id', '=', 'collections_functional_units.functional_unit_id')
            ->leftJoin('consortia', 'consortia.id', '=', 'functional_units.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->where('collections_functional_units.id', $value)
            ->count();

        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Collection does not correspond to the Administration';
    }

}
