<?php

namespace App\Validators\Rules\Customs;

use App\Models\Consortium;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ConsortiumFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = Consortium::fromAdministrationShow($value)
            ->where(['id' => $value,
                'administration_id' => Auth::user()->administration_id])
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Consortium does not correspond to the administration';
    }

}
