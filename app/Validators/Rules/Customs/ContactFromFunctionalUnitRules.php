<?php

namespace App\Validators\Rules\Customs;

use App\Models\Consortium;
use App\Models\FunctionalUnitContact;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactFromFunctionalUnitRules implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = Consortium::join('functional_units', 'consortia.id', '=', 'functional_units.consortium_id')
            ->join('functional_unit_contacts', 'functional_units.id', '=', 'functional_unit_contacts.functional_unit_id')
            ->where('functional_unit_contacts.id', $value)
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Contact does not correspond to the Functional Unit.';
    }

}
