<?php

namespace App\Validators\Rules\Customs;

use App\Models\Consortium;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class  FunctionalUnitFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('functional_units')
            ->leftJoin('consortia', 'consortia.id', '=', 'functional_units.consortium_id')
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->where('functional_units.id', $value)
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Functional Unit does not correspond to the administration';
    }

}
