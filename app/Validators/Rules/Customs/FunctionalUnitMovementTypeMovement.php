<?php

namespace App\Validators\Rules\Customs;

use App\Models\Consortium;
use App\Models\FunctionalUnitMovement;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class  FunctionalUnitMovementTypeMovement implements Rule
{
    /**
     * @var
     */
    private $functionalUnitMovement;

    /**
     * FunctionalUnitMovementTypeMovement constructor.
     * @param $functionalUnitMovement
     */
    public function __construct($functionalUnitMovement)
    {
        $this->functionalUnitMovement = $functionalUnitMovement;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value === 'income') {
            $functional_unit_id = $this->functionalUnitMovement['functional_unit_id'];

            $cap = true;
            if (isset($this->functionalUnitMovement['capital'])) {
                $capitalPayment = $this->functionalUnitMovement['capital'];
                $capitalAccumulated = FunctionalUnitMovement::accumulatedCapital($functional_unit_id)
                    ->where('functional_unit_id', $functional_unit_id)->first();
                $cap = $capitalPayment + $capitalAccumulated->capital <= 0;
            }

            $int = true;
            if (isset($this->functionalUnitMovement['interest'])) {
                $interestPayment = $this->functionalUnitMovement['interest'];
                $interestAccumulated = FunctionalUnitMovement::accumulatedInterest($functional_unit_id)
                    ->where('functional_unit_id', $functional_unit_id)->first();
                $int = $interestPayment + $interestAccumulated->interest <= 0;
            }
            if ($cap && $int) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El monto debe ser menor o igual a la deuda pendiente';
    }
}
