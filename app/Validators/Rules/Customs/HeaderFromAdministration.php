<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class HeaderFromAdministration implements Rule
{
    protected $administration_id;

    public function __construct($administration_id)
    {
        $this->administration_id = $administration_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('expense_headers')
            ->where(['id' => $value,
                'administration_id' => $this->administration_id])
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Header does not correspond to the Administration.';
    }
}
