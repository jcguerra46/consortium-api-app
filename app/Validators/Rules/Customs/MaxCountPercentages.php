<?php

namespace App\Validators\Rules\Customs;

use App\Models\PercentageConsortium;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class  MaxCountPercentages implements Rule
{
    public function __construct($maxCount)
    {
        $this->maxCountPercentages = $maxCount;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = PercentageConsortium::where('consortium_id', $value)->whereNull('deleted_at')->count();
        return $resultCount < $this->maxCountPercentages;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Consortium cannot have more than ' . $this->maxCountPercentages . ' percentages.';
    }

}
