<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MovementFUFromConsortium implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('functional_unit_movements')
            ->leftJoin('functional_units', 'functional_unit_movements.functional_unit_id', '=', 'functional_units.id')
            ->leftJoin('consortia', 'functional_units.consortium_id', '=', 'consortia.id')
            ->where([
                'functional_unit_movements.functional_unit_id' => $value,
                'consortia.administration_id' => Auth::user()->administration_id])
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Functional Unit Movement does not correspond to the functional unit';
    }
}
