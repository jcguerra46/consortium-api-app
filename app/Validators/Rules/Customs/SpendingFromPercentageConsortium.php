<?php

namespace App\Validators\Rules\Customs;

use App\Models\PercentageConsortium;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SpendingFromPercentageConsortium implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = PercentageConsortium::where('id', $value)
            ->whereHas('spending', function($spending) use ($value) {
                $spending->where('percentage_consortium_id', $value);
            })->count();
        return $resultCount === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El porcentaje está asignado a un gasto y no se puede eliminar.';
    }
}
