<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SpendingInstallmentFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('spending_installments')
            ->leftJoin('spendings', 'spendings.id', '=', 'spending_installments.spending_id')
            ->leftJoin('consortia', 'consortia.id', '=', 'spendings.consortium_id')
            ->where('spending_installments.id', $value)
            ->where('consortia.administration_id',Auth::user()->administration_id )
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Spending Installment does not correspond to the Administration';
    }

}
