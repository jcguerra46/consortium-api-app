<?php

namespace App\Validators\Rules\Customs;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class VirtualCashboxFromAdministration implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $resultCount = DB::table('virtual_cashboxes')
            ->leftJoin('consortia', 'consortia.id', '=', 'virtual_cashboxes.consortium_id')
            ->where('virtual_cashboxes.id', $value)
            ->where('consortia.administration_id', Auth::user()->administration_id)
            ->count();
        return $resultCount != 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Virtual Cashbox does not correspond to the administration';
    }

}
