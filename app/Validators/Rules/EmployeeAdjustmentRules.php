<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\EmployeeAdjustmentFromAdministration;
use App\Validators\Rules\Customs\EmployeeFromAdministration;
use Illuminate\Validation\Rule;

class EmployeeAdjustmentRules extends BaseRules
{
    /**
     * @return array[]|string[]
     */
    public static function indexRules()
    {
        return [
                'employee_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    new EmployeeFromAdministration(),
                    'bail'
                ]
            ] + parent::indexRules();
    }

    /**
     * @return array
     */
    public static function createRules()
    {
        return [
            'employee_id' => 'required|integer|exists:employees,id',
            'type' => [
                'required',
                'string',
                Rule::in(['percentage', 'value']),
            ],
            'haber_descuento' => [
                'required',
                'string',
                Rule::in(['haber', 'descuento']),
            ],
            'description' => [
                'required',
                'string',
                'max:255'
            ],
            'value' => [
                'required',
                'numeric',
                'gt:0'
            ],
            'used' => 'nullable|boolean',
            'suma_sueldo_jornal' => 'required|boolean',
            'recurrent' => 'required|boolean',
            'es_remunerativo' => 'required|boolean',
            'unit' => 'nullable|string|max:255',
            'percentage' => [
                'nullable',
                'numeric',
                'gt:0',
                'max:100'
            ],
            'percentage_sobre_base' => [
                'nullable',
                'string',
                'max:255',
                Rule::in(['basic', 'jornal', 'total_neto', 'total_bruto'])
            ],
            'active' => 'required|boolean',
        ];
    }

    /**
     * @return array|array[]
     */
    public static function editRules()
    {
        return self::createRules() + self::showRules();
    }

    /**
     * @return array[]
     */
    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:employee_adjustments,id',
                new EmployeeAdjustmentFromAdministration(),
                'bail'
            ]
        ];
    }

    /**
     * @return array[]|\string[][]
     */
    public static function destroyRules()
    {
        return self::showRules();
    }

    public static function exportRules()
    {
        return [
            'employee_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new EmployeeFromAdministration(),
                'bail'
            ]
        ];
    }
}
