<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;
use App\Validators\Rules\Customs\CuitValidator;
use Illuminate\Validation\Rule;

class EmployeeBeneficiaryRules extends BaseRules {

    /**
     * Rules for creating a employee beneficiary
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'employee_id' => 'required|integer|exists:employees,id',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'cuil' => [
                'required',
                'string',
                'max:255',
                new CuitValidator()
            ],
            'document_type' => [
                'required',
                'string',
                'max:255',
                Rule::in(['lc', 'le', 'dni'])
            ],
            'dni' => 'required|string|max:255',
            'relationship' => 'required|string|max:255',
            'porcentage' => 'required|numeric|gt:0|max:100',
        ];
    }

    /**
     * Rules for editing a employee beneficiary
     *
     * @return array
     */
    public static function updateRules()
    {
        return [
            'beneficiary_id' => 'required|integer|exists:employee_beneficiaries,id',
            'employee_id' => 'sometimes|required|integer|exists:employees,id',
            'first_name' => 'sometimes|required|string|max:255',
            'last_name' => 'sometimes|required|string|max:255',
            'cuil' => [
                'sometimes',
                'required',
                'string',
                'max:255',
                new CuitValidator()
            ],
            'document_type' => [
                'sometimes',
                'required',
                'string',
                'max:255',
                Rule::in(['lc', 'le', 'dni'])
            ],
            'dni' => 'sometimes|required|string|max:255',
            'relationship' => 'sometimes|required|string|max:255',
            'porcentage' => 'sometimes|required|numeric|gt:0|max:100',
        ];
    }

    /**
     * Rules for show a employee beneficiary
     *
     * @return array
     */
    public static function showRules()
    {
        return [
            'beneficiary_id' => 'required|integer|exists:employee_beneficiaries,id'
        ];
    }

    /**
     * Rules for delete a employee beneficiary
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [
            'beneficiary_id' => 'required|integer|exists:employee_beneficiaries,id'
        ];
    }
}
