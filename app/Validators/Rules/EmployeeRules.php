<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\EmployeeFromAdministration;
use Illuminate\Validation\Rule;
use App\Validators\Rules\Customs\CuitValidator;

class EmployeeRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'professional_function_id' => 'required|integer|exists:professional_functions,id|bail',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'birth_date' => 'required|date_format:d-m-Y',
            'type_dni' => [
                'required',
                'string',
                Rule::in(['LC', 'LE', 'DNI'])
            ],
            'dni' => 'required|string|max:255',
            'cuil' => [
                'required',
                'string',
                'max:255',
                new CuitValidator(),
            ],
            'number_docket' => 'required|string|max:255',
            'active' => 'required|boolean',
            'employee_status_id' => 'required|integer|exists:employee_status,id|bail',
            'consortium_id' => 'required|integer|exists:consortia,id|bail',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:employees,id',
                new EmployeeFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:employees,id',
                new EmployeeFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function editAfipRules()
    {
        return self::showRules() + [
                "conyuge" => 'sometimes|boolean',
                "seguro_colectivo_vida" => 'sometimes|boolean',
                "trabajador_convencionado" => 'sometimes|boolean',
                "adicional_obra_sociales" => 'sometimes|boolean',
                "maternidad" => 'sometimes|boolean',
                "situacion" => 'sometimes|integer|exists:afip_situaciones,id',
                "condicion" => 'sometimes|integer|exists:afip_condiciones,id',
                "actividad" => 'sometimes|integer|exists:afip_actividades,id',
                "zona" => 'sometimes|integer|exists:afip_zonas,id',
                "adicional_seguridad_social" => 'sometimes|numeric|min:0',
                "adicionales" => 'sometimes|string|max:255',
                "cantidad_hijos" => 'sometimes|integer|min:0|max:100',
                "cantidad_adherentes" => 'sometimes|integer|min:0|max:100',
                "modalidad_contratacion" => "sometimes|integer|exists:afip_modos_contratacion,id",
                "provincia_localidad" => "sometimes|integer|exists:afip_provincias,id",
                "siniestrado" => "sometimes|integer|exists:afip_siniestros,id",
                "importe_adicional_obra_social" => 'sometimes|numeric|min:0',
                "revista1" => "sometimes|integer|exists:afip_situaciones,id",
                "revista2" => "sometimes|integer|exists:afip_situaciones,id",
                "revista3" => "sometimes|integer|exists:afip_situaciones,id",
                "dia_revista1" => "sometimes|integer|min:0",
                "dia_revista2" => "sometimes|integer|min:0",
                "dia_revista3" => "sometimes|integer|min:0",
                "premios" => "sometimes|string|max:255",
                "conceptos_no_remunerativos" => "sometimes|string|max:255",
                "rectificacion_remuneracion" => "sometimes|string|max:255",
                "contribucion_tarea_diferencial" => "sometimes|string|max:255",
            ];
    }

    public static function editProfileRules()
    {
        return self::showRules() + [
                'social_work_id' => 'sometimes|integer|exists:social_works,id',
                'retired' => 'required|boolean',
                'adicional_obra_social' => 'required|boolean',
                'jornalizado_hours_per_day' => 'sometimes|integer',
                'entry_date' => 'required|date_format:d-m-Y',
                'departure_date' => 'required|date_format:d-m-Y',
                'antiquity' => 'required|boolean',
                'afiliado_sindicato' => 'required|boolean',
                'afiliado_f_m_v_d_d' => 'required|boolean',
                'descuento_c_p_f' => 'required|boolean',
                'd_g_d_y_p_c' => 'required|boolean',
                'calculate_obra_social_by_law' => 'required|boolean',
                'extra_hours_50' => 'required|integer',
                'extra_hours_saturday_50' => 'required|integer',
                'extra_hours_saturday_100' => 'required|integer',
                'extra_hours_sunday_100' => 'required|integer',
                'extra_hours_holiday_100' => 'required|integer',
                'law27430' => 'required|boolean',
            ];
    }

    public static function editDataRules()
    {
        return self::showRules() + [
                'sex' => [
                    'required',
                    'string',
                    Rule::in(['M', 'F'])
                ],
                'civil_status_code' => 'sometimes|integer',
                'address_street' => 'sometimes|string|max:255',
                'address_number' => 'sometimes|integer',
                'address_floor' => 'sometimes|string|max:255',
                'address_departament' => 'sometimes|string|max:255',
                'postal_code' => 'sometimes|integer',
                'phone' => 'sometimes|string|max:255',
                'email' => 'sometimes|email',
                'nationality_code' => 'sometimes|integer',
                'nationality' => 'string|max:255',
            ];
    }
}
