<?php

namespace App\Validators\Rules;

use App\Utils\RulesHelper;
use App\Validators\Rules\BaseRules;
use App\Validators\Rules\Customs\CuitValidator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Validators\Rules\Customs\HeaderFromAdministration;

class ExpenseHeaderRules extends BaseRules
{

    /**
     * Rules for creating a expense header
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'name_header' => 'required|string|max:255',
            'cuit' => [
                'required',
                'string',
                'max:255',
                new CuitValidator()
            ],
            'name' => 'required|string|max:255',
            'address_street' => 'required|string|max:255',
            'address_number' => 'required|string|max:255',
            'email' => 'required|string|max:255|email',
            'phone' => 'required|string|max:255',
            'fiscal_situation' => [
                'required',
                'string',
                Rule::in(['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento'])
            ],
            'postal_code' => 'required|string|max:255',
            'rpa' => 'required|string|max:255',
        ];
    }

    /**
     * Rules for editing a expense header
     *
     * @return array
     */
    public static function updateRules()
    {
        return self::showRules() +
            RulesHelper::formatEditRules(self::storeRules());
    }

    /**
     * Rules for show a expense header
     *
     * @return array
     */
    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:expense_headers,id',
                new HeaderFromAdministration(Auth::user()->administration_id)
            ],
        ];
    }

    /**
     * Rules for delete a expense header
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:expense_headers,id',
                new HeaderFromAdministration(Auth::user()->administration_id)
            ],
        ];
    }
}
