<?php


namespace App\Validators\Rules;

use Illuminate\Validation\Rule;
use App\Validators\Rules\Customs\ContactFromAdministration;
use App\Validators\Rules\Customs\ContactFromFunctionalUnitRules;
use App\Validators\Rules\Customs\FunctionalUnitFromAdministration;

class FunctionalUnitContactRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'functional_unit_id' => [
                'required',
                'integer',
                'exists:functional_units,id',
                new FunctionalUnitFromAdministration()
            ],
            'contact_id' => [
                'required',
                'integer',
                'exists:contacts,id',
                new ContactFromAdministration()
            ],
            'contact_type' => [
                'required',
                'string',
                'max:255',
                Rule::in(['PROPIETARIO', 'INQUILINO','OTRO']),
                'bail'
            ],
            'primary' => 'required|boolean'
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:functional_unit_contacts,id',
                new ContactFromFunctionalUnitRules()
            ],
            'functional_unit_id' => [
                'required',
                'integer',
                'exists:functional_units,id',
                new FunctionalUnitFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
