<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\MovementFUFromConsortium;
use App\Validators\Rules\Customs\FunctionalUnitFromAdministration;
use App\Validators\Rules\Customs\FunctionalUnitMovementTypeMovement;
use Illuminate\Validation\Rule;

class FunctionalUnitMovementRules extends BaseRules
{
    public static function indexRules()
    {
        return parent::indexRules() + [
            'functional_unit_id' => [
                'required',
                'integer',
                new MovementFUFromConsortium()
            ]
        ];
    }

    public static function storeRules($request)
    {
        return self::indexRules() + [
            'capital' => 'required_without:interest|numeric|min:1',
            'interest' => 'required_without:capital|numeric|min:1',
            'type_movement' => [
                'required',
                'string',
                Rule::in(['income', 'outflow']),
                new FunctionalUnitMovementTypeMovement($request)
            ],
            'description' => 'required|string|max:255',

        ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                'exists:functional_unit_movements,id',
                'bail'
            ]
        ];
    }

    public static function getBalanceRules()
    {
        return [
            'functional_unit_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new FunctionalUnitFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function getDebtsRules()
    {
        return self::indexRules();
    }

}
