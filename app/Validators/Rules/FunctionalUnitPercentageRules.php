<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\PercentageFUFromConsortium;

class FunctionalUnitPercentageRules extends BaseRules
{

    public static function indexRules()
    {
        return parent::indexRules() + [
            'functional_unit_id' => [
                'required',
                'integer',
                new PercentageFUFromConsortium()
            ]
        ];
    }

    public static function editRules()
    {
        return self::showRules() + [
                'value' => [
                    'required',
                    'numeric',
                    'min:0',
                    'max:100',
                    'bail'
                ]
            ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                'exists:percentages_functional_units,id',
                'bail'
            ],
            'functional_unit_id' => [
                'required',
                'integer',
                new PercentageFUFromConsortium()
            ]
        ];
    }

    public static function massUpdateRules()
    {
        return self::indexPercentageFU() + [
                'percentages' => 'required|array',
                'percentages.*.id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    'bail',
                    new PercentageFUFromConsortium()
                ],
                'percentages.*.value' => [
                    'required',
                    'numeric',
                    'min:0',
                    'max:100',
                    'bail'
                ]
            ];
    }
}
