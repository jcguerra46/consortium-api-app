<?php


namespace App\Validators\Rules;

use App\Utils\RulesHelper;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\FunctionalUnitFromAdministration;
use Illuminate\Validation\Rule;

class FunctionalUnitRules extends BaseRules
{
    public static function indexRules()
    {
        return parent::indexRules() + [
                'consortium_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'bail',
                    new ConsortiumFromAdministration(),
                ]
            ];
    }

    public static function createRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'package_id' => 'nullable|integer|exists:packages,id',
            'functional_unit_type_id' => 'nullable|integer|exists:functional_unit_types,id',
            'owner_first_name' => 'required|string|max:255',
            'owner_last_name' => 'string|max:255',
            'department' => 'string|max:255',
            'floor' => 'required|numeric|max:255',
            'functional_unit_number' => 'required|string|max:255',
            'initial_balance' => 'numeric',
            'type_forgive_interest' => [
                Rule::in(['until_further_notice', 'this_liquidation', null])
            ],
            'legal_state' => [
                Rule::in(['trial', 'agreement', 'lawyer', null])
            ],
            'm2' => 'nullable||numeric',
            'order' => 'nullable|integer',
        ];
    }

    public static function editRules()
    {
        return self::showRules() +
            RulesHelper::formatEditRules(self::createRules());
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:functional_units,id',
                new FunctionalUnitFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:functional_units,id',
                new FunctionalUnitFromAdministration()
            ]
        ];
    }

    public static function indexWithConsortiumRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'limit' => 'integer|max:100',
        ];
    }

    public static function massCreate()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'functional_units.*.package_id' => 'nullable|integer|exists:packages,id',
            'functional_units.*.functional_unit_type_id' => 'nullable|integer|exists:functional_unit_types,id',
            'functional_units.*.owner_first_name' => 'required|string|max:255',
            'functional_units.*.owner_last_name' => 'string|max:255',
            'functional_units.*.department' => 'string|max:255',
            'functional_units.*.floor' => 'numeric|max:255',
            'functional_units.*.functional_unit_number' => 'string|max:255',
            'functional_units.*.initial_balance' => 'numeric',
            'functional_units.*.type_forgive_interest' => [
                Rule::in(['until_further_notice', 'this_liquidation', null])
            ],
            'functional_units.*.legal_state' => [
                Rule::in(['trial', 'agreement', 'lawyer', null])
            ],
            'functional_units.*.m2' => 'nullable||numeric',
            'functional_units.*.order' => 'nullable|integer',
        ];
    }
}
