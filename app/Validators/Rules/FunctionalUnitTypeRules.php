<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;

class FunctionalUnitTypeRules extends BaseRules {

    /**
     * Rules for creating a functional unit type
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'description' => 'required|string|max:255',
            'active' => 'boolean'
        ];
    }

    /**
     * Rules for editing a functional unit type
     *
     * @return array
     */
    public static function updateRules()
    {
        return [
            'description' => 'sometimes|string|max:255',
            'active' => 'sometimes|boolean'
        ];
    }

    /**
     * Rules for show a functional unit type
     *
     * @return array
     */
    public static function showRules()
    {
        return [

        ];
    }

    /**
     * Rules for delete a functional unit type
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [

        ];
    }
}
