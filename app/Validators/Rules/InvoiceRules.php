<?php


namespace App\Validators\Rules;


use App\Validators\Rules\Customs\InvoiceFromAdministration;
use App\Validators\Rules\Customs\ProviderFromAdministration;

class InvoiceRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'provider_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new ProviderFromAdministration()
            ],
            'invoice_number' => 'required|string|max:255',
            'description' => 'nullable|string|max:255',
            'date' => 'required|date_format:d-m-Y',
            'amount' => 'required|numeric|gt:0',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
          'id' => [
              'required',
              'integer',
              'gt:0',
              'max:2147483647',
              new InvoiceFromAdministration()
          ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
