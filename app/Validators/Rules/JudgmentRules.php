<?php


namespace App\Validators\Rules;

use App\Utils\RulesHelper;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\JudgmentFromAdministration;

class JudgmentRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                new ConsortiumFromAdministration()
            ],
            'cover' => 'required|string|max:255',
            'number_expedient' => 'required|string|max:255',
            'judged' => 'required|string|max:255',
            'objet' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'reclaimed_amount' => 'required|numeric|gt:0',
            'active' => 'required|boolean',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + RulesHelper::formatEditRules(self::createRules());
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:judgments,id',
                new JudgmentFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }

}
