<?php

namespace App\Validators\Rules;

class MenuRules
{

    public static function userMenuRules()
    {
        return [
            'menu' => 'required|json'
        ];
    }

    public static function array()
    {
        return [
            'menu' => 'required|array',
            'menu.*.title' => 'required|string|max:255',
            'menu.*.fav' => 'required|boolean',
            'menu.*.route' => 'required_without:menu.*.childs|nullable|string',
            'menu.*.childs' => 'nullable|array',
        ];
    }


}
