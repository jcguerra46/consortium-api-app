<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\NoteFromAdministration;

class NoteRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'is_debt_note' => 'required|boolean',
            'prorrateo_in_expense' => 'required|boolean',
            'limit_date' => 'nullable|date_format:d-m-Y',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
          'id' => [
              'required',
              'integer',
              'exists:notes,id',
              new NoteFromAdministration()
          ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
