<?php

namespace App\Validators\Rules;

class PaymentMethodsRules extends BaseRules
{
    public static function showRules()
    {
        return [
            'id' => 'required|integer|gt:0|max:500|bail'
        ];
    }
}
