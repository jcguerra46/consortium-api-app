<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\BankAccountFromAdministration;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\PaymentOrderFromAdministration;
use App\Validators\Rules\Customs\PaymentOrderStatusIs;
use App\Validators\Rules\Customs\ProviderFromAdministration;
use App\Validators\Rules\Customs\SpendingInstallmentFromAdministration;
use Illuminate\Support\Arr;

class PaymentOrderRules extends BaseRules
{
    /**
     * @return array
     */
    public static function createRules()
    {
        return [
            'provider_id' => [
                'required',
                'integer',
                'gt:0',
                new ProviderFromAdministration(),
                'bail'
            ],
            'description' => 'required|string',
            'date' => 'required|date_format:d-m-Y',
            'installments' => 'required|array|min:2|bail',
            'installments.*.id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'distinct',
                'exists:spending_installments,id',
                new SpendingInstallmentFromAdministration(),
                'bail'
            ]
        ];
    }

    /**
     * @return array|array[]
     */
    public static function editRules()
    {
        return Arr::except(self::createRules(), 'provider_id')
            + self::annularRules();
    }

    /**
     * @return array[]
     */
    public static function destroyRules()
    {
        return self::annularRules();
    }

    /**
     * @return array[]
     */
    public static function annularRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new PaymentOrderFromAdministration(),
                new PaymentOrderStatusIs('open'),
                'bail'
            ]
        ];
    }

    /**
     * @return array[]
     */
    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new PaymentOrderFromAdministration(),
                'bail'
            ]
        ];
    }

    /**
     * @return array[]
     */
    public static function duplicateRules()
    {
        return self::showRules();
    }

    /**
     * @return array[]
     */
    public static function printOrderRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new PaymentOrderFromAdministration(),
                new PaymentOrderStatusIs('open'),
                'bail'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function doPayment()
    {
        $paymentRules = [
            'cashboxes' => 'required|min:1|bail|array',
            'cashboxes.*.cash_amount' => 'required|numeric|bail',
            'cashboxes.*.date' => 'required|date_format:d-m-Y',
            'cashboxes.*.consortium_id' => [
                'required',
                'bail',
                'integer',
                new ConsortiumFromAdministration()
            ],
            'cashboxes.*.checks' => 'bail|array',
            'cashboxes.*.checks.*' => 'required|bail|integer',
            'cashboxes.*.transfers' => 'bail|array',
            'cashboxes.*.transfers.*.bank_account_id' => [
                'required',
                'integer',
                'max:2147483647',
                new BankAccountFromAdministration(),
                'bail'
            ],
            'cashboxes.*.transfers.*.amount' => 'required|numeric|gt:0',
        ];

        return array_merge($paymentRules, self::annularRules());
    }
}
