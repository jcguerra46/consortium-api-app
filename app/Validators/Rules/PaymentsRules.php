<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\BankAccountMovementFromAdministration;
use App\Validators\Rules\Customs\BankAccountMovementIsOutflow;
use App\Validators\Rules\Customs\BankAccountMovementWithoutCashbox;
use App\Validators\Rules\Customs\CheckFromAdministration;
use App\Validators\Rules\Customs\CheckTotalAmount;
use App\Validators\Rules\Customs\CheckTotalAmountEqualSpending;
use App\Validators\Rules\Customs\PaymentFromAdministration;
use App\Validators\Rules\Customs\PaymentOrderFromAdministration;
use App\Validators\Rules\Customs\PaymentOrderStatusIs;
use App\Validators\Rules\Customs\ProviderFromAdministration;
use App\Validators\Rules\Customs\SpendingInstallmentFromAdministration;
use App\Validators\Rules\Customs\SpendingInstallmentNotPaid;

class PaymentsRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'provider_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    new ProviderFromAdministration(),
                    'bail',
                ]
            ] + parent::indexRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:payments,id',
                new PaymentFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function createRules()
    {
        return [
            'payment_order_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:payment_orders,id',
                new PaymentOrderFromAdministration(),
                new PaymentOrderStatusIs('open'),
                'bail',
            ],
            'date' => 'required|date_format:d-m-Y',
            'cash_amount' => 'required|numeric|gte:0',
            'checks' => 'nullable|array',
            'checks.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:checks,id',
                new CheckFromAdministration(),
                'bail',
            ],
            'transfers' => 'nullable|array',
            'transfers.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:bank_account_movements.id',
                new BankAccountMovementFromAdministration(),
                new BankAccountMovementWithoutCashbox(),
                new BankAccountMovementIsOutflow(),
                'bail'
            ],
            'total_amount' => [
                'required',
                'numeric',
                'gt:0',
                new CheckTotalAmount(),
                'bail'
            ],
        ];
    }

    public static function createDirectRules()
    {
        return [
            'spending_installment_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:spending_installments,id',
                new SpendingInstallmentFromAdministration(),
                new SpendingInstallmentNotPaid(),
                'bail',
            ],
            'date' => 'required|date_format:d-m-Y',
            'cash_amount' => 'required|numeric|gte:0',
            'checks' => 'nullable|array',
            'checks.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:checks,id',
                new CheckFromAdministration(),
                'bail',
            ],
            'transfers' => 'nullable|array',
            'transfers.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:bank_account_movements.id',
                new BankAccountMovementFromAdministration(),
                new BankAccountMovementWithoutCashbox(),
                new BankAccountMovementIsOutflow(),
                'bail'
            ],
            'total_amount' => [
                'required',
                'numeric',
                'gt:0',
                new CheckTotalAmount(),
                new CheckTotalAmountEqualSpending(),
                'bail'
            ]
        ];
    }

}
