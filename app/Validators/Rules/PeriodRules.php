<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\MaxCountPercentages;
use App\Validators\Rules\Customs\NoteFromAdministration;
use App\Validators\Rules\Customs\PercentageConsortiumFromAdministration;
use App\Validators\Rules\Customs\PeriodFromAdministration;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class PeriodRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'consortium_id' => [
                    'required',
                    'integer',
                    'exists:consortia,id',
                    'max:2147483647',
                    new ConsortiumFromAdministration(),
                    'bail'
                ]
            ] + parent::indexRules();
    }

    public static function editRules()
    {
        return self::showRules() + [
                'name' => 'required|string|max:255',
                'description' => 'nullable|string'
            ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                new PeriodFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function exportRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                'max:2147483647',
                new ConsortiumFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function getOpenRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'exists:consortia,id',
                'max:2147483647',
                new ConsortiumFromAdministration(),
                'bail'
            ]
        ];
    }
}
