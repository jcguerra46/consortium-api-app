<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;

class PermissionRules extends BaseRules {

    /**
     * Rules for creating a permission
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'active' => 'required|boolean'
        ];
    }

    /**
     * Rules for editing a permission
     *
     * @return array
     */
    public static function updateRules()
    {
        return [
            'name' => 'sometimes|string|max:255',
            'description' => 'sometimes|string|max:255',
            'active' => 'sometimes|boolean',
        ];
    }

    /**
     * Rules for show a permission
     *
     * @return array
     */
    public static function showRules()
    {
        return [

        ];
    }

    /**
     * Rules for delete a permission
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [

        ];
    }
}
