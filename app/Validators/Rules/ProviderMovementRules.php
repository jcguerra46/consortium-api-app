<?php


namespace App\Validators\Rules;


use App\Validators\Rules\Customs\ProviderFromAdministration;

class ProviderMovementRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'provider_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    new ProviderFromAdministration(),
                    'bail'
                ]
            ] + parent::indexRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'max:2147483647',
                'exists:provider_financial_movements,id',
                'bail'
            ]
        ];
    }

    public static function exportRules()
    {
        return [
            'provider_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new ProviderFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function getBalanceRules()
    {
        return [
            'provider_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new ProviderFromAdministration(),
                'bail'
            ]
        ];
    }
}
