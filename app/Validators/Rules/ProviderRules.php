<?php


namespace App\Validators\Rules;

use App\Utils\RulesHelper;
use App\Validators\Rules\Customs\CuitValidator;
use App\Validators\Rules\Customs\ProviderFromAdministration;
use App\Validators\Rules\Customs\ServiceFromAdministration;
use Illuminate\Validation\Rule;

class ProviderRules extends BaseRules
{
    public static function indexRules()
    {
        return parent::indexRules() + [
                'filters.id' => 'integer|gt:0',
                'filters.business_name' => 'string|max:255',
                'filters.name' => 'string|max:255',
                'filters.cuit' => 'string|max:255'
            ];
    }

    public static function createRules()
    {
        return [
            'business_name' => 'required|string|max:255',
            'name' => 'nullable|string|max:255',
            'address' => 'required|string|max:255',
            'location' => 'nullable|string|max:255',
            'province_id' => 'nullable|integer|exists:provinces,id',
            'postal_code' => 'nullable|string|max:255',
            'cuit' => [
                'required',
                'string',
                'max:255',
                new CuitValidator(),
            ],
            'email' => 'nullable|string|max:255|email',
            'phone' => 'nullable|string|max:255',
            'note' => 'nullable|string',
            'license' => 'nullable|string|max:255',
            'attention_hours' => 'nullable|string|max:255',
            'fiscal_situation' => [
                'nullable',
                'string',
                'max:255',
                Rule::in(['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento'])
            ],
            'services' => 'nullable|array',
            'services.*' => [
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:services,id',
                new ServiceFromAdministration(),
                'bail'
            ]
        ];
    }

    public static function editRules()
    {
        return self::showRules() + RulesHelper::formatEditRules(self::createRules());
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'exists:providers,id',
                new ProviderFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }

    public static function getProviderServicesRules()
    {
        return self::showRules() + parent::indexRules();
    }
}
