<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;

class RoleRules extends BaseRules {

    /**
     * Rules for creating a role
     *
     * @return array
     */
    public static function createRules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'active' => 'required|boolean',
            'permissions' => 'array',
            'permissions.*' => 'integer|distinct'
        ];
    }

    /**
     * Rules for editing a role
     *
     * @return array
     */
    public static function editRules()
    {
        return [
            'name' => 'sometimes|string|max:255',
            'description' => 'sometimes|string|max:255',
            'active' => 'sometimes|boolean',
            'permissions' => 'array',
            'permissions.*' => 'integer|distinct'
        ];
    }

    /**
     * Rules for show a role
     *
     * @return array
     */
    public static function showRules()
    {
        return [

        ];
    }

    /**
     * Rules for delete a role
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [

        ];
    }
}
