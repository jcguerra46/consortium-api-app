<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\EmployeeFromAdministration;

class SalariesRules extends BaseRules
{
    public static function indexRules()
    {
        return parent::indexRules() + [
                'employee_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    new EmployeeFromAdministration(),
                    'bail'
                ]
            ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:salaries,id',
                'bail'
            ]
        ];
    }

}
