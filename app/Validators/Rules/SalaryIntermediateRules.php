<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\EmployeeFromAdministration;
use Illuminate\Validation\Rule;

class SalaryIntermediateRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'employee_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:employees,id',
                new EmployeeFromAdministration(),
                'bail'
            ],
            'month' => 'required|date_format:m',
            'year' => 'required|date_format:Y',
            'extra_hours' => 'array',
            'extra_hours.*.concept' => 'required|string|max:255',
            'extra_hours.*.value' => 'required|numeric|min:0',
            'payslip_contributions_month' => 'required|date_format:m',
            'payslip_contributions_year' => 'required|date_format:Y',
            'payslip_deposit_date' => 'required|date_format:d-m-Y',
            'payslip_contributions_bank' => 'required|string|max:255',
        ];
    }
}
