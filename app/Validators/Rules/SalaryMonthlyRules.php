<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\EmployeeFromAdministration;
use Illuminate\Validation\Rule;

class SalaryMonthlyRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'employee_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:employees,id',
                new EmployeeFromAdministration(),
                'bail'
            ],
            'month' => 'required|date_format:m',
            'year' => 'required|date_format:Y',
            'worked_hours' => 'numeric',
            'extra_hours' => 'array',
            'extra_hours.*.id' => [
                Rule::in([1, 2, 3, 4, 5, null])
            ],
            'extra_hours.*.percentage' => 'required|numeric|gt:0:|max:100',
            'extra_hours.*.description' => 'required|strung|max:255',
            'extra_hours.*.quantity' => 'required|numeric|gt:0',
            'vacation_days' => 'required|integer|gte:0|max:3276',
            'not_vacation_days' => 'required|integer|min:0|max:3276',
            'license_days' => 'required|integer|min:0|max:3276',
            'license_days_justified' => 'required|integer|min:0|max:3276',
            'payslip_concept' => 'required|string|max:255',
            'payslip_deposit_date' => 'required|date_format:d-m-Y',
            'payslip_contributions_month' => 'required|date_format:m',
            'payslip_contributions_year' => 'required|date_format:Y',
            'payslip_contributions_bank' => 'required|string|max:255',
        ];
    }
}
