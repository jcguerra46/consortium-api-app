<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\ServiceFromAdministration;
use Illuminate\Validation\Rule;

class ServiceRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'description' => 'required|string|max:255',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
          'id' => [
              'required',
              'integer',
              'exists:services,id',
              new ServiceFromAdministration()
          ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
