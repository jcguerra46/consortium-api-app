<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\SpendingDescriptionFromAdministration;

class SpendingDescriptionRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'bail',
                'required',
                'integer',
                'exists:spending_descriptions,id',
                new SpendingDescriptionFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'bail',
                'required',
                'integer',
                'exists:spending_descriptions,id',
                new SpendingDescriptionFromAdministration()
            ]
        ];
    }

    public static function paginateRules()
    {
        return parent::paginateRules() + [
                'filters.id' => 'integer|max:2147483647',
                'filters.title' => 'string|max:255',
                'filters.description' => 'string'
            ];
    }
}
