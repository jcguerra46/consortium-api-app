<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\SpendingInstallmentFromAdministration;

class SpendingInstallmentRules extends BaseRules
{
    public static function showRules()
    {
        return [
            'id' => [
                'bail',
                'required',
                'integer',
                'gt:0',
                new SpendingInstallmentFromAdministration()
            ]
        ];
    }
}
