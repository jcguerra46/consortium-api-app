<?php


namespace App\Validators\Rules;

use App\Utils\RulesHelper;
use App\Validators\Rules\Customs\CategoryFromAdministration;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\PercentageConsortiumFromAdministration;
use App\Validators\Rules\Customs\ProviderFromAdministration;
use App\Validators\Rules\Customs\SpendingRecurrentFromAdministration;

class SpendingRecurrentRules extends BaseRules
{
    public static function indexRules()
    {
        return parent::indexRules() + [
                'filters.id' => 'integer|gt:0|max:2147483647',
                'filters.consortium_id' => 'integer|gt:0|max:2147483647',
                'filters.provider_id' => 'integer|gt:0|max:2147483647',
                'filters.type' => 'string|in:fixed,variable'
            ];
    }

    public static function createRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'gt:0',
                new ConsortiumFromAdministration(),
                'bail'
            ],
            'provider_id' => [
                'nullable',
                'integer',
                'gt:0',
                new ProviderFromAdministration(),
                'bail'
            ],
            'service_id' => [
                'nullable',
                'integer',
                'gt:0',
                'exists:services,id',
                'bail',
            ],
            'percentage_consortium_id' => [
                'required',
                'integer',
                'gt:0',
                new PercentageConsortiumFromAdministration(),
                'bail',
            ],
            'category_id' => [
                'required',
                'integer',
                'gt:0',
                new CategoryFromAdministration(),
                'bail',
            ],
            'description' => 'required|string',
            'amount' => [
                'nullable',
                'required_if:type,==,fixed',
                'numeric',
                'gt:0'
            ],
            'type' => 'required|string|in:fixed,variable',
            'periodicity' => 'required|integer|min:1|max:12',
            'due_date' => 'nullable|date_format:d-m-Y',
            'start_date' => 'required|date_format:d-m-Y'
        ];
    }

    public static function editRules()
    {
        return self::showRules() + RulesHelper::formatEditRules(self::createRules());
    }

    public static function showRules()
    {
        return [
            'id' => [
                'bail',
                'required',
                'integer',
                'gt:0',
                new SpendingRecurrentFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
