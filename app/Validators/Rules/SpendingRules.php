<?php


namespace App\Validators\Rules;

use App\Validators\Rules\Customs\CategoryFromAdministration;
use App\Validators\Rules\Customs\CheckSumInstallments;
use App\Validators\Rules\Customs\CheckSumPercentages;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\FunctionalUnitFromAdministration;
use App\Validators\Rules\Customs\PercentageConsortiumFromAdministration;
use App\Validators\Rules\Customs\ProviderFromAdministration;
use App\Validators\Rules\Customs\ServiceFromAdministration;
use App\Validators\Rules\Customs\SpendingFromAdministration;

class SpendingRules extends BaseRules
{
    public static function createRules()
    {
        return [
            'number' => 'nullable|integer',
            'amount' => 'required|numeric|gt:0|bail',
            'description' => 'required|string',
            'date' => 'required|date_format:d-m-Y',
            'affect_financial_section' => 'required|boolean',
            'prorate_in_expenses' => 'required|boolean',
            'invoice_number' => 'nullable|integer|max:2147483647',
            'category_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new CategoryFromAdministration(),
                'bail',
            ],
            'provider_id' => [
                'nullable',
                'integer',
                'gt:0',
                'max:2147483647',
                new ProviderFromAdministration(),
                'bail',
            ],
            'service_id' => [
                'nullable',
                'integer',
                'gt:0',
                'max:2147483647',
                new ServiceFromAdministration(),
                'bail',
            ],
            'consortium_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new ConsortiumFromAdministration()
            ],
            'functional_unit_id' => [
                'nullable',
                'integer',
                'gt:0',
                'max:2147483647',
                new FunctionalUnitFromAdministration(),
                'bail',
            ],
            'percentages' => [
                'required_without:functional_unit_id',
                'array',
                new CheckSumPercentages(),
                'bail'
            ],
            'percentages.*.value' => 'required|numeric|gt:0|max:100',
            'percentages.*.percentage_consortium_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'distinct',
                new PercentageConsortiumFromAdministration(),
                'bail',
            ],
            'installments' => [
                'array',
                'max:48',
                new CheckSumInstallments(),
                'bail'
            ],
            'installments.*.date_pay' => 'nullable|date_format:d-m-Y',
            'installments.*.amount' => 'required|numeric|gt:0',
            'installments.*.number' => 'required|integer|distinct|gt:0|max:48',
            'installments.*.invoice_number' => 'nullable|string|max:255',
            'installments.*.include_open_period' => 'required|boolean',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + self::createRules();
    }

    public static function showRules()
    {
        return [
            'id' => [
                'bail',
                'required',
                'integer',
                'gt:0',
                new SpendingFromAdministration()
            ]
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
