<?php

namespace App\Validators\Rules;

use App\Validators\Rules\BaseRules;

class UserAdministrationRules extends BaseRules {

    /**
     * Rules for creating a user administration
     *
     * @return array
     */
    public static function storeRules()
    {
        return [
            'principal' => 'required|boolean',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users_administrations,email',
            'password' => 'required|min:6|string|confirmed',
            'active' => 'required|boolean',
            'menu' => 'json',
            'roles' => 'array',
            'roles.*' => 'integer|distinct',
            'consortiums' => 'array',
            'consortiums.*' => 'integer|distinct'
        ];
    }

    /**
     * Rules for editing a user administration
     *
     * @return array
     */
    public static function updateRules()
    {
        return [
            'principal' => 'sometimes|boolean',
            'first_name' => 'sometimes|string|max:255',
            'last_name' => 'sometimes|string|max:255',
            'email' => 'sometimes|email|unique:users_administrations,email',
            'password' => 'sometimes|min:6|string|confirmed',
            'active' => 'sometimes|boolean',
            'menu' => 'sometimes|json',
            'roles' => 'sometimes|array',
            'roles.*' => 'sometimes|integer|distinct',
            'consortiums' => 'sometimes|array',
            'consortiums.*' => 'sometimes|integer|distinct'
        ];
    }

    /**
     * Rules for show a user administration
     *
     * @return array
     */
    public static function showRules()
    {
        return [

        ];
    }

    /**
     * Rules for delete a user administration
     *
     * @return array
     */
    public static function destroyRules()
    {
        return [

        ];
    }
}
