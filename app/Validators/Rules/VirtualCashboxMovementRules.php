<?php

namespace App\Validators\Rules;

use App\Validators\Rules\Customs\BankAccountMovementFromAdministration;
use App\Validators\Rules\Customs\BankAccountMovementIsIncome;
use App\Validators\Rules\Customs\BankAccountMovementIsOutflow;
use App\Validators\Rules\Customs\BankAccountMovementWithoutCashbox;
use App\Validators\Rules\Customs\CheckFromAdministration;
use App\Validators\Rules\Customs\CheckTotalAmountIncome;
use App\Validators\Rules\Customs\VirtualCashboxFromAdministration;
use App\Validators\Rules\Customs\VirtualCashboxWithoutMovements;

class VirtualCashboxMovementRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'virtual_cashbox_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    new VirtualCashboxFromAdministration(),
                    'bail'
                ]
            ] + parent::indexRules();
    }

    public static function createInitialBalanceRules()
    {
        return [
            'virtual_cashbox_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new VirtualCashboxFromAdministration(),
                new VirtualCashboxWithoutMovements(),
                'bail',
            ],
            'cash_amount' => 'required|numeric|gte:0',
            'checks' => 'nullable|array',
            'checks.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:checks,id',
                new CheckFromAdministration(),
                'bail',
            ],
            'bank_account_movements' => 'nullable|array',
            'bank_account_movements.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:bank_account_movements,id',
                new BankAccountMovementFromAdministration(),
                new BankAccountMovementWithoutCashbox(),
                new BankAccountMovementIsIncome(),
                'bail'
            ],
            'total_amount' => [
                'required',
                'numeric',
                'gt:0',
                new CheckTotalAmountIncome(),
                'bail'
            ],
        ];
    }

    public static function createIncomeRules()
    {
        return [
            'virtual_cashbox_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                new VirtualCashboxFromAdministration(),
                'bail',
            ],
            'description' => 'required|string|max:255',
            'date' => 'required|date_format:d-m-Y',
            'cash_amount' => 'required|numeric|gte:0',
            'checks' => 'nullable|array',
            'checks.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:checks,id',
                new CheckFromAdministration(),
                'bail',
            ],
            'bank_account_movements' => 'nullable|array',
            'bank_account_movements.*' => [
                'integer',
                'distinct',
                'gt:0',
                'max:2147483647',
                'exists:bank_account_movements,id',
                new BankAccountMovementFromAdministration(),
                new BankAccountMovementWithoutCashbox(),
                new BankAccountMovementIsIncome(),
                'bail'
            ],
            'total_amount' => [
                'required',
                'numeric',
                'gt:0',
                new CheckTotalAmountIncome(),
                'bail'
            ],
        ];
    }

    public static function createOutflowRules()
    {
        return [

        ];
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'min:0',
                'max:2147483647',
                'exists:virtual_cashboxes_movements,id',
                new BankAccountMovementFromAdministration(),
                'bail',
            ],
        ];
    }

    public static function destroyRules()
    {
        return self::showRules();
    }
}
