<?php

namespace App\Validators\Rules;

use App\Utils\RulesHelper;
use App\Validators\Rules\Customs\ConsortiumFromAdministration;
use App\Validators\Rules\Customs\VirtualCashboxFromAdministration;
use App\Validators\Rules\Customs\VirtualCashboxWithoutMovements;

class VirtualCashboxRules extends BaseRules
{
    public static function indexRules()
    {
        return [
                'consortium_id' => [
                    'required',
                    'integer',
                    'gt:0',
                    'max:2147483647',
                    new ConsortiumFromAdministration()
                ]
            ] + parent::indexRules();
    }

    public static function createRules()
    {
        return [
            'consortium_id' => [
                'required',
                'integer',
                'gt:0',
                'max:2147483647',
                'exists:consortia,id',
                new ConsortiumFromAdministration(),
                'bail'
            ],
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:255',
            'show_in_expense' => 'required|boolean',
            'independent' => 'required|boolean',
        ];
    }

    public static function editRules()
    {
        return self::showRules() + RulesHelper::formatEditRules(self::createRules());
    }

    public static function showRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'min:0',
                'max:2147483647',
                'exists:virtual_cashboxes,id',
                new VirtualCashboxFromAdministration(),
                'bail'
            ],
        ];
    }

    public static function destroyRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'min:0',
                'max:2147483647',
                'exists:virtual_cashboxes,id',
                new VirtualCashboxFromAdministration(),
                new VirtualCashboxWithoutMovements(),
                'bail'
            ],
        ];
    }

    public static function getBalancesRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                'min:0',
                'max:2147483647',
                'exists:virtual_cashboxes,id',
                new VirtualCashboxFromAdministration(),
                'bail'
            ],
        ];
    }
}
