<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Administration::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(2, true),
        'business_name' => $faker->company,
        'postal_code' => $faker->postcode,
        'email' => $faker->unique()->safeEmail,
        'cuit' => $faker->numerify('##-########-#'),
        'phone' => $faker->phoneNumber,
        'image' => $faker->imageUrl(),
        'signature' => $faker->imageUrl(),
        'address_street' => $faker->streetName,
        'address_number' => $faker->numerify('####'),
        'address_floor' => $faker->numerify('##'),
        'address_department' => $faker->randomLetter,
        'responsible' => $faker->firstName . ' ' . $faker->lastName,
        'unregistered_payment' => $faker->boolean,
        'fiscal_situation' => $faker->randomElement(['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento']),
        'active' => true,
        'province_id' => $faker->numberBetween(1, 23),
        'contact' => $faker->firstName . ' ' . $faker->lastName,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
