<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\Consortium;
use App\Models\AgendaEvent;
use Faker\Generator as Faker;

$factory->define(AgendaEvent::class, function (Faker $faker) {
    return [
        'administration_id' => Administration::all()->random()->id,
        'consortium_id' => Consortium::all()->random()->id,
        'eventable' => $faker->randomElement([1,2,3,4,5,6]),
        'title' => $faker->sentence(3, true),
        'description' => $faker->paragraph(3, true),
        'date' => $faker->date('Y-m-d'),
        'trans' => $faker->sentence(2, true),
        'service_class_name' => $faker->sentence(2, true),
        'route_class_name' => $faker->sentence(2, true)
    ];
});
