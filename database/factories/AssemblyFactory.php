<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Assembly::class, function (Faker $faker) {
    return [
        'consortium_id' => 1,
        'date' => $faker->date(),
        'time' => $faker->time(),
        'second_time' => $faker->time(),
        'place' => $faker->city,
        'issues' => $faker->realText(),
        'send' => $faker->boolean(),
    ];
});
