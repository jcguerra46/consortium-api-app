<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\BankAccount;
use App\Models\Consortium;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(BankAccount::class, function (Faker $faker) {
    return [
        'bank_id' => $faker->numberBetween(1, 77),
        'account_number' => (string)$faker->randomNumber(),
        'cbu' => (string)$faker->randomNumber(),
        'alias' => $faker->userName,
        'cuit' => '20-35226046-1',
        'type' => $faker->randomElement(['savings_box', 'current_account', 'single_account']),
        'branch_office' => $faker->city,
        'owner' => $faker->name,
        'show_data_in_expense' => $faker->boolean,
        'email' => $faker->email,
        'signatorie_1' => $faker->name,
        'signatorie_2' => $faker->name,
        'signatorie_3' => $faker->name,
        'consortium_id' => Consortium::all()->random()->id
    ];
});
