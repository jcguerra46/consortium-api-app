<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\BankAccountMovement::class, function (Faker $faker) {
    return [
        'bank_account_id' => $faker->numberBetween(1, 80),
        'description' => $faker->realText(),
        'date' => $faker->date('d-m-Y'),
        'amount' => $faker->randomFloat(2),
        'type' => $faker->randomElement(['income', 'outflow']),
        'type_movement' => $faker->randomElement(['deposit', 'transfer', 'extraction']),
    ];
});
