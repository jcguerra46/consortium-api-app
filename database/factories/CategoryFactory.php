<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\Models\Category;
use App\Models\Administration;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'number' => $faker->randomElement([1,2,3,4,5,6,7,8]),
        'name' => $faker->sentence(1, true),
        'position' => $faker->randomElement([1,2,3,4,5,6,7,8]),
        'hidden' => true,
        'salaries' => true,
        'administration_id' => Administration::all()->random()->id,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
