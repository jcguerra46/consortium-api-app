<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\Consortium;
use App\Models\Checkbook;
use App\Models\Check;
use Faker\Generator as Faker;

$factory->define(Check::class, function (Faker $faker) {
    return [
        'administration_id' => Administration::all()->random()->id,
        'bank_id' => $faker->numberBetween(1, 77),
        'number' => $faker->randomNumber(),
        'deposit_date' => $faker->date(),
        'issuance_date' => $faker->date(),
        'amount' => $faker->randomNumber(),
        'crossed' => $faker->boolean,
        'is_to_the_order' => $faker->boolean,
        'endorsements' => $faker->boolean,
        'own' => $faker->boolean,
        'checkbook_id' => Checkbook::all()->random()->id,
        'consortium_id' => Consortium::all()->random()->id
    ];
});
