<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\Checkbook;
use Faker\Generator as Faker;

$factory->define(Checkbook::class, function (Faker $faker) {
    return [
        'administration_id' => Administration::all()->random()->id,
        'bank_id' => $faker->numberBetween(1, 77),
        'number' => $faker->randomNumber(),
        'first_check' => 1,
        'last_check' => 50,
    ];
});
