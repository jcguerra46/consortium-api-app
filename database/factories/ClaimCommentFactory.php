<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\Models\Claim;
use App\Models\UserPortal;
use App\Models\ClaimComment;
use Faker\Generator as Faker;

$factory->define(ClaimComment::class, function (Faker $faker) {
    return [
        'claim_id' => Claim::all()->random()->id,
        'user_portal_id' => UserPortal::all()->random()->id,
        'title' => $faker->sentence(6, true),
        'body' => $faker->paragraph(1, true),
        'viewed' => $faker->randomElement([true, false]),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
