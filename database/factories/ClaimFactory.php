<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\Models\Claim;
use App\Models\UserPortal;
use App\Models\FunctionalUnit;
use Faker\Generator as Faker;

$factory->define(Claim::class, function (Faker $faker) {
    return [
        'functional_unit_id' => FunctionalUnit::all()->random()->id,
        'user_portal_id' => UserPortal::all()->random()->id,
        'title' => $faker->sentence(6, true),
        'body' => $faker->paragraph(1, true),
        'state' => $faker->randomElement(['INICIADO', 'RESUELTO', 'EJECUCION', 'PROCESADO']),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
