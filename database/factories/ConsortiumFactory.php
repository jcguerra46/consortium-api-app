<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Consortium::class, function (Faker $faker) {
    return [
        'administration_id' => Administration::all()->random()->id,
        'order' => $faker->randomNumber(),
        'code' => $faker->randomNumber(),
        'fancy_name' => $faker->name,
        'business_name' => $faker->streetName,
        'address_number' => $faker->numberBetween(),
        'address_street' => $faker->streetName,
        'country_id' => 1,
        'province_id' => 1,
        'postal_code' => 3260,
        'cuit' => $faker->numerify('##-########-#'),
        'start_date' => \Carbon\Carbon::now(),
        'payment_method_id' => 1,
        'suterh_code' => $faker->numerify('####'),
        'siro_code' => $faker->numerify('####'),
        'pmc_code' => $faker->numerify('####'),
        'pme_code' => $faker->numerify('####'),
        'ep_code' => $faker->numerify('####'),
        'siro_verification_number' => $faker->numerify('####'),
        'count_floors' => $faker->numerify('##'),
        'count_functional_units' => $faker->numerify('###'),
        'type_of_building' => 'H',
        'state' => $faker->randomElement(['PENDIENTE', 'INACTIVO', 'ACTIVO', 'BORRADO']),
        'category' => $faker->numberBetween(1, 4),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
    ];
});
