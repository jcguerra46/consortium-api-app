<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\Contact;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Contact::class, function (Faker $faker) {
    return [
        'administration_id' => Administration::all()->random()->id,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'address_street' => $faker->streetAddress,
        'postal_code' => $faker->postcode,
        'document_number' => $faker->numerify('########'),
        'birth_date' => $faker->date('Y-m-d'),
        'phone_personal' => $faker->phoneNumber,
        'phone_work' => $faker->phoneNumber,
        'phone_cel' => $faker->e164PhoneNumber,
        'email' => $faker->email,
        'expenses_by_email' => $faker->boolean,
        'council_member' => $faker->boolean,
        'observations' => $faker->realText(),
    ];
});
