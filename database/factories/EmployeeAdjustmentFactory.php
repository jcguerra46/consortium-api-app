<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EmployeeAdjustment;
use Faker\Generator as Faker;

$factory->define(EmployeeAdjustment::class, function (Faker $faker) {
    return [
        'employee_id' => \App\Models\Employee::all()->random()->id,
        'type' => $faker->randomElement(['value', 'percentage']),
        'haber_descuento' => $faker->randomElement(['haber', 'descuento']),
        'description' => $faker->text,
        'value' => $faker->numberBetween(0, 5000),
        'used' => $faker->boolean,
        'suma_sueldo_jornal' => $faker->boolean,
        'recurrent' => $faker->boolean,
        'es_remunerativo' => $faker->boolean,
        'unit' => $faker->randomElement(['a', 'b']),
        'percentage' => $faker->numberBetween(0, 100),
        'percentage_sobre_base' => $faker->randomElement(['basic', 'jornal', 'total_neto', 'total_bruto', null]),
        'active' => $faker->boolean,
    ];
});
