<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EmployeeBeneficiary;
use Faker\Generator as Faker;

$factory->define(EmployeeBeneficiary::class, function (Faker $faker) {
    return [
        'employee_id' => \App\Models\Employee::all()->random()->id,
        'cuil' => '20-95660815-6',
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'document_type' => $faker->randomElement(['le', 'lc', 'dni']),
        'dni' => '95660815',
        'relationship' => $faker->sentence(1, true),
        'porcentage' => $faker->randomFloat(3, 0, 90)
    ];
});
