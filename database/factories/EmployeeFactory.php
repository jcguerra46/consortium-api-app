<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Consortium;
use App\Models\Employee;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'professional_function_id' => $faker->numberBetween(1, 23),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'birth_date' => $faker->date(),
        'type_dni' => $faker->randomElement(['LC', 'LE', 'DNI']),
        'dni' => $faker->numerify('########'),
        'cuil' => $faker->numerify('##-########-#'),
        'number_docket' => $faker->numerify('########'),
        'active' => $faker->boolean,
        'employee_status_id' => $faker->numberBetween(1, 9),
        'consortium_id' => Consortium::all()->random()->id,
    ];
});
