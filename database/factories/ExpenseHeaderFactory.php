<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\ExpenseHeader;
use Faker\Generator as Faker;

$factory->define(ExpenseHeader::class, function (Faker $faker) {
    return [
        'administration_id' => Administration::all()->random()->id,
        'name_header' => $faker->sentence(2, true),
        'cuit' => $faker->numerify('##-########-#'),
        'name' => $faker->sentence(2, true),
        'address_street' => $faker->streetName,
        'address_number' => $faker->buildingNumber,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'fiscal_situation' => $faker->randomElement(['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento']),
        'postal_code' => $faker->postcode,
        'rpa' => $faker->numerify('########')
    ];
});
