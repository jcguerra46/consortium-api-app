<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Consortium;
use App\Models\FunctionalUnit;
use App\Models\Package;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(FunctionalUnit::class, function (Faker $faker) {
    return [
        'consortium_id' => Consortium::all()->random()->id,
        'package_id' => Package::all()->random()->id,
        'functional_unit_type_id' => '1',
        'owner_first_name' => $faker->firstName,
        'owner_last_name' => $faker->lastName,
        'floor' => $floor = $faker->numberBetween($min = 1, $max = 20),
        'functional_unit_number' => $floor . strtoupper($faker->randomLetter),
        'initial_balance' => $faker->randomFloat(2, 0, 10000),
        'type_forgive_interest' => $faker->randomElement([null, 'until_further_notice', 'this_liquidation']),
        'legal_state' => $faker->randomElement([null, 'trial', 'agreement', 'lawyer']),
        'm2' => $faker->randomFloat('2'),
        'order' => $faker->randomNumber('1'),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now()
    ];
});
