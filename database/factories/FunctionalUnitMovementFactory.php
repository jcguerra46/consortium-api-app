<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\FunctionalUnitMovement;
use Faker\Generator as Faker;

$factory->define(FunctionalUnitMovement::class, function (Faker $faker) {
    return [
        'functional_unit_id' => 1,
        'amount' => $faker->numerify(1000, 9000),
        'date' => $faker->dateTimeBetween('-1 years', 'now', null),
        'description' => $faker->realText(),
        'type' => $faker->randomElement([
            'month_interest',
            'initial_balance',
            'accumulated_interest',
            'expiration_interest',
            'accumulated_capital',
            'early_payment_discount',
            'capital',
            'debt_capital',
        ]),
        'type_movement' => $faker->randomElement(['income', 'outflow']),
        'identified' => $faker->randomElement([true, false]),
        'accumulate_to_expense'=> $faker->randomElement([true, false])
    ];
});
