<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\Invoice;
use App\Models\Provider;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'provider_id' => Provider::all()->random()->id,
        'invoice_number' => $faker->randomNumber(5),
        'description' => $faker->realText(),
        'date' => $faker->date(),
        'amount' => $faker->randomFloat(2),
        'administration_id' => Administration::all()->random()->id,
    ];
});
