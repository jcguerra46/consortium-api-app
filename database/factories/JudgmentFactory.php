<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Judgment::class, function (Faker $faker) {
    return [
        'consortium_id' => 1,
        'cover' => $faker->text,
        'number_expedient' => $faker->randomNumber(),
        'judged' => $faker->streetName,
        'objet' => $faker->randomElement(),
        'state' => $faker->state,
        'reclaimed_amount' => $faker->numerify(),
        'active' => $faker->boolean,
    ];
});
