<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Menu;
use Faker\Generator as Faker;

$factory->define(Menu::class, function (Faker $faker) {
  return [
    'data' => '
            {
              "menu": [{"id":"consorcio","to":"/app/consorcios","fav":true,"icon":"iconsminds-building","label":"Consorcios"},{"id":"gastos","to":"/app/gastos","fav":true,"icon":"iconsminds-coins","subs":[{"to":"/app/gastos/recurrentes","fav":true,"icon":"iconsminds-financial","label":"Gastos Recurrentes"},{"to":"/app/gastos/descripciones","fav":true,"icon":"iconsminds-file-edit","label":"Descripción de Gastos"},{"to":"/app/gastos/proveedores","fav":true,"icon":"iconsminds-shop","subs":[{"to":"/app/gastos/proveedores/cuenta-corriente","fav":false,"icon":"iconsminds-library","label":"CC Proveedor"}],"label":"Proveedores"}],"label":"Gastos"},{"id":"cobranzas","to":"/app/cobranzas","fav":true,"icon":"iconsminds-handshake","label":"Cobranzas"},{"id":"sueldos","to":"/app/sueldos","fav":true,"icon":"iconsminds-wallet","subs":[{"to":"/app/sueldos/empleados","fav":true,"icon":"iconsminds-business-man-woman","label":"Empleados"}],"label":"Sueldos"},{"id":"caja","to":"/app/caja","fav":true,"icon":"iconsminds-box-close","subs":[{"to":"/app/caja/cuenta-corriente-consorcio","fav":true,"icon":"iconsminds-library","label":"CC Consorcio"},{"to":"/app/caja/virtuales","fav":true,"icon":"iconsminds-safe-box","label":"Cajas Virtuales"},{"to":"/app/caja/cheques","fav":true,"icon":"iconsminds-shop","subs":[{"to":"/app/caja/cheques/chequeras","fav":true,"icon":"iconsminds-password-field","label":"Chequeras"}],"label":"Cheques"},{"to":"/app/caja/cuentas-bancarias","fav":true,"icon":"iconsminds-bank","label":"Cuentas Bancarias"}],"label":"Caja"},{"id":"unidadesFuncionales","to":"/app/unidades-funcionales","fav":true,"icon":"iconsminds-home","subs":[{"to":"/app/unidades-funcionales/cuenta-corriente","fav":true,"icon":"iconsminds-library","label":"CC Unidades Funcionales"},{"to":"/app/unidades-funcionales/contactos","fav":true,"icon":"iconsminds-male-female","label":"Contactos"},{"to":"/app/unidades-funcionales/reclamos","fav":true,"icon":"iconsminds-danger","label":"Reclamos"}],"label":"Unidades Funcionales"},{"id":"reportes","to":"/app/reportes","fav":true,"icon":"iconsminds-pen","subs":[{"to":"/app/reportes/varios","fav":true,"icon":"iconsminds-notepad","label":"Reportes Varios"},{"to":"/app/reportes/cajas-usuarios","fav":true,"icon":"iconsminds-box-close","label":"Cajas por Usuarios"},{"to":"/app/reportes/auditoria","fav":true,"icon":"iconsminds-books","label":"Auditoría"}],"label":"Reportes"},{"id":"configuraciones","to":"/app/configuraciones","fav":false,"icon":"iconsminds-gears","subs":[{"to":"/app/configuraciones/periodos","fav":false,"icon":"simple-icon-calendar","label":"Periodos"},{"to":"/app/configuraciones/cabeceras","fav":false,"icon":"simple-icon-docs","label":"Cabeceras"},{"to":"/app/configuraciones/notas","fav":false,"icon":"iconsminds-notepad","label":"Notas"},{"to":"/app/configuraciones/juicios","fav":false,"icon":"iconsminds-student-hat","label":"Juicios"},{"to":"/app/configuraciones/categorias","fav":false,"icon":"simple-icon-list","label":"Categorías"},{"to":"/app/configuraciones/servicios","fav":false,"icon":"simple-icon-wrench","label":"Servicios"}],"label":"Configuraciones"}]
            }
        ',
    'active' => true,
    'updated_at' => \Carbon\Carbon::now(),
    'created_at' => \Carbon\Carbon::now(),
  ];
});
