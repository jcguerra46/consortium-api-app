<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\Note;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Note::class, function (Faker $faker) {
    return [
        'title' => $faker->text,
        'content' => $faker->randomHtml(),
        'is_debt_note' => $faker->boolean,
        'prorrateo_in_expense' => $faker->boolean,
        'limit_date' => $faker->date(),
        'administration_id' => Administration::all()->random()->id,
    ];
});
