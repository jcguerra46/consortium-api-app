<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Consortium;
use App\Models\PercentageConsortium;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(PercentageConsortium::class, function (Faker $faker) {
    return [
        'consortium_id' => Consortium::all()->random()->id,
        'name' => $faker->randomElement(['GASTOS', 'EXPENSAS', 'VARIOS', 'PORCENTUAL']) .' '. strtoupper($faker->randomLetter),
        'second_line_name' => $faker->text,
        'description' => $faker->realText(),
        'type' => $faker->randomElement(['spendings', 'fixed', 'forced_fixed']),
        'position' => $faker->numberBetween(1, 100),
        'last_fixed_amount' => $faker->randomNumber(),
        'particular_percentage' => $faker->boolean,
        'hidden_expense_spending' => $faker->boolean,
        'hidden_in_proration' => $faker->boolean,
        'hidden_percentage_value' => $faker->boolean,
    ];
});
