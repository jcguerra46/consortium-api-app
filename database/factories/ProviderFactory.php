<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\Provider;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Provider::class, function (Faker $faker) {
    return [
        'business_name' => $faker->name,
        'name' => $faker->name,
        'address' => $faker->address,
        'location' => $faker->city,
        'province_id' => 1,
        'postal_code' => $faker->postcode,
        'cuit' => '20-35226046-1',
        'administration_id' => Administration::all()->random()->id,
    ];
});
