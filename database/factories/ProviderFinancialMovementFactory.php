<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Consortium;
use App\Models\Provider;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\ProviderFinancialMovement::class, function (Faker $faker) {
    return [
        'provider_id' => Provider::all()->random()->id,
        'amount' => $faker->numberBetween(1, 500000),
        'date' => $faker->date(),
        'description' => $faker->text,
        'transaction_type' => $faker->randomElement(['debit', 'credit']),
        'spending_installment_id' => null,
        'payment_id' => null,
        'consortium_id' => Consortium::all()->random()->id,
    ];
});
