<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Administration;
use App\Models\SpendingDescription;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(SpendingDescription::class, function (Faker $faker) {
    return [
        'administration_id' => Administration::all()->random()->id,
        'title' => $faker->title,
        'description' => $faker->text,
    ];
});
