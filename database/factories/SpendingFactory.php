<?php

/** @var Factory $factory */

use App\Models\Category;
use App\Models\Consortium;
use App\Models\Provider;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    App\Models\Spending::class,
    function (Faker $faker) {
        $consortia = Consortium::where('administration_id', 1)->pluck('id')->toArray();
        $providers = Provider::where('administration_id', 1)->pluck('id')->toArray();
        $categories = Category::pluck('id')->toArray();

        return [
            'number'        => $faker->randomNumber(5),
            'amount'        => $faker->randomNumber(5),
            'date'          => $faker->date(),
            'description'   => $faker->realText(),
            'type'          => $faker->randomElement(['spending', 'salary', 'contribution']),
            'category_id'   => $faker->randomElement($categories),
            'consortium_id' => $faker->randomElement($consortia),
            'provider_id'   => $faker->randomElement($providers),
        ];
    }
);
