<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(
    App\Models\SpendingInstallment::class,
    function (Faker $faker) {
        return [
            'number'         => $faker->randomNumber(4),
            'date_pay'       => $faker->date(),
            'invoice_number' => $faker->randomNumber(6),
        ];
    }
);
