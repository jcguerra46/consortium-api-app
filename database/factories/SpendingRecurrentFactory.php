<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\SpendingRecurrent::class, function (Faker $faker) {
    $consortia = \App\Models\Consortium::pluck('id')->toArray();
    $providers = \App\Models\Provider::pluck('id')->toArray();
    $percentages = \App\Models\PercentageConsortium::pluck('id')->toArray();
    $categories = \App\Models\Category::pluck('id')->toArray();

    return [
        'consortium_id' => $faker->randomElement($consortia),
        'provider_id' => $faker->randomElement($providers),
        'percentage_consortium_id' => $faker->randomElement($percentages),
        'category_id' => $faker->randomElement($categories),
        'description' => $faker->realText(),
        'amount' => $faker->randomNumber(),
        'type' => $faker->randomElement(['fixed', 'variable']),
        'periodicity' => $faker->numberBetween(1, 12),
        'start_date' => $faker->date(),
    ];
});
