<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\UserPortal::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'active' => true,
        'birth_date' =>  $faker->dateTimeBetween('1960-01-01', '2002-12-31')->format('Y/m/d'),
        'address' => $faker->address,
        'postal_code' => $faker->postcode,
        'dni' => $faker->numerify('########'),
        'cuil'  => $faker->numerify('##-########-#'),
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now()
    ];
});
