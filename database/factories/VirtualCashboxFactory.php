<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Consortium;
use App\Models\VirtualCashbox;
use Faker\Generator as Faker;

$factory->define(VirtualCashbox::class, function (Faker $faker) {
    return [
        'consortium_id' => Consortium::all()->random()->id,
        'name' => 'Virtual Cashbox N°' . $faker->randomNumber(),
        'description' => $faker->realText(),
        'show_in_expense' => $faker->boolean,
        'independent' => $faker->boolean,
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
    ];
});
