<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('business_name');
            $table->string('postal_code');
            $table->string('email');
            $table->string('cuit');
            $table->string('phone');
            $table->enum('fiscal_situation', ['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento'])->nullable();
            $table->string('image')->nullable();
            $table->string('signature')->nullable();
            $table->string('address_street')->nullable();
            $table->string('address_number')->nullable();
            $table->string('address_floor')->nullable();
            $table->string('address_department')->nullable();
            $table->string('responsible')->nullable();
            $table->boolean('unregistered_payment')->default(false);
            $table->boolean('active')->default(true);
            $table->unsignedTinyInteger('province_id')->nullable();
            $table->string('contact')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrations');
    }
}
