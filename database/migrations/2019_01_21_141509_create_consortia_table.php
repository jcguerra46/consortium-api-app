<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsortiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortia', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('administration_id');
            $table->bigInteger('order');
            $table->string('code')->nullable();
            $table->string('fancy_name')->nullable();
            $table->string('business_name');
            $table->string('address_number')->nullable();
            $table->string('address_street')->nullable();
            $table->unsignedTinyInteger('country_id');
            $table->unsignedTinyInteger('province_id');
            $table->string('location')->nullable();
            $table->string('postal_code');
            $table->string('cuit');
            $table->date('start_date')->nullable();
            $table->unsignedInteger('payment_method_id')->nullable();
            $table->string('suterh_code')->nullable();
            $table->string('siro_code')->nullable();
            $table->string('pmc_code')->nullable();
            $table->string('pme_code')->nullable();
            $table->string('ep_code')->nullable();
            $table->string('pf_code')->nullable();
            $table->string('interfast_code')->nullable();
            $table->unsignedSmallInteger('siro_verification_number')->nullable();
            $table->unsignedTinyInteger('count_floors')->nullable();
            $table->unsignedSmallInteger('count_functional_units')->nullable();
            $table->enum('type_of_building', ['R', 'H'])->nullable();
            $table->enum('state', ['PENDIENTE', 'INACTIVO', 'ACTIVO', 'BORRADO'])->default('PENDIENTE');
            $table->enum('category', ['1', '2', '3', '4']);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('administration_id')->references('id')->on('administrations');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortia');
    }
}
