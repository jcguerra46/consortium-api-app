<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('business_name');
            $table->string('name')->nullable();
            $table->string('address');
            $table->string('location')->nullable();
            $table->unsignedInteger('province_id')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('cuit');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('note')->nullable();
            $table->string('license')->nullable();
            $table->string('attention_hours')->nullable();
            $table->enum('fiscal_situation', ['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento'])->nullable();
            $table->unsignedInteger('administration_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('administration_id')->references('id')->on('administrations');
            $table->foreign('province_id')->references('id')->on('provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
    }
}
