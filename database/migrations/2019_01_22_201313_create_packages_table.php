<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->text('observations')->nullable();
            $table->float('cost', 4, 2);
            $table->float('discount', 4, 2)->nullable();
            $table->string('legacy_description');
            $table->unsignedBigInteger('package_type_id');
            $table->timestamps();

            $table->foreign('package_type_id')->references('id')->on('package_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
