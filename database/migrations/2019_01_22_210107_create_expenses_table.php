<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consortium_id');
            $table->unsignedInteger('period_id');
            $table->unsignedInteger('first_payment_receipt_number')->nullable();
            $table->enum('status', ['generando_pdfs', 'generada'])->default('generando_pdfs');
            $table->date('close_date');
            $table->timestamp('sent_to_process')->nullable();
            $table->float('amount_to_collect', 8, 2);
            $table->float('amount_collected', 8, 2)->nullable();
            $table->date('first_due_date');
            $table->date('second_due_date')->nullable();
            $table->float('penalty_interests', 8, 2)->nullable();
            $table->enum('penalty_interests_mode', ['interes', 'capital'])->nullable();
            $table->float('second_due_date_interests', 8, 2)->nullable();
            $table->timestamp('exported')->nullable();
            $table->string('zip_path')->nullable();
            $table->string('lq_path')->nullable();

            $table->timestamps();

            $table->foreign('period_id')->references('id')->on('periods');
            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
