<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('administration_id');
            $table->string('first_name')->nullable();
            $table->string('last_name');
            $table->string('address_street')->nullable();
            $table->string('postal_code')->nullable();
            $table->integer('document_number')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('phone_personal')->nullable();
            $table->string('phone_work')->nullable();
            $table->string('phone_cel')->nullable();
            $table->string('email')->nullable();
            $table->string('expenses_by_email')->nullable();
            $table->string('council_member')->nullable();
            $table->text('observations')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('administration_id')->references('id')->on('administrations');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
