<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionalUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('functional_units', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consortium_id');
            $table->unsignedTinyInteger('package_id')->nullable();
            $table->unsignedInteger('functional_unit_type_id');
            $table->string('owner_first_name');
            $table->string('owner_last_name');
            $table->string('floor')->nullable();
            $table->string('department')->nullable();
            $table->string('functional_unit_number')->nullable();
            $table->float('initial_balance', 4, 2)->nullable();
            $table->enum('type_forgive_interest', ['until_further_notice', 'this_liquidation'])->nullable();
            $table->enum('legal_state', ['trial', 'agreement', 'lawyer'])->nullable();
            $table->float('m2', 4, 2)->nullable();
            $table->unsignedInteger('order')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('functional_unit_type_id')->references('id')->on('functional_unit_types');
            $table->foreign('package_id')->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('functional_units');
    }
}
