<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePercentagesConsortiumTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('percentages_consortium', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('consortium_id');
            $table->string('name');
            $table->string('second_line_name')->nullable();
            $table->text('description')->nullable();
            $table->enum('type', ['spendings', 'fixed', 'forced_fixed'])->default('spendings');
            $table->unsignedSmallInteger('position');
            $table->float('last_fixed_amount', 8, 2)->nullable();
            $table->boolean('particular_percentage')->default(false);
            $table->boolean('hidden_expense_spending')->default(false);
            $table->boolean('hidden_in_proration')->default(false);
            $table->boolean('hidden_percentage_value')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('percentages_consortium');
    }
}
