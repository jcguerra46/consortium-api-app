<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersAdministrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_administrations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('administration_id');
            $table->boolean('principal')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('password');
            $table->string('reset_password_token')->nullable();
            $table->string('reset_password_expired')->nullable();
            $table->boolean('active');
            $table->jsonb('menu')->nullable();

            $table->timestamps();

            $table->foreign('administration_id')->references('id')->on('administrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_administrations');
    }
}
