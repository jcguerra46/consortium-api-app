<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersPortalFunctionalUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_portal_functional_units', function (Blueprint $table) {
            $table->unsignedInteger('user_portal_id');
            $table->unsignedInteger('functional_unit_id');

            $table->timestamp('created_at');

            $table->foreign('user_portal_id')->references('id')->on('users_portal');
            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_portal_functional_units');
    }
}
