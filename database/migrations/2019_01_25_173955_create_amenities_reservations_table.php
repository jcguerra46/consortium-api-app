<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenitiesReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amenities_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('amenity_id');
            $table->unsignedInteger('user_portal_id');
            $table->timestamp('since');
            $table->timestamp('to');
            $table->boolean('active');

            $table->timestamps();

            $table->foreign('amenity_id')->references('id')->on('amenities');
            $table->foreign('user_portal_id')->references('id')->on('users_portal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amenities_reservations');
    }
}
