<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirtualCashboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('virtual_cashboxes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consortium_id');
            $table->string('name');
            $table->string('description')->nullable();
            $table->boolean('show_in_expense');
            $table->boolean('independent');

            $table->timestamps();

            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_cashboxes');
    }
}
