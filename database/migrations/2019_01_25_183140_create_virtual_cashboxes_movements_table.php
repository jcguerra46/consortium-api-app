<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirtualCashboxesMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('virtual_cashboxes_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('virtual_cashbox_id');
            $table->text('description');
            $table->date('date');
            $table->float('total_amount');
            $table->float('cash_amount');
            $table->enum('type_movement', ['income', 'outflow']);

            $table->timestamps();

            $table->foreign('virtual_cashbox_id')->references('id')->on('virtual_cashboxes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virtual_cashboxes_movements');
    }
}
