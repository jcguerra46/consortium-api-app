<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecurrentIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recurrent_incomes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consortium_id');
            $table->enum('type', ['fijo','variable']);
            $table->text('description');
            $table->float('amount', 8, 2);
            $table->date('first_payment_date');
            $table->date('expires_date');
            $table->timestamps();

            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recurrent_incomes');
    }
}
