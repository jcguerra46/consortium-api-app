<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpendingsRecurrentsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('spendings_recurrents', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consortium_id');
            $table->text('description');
            $table->float('amount', 8, 2)->nullable();
            $table->unsignedInteger('percentage_consortium_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('provider_id')->nullable();
            $table->unsignedInteger('service_id')->nullable();
            $table->unsignedSmallInteger('periodicity');
            $table->enum('type', ['fixed', 'variable']);
            $table->date('start_date');
            $table->date('due_date')->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->foreign('percentage_consortium_id')->references('id')->on('percentages_consortium');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('spendings_recurrents');
    }
}
