<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('professional_function_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->date('birth_date')->nullable();
            $table->enum('type_dni', ['LC', 'LE', 'DNI'])->default('DNI');
            $table->string('dni');
            $table->string('cuil');
            $table->string('number_docket')->nullable();
            $table->boolean('active')->default(true);
            $table->unsignedInteger('employee_status_id');
            $table->unsignedInteger('consortium_id');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('professional_function_id')->references('id')->on('professional_functions');
            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('employee_status_id')->references('id')->on('employee_status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
