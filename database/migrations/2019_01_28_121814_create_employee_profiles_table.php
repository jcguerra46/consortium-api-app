<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeProfilesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('employee_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->unique();
            $table->unsignedInteger('social_work_id')->nullable();
            $table->boolean('retired')->default(false);
            $table->boolean('adicional_obra_social')->default(false);
            $table->unsignedSmallInteger('jornalizado_hours_per_day')->nullable();
            $table->date('entry_date');
            $table->date('departure date')->nullable();
            $table->boolean('antiquity')->default(false);
            $table->boolean('afiliado_sindicato')->default(false);
            $table->boolean('afiliado_f_m_v_d_d')->default(false);
            $table->boolean('descuento_c_p_f')->default(false);
            $table->boolean('d_g_d_y_p_c')->default(false);
            $table->boolean('calculate_obra_social_by_law')->default(false);
            $table->unsignedSmallInteger('extra_hours_50')->nullable();
            $table->unsignedSmallInteger('extra_hours_saturday_50')->nullable();
            $table->unsignedSmallInteger('extra_hours_saturday_100')->nullable();
            $table->unsignedSmallInteger('extra_hours_sunday_100')->nullable();
            $table->unsignedSmallInteger('extra_hours_holiday_100')->nullable();
            $table->boolean('law27430')->default(false);

            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('social_work_id')->references('id')->on('social_works');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('employee_profiles');
    }
}
