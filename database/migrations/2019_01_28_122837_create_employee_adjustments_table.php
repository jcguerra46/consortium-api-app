<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_adjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->enum('type', ['percentage', 'value']);
            $table->enum('haber_descuento', ['haber', 'descuento']);
            $table->text('description');
            $table->float('value', 2, 4)->nullable();
            $table->boolean('used')->nullable();
            $table->boolean('suma_sueldo_jornal');
            $table->boolean('recurrent');
            $table->boolean('es_remunerativo')->nullable();
            $table->string('unit')->nullable();
            $table->float('percentage')->nullable();
            $table->enum('percentage_sobre_base', ['basic', 'jornal', 'total_neto', 'total_bruto'])->nullable();
            $table->boolean('active');

            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_adjustments');
    }
}
