<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeAfipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_afips', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->unique();
            $table->boolean('conyuge')->default(false);
            $table->boolean('seguro_colectivo_vida')->default(false);
            $table->boolean('trabajador_convencionado')->default(false);
            $table->boolean('adicional_obra_sociales')->default(false);
            $table->boolean('maternidad')->default(false);
            $table->unsignedInteger('situacion')->nullable();
            $table->unsignedInteger('condicion')->nullable();
            $table->unsignedInteger('actividad')->nullable();
            $table->unsignedInteger('zona')->nullable();
            $table->float('adicional_seguridad_social', 4, 2)->nullable();
            $table->string('adicionales')->nullable();
            $table->unsignedTinyInteger('cantidad_hijos')->default(0);
            $table->unsignedTinyInteger('cantidad_adherentes')->default(0);
            $table->unsignedInteger('modalidad_contratacion')->nullable();
            $table->unsignedInteger('provincia_localidad')->nullable();
            $table->unsignedInteger('siniestrado')->nullable();
            $table->float('importe_adicional_obra_social', 4, 2)->nullable();
            $table->unsignedInteger('revista1')->nullable();
            $table->unsignedInteger('revista2')->nullable();
            $table->unsignedInteger('revista3')->nullable();
            $table->integer('dia_revista1')->nullable();
            $table->integer('dia_revista2')->nullable();
            $table->integer('dia_revista3')->nullable();
            $table->string('premios')->nullable();
            $table->string('conceptos_no_remunerativos')->nullable();
            $table->string('rectificacion_remuneracion')->nullable();
            $table->string('contribucion_tarea_diferencial')->nullable();

            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('revista1')->references('id')->on('afip_situaciones');
            $table->foreign('revista2')->references('id')->on('afip_situaciones');
            $table->foreign('revista3')->references('id')->on('afip_situaciones');
            $table->foreign('siniestrado')->references('id')->on('afip_siniestros');
            $table->foreign('situacion')->references('id')->on('afip_situaciones');
            $table->foreign('condicion')->references('id')->on('afip_condiciones');
            $table->foreign('actividad')->references('id')->on('afip_actividades');
            $table->foreign('zona')->references('id')->on('afip_zonas');
            $table->foreign('modalidad_contratacion')->references('id')->on('afip_modos_contratacion');
            $table->foreign('provincia_localidad')->references('id')->on('afip_provincias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_afips');
    }
}
