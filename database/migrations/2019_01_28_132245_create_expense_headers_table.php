<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_headers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('administration_id');
            $table->string('name_header');
            $table->string('cuit');
            $table->string('name');
            $table->string('address_street');
            $table->string('address_number');
            $table->string('email');
            $table->string('phone');
            $table->string('fiscal_situation');
            $table->string('postal_code');
            $table->string('rpa');

            $table->timestamps();
            $table->foreign('administration_id')->references('id')->on('administrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_headers');
    }
}
