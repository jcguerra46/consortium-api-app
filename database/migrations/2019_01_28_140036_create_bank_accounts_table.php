<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bank_id')->nullable();
            $table->string('account_number');
            $table->string('cbu')->nullable();
            $table->string('alias')->nullable();
            $table->string('cuit')->nullable();
            $table->enum('type',
                [
                    'savings_box',
                    'current_account',
                    'single_account'
                ]
            )->nullable();
            $table->string('branch_office')->nullable();
            $table->string('owner')->nullable();
            $table->boolean('show_data_in_expense')->default(false);
            $table->string('email')->nullable();
            $table->string('signatorie_1')->nullable();
            $table->string('signatorie_2')->nullable();
            $table->string('signatorie_3')->nullable();
            $table->unsignedSmallInteger('consortium_id');

            $table->timestamps();

            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
