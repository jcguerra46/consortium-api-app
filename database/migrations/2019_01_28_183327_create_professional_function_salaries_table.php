<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionalFunctionSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_function_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('consortium_category_id')->nullable();
            $table->unsignedSmallInteger('professional_function_id');
            $table->float('value', 4, 2);
            $table->date('period');
            $table->timestamps();

            $table->foreign('professional_function_id')->references('id')->on('professional_functions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_function_salaries');
    }
}
