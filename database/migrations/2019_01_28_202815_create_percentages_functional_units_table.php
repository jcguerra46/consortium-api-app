<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePercentagesFunctionalUnitsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('percentages_functional_units', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('functional_unit_id');
            $table->unsignedInteger('percentage_consortium_id');
            $table->float('value', 8, 4);

            $table->unique(['functional_unit_id', 'percentage_consortium_id']);

            $table->timestamps();

            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
            $table->foreign('percentage_consortium_id')->references('id')->on('percentages_consortium');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('percentages_functional_units');
    }
}
