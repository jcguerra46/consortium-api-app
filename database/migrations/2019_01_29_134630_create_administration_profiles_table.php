<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministrationProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administration_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('bank_account_id')->nullable();
            $table->unsignedSmallInteger('administration_id');
            $table->string('inscription_r_p_a');
            $table->string('zona_logistica');
            $table->unsignedInteger('pago_facil_code_admin')->nullable();
            $table->unsignedInteger('p_m_e_code')->nullable();
            $table->unsignedInteger('expensas_pagas_code')->nullable();
            $table->unsignedInteger('expensas_pagas_internal_code')->nullable();
            $table->boolean('p_m_e_activado')->nullable();
            $table->string('merchant')->nullable();
            $table->string('empresa_proveedora_servicios')->nullable();
            $table->unsignedSmallInteger('camara')->nullable();
            $table->unsignedSmallInteger('days_of_debt')->nullable();
            $table->string('link_intiza')->nullable();
            $table->string('image')->nullable();

            $table->timestamps();

            $table->foreign('administration_id')->references('id')->on('administrations');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administration_profiles');
    }
}
