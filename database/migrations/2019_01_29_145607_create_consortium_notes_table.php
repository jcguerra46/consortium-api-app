<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiumNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortium_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('consortium_id');
            $table->unsignedSmallInteger('note_id');
            $table->unsignedInteger('position');

            $table->timestamps();

            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('note_id')->references('id')->on('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortium_notes');
    }
}
