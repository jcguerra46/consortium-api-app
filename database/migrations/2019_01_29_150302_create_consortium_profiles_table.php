<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiumProfilesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('consortium_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('consortium_id')->unique();
            $table->unsignedSmallInteger('bank_account_id')->nullable();
            $table->boolean('escrow')->default(false);
            $table->string('license')->nullable();
            $table->string('email')->nullable();
            $table->enum('liquidation', ['vencido', 'adelantado'])->nullable();
            $table->enum('rounding_type', ['fu', 'yes', 'no'])->default('fu');
            $table->unsignedSmallInteger('first_due_date')->nullable();
            $table->unsignedSmallInteger('second_due_date')->nullable();
            $table->float('second_due_date_interests', 8, 2)->nullable();
            $table->float('penalty_interests', 8, 2)->nullable();
            $table->enum('penalty_interests_mode', ['capital', 'interest'])->default('capital');
            $table->boolean('rounding_payslips')->default(false);
            $table->unsignedTinyInteger('garage_amount')->nullable();
            $table->float('art_amount', 8, 2)->nullable();
            $table->float('art_percentage', 8, 2)->nullable();
            $table->boolean('collect_interest_outside_due_date')->default(false);;
            $table->boolean('independent_cashbox')->default(false);;
            $table->text('notes')->nullable();
            $table->unsignedInteger('next_payment_receipt_number')->default(1);
            $table->boolean('payslips_details')->default(true);
            $table->unsignedInteger('next_moving_sheet_number')->default(1);
            $table->string('private_spendings_title')->nullable();
            $table->boolean('self_managment')->default(false);
            $table->boolean('early_payment')->default(false);
            $table->integer('early_payment_discount_days')->nullable();
            $table->float('early_payment_discount')->nullable();
            $table->boolean('show_bank_movements')->default(true);
            $table->boolean('show_patrimonial_status')->default(true);
            $table->boolean('show_provider_data')->default(false);
            $table->boolean('legends_my_expenses')->default(true);
            $table->boolean('show_judgments_section')->default(true);
            $table->boolean('auto_billing')->default(true);
            $table->unsignedSmallInteger('package_id')->nullable();
            $table->unsignedSmallInteger('expense_header_id')->nullable();
            $table->text('observations')->nullable();
            $table->float('waste_removal_ammount', 8, 2)->nullable();
            $table->enum('provider_spending_location', ['delante', 'detras'])->default('delante');
            $table->boolean('show_previous_balance')->default(true);
            $table->boolean('show_payments')->default(true);



            $table->timestamps();

            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->foreign('package_id')->references('id')->on('packages');
            $table->foreign('expense_header_id')->references('id')->on('expense_headers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('consortium_profiles');
    }
}
