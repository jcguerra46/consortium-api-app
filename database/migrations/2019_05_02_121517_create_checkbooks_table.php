<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('administration_id');
            $table->unsignedBigInteger('bank_id')->nullable();
            $table->integer('number');
            $table->integer('first_check');
            $table->integer('last_check');
            $table->timestamps();

            $table->foreign('administration_id')->references('id')->on('administrations');
            $table->foreign('bank_id')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkbooks');
    }
}
