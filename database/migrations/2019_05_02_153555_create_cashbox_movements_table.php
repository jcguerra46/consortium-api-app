<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashboxMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashbox_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cashbox_id');
            $table->date('date');
            $table->text('description');
            $table->boolean('identified')->default(true);
            $table->float('total_amount');
            $table->float('cash_amount');
            $table->enum('type_movement', ['income', 'outflow']);
            $table->unsignedInteger('virtual_cashbox_movement_id')->nullable();
            $table->unsignedInteger('payment_id')->nullable();

            $table->timestamps();

            $table->foreign('cashbox_id')->references('id')->on('cashboxes');
            $table->foreign('virtual_cashbox_movement_id')->references('id')->on('virtual_cashboxes_movements');
            $table->foreign('payment_id')->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashbox_movements');
    }
}
