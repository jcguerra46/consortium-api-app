<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFunctionalUnitContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('functional_unit_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('functional_unit_id');
            $table->unsignedInteger('contact_id');
            $table->enum('contact_type', ['PROPIETARIO', 'INQUILINO','OTRO'])->default('PROPIETARIO');
            $table->boolean('primary')->default(false);

            $table->timestamps();

            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
            $table->foreign('contact_id')->references('id')->on('contacts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('functional_unit_contacts');
    }
}
