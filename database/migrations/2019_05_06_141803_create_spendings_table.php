<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpendingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spendings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('number')->nullable();
            $table->float('amount', 8, 2);
            $table->date('date');
            $table->boolean('prorate_in_expenses')->default(true);
            $table->boolean('affect_financial_section')->default(true);
            $table->text('description')->nullable();
            $table->enum('type', ['spending', 'salary', 'contribution'])->default('spending');

            $table->unsignedInteger('category_id');
            $table->unsignedInteger('spending_recurrent_id')->nullable();
            $table->unsignedInteger('consortium_id');
            $table->unsignedInteger('functional_unit_id')->nullable();
            $table->unsignedInteger('provider_id')->nullable();
            $table->unsignedInteger('service_id')->nullable();

            $table->timestamps();

            $table->foreign('spending_recurrent_id')->references('id')->on('spendings_recurrents');
            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
            $table->foreign('provider_id')->references('id')->on('providers');
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spendings');
    }
}
