<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpendingInstallmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spending_installments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedSmallInteger('number');
            $table->float('amount', 8, 2);
            $table->date('date_pay')->nullable();
            $table->boolean('payed')->default(false);
            $table->unsignedInteger('spending_id');
            $table->unsignedInteger('payment_id')->nullable();
            $table->string('invoice_number')->nullable();
            $table->unsignedInteger('period_id')->nullable();

            $table->timestamps();

            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('spending_id')->references('id')->on('spendings');
            $table->foreign('period_id')->references('id')->on('periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spending_installments');
    }
}
