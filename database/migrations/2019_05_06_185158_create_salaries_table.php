<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('spending_id')->nullable();
            $table->enum('type', ['monthly', 'sac', 'vacation', 'intermediate']);
            $table->date('period');
            $table->float('basico', 8, 2);
            $table->float('jornal', 8, 2);
            $table->float('neto', 8, 2);
            $table->float('bruto', 8, 2);
            $table->float('value_porcentual_art', 8, 2);
            $table->float('value_fijo_art', 8, 2);
            $table->float('extra_hours_worked', 8, 2);
            $table->float('value_extra_hours_worked', 8, 2);
            $table->float('vacation_days', 8, 2);
            $table->float('not_vacation_days', 8, 2);
            $table->date('contributions_period');
            $table->date('contributions_deposit_date');
            $table->string('contributions_bank');
            $table->float('contributions_employee_total', 8, 2);
            $table->float('contributions_employer_total', 8, 2);
            $table->jsonb('inputs')->nullable();

            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('spending_id')->references('id')->on('spendings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
