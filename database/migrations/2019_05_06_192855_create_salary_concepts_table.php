<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_concepts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salary_id');
            $table->unsignedInteger('employee_adjustment_id')->nullable();
            $table->float('value', 8, 2);
            $table->string('description')->nullable();
            $table->enum('type', ['haber', 'descuento']);
            $table->string('unit')->nullable();

            $table->foreign('employee_adjustment_id')->references('id')->on('employee_adjustments');
            $table->foreign('salary_id')->references('id')->on('salaries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_concepts');
    }
}
