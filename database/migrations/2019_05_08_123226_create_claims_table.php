<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('functional_unit_id');
            $table->unsignedInteger('user_portal_id');
            $table->string('title');
            $table->text('body');
            $table->enum('state', ['INICIADO', 'RESUELTO', 'EJECUCION', 'PROCESADO']);

            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
            $table->foreign('user_portal_id')->references('id')->on('users_portal');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
