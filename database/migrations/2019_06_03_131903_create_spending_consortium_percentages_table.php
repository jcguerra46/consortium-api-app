<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpendingConsortiumPercentagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spending_consortium_percentages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('spending_id');
            $table->unsignedInteger('percentage_consortium_id');
            $table->float('value', 8, 2);

            $table->foreign('spending_id')->references('id')->on('spendings');
            $table->foreign('percentage_consortium_id')->references('id')->on('percentages_consortium');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spending_consortium_percentages');
    }
}
