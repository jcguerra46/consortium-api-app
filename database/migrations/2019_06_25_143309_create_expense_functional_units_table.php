<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseFunctionalUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_functional_units', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('expense_id');
            $table->unsignedInteger('functional_unit_id');
            $table->float('saldo_total');
            $table->float('deuda');
            $table->float('intereses');


            $table->timestamps();

            $table->foreign('expense_id')->references('id')->on('expenses');
            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_functional_units');
    }
}
