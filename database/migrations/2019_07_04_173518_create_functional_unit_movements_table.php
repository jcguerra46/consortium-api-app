<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFunctionalUnitMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('functional_unit_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('functional_unit_id');
            $table->float('amount', 8, 2);
            $table->date('date');
            $table->string('description');
            $table->enum('type', [
                'month_interest',
                'initial_balance',
                'accumulated_interest',
                'expiration_interest',
                'accumulated_capital',
                'early_payment_discount',
                'capital',
                'debt_capital',
            ]);
            $table->enum('type_movement', ['income', 'outflow']);
            $table->boolean('identified');
            $table->boolean('accumulate_to_expense');
            $table->unsignedInteger('expense_id')->nullable();
            $table->bigInteger('spending_id')->nullable();

            $table->timestamps();

            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
            $table->foreign('expense_id')->references('id')->on('expenses');
            $table->foreign('spending_id')->references('id')->on('spendings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('functional_unit_movements');
    }
}
