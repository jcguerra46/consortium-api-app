<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiumFinancialMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortium_financial_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consortium_id');
            $table->unsignedInteger('spending_id')->nullable();
            $table->float('amount', 8, 2);
            $table->date('date');
            $table->text('description');
            $table->enum('type_movement', ['income', 'outflow'])->default('income');
            $table->enum('type', [
                'not_affect_finantial_section',
                'initial_balance',
                'payslip',
                'bank_account_initial_balance',
                'balance_correction',
                'spending'
            ])->nullable();
            $table->unsignedInteger('expense_id')->nullable();

            $table->boolean('identified');
            $table->boolean('incomplete');
            $table->boolean('created_from_recurrent');
            $table->boolean('included_in_the_financial_statement');

            $table->timestamps();

            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('spending_id')->references('id')->on('spendings');
            $table->foreign('expense_id')->references('id')->on('expenses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortium_financial_movements');
    }
}
