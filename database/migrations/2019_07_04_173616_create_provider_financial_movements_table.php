<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProviderFinancialMovementsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('provider_financial_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('provider_id');
            $table->float('amount', 8, 2);
            $table->date('date');
            $table->string('description');
            $table->enum('transaction_type', [
                'debit',
                'credit',
            ]);
            $table->unsignedInteger('spending_installment_id')->nullable()->index();
            $table->unsignedInteger('payment_id')->nullable()->index();
            $table->unsignedInteger('consortium_id')->nullable()->index();

            $table->timestamps();

            $table->foreign('provider_id')->references('id')->on('providers');
            $table->foreign('spending_installment_id')->references('id')->on('spending_installments');
            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provider_financial_movements');
    }
}
