<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpendingDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spending_descriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('administration_id');
            $table->string('title');
            $table->text('description');
            $table->timestamps();

            $table->foreign('administration_id')->references('id')->on('administrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spending_descriptions');
    }
}
