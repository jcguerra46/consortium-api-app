<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashboxMovementFunctionalUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashbox_movement_functional_units', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('cashbox_movement_id');
            $table->bigInteger('functional_unit_id');
            $table->float('amount');

            $table->foreign('cashbox_movement_id')->references('id')->on('cashbox_movements');
            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashbox_movement_functional_units');
    }
}
