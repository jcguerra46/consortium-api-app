<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->date('deposit_date')->nullable();
            $table->date('issuance_date')->nullable();
            $table->date('due_date')->nullable();
            $table->float('amount', 8, 2)->nullable();
            $table->boolean('crossed')->default(false);
            $table->boolean('is_to_the_order')->default(false);
            $table->boolean('endorsements')->default(false);
            $table->boolean('own')->default(true);

            // Foraneas
            $table->unsignedBigInteger('bank_id')->nullable();
            $table->unsignedBigInteger('cashbox_movement_income_id')->nullable();
            $table->unsignedBigInteger('cashbox_movement_outflow_id')->nullable();
            $table->unsignedBigInteger('virtual_cashbox_movement_id')->nullable();
            $table->unsignedInteger('checkbook_id')->nullable();
            $table->unsignedInteger('consortium_id')->nullable();
            $table->unsignedInteger('administration_id');

            $table->timestamps();

            $table->foreign('cashbox_movement_income_id')->references('id')->on('cashbox_movements');
            $table->foreign('cashbox_movement_outflow_id')->references('id')->on('cashbox_movements');
            $table->foreign('virtual_cashbox_movement_id')->references('id')->on('virtual_cashboxes_movements');
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('checkbook_id')->references('id')->on('checkbooks');
            $table->foreign('consortium_id')->references('id')->on('consortia');
            $table->foreign('administration_id')->references('id')->on('administrations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checks');
    }
}
