<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_movements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bank_account_id');
            $table->string('description')->nullable();
            $table->date('date');
            $table->double('amount');
            $table->enum('type', ['income', 'outflow']);
            $table->enum('type_movement',
                [
                    'deposit',
                    'transfer',
                    'extraction'
                ]
            )->nullable();
            $table->unsignedBigInteger('cashbox_movement_id')->nullable();
            $table->unsignedBigInteger('virtual_cashbox_movement_id')->nullable();

            $table->timestamps();

            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->foreign('cashbox_movement_id')->references('id')->on('cashbox_movements');
            $table->foreign('virtual_cashbox_movement_id')->references('id')->on('virtual_cashboxes_movements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_account_movements');
    }
}
