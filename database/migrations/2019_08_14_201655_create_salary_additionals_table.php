<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryAdditionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_additionals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salary_plus_id')->nullable();
            $table->date('period');
            $table->string('type');
            $table->string('description')->nullable();
            $table->float('value');
            $table->integer('quantity')->nullable();
            $table->float('extra_value')->nullable();

            $table->timestamps();

            $table->foreign('salary_plus_id')->references('id')->on('salary_pluses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_additionals');
    }
}
