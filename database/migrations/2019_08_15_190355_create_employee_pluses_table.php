<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeePlusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_pluses', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('salary_plus_id');

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('salary_plus_id')->references('id')->on('salary_pluses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_pluses');
    }
}
