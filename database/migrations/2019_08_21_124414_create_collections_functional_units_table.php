<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectionsFunctionalUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections_functional_units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->date('date');
            $table->float('total_amount');

            //Contables
            $table->unsignedInteger('cashbox_movement_id');
            $table->unsignedInteger('period_id');
            $table->unsignedInteger('functional_unit_id');
            $table->timestamps();

            //CONSTRAINTS FKs
            $table->foreign('cashbox_movement_id')->references('id')->on('cashbox_movements');
            $table->foreign('period_id')->references('id')->on('periods');
            $table->foreign('functional_unit_id')->references('id')->on('functional_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections_functional_units');
    }
}
