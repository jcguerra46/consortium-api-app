<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryConceptsContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_concepts_contributions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salary_id');
            $table->float('value');
            $table->enum('type_contribution', ['employee', 'employer']);
            $table->string('description')->nullable();
            $table->enum('tipo_boleta', ['afip931', 'SERACARH', 'SUTERH', 'FATERYH']);
            $table->string('concept')->nullable();
            $table->unsignedInteger('spending_contribution_id')->nullable();

            $table->foreign('salary_id')->references('id')->on('salaries');
            $table->foreign('spending_contribution_id')->references('id')->on('spendings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_concepts_contributions');
    }
}
