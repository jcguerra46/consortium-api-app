<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryOSValueLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_os_value_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('salary_id');
            $table->enum('tipo',
                [
                    'empleado_obra_social',
                    'empleado_adc_obra_social',
                    'empleador_obra_social'
                ]);
            $table->float('valor');

            $table->timestamps();

            $table->foreign('salary_id')->references('id')->on('salaries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_os_value_logs');
    }
}
