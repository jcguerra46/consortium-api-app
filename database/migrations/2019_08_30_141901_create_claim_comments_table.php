<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('claim_id');
            $table->unsignedInteger('user_portal_id')->nullable();
            $table->string('title');
            $table->string('body');
            $table->boolean('viewed')->default(true);

            $table->timestamps();

            $table->foreign('claim_id')->references('id')->on('claims');
            $table->foreign('user_portal_id')->references('id')->on('users_portal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_comments');
    }
}
