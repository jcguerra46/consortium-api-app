<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJudgmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('judgments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('consortium_id');
            $table->string('cover');
            $table->string('number_expedient');
            $table->string('judged')->nullable();
            $table->string('objet')->nullable();
            $table->string('state')->nullable();
            $table->float('reclaimed_amount');
            $table->boolean('active')->default(true);

            $table->timestamps();

            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('judgments');
    }
}
