<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_events', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('administration_id');
            $table->unsignedInteger('consortium_id')->nullable();
            $table->integer('eventable')->nullable();
            $table->string('title');
            $table->text('description');
            $table->timestamp('date');
            $table->string('trans');
            $table->string('service_class_name')->nullable();
            $table->string('route_class_name');
            $table->timestamps();

            $table->foreign('administration_id')->references('id')->on('administrations');
            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_events');
    }
}
