<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiaUserAdministrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortia_user_administration', function (Blueprint $table) {
            $table->integer('user_administration_id')->unsigned();
            $table->integer('consortium_id')->unsigned();

            $table->foreign('user_administration_id')->references('id')->on('users_administrations');
            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortia_user_administration');
    }
}
