<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignedPermissionsToUserAdministrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_permissions_to_user_administration', function (Blueprint $table) {
            $table->integer('user_administration_id')->unsigned();
            $table->integer('permission_id')->unsigned();

            $table->foreign('user_administration_id')->references('id')->on('users_administrations');
            $table->foreign('permission_id')->references('id')->on('permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_permissions_to_user_administration');
    }
}
