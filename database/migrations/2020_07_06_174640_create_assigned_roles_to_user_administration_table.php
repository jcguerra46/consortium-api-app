<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignedRolesToUserAdministrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assigned_roles_to_user_administration', function (Blueprint $table) {
            $table->integer('user_administration_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_administration_id')->references('id')->on('users_administrations');
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assigned_roles_to_user_administration');
    }
}
