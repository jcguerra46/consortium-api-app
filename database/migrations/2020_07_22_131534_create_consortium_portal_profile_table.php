<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsortiumPortalProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortium_portal_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('consortium_id');
            $table->boolean('show_claims')->default(true);
            $table->boolean('show_ammenities')->default(true);
            $table->boolean('need_ammenities_approval')->default(true);
            $table->boolean('show_consortium_current_account')->default(true);
            $table->boolean('show_consortium_bank_account')->default(true);
            $table->boolean('show_money_in_debt')->default(true);
            $table->boolean('show_spendings')->default(true);

            $table->timestamps();

            $table->foreign('consortium_id')->references('id')->on('consortia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortium_portal_profiles');
    }
}
