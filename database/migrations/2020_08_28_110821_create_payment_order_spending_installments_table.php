<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentOrderSpendingInstallmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'payment_order_spending_installments',
            function (Blueprint $table) {
                $table->unsignedInteger('payment_order_id');
                $table->unsignedInteger('spending_installment_id');

                $table->foreign('payment_order_id')->references('id')->on('payment_orders');
                $table->foreign('spending_installment_id')->references('id')->on('spending_installments');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_order_spending_installments');
    }
}
