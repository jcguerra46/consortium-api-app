<?php

use App\Models\Administration;
use Illuminate\Database\Seeder;

class AdministrationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Administration::class, 3)->create();
    }
}
