<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AfipConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('afip_condiciones')->insert(
            [
                ['code' => '00', 'description' => 'Jubilado Decreto Nro 894/01 y/o Dec 2288/02'],
                ['code' => '01', 'description' => 'SERVICIOS COMUNES Mayor de 18 años'],
                ['code' => '02', 'description' => 'Jubilado'],
                ['code' => '03', 'description' => 'Menor'],
                ['code' => '05', 'description' => 'SERVICIOS DIFERENCIADOS Mayor de 18 años'],
                ['code' => '06', 'description' => 'Pre- jubilables Sin relación de dependencia -Sin servicios reales '],
                ['code' => '09', 'description' => 'Jubilado Decreto Nro 206/00 y/o Decreto Nro 894/01. '],
                ['code' => '10', 'description' => 'Pensión (NO SIPA)'],
                ['code' => '11', 'description' => 'Pensión no Contributiva (NO SIPA)'],
            ]
        );
    }
}
