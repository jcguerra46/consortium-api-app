<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AfipModoContratacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('afip_modos_contratacion')->insert(
            [
                ['code' => '001', 'description' => 'A tiempo parcial: Indeterminado /permanente'],
                ['code' => '002', 'description' => 'Becarios- Residencias médicas Ley 22127'],
                ['code' => '003', 'description' => 'De aprendizaje  l.25013'],
                ['code' => '008', 'description' => 'A Tiempo completo indeterminado /Trabajo permanente'],
                ['code' => '010', 'description' => 'Pasantías. -sin obra  social '],
                ['code' => '011', 'description' => 'Trabajo de temporada.'],
                ['code' => '012', 'description' => 'Trabajo eventual. '],
                ['code' => '014', 'description' => 'Nuevo Período de Prueba .'],
                ['code' => '015', 'description' => 'Puesto Nuevo Varones y Mujeres de  25 a 44 años Ley 25250'],
                ['code' => '016', 'description' => 'Nuevo Periodo de Prueba Trabajador Discapacitado Art.34 Ley 24147 '],
                ['code' => '017', 'description' => 'Puesto Nuevo menor de 25 años, Varones y Mujeres de 45 o más años  y  Mujer Jefe de filia. S/límite/edad  Ley 25250'],
                ['code' => '018', 'description' => 'Trabajador Discapacitado Art. 34. Ley 24147'],
                ['code' => '019', 'description' => 'Puesto Nuevo. Varones y Mujeres  25 a 44 años Art. 34. Ley 24147 Ley 25250'],
                ['code' => '020', 'description' => 'Pto. Nuevo Menor 25 años, Varones y Mujeres 45 años más y Mujer Jefe de flia. S/límite de  edad. Art 34 L. 24147Ley 25250'],
                ['code' => '021', 'description' => 'A tiempo parcial determinado (contrato a plazo fijo)'],
                ['code' => '022', 'description' => 'A Tiempo completo  determinado (contrato a plazo fijo)'],
                ['code' => '023', 'description' => 'Personal no permanente L  22248'],
                ['code' => '024', 'description' => 'Personal de la Construcción L 22250'],
                ['code' => '025', 'description' => 'Empleo público provincial'],
                ['code' => '026', 'description' => 'Beneficiario de programa de empleo, capacitación y de recuperación productiva'],
                ['code' => '027', 'description' => 'Pasantías Ley 26427 -con obra social-'],
                ['code' => '028', 'description' => 'Programas Jefes y Jefas de  Hogar'],
                ['code' => '029', 'description' => 'Decreto Nro 1212/03 Aportante Autónomo'],
                ['code' => '030', 'description' => 'Nuevo Periodo de Prueba Trabajador Discapacitado Art. 87. L 24013'],
                ['code' => '031', 'description' => 'Trabajador Discapacitado Art. 87  L 24013'],
                ['code' => '044', 'description' => 'Changa Solidaria. CCT 62/75'],
                ['code' => '045', 'description' => 'Personal no permanente hoteles CCT 362/03 art.68 inc b'],
                ['code' => '046', 'description' => 'Planta transitoria Adm Pública Nacional, Provincial y/o Municipal'],
                ['code' => '047', 'description' => 'Representación gremial'],
                ['code' => '048', 'description' => 'Art 4to L 24241.Traslado temporario desde el exterior.Con bilaterales de Seg Social'],
                ['code' => '049', 'description' => 'Directores  - empleado   SA con Obra Social y LRT'],
                ['code' => '051', 'description' => 'Pasantías Ley 26427 -con obra social-beneficiario pensión de discapacidad'],
                ['code' => '060', 'description' => 'Taller Protegido Especial para el Empleo (TPEE) Ley 26816, art. 2º punto1'],
                ['code' => '061', 'description' => 'Actor - Interprete en Empleador Contratante Ley 27.203- c/Obra Social'],
                ['code' => '062', 'description' => 'Actor - Interprete en Empleador Contratante Ley 27.203- s/Obra Social'],
                ['code' => '095', 'description' => 'Planes Ministerio de Trabajo'],
                ['code' => '096', 'description' => 'Plan Trabajo por San Luis Ley 5411/03'],
                ['code' => '097', 'description' => 'Programa Trabajo para jóvenes tucumanos'],
                ['code' => '098', 'description' => 'BONUS 2da Oportunidad'],
                ['code' => '099', 'description' => 'LRT (Directores SA, municipios, org, cent y descent. Emp mixt  docentes privados o públicos de jurisdicciones incorporadas o no al SIJP)'],
                ['code' => '102', 'description' => 'Personal permanente discontinuo con ART(para uso de la EU Decreto Nro 762/14)'],
                ['code' => '110', 'description' => 'Trabajo permanente prestación continua Ley 26727'],
                ['code' => '111', 'description' => 'Trabajo temporario Ley 26727'],
                ['code' => '112', 'description' => 'Trabajo permanente discontinuo ley 26727'],
                ['code' => '113', 'description' => 'Trabajo por equipo o cuadrilla fafmilar Ley 26727'],
                ['code' => '301', 'description' => 'Art. 19 Ley 26940. Tiempo indeterminado'],
                ['code' => '302', 'description' => 'Art. 19 Ley 26940. Trabajador discapacitado art. 34 L 24147. Tiempo indeterminado'],
                ['code' => '303', 'description' => 'Art. 19 Ley 26940. Trabajador discapacitado art. 87 L 24013. Tiempo indeterminado'],
                ['code' => '304', 'description' => 'Art. 19 Ley 26940. Tiempo parcial. Art. 92 ter LCT'],
                ['code' => '305', 'description' => 'Art. 19 Ley 26940. Trabajador discapacitado art. 34 L 24147. Tiempo parcial. Art. 92 ter LCT'],
                ['code' => '306', 'description' => 'Art. 19 Ley 26940. Trabajador discapacitado art. 87 L 24013. Tiempo parcial. Art. 92 ter LCT'],
                ['code' => '307', 'description' => 'Art. 24 Ley 26940. Hasta 15 empleados. Primeros 12 meses. Tiempo indeterminado'],
                ['code' => '308', 'description' => 'Art. 24 Ley 26940. Hasta 15 empleados. Primeros 12 meses. Tiempo indeterminado/Trabajador discapacitado art. 34 Ley 24147'],
                ['code' => '309', 'description' => 'Art. 24 Ley 26940. Hasta 15 empleados. Primeros 12 meses. Tiempo indeterminado/Trabajador discapacitado art. 87 Ley 24013'],
                ['code' => '310', 'description' => 'Art. 24 Ley 26940. Hasta 15 empleados. Segundos 12 meses. Tiempo indeterminado'],
                ['code' => '311', 'description' => 'Art. 24 Ley 26940. Hasta 15 empleados. Segundos 12 meses. Tiempo indeterminado. Trabajador discapacitado art. 34 Ley 24147'],
                ['code' => '312', 'description' => 'Art. 24 Ley 26940. Hasta 15 empleados. Segundos 12 meses. Tiempo indeterminado. Trabajador discapacitado art. 87 Ley 24013'],
                ['code' => '313', 'description' => 'Art. 24 Ley 26940. 16 a 80 empleados. Tiempo indeterminado'],
                ['code' => '314', 'description' => 'Art. 24 Ley 26940. 16 a 80 empleados. Tiempo indeterminado. Trabajador discapacitado art. 34 Ley 24147'],
                ['code' => '315', 'description' => 'Art. 24 Ley 26940. 16 a 80 empleados. Tiempo indeterminado. Trabajador discapacitado art. 87 Ley 24013'],
                ['code' => '982', 'description' => 'CCG vitivinícola de Catamarca'],
                ['code' => '983', 'description' => 'CCG vitivinícola de Salta'],
                ['code' => '984', 'description' => 'CCG foresto industrial del Chaco'],
                ['code' => '985', 'description' => 'CCG vitivinícola de Neuquén'],
                ['code' => '987', 'description' => 'CCG vitivinícola de La Rioja'],
                ['code' => '989', 'description' => 'CCG Tabaco Salta'],
                ['code' => '990', 'description' => 'CCG Tabaco Jujuy'],
                ['code' => '991', 'description' => 'CCG vitivinícola de Río Negro'],
                ['code' => '992', 'description' => 'CCG multiproducto del Chaco'],
                ['code' => '993', 'description' => 'CCG frutihortícola de San Juan'],
                ['code' => '994', 'description' => 'CCG yerba mate Misiones y Corrientes'],
                ['code' => '995', 'description' => 'CCG vitivinícola de San Juan'],
                ['code' => '996', 'description' => 'CCG vitivinícola de Mendoza'],
                ['code' => '997', 'description' => 'CCG forestal del Chaco'],
                ['code' => '998', 'description' => 'CCG tabaco Virginia del Chaco'],
            ]
        );
    }
}
