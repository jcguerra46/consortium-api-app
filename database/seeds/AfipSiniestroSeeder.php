<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AfipSiniestroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('afip_siniestros')->insert(
            [
                ['code' => '00', 'description' => 'No Incapacitado'],
                ['code' => '01', 'description' => 'ILT Incapacidad Laboral Temporaria '],
                ['code' => '02', 'description' => 'ILPPP Incapacidad Laboral Permanente Parcial Provisoria.'],
                ['code' => '03', 'description' => 'ILPPD Incapacidad Laboral Permanente Parcial Definitiva.'],
                ['code' => '04', 'description' => 'ILPTP Incapacidad Laboral Permanente Total Provisoria'],
                ['code' => '05', 'description' => 'Capital de recomposición Art. 15, ap. 3, Ley 24557'],
                ['code' => '06', 'description' => 'Ajuste Definitivo ILPPD de pago mensual'],
                ['code' => '07', 'description' => 'RENTA PERIODICA ILPPD Inc. Lab Perm Parc Def   > 50% < 66%'],
                ['code' => '08', 'description' => 'SRT/SSN F.Garantía/F Reserva  ILT Incapacidad Laboral Temporaria '],
                ['code' => '09', 'description' => 'SRT/SSN F.Garantía/F Reserva   ILPPP Inc Lab Perm Parc Prov'],
                ['code' => '10', 'description' => 'SRT/SSN F.Garantía/F Reserva   ILPTP Inc Lab Perm Total Prov'],
                ['code' => '11', 'description' => 'SRT/SSN F.Garantía/F Reserva   ILPPD Inc Laboral Perm Parc Definitiva'],
                ['code' => '12', 'description' => 'ILPPD Beneficios devengados  art. 11 p.4'],
                ['code' => '13', 'description' => 'INFORME Incremento salarial de trabajador siniestrado a ART'],
            ]
        );
    }
}
