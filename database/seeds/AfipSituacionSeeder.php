<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AfipSituacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('afip_situaciones')->insert(
            [
                ['code' => '01', 'description' => 'Activo'],
                ['code' => '05', 'description' => 'Licencia por maternidad '],
                ['code' => '06', 'description' => 'Suspensiones otras causales'],
                ['code' => '09', 'description' => 'Suspendido. Ley 20744 art.223bis'],
                ['code' => '10', 'description' => 'Licencia por excedencia'],
                ['code' => '11', 'description' => 'Licencia por maternidad Down'],
                ['code' => '12', 'description' => 'Licencia por vacaciones'],
                ['code' => '13', 'description' => 'Licencia sin goce de haberes'],
                ['code' => '14', 'description' => 'Reserva de puesto'],
                ['code' => '15', 'description' => 'E.S.E. Cese transitorio de servicios  (art.6, i.6/7 Dto.342/92)'],
                ['code' => '16', 'description' => 'Personal Siniestrado de terceros uso por la ART'],
                ['code' => '17', 'description' => 'Reingreso por disposición judicial'],
                ['code' => '18', 'description' => 'ILT primeros 10 días'],
                ['code' => '19', 'description' => 'ILT días 11 y siguientes '],
                ['code' => '20', 'description' => 'Trabajador siniestrado  en nomina de ART '],
                ['code' => '21', 'description' => 'Trabajador de temporada Reserva de puesto'],
                ['code' => '31', 'description' => 'Activo - Funciones en el exterior'],
                ['code' => '32', 'description' => 'Licencia por paternidad'],
                ['code' => '33', 'description' => 'Licencia por fuerza mayor (ART. 221 LCT)'],
                ['code' => '42', 'description' => 'Empleado eventual en EU (para uso de la ESE)'],
                ['code' => '43', 'description' => 'Empl. eventual en EU (p/uso ESE) mes incompleto'],
                ['code' => '44', 'description' => 'Conservacion del empleo p/accidente o enf. Inculpable art. 211 LCT'],
                ['code' => '45', 'description' => 'Suspensiones p/causas disciplinarias'],
            ]
        );
    }
}
