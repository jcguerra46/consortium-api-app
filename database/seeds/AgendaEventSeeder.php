<?php

use Illuminate\Database\Seeder;

class AgendaEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\AgendaEvent::class, 100)->create();
    }
}
