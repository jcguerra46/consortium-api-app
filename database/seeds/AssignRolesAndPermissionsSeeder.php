<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssignRolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Assign role
        DB::table('assigned_roles_to_user_administration')->insert(
            [
                [ 'user_administration_id' => 1, 'role_id' => 1 ],
                [ 'user_administration_id' => 2, 'role_id' => 2 ],
                [ 'user_administration_id' => 3, 'role_id' => 3 ],
                [ 'user_administration_id' => 4, 'role_id' => 2 ],
                [ 'user_administration_id' => 5, 'role_id' => 3 ],
            ]
        );

        // Assign Permissions
        DB::table('assigned_permissions_to_roles')->insert(
            [
                [ 'role_id' => 2, 'permission_id' => 1 ],
                [ 'role_id' => 2, 'permission_id' => 2 ],
                [ 'role_id' => 2, 'permission_id' => 3 ],
                [ 'role_id' => 2, 'permission_id' => 4 ],
                [ 'role_id' => 2, 'permission_id' => 5 ],
                [ 'role_id' => 2, 'permission_id' => 6 ],
                [ 'role_id' => 2, 'permission_id' => 7 ],
                [ 'role_id' => 2, 'permission_id' => 8 ],
                [ 'role_id' => 2, 'permission_id' => 9 ],
                [ 'role_id' => 2, 'permission_id' => 10 ],
                [ 'role_id' => 2, 'permission_id' => 11 ],
                [ 'role_id' => 2, 'permission_id' => 12 ],
                [ 'role_id' => 2, 'permission_id' => 13 ],
                [ 'role_id' => 2, 'permission_id' => 14 ],
                [ 'role_id' => 2, 'permission_id' => 15 ],
                [ 'role_id' => 2, 'permission_id' => 16 ],
                [ 'role_id' => 2, 'permission_id' => 17 ],
                [ 'role_id' => 2, 'permission_id' => 18 ],
                [ 'role_id' => 2, 'permission_id' => 19 ],
                [ 'role_id' => 2, 'permission_id' => 20 ],
                [ 'role_id' => 2, 'permission_id' => 21 ],
                [ 'role_id' => 2, 'permission_id' => 22 ],
                [ 'role_id' => 2, 'permission_id' => 23 ],
                [ 'role_id' => 2, 'permission_id' => 24 ],
                [ 'role_id' => 2, 'permission_id' => 25 ],
                [ 'role_id' => 2, 'permission_id' => 26 ],
                [ 'role_id' => 2, 'permission_id' => 27 ],
                [ 'role_id' => 2, 'permission_id' => 28 ],
                [ 'role_id' => 2, 'permission_id' => 29 ],
                [ 'role_id' => 2, 'permission_id' => 30 ],
                [ 'role_id' => 2, 'permission_id' => 31 ],
                [ 'role_id' => 2, 'permission_id' => 32 ],
                [ 'role_id' => 2, 'permission_id' => 33 ],
                [ 'role_id' => 2, 'permission_id' => 34 ],
                [ 'role_id' => 2, 'permission_id' => 35 ],
                [ 'role_id' => 2, 'permission_id' => 36 ],
                [ 'role_id' => 2, 'permission_id' => 37 ],

                [ 'role_id' => 3, 'permission_id' => 1 ],
                [ 'role_id' => 3, 'permission_id' => 3 ],
                [ 'role_id' => 3, 'permission_id' => 6 ],
                [ 'role_id' => 3, 'permission_id' => 8 ],
                [ 'role_id' => 3, 'permission_id' => 10 ],
                [ 'role_id' => 3, 'permission_id' => 13 ],
                [ 'role_id' => 3, 'permission_id' => 15 ],
                [ 'role_id' => 3, 'permission_id' => 17 ],
                [ 'role_id' => 3, 'permission_id' => 20 ],
            ]
        );

        // Assign consortium to user administration
        DB::table('consortia_user_administration')->insert(
            [
                [ 'user_administration_id' => 1, 'consortium_id' => 1 ],
                [ 'user_administration_id' => 1, 'consortium_id' => 4 ],
                [ 'user_administration_id' => 1, 'consortium_id' => 9 ],

                [ 'user_administration_id' => 2, 'consortium_id' => 2 ],
                [ 'user_administration_id' => 2, 'consortium_id' => 4 ],
                [ 'user_administration_id' => 2, 'consortium_id' => 7 ],
                [ 'user_administration_id' => 2, 'consortium_id' => 9 ],

                [ 'user_administration_id' => 3, 'consortium_id' => 1 ],
                [ 'user_administration_id' => 3, 'consortium_id' => 2 ],
                [ 'user_administration_id' => 3, 'consortium_id' => 9 ],

                [ 'user_administration_id' => 4, 'consortium_id' => 3 ],
                [ 'user_administration_id' => 4, 'consortium_id' => 5 ],
                [ 'user_administration_id' => 4, 'consortium_id' => 8 ],

                [ 'user_administration_id' => 5, 'consortium_id' => 6 ],
                [ 'user_administration_id' => 5, 'consortium_id' => 10 ],
            ]
        );
    }
}
