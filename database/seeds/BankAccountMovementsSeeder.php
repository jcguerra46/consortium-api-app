<?php

use Illuminate\Database\Seeder;

class BankAccountMovementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\BankAccountMovement::class, 100)->create();
    }
}
