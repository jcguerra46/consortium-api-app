<?php

use Illuminate\Database\Seeder;

class BankAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\BankAccount::class, 80)->create();
    }
}
