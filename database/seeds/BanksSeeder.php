<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BanksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            ['name' => 'BANCO DE GALICIA Y BUENOS AIRES S.A.U.'],
            ['name' => 'BANCO DE LA NACION ARGENTINA'],
            ['name' => 'BANCO DE LA PROVINCIA DE BUENOS AIRES'],
            ['name' => 'INDUSTRIAL AND COMMERCIAL BANK OF CHINA'],
            ['name' => 'CITIBANK N.A.'],
            ['name' => 'BBVA BANCO FRANCES S.A.'],
            ['name' => 'BANCO DE LA PROVINCIA DE CORDOBA S.A.'],
            ['name' => 'BANCO SUPERVIELLE S.A.'],
            ['name' => 'BANCO DE LA CIUDAD DE BUENOS AIRES'],
            ['name' => 'BANCO PATAGONIA S.A.'],
            ['name' => 'BANCO HIPOTECARIO S.A.'],
            ['name' => 'BANCO DE SAN JUAN S.A.'],
            ['name' => 'BANCO DEL TUCUMAN S.A.'],
            ['name' => 'BANCO MUNICIPAL DE ROSARIO'],
            ['name' => 'BANCO SANTANDER RIO S.A.'],
            ['name' => 'BANCO DEL CHUBUT S.A.'],
            ['name' => 'BANCO DE SANTA CRUZ S.A.'],
            ['name' => 'BANCO DE LA PAMPA SOCIEDAD DE ECONOMÍA M'],
            ['name' => 'BANCO DE CORRIENTES S.A.'],
            ['name' => 'BANCO PROVINCIA DEL NEUQUÉN SOCIEDAD ANÓ'],
            ['name' => 'BRUBANK S.A.U.'],
            ['name' => 'BANCO INTERFINANZAS S.A.'],
            ['name' => 'HSBC BANK ARGENTINA S.A.'],
            ['name' => 'JPMORGAN CHASE BANK, NATIONAL ASSOCIATIO'],
            ['name' => 'BANCO CREDICOOP COOPERATIVO LIMITADO'],
            ['name' => 'BANCO DE VALORES S.A.'],
            ['name' => 'BANCO ROELA S.A.'],
            ['name' => 'BANCO MARIVA S.A.'],
            ['name' => 'BANCO ITAU ARGENTINA S.A.'],
            ['name' => 'BANK OF AMERICA, NATIONAL ASSOCIATION'],
            ['name' => 'BNP PARIBAS'],
            ['name' => 'BANCO PROVINCIA DE TIERRA DEL FUEGO'],
            ['name' => 'BANCO DE LA REPUBLICA ORIENTAL DEL URUGU'],
            ['name' => 'BANCO SAENZ S.A.'],
            ['name' => 'BANCO MERIDIAN S.A.'],
            ['name' => 'BANCO MACRO S.A.'],
            ['name' => 'BANCO COMAFI SOCIEDAD ANONIMA'],
            ['name' => 'BANCO DE INVERSION Y COMERCIO EXTERIOR S'],
            ['name' => 'BANCO PIANO S.A.'],
            ['name' => 'BANCO JULIO SOCIEDAD ANONIMA'],
            ['name' => 'BANCO RIOJA SOCIEDAD ANONIMA UNIPERSONAL'],
            ['name' => 'BANCO DEL SOL S.A.'],
            ['name' => 'NUEVO BANCO DEL CHACO S. A.'],
            ['name' => 'BANCO VOII S.A.'],
            ['name' => 'BANCO DE FORMOSA S.A.'],
            ['name' => 'BANCO CMF S.A.'],
            ['name' => 'BANCO DE SANTIAGO DEL ESTERO S.A.'],
            ['name' => 'BANCO INDUSTRIAL S.A.'],
            ['name' => 'NUEVO BANCO DE SANTA FE SOCIEDAD ANONIMA'],
            ['name' => 'BANCO CETELEM ARGENTINA S.A.'],
            ['name' => 'BANCO DE SERVICIOS FINANCIEROS S.A.'],
            ['name' => 'BANCO BRADESCO ARGENTINA S.A.U.'],
            ['name' => 'BANCO DE SERVICIOS Y TRANSACCIONES S.A.'],
            ['name' => 'RCI BANQUE S.A.'],
            ['name' => 'BACS BANCO DE CREDITO Y SECURITIZACION S'],
            ['name' => 'BANCO MASVENTAS S.A.'],
            ['name' => 'WILOBANK S.A.'],
            ['name' => 'NUEVO BANCO DE ENTRE RÍOS S.A.'],
            ['name' => 'BANCO COLUMBIA S.A.'],
            ['name' => 'BANCO BICA S.A.'],
            ['name' => 'BANCO COINAG S.A.'],
            ['name' => 'BANCO DE COMERCIO S.A.'],
            ['name' => 'FORD CREDIT COMPAÑIA FINANCIERA S.A.'],
            ['name' => 'COMPAÑIA FINANCIERA ARGENTINA S.A.'],
            ['name' => 'VOLKSWAGEN FINANCIAL SERVICES COMPAÑIA F'],
            ['name' => 'CORDIAL COMPAÑÍA FINANCIERA S.A.'],
            ['name' => 'FCA COMPAÑIA FINANCIERA S.A.'],
            ['name' => 'GPAT COMPAÑIA FINANCIERA S.A.U.'],
            ['name' => 'MERCEDES-BENZ COMPAÑÍA FINANCIERA ARGENT'],
            ['name' => 'ROMBO COMPAÑÍA FINANCIERA S.A.'],
            ['name' => 'JOHN DEERE CREDIT COMPAÑÍA FINANCIERA S.'],
            ['name' => 'PSA FINANCE ARGENTINA COMPAÑÍA FINANCIER'],
            ['name' => 'TOYOTA COMPAÑÍA FINANCIERA DE ARGENTINA'],
            ['name' => 'FINANDINO COMPAÑIA FINANCIERA S.A.'],
            ['name' => 'MONTEMAR COMPAÑIA FINANCIERA S.A.'],
            ['name' => 'TRANSATLANTICA COMPAÑIA FINANCIERA S.A.'],
            ['name' => 'CAJA DE CREDITO "CUENCA" COOPERATIVA LIM']
        ]);
    }
}
