<?php

use Illuminate\Database\Seeder;
use App\Models\CategoryDefault;

class CategoryDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryDefault::insert(
            [
                [
                    "number" => 1,
                    "name" => "Detalle de salaries y cargas sociales",
                    "position" => 1,
                    "hidden" => false,
                    "salaries" => true,
                ],
                [
                    "number" => 2,
                    "name" => "Servicios públicos",
                    "position" => 2,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 3,
                    "name" => "Abonos de servicios",
                    "position" => 3,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 4,
                    "name" => "Mantenimiento de partes comunes",
                    "position" => 4,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 5,
                    "name" => "Trabajos de reparación en Unidades Funcionales",
                    "position" => 5,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 6,
                    "name" => "Gastos bancarios",
                    "position" => 6,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 7,
                    "name" => "Gastos de limpieza",
                    "position" => 7,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 8,
                    "name" => "Gastos de administración",
                    "position" => 8,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 9,
                    "name" => "Pagos del periodo por seguros",
                    "position" => 9,
                    "hidden" => false,
                    "salaries" => false,
                ],
                [
                    "number" => 10,
                    "name" => "Otros",
                    "position" => 10,
                    "hidden" => false,
                    "salaries" => false,
                ]
            ]
        );
    }
}
