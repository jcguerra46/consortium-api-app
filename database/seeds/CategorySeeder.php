<?php

use Illuminate\Database\Seeder;
use App\Models\Administration;
use App\Models\CategoryDefault;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrations = Administration::get();
        $categoriesDefault = CategoryDefault::get();

        foreach ($administrations as $administration) {
            foreach ($categoriesDefault as $categoryDefault) {
                Category::create([
                    'number' => $categoryDefault->number,
                    'name' => $categoryDefault->name,
                    'position' => $categoryDefault->position,
                    'hidden' => $categoryDefault->hidden,
                    'salaries' => $categoryDefault->salaries,
                    'administration_id' => $administration->id,
                ]);
            }
        }
    }
}
