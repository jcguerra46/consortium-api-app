<?php

use Illuminate\Database\Seeder;

class ClaimCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\ClaimComment::class, 100)->create();
    }
}
