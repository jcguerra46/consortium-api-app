<?php

use Illuminate\Database\Seeder;

class ClaimsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Claim::class, 50)->create();
    }
}
