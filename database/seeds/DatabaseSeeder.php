<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CountriesSeeder');
        $this->call('BanksSeeder');
        $this->call('CategoryDefaultSeeder');
        $this->call('ProvincesSeeder');
        $this->call('PaymentMethodsSeeder');
        $this->call('AdministrationsSeeder');
        $this->call('ConsortiaSeeder');
        $this->call('AmenitiesSeeder');
        $this->call('PackageTypesSeeder');
        $this->call('PackagesSeeder');
        $this->call('FunctionalUnitTypeSeeder');
        $this->call('FunctionalUnitsSeeder');
        $this->call('UsersAdministrationsSeeder');
        $this->call('JudgmentsSeeder');
        $this->call('AssembliesSeeder');
        $this->call('ServicesSeeder');
        $this->call('ProvidersSeeder');
        $this->call('ContactSeeder');
        $this->call('ProfessionalFunctionsSeeder');
        $this->call('ProfessionalFunctionsSalariesSeeder');
        $this->call('EmployeeStatusSeeder');
        $this->call('EmployeesSeeder');
        $this->call('UserPortalSeeder');
        $this->call('ClaimsSeeder');
        $this->call('SocialWorksSeeder');
        $this->call('ClaimCommentSeeder');
        $this->call('CategorySeeder');
        $this->call('SpendingDescriptionsSeeder');
        $this->call('NotesSeeder');
        $this->call('InvoicesSeeder');
        $this->call('MenuSeeder');
        $this->call('PaymentOrdersSeeder');
        $this->call('ExpenseHeaderSeeder');
        $this->call('BankAccountsSeeder');
        $this->call('BankAccountMovementsSeeder');
        $this->call('EmployeeBeneficiarySeeder');
        $this->call('EmployeeAdjustmentSeeder');
        $this->call('CheckbooksSeeder');
        $this->call('ChecksSeeder');
        $this->call('SalaryPlusSeeder');
        $this->call('SalaryAdditionalsSeeder');
        $this->call('VirtualCashboxSeeder');
        $this->call('AfipActivitySeeder');
        $this->call('AfipConditionSeeder');
        $this->call('AfipModoContratacionSeeder');
        $this->call('AfipProvinciaSeeder');
        $this->call('AfipSiniestroSeeder');
        $this->call('AfipSituacionSeeder');
        $this->call('AfipZonaSeeder');
        $this->call('AgendaEventSeeder');
        $this->call('RoleSeeder');
        $this->call('PermissionSeeder');
        $this->call('AssignRolesAndPermissionsSeeder');
        $this->call('ProviderFinancialMovementsSeeder');
        $this->call('SpendingSeeder');
        $this->call('PercentagesConsortiumSeeder');
    }
}
