<?php

use Illuminate\Database\Seeder;

class EmployeeAdjustmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\EmployeeAdjustment::class, 50)->create();
    }
}
