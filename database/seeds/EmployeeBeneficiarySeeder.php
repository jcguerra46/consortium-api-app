<?php

use Illuminate\Database\Seeder;

class EmployeeBeneficiarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\EmployeeBeneficiary::class, 50)->create();
    }
}
