<?php

use Illuminate\Database\Seeder;
use App\Models\EmployeeStatus;

class EmployeeStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmployeeStatus::insert([
            [
                'number' => 1,
                'description' => 'Baja por fallecimiento',
                'default' => false
            ],
            [
                'number' => 2,
                'description' => 'Baja otras causales',
                'default' => false
            ],
            [
                'number' => 3,
                'description' => 'Licencia por maternidad',
                'default' => false
            ],
            [
                'number' => 4,
                'description' => 'Reserva de puesto',
                'default' => false
            ],
            [
                'number' => 5,
                'description' => 'Baja por renuncia',
                'default' => false
            ],
            [
                'number' => 6,
                'description' => 'Recibe haberes normalmente',
                'default' => true
            ],
            [
                'number' => 7,
                'description' => 'suspensiones_otras_causales',
                'default' => false
            ],
            [
                'number' => 8,
                'description' => 'Baja por despido',
                'default' => false
            ],
            [
                'number' => 9,
                'description' => 'Licencia sin_goce_de_haberes',
                'default' => false
            ]
        ]);
    }
}
