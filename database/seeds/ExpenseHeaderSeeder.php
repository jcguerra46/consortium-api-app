<?php

use Illuminate\Database\Seeder;

class ExpenseHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\ExpenseHeader::class, 10)->create();
    }
}
