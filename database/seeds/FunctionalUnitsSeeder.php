<?php

use Illuminate\Database\Seeder;

class FunctionalUnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\FunctionalUnit::class, 500)->create()->each(
            function($functional_unit) {
                factory(App\Models\FunctionalUnitMovement::class, 5)->create([
                    'functional_unit_id' => $functional_unit->id
                ]);
            }
        );
    }
}
