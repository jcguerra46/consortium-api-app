<?php

use Illuminate\Database\Seeder;

class JudgmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Judgment::class, 50)->create();
    }
}
