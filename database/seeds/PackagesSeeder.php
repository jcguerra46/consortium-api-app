<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            [
                'description' => '100% Online',
                'observations' => null,
                'cost' => 29.00,
                'discount' => null,
                'legacy_description' => 1600,
                'package_type_id' => 1,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'description' => 'Solo Recibos',
                'observations' => null,
                'cost' => 30.00,
                'discount' => null,
                'legacy_description' => 1300,
                'package_type_id' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'description' => '100% Impreso 43',
                'observations' => null,
                'cost' => 43.00,
                'discount' => null,
                'legacy_description' => 1000,
                'package_type_id' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'description' => '100% Impreso 31',
                'observations' => null,
                'cost' => 43.00,
                'discount' => null,
                'legacy_description' => 1000,
                'package_type_id' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ],
            [
                'description' => '100% Impreso 31',
                'observations' => null,
                'cost' => 43.00,
                'discount' => null,
                'legacy_description' => 1000,
                'package_type_id' => 3,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]
        ]);
    }
}
