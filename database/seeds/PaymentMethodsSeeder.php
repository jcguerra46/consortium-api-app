<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_methods')->insert([
            [
                'name' => 'Pago Fácil',
            ],
            [
                'name' => 'Siro',
            ],
            [
                'name' => 'Pago Mis Expensas',
            ],
            [
                'name' => 'Expensas Pagas',
            ],
            [
                'name' => 'Interfast',
            ],
        ]);
    }
}
