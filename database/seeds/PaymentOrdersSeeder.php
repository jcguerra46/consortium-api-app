<?php

use Illuminate\Database\Seeder;

class PaymentOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\PaymentOrder::class, 50)->create();
    }
}
