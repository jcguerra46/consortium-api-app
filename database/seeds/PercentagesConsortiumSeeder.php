<?php

use Illuminate\Database\Seeder;

class PercentagesConsortiumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\PercentageConsortium::class, 50)->create();
    }
}
