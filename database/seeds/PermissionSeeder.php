<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $resources = [
            'user-administrations' => [
                'name' => 'user-administrations',
                'controller' => 'UserAdministrationController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'consortium' => [
                'name' => 'consortium',
                'controller' => 'ConsortiumController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'functional-units' => [
                'name' => 'functional-units',
                'controller' => 'FunctionalUnitController',
                'methods' => ['index','store','show','update','destroy','paginate','export','storeBatchOfFunctionalUnits']
            ],
            'functional-unit-contact' => [
                'name' => 'functional-contact',
                'controller' => 'FunctionalUnitContactController',
                'methods' => ['index','store','show','update','destroy','paginate']
            ],
            'functional-unit-percentage' => [
                'name' => 'functional-unit-percentage',
                'controller' => 'FunctionalUnitPercentageController',
                'methods' => ['index','show','update','paginate']
            ],
            'functional-unit-current-account' => [
                'name' => 'functional-unit-current-account',
                'controller' => 'FunctionalUnitMovementController',
                'methods' => ['index','show','paginate','export','getBalance']
            ],



            'afip-actividades' => [
                'name' => 'afip-actividades',
                'controller' => 'AfipActivityController',
                'methods' => ['index','show','paginate']
            ],
            'afip-condiciones' => [
                'name' => 'afip-condiciones',
                'controller' => 'AfipConditionController',
                'methods' => ['index','show','paginate']
            ],
            'afip-provincias' => [
                'name' => 'afip-provincias',
                'controller' => 'AfipProvinciaController',
                'methods' => ['index','show','paginate']
            ],
            'afip-siniestros' => [
                'name' => 'afip-siniestros',
                'controller' => 'AfipSiniestroController',
                'methods' => ['index','show','paginate']
            ],
            'afip-situaciones' => [
                'name' => 'afip-situaciones',
                'controller' => 'AfipSituacionController',
                'methods' => ['index','show','paginate']
            ],
            'afip-zonas' => [
                'name' => 'afip-zonas',
                'controller' => 'AfipZonaController',
                'methods' => ['index','show','paginate']
            ],
            'agenda-events' => [
                'name' => 'agenda-events',
                'controller' => 'AgendaEventController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'amenity' => [
                'name' => 'amenity',
                'controller' => 'AmenityController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'assembly' => [
                'name' => 'assembly',
                'controller' => 'AssemblyController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'consortium-bank-account' => [
                'name' => 'consortium-bank-account',
                'controller' => 'BankAccountController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'consortium-bank-account-movement' => [
                'name' => 'consortium-bank-account-movement',
                'controller' => 'BankAccountMovementController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'banks' => [
                'name' => 'banks',
                'controller' => 'BankController',
                'methods' => ['index','show','paginate']
            ],
            'cashbox' => [
                'name' => 'cashbox',
                'controller' => 'CashboxController',
                'methods' => ['index','show','update','paginate','export']
            ],
            'categories' => [
                'name' => 'categories',
                'controller' => 'CategoriesController',
                'methods' => ['index','store','show','update','destroy']
            ],
            'check' => [
                'name' => 'check',
                'controller' => 'CheckController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'checkbook' => [
                'name' => 'checkbook',
                'controller' => 'CheckbookController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'claims-comments' => [
                'name' => 'claims-comments',
                'controller' => 'ClaimCommentController',
                'methods' => ['index','store','show','update','destroy','paginate']
            ],
            'claims' => [
                'name' => 'claims',
                'controller' => 'ClaimCommentController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'collection' => [
                'name' => 'collection',
                'controller' => 'CollectionController',
                'methods' => ['index','store','show','paginate','export']
            ],
            'consortium-movement' => [
                'name' => 'consortium-movement',
                'controller' => 'CollectionController',
                'methods' => ['index','store','show','paginate','export']
            ],
            'consortium-current-account' => [
                'name' => 'consortium-current-account',
                'controller' => 'ConsortiumMovementController',
                'methods' => ['index','show','paginate','export','getBalance']
            ],
            'consortium-note' => [
                'name' => 'consortium-note',
                'controller' => 'ConsortiumNoteController',
                'methods' => ['index','store','show','update','paginate','export']
            ],
            'consortium-percentage' => [
                'name' => 'consortium-percentage',
                'controller' => 'ConsortiumPercentageController',
                'methods' => ['index','store','show','update','destroy','paginate','export',
                    'duplicate','massCreate','getFunctionalUnitsPercentages','massUpdateFunctionalUnitsPercentages']
            ],
            'consortium-profile' => [
                'name' => 'consortium-profile',
                'controller' => 'ConsortiumProfileController',
                'methods' => ['show','update']
            ],
            'countries' => [
                'name' => 'countries',
                'controller' => 'CountryController',
                'methods' => ['index','show','paginate']
            ],
            'employee-adjustment' => [
                'name' => 'employee-adjustment',
                'controller' => 'EmployeeAdjustmentController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'employee-afip' => [
                'name' => 'employee-afip',
                'controller' => 'EmployeeAfipController',
                'methods' => ['index','update']
            ],
            'employee-beneficiaries' => [
                'name' => 'employee-beneficiaries',
                'controller' => 'EmployeeBeneficiaryController',
                'methods' => ['index','store','show','update','destroy','paginate']
            ],
            'employee' => [
                'name' => 'employee',
                'controller' => 'EmployeeController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'employee-data' => [
                'name' => 'employee-data',
                'controller' => 'EmployeeController',
                'methods' => ['show','update']
            ],
            'employee-status' => [
                'name' => 'employee-status',
                'controller' => 'EmployeeStatusController',
                'methods' => ['index','show','paginate']
            ],
            'expense-headers' => [
                'name' => 'expense-headers',
                'controller' => 'ExpenseHeaderController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'functional-unit-types' => [
                'name' => 'functional-unit-types',
                'controller' => 'FunctionalUnitTypeController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'invoice' => [
                'name' => 'invoice',
                'controller' => 'InvoiceController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'menus' => [
                'name' => 'menus',
                'controller' => 'MenuController',
                'methods' => ['show','setUserMenu','getMenuDefault','setMenuDefault']
            ],
            'note' => [
                'name' => 'note',
                'controller' => 'NoteController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'payment-order' => [
                'name' => 'payment-order',
                'controller' => 'PaymentOrderController',
                'methods' => ['index','store','show','update','destroy','paginate','export','duplicate','annular','printOrder']
            ],
            'payments' => [
                'name' => 'payments',
                'controller' => 'PaymentsController',
                'methods' => ['index','store','show','paginate','export','createDirect']
            ],
            'consortium-period' => [
                'name' => 'consortium-period',
                'controller' => 'PeriodController',
                'methods' => ['index','show','update','paginate','export','getOpenPeriod']
            ],
            'professional-functions' => [
                'name' => 'professional-functions',
                'controller' => 'ProfessionalFunctionController',
                'methods' => ['index','show','paginate']
            ],
            'provider' => [
                'name' => 'provider',
                'controller' => 'ProviderController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'provider-current-account' => [
                'name' => 'provider-current-account',
                'controller' => 'ProviderMovementController',
                'methods' => ['index','show','paginate','export','getBalance']
            ],
            'countries-provinces' => [
                'name' => 'countries-provinces',
                'controller' => 'ProvinceController',
                'methods' => ['index','show','paginate']
            ],
            'salary-pluses' => [
                'name' => 'salary-pluses',
                'controller' => 'SalaryPlusController',
                'methods' => ['index','show','paginate']
            ],
            'service' => [
                'name' => 'service',
                'controller' => 'ServiceController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'social-works' => [
                'name' => 'social-works',
                'controller' => 'SocialWorkController',
                'methods' => ['index','show','paginate']
            ],
            'spending' => [
                'name' => 'spending',
                'controller' => 'SpendingController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'spending-description' => [
                'name' => 'spending-description',
                'controller' => 'SpendingDescriptionController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'spending-installment' => [
                'name' => 'spending-installment',
                'controller' => 'SpendingInstallmentController',
                'methods' => ['index','show','paginate','export']
            ],
            'consortium-virtual-cashbox' => [
                'name' => 'consortium-virtual-cashbox',
                'controller' => 'VirtualCashboxController',
                'methods' => ['index','store','show','update','destroy','paginate','export']
            ],
            'consortium-virtual-cashbox-movement' => [
                'name' => 'consortium-virtual-cashbox-movement',
                'controller' => 'VirtualCashboxMovementController',
                'methods' => ['index','store','show','destroy','paginate','export']
            ],





        ];


        foreach($resources as $resource) {
           foreach($resource['methods'] as $method) {
               $description = $this->getMethodDescription($method);

               DB::table('permissions')->insert([
                   'name' => $resource['name'] . '-' . $method,
                   'resource' => $resource['name'],
                   'controller' => $resource['controller'],
                   'method' => $method,
                   'display_name' => Str::ucfirst($method),
                   'description' => $description,
                   'created_at' => Carbon::now(),
                   'updated_at' => Carbon::now()
               ]);
           }
        }
    }

    public function getMethodDescription($action)
    {
        $methods = [

            // CRUD
            'index' => [ 'description' => 'Permission to display a listing of the resource' ],
            'store' => [ 'description' => 'Permission to store a newly created resource in storage' ],
            'show' => [ 'description' => 'Permission to display the specified resource' ],
            'update' => [ 'description' => 'Permission to update the specified resource in storage' ],
            'destroy' => [ 'description' => 'Permission to remove the specified resource from storage' ],
            'paginate' => [ 'description' => 'Permission to display a listing of the resource with paginate' ],
            'export' => [ 'description' => 'Permission to export a listing of the resource' ],

            // functional units
            'createBatchOfFunctionalUnits' => [ 'description' => 'Permission to store a batch of the resource' ],

            // functional unit movement
            'getBalance' => [ 'description' => 'Permission to get balance of the resource' ],

            // Consortium Percentage
            'duplicate' => [ 'description' => 'Permission to uplicate of the resource' ],
            'massCreate' => [ 'description' => 'Permission to store a batch of the resource' ],

            // Menus
            'setUserMenu' => [ 'description'=> 'Permission to store user menu.' ],
            'getMenuDefault' => [ 'description'=> 'Permission to get default menu.' ],
            'setMenuDefault' => [ 'description'=> 'Permission to store default menu.' ],

            // payment-order
            'annular' => [ 'description' => 'Permission to annular a resource' ],
            'printOrder' => [ 'description' => 'Permission to print order' ],

            // Payments
            'createDirect' => [ 'description' => 'Permission to store a resource' ],

            // consortium-period
            'getOpenPeriod' => [ 'description' => 'Permission to show a open period' ],



        ];

        foreach($methods as $key => $value) {
            if ($key === $action) {
                return $value['description'];
            }
        }
    }
}
