<?php

use Illuminate\Database\Seeder;

class ProductionDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('CountriesSeeder');
        $this->call('ProvincesSeeder');
        $this->call('PaymentMethodsSeeder');
        $this->call('ProfessionalFunctionsSeeder');
        $this->call('EmployeeStatusSeeder');
    }
}
