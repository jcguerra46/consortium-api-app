<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\ProfessionalFunctionSalarie;

class ProfessionalFunctionsSalariesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        ProfessionalFunctionSalarie::insert([
            ['id' => 2324, 'consortium_category_id' => 1, 'professional_function_id' => 1, 'value' => 25655, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2325, 'consortium_category_id' => 2, 'professional_function_id' => 1, 'value' => 24586, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2326, 'consortium_category_id' => 3, 'professional_function_id' => 1, 'value' => 23517, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2327, 'consortium_category_id' => 4, 'professional_function_id' => 1, 'value' => 21379, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2328, 'consortium_category_id' => 1, 'professional_function_id' => 2, 'value' => 30155, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2329, 'consortium_category_id' => 2, 'professional_function_id' => 2, 'value' => 28898, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2330, 'consortium_category_id' => 3, 'professional_function_id' => 2, 'value' => 27642, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2331, 'consortium_category_id' => 4, 'professional_function_id' => 2, 'value' => 25129, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2332, 'consortium_category_id' => 1, 'professional_function_id' => 3, 'value' => 25655, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2333, 'consortium_category_id' => 2, 'professional_function_id' => 3, 'value' => 24586, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2334, 'consortium_category_id' => 3, 'professional_function_id' => 3, 'value' => 23517, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2335, 'consortium_category_id' => 4, 'professional_function_id' => 3, 'value' => 21379, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2336, 'consortium_category_id' => 1, 'professional_function_id' => 4, 'value' => 30155, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2337, 'consortium_category_id' => 2, 'professional_function_id' => 4, 'value' => 28898, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2338, 'consortium_category_id' => 3, 'professional_function_id' => 4, 'value' => 27642, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2339, 'consortium_category_id' => 4, 'professional_function_id' => 4, 'value' => 25129, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2340, 'consortium_category_id' => 1, 'professional_function_id' => 5, 'value' => 15077, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2341, 'consortium_category_id' => 2, 'professional_function_id' => 5, 'value' => 14449, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2342, 'consortium_category_id' => 3, 'professional_function_id' => 5, 'value' => 13821, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2343, 'consortium_category_id' => 4, 'professional_function_id' => 5, 'value' => 12565, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2344, 'consortium_category_id' => 1, 'professional_function_id' => 6, 'value' => 25655, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2345, 'consortium_category_id' => 2, 'professional_function_id' => 6, 'value' => 24586, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2346, 'consortium_category_id' => 3, 'professional_function_id' => 6, 'value' => 23517, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2347, 'consortium_category_id' => 4, 'professional_function_id' => 6, 'value' => 21379, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2348, 'consortium_category_id' => 1, 'professional_function_id' => 7, 'value' => 30155, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2349, 'consortium_category_id' => 2, 'professional_function_id' => 7, 'value' => 28898, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2350, 'consortium_category_id' => 3, 'professional_function_id' => 7, 'value' => 27642, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2351, 'consortium_category_id' => 4, 'professional_function_id' => 7, 'value' => 25129, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2352, 'consortium_category_id' => 1, 'professional_function_id' => 8, 'value' => 26607, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2353, 'consortium_category_id' => 2, 'professional_function_id' => 8, 'value' => 25498, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2354, 'consortium_category_id' => 3, 'professional_function_id' => 8, 'value' => 24390, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2355, 'consortium_category_id' => 4, 'professional_function_id' => 8, 'value' => 22173, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2356, 'consortium_category_id' => 1, 'professional_function_id' => 9, 'value' => 31387, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2357, 'consortium_category_id' => 2, 'professional_function_id' => 9, 'value' => 30079, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2358, 'consortium_category_id' => 3, 'professional_function_id' => 9, 'value' => 28771, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2359, 'consortium_category_id' => 4, 'professional_function_id' => 9, 'value' => 26156, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2360, 'consortium_category_id' => 1, 'professional_function_id' => 10, 'value' => 38417, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2361, 'consortium_category_id' => 2, 'professional_function_id' => 10, 'value' => 38417, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2362, 'consortium_category_id' => 3, 'professional_function_id' => 10, 'value' => 38417, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2363, 'consortium_category_id' => 4, 'professional_function_id' => 10, 'value' => 38417, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2364, 'consortium_category_id' => 1, 'professional_function_id' => 11, 'value' => 25655, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2365, 'consortium_category_id' => 2, 'professional_function_id' => 11, 'value' => 24586, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2366, 'consortium_category_id' => 3, 'professional_function_id' => 11, 'value' => 23517, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2367, 'consortium_category_id' => 4, 'professional_function_id' => 11, 'value' => 21379, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2368, 'consortium_category_id' => 1, 'professional_function_id' => 12, 'value' => 30154, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2369, 'consortium_category_id' => 2, 'professional_function_id' => 12, 'value' => 28898, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2370, 'consortium_category_id' => 3, 'professional_function_id' => 12, 'value' => 27642, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2371, 'consortium_category_id' => 4, 'professional_function_id' => 12, 'value' => 25129, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2372, 'consortium_category_id' => 1, 'professional_function_id' => 13, 'value' => 22620, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2373, 'consortium_category_id' => 2, 'professional_function_id' => 13, 'value' => 22620, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2374, 'consortium_category_id' => 3, 'professional_function_id' => 13, 'value' => 22620, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2375, 'consortium_category_id' => 4, 'professional_function_id' => 13, 'value' => 22620, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2376, 'consortium_category_id' => 1, 'professional_function_id' => 14, 'value' => 24654, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2377, 'consortium_category_id' => 2, 'professional_function_id' => 14, 'value' => 24654, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2378, 'consortium_category_id' => 3, 'professional_function_id' => 14, 'value' => 24654, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2379, 'consortium_category_id' => 4, 'professional_function_id' => 14, 'value' => 24654, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2380, 'consortium_category_id' => 1, 'professional_function_id' => 15, 'value' => 27167, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2381, 'consortium_category_id' => 2, 'professional_function_id' => 15, 'value' => 27167, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2382, 'consortium_category_id' => 3, 'professional_function_id' => 15, 'value' => 27167, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2383, 'consortium_category_id' => 4, 'professional_function_id' => 15, 'value' => 27167, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2384, 'consortium_category_id' => 1, 'professional_function_id' => 16, 'value' => 25294, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2385, 'consortium_category_id' => 2, 'professional_function_id' => 16, 'value' => 25294, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2386, 'consortium_category_id' => 3, 'professional_function_id' => 16, 'value' => 25294, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2387, 'consortium_category_id' => 4, 'professional_function_id' => 16, 'value' => 25294, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2388, 'consortium_category_id' => 1, 'professional_function_id' => 17, 'value' => 12647, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2389, 'consortium_category_id' => 2, 'professional_function_id' => 17, 'value' => 12647, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2390, 'consortium_category_id' => 3, 'professional_function_id' => 17, 'value' => 12647, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2391, 'consortium_category_id' => 4, 'professional_function_id' => 17, 'value' => 12647, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2392, 'consortium_category_id' => 1, 'professional_function_id' => 18, 'value' => 12172, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2393, 'consortium_category_id' => 2, 'professional_function_id' => 18, 'value' => 12172, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2394, 'consortium_category_id' => 3, 'professional_function_id' => 18, 'value' => 12172, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2395, 'consortium_category_id' => 4, 'professional_function_id' => 18, 'value' => 12172, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2396, 'consortium_category_id' => 1, 'professional_function_id' => 19, 'value' => 13229, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2397, 'consortium_category_id' => 2, 'professional_function_id' => 19, 'value' => 13229, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2398, 'consortium_category_id' => 3, 'professional_function_id' => 19, 'value' => 13229, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2399, 'consortium_category_id' => 4, 'professional_function_id' => 19, 'value' => 13229, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2400, 'consortium_category_id' => 1, 'professional_function_id' => 20, 'value' => 24777, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2401, 'consortium_category_id' => 2, 'professional_function_id' => 20, 'value' => 24777, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2402, 'consortium_category_id' => 3, 'professional_function_id' => 20, 'value' => 24777, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2403, 'consortium_category_id' => 4, 'professional_function_id' => 20, 'value' => 24777, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2404, 'consortium_category_id' => 1, 'professional_function_id' => 21, 'value' => 12389, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2405, 'consortium_category_id' => 2, 'professional_function_id' => 21, 'value' => 12389, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2406, 'consortium_category_id' => 3, 'professional_function_id' => 21, 'value' => 12389, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2407, 'consortium_category_id' => 4, 'professional_function_id' => 21, 'value' => 12389, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2408, 'consortium_category_id' => null, 'professional_function_id' => 22, 'value' => 256.2, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2409, 'consortium_category_id' => null, 'professional_function_id' => 23, 'value' => 1118.8, 'period' => '2019-02-01', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
