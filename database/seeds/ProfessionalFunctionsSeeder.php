<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\ProfessionalFunction;

class ProfessionalFunctionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProfessionalFunction::insert( [
            [ 'id' => 1,  'name' => 'Encargado Permanente con vivienda',       'description' => 'Encargado Permanente con vivienda',       'has_department' => true,  'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 2,  'name' => 'Encargado Permanente sin vivienda',       'description' => 'Encargado Permanente sin vivienda',       'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 3,  'name' => 'Ayudante Permanente con vivienda',        'description' => 'Ayudante Permanente con vivienda',        'has_department' => true,  'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 4,  'name' => 'Ayudante Permanente sin vivienda',        'description' => 'Ayudante Permanente sin vivienda',        'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 5,  'name' => 'Ayudante Media jornada',                  'description' => 'Ayudante Media jornada',                  'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 1, 'part_time' => true,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 6,  'name' => 'Personal Asimilado Con vivienda',         'description' => 'Personal Asimilado Con vivienda',         'has_department' => true,  'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 7,  'name' => 'Personal Asimilado Sin vivienda',         'description' => 'Personal Asimilado Sin vivienda',         'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 8,  'name' => 'Mayordomo Con vivienda',                  'description' => 'Mayordomo Con vivienda',                  'has_department' => true,  'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 9,  'name' => 'Mayordomo Sin vivienda',                  'description' => 'Mayordomo Sin vivienda',                  'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 10, 'name' => 'Intendente',                              'description' => 'Intendente',                              'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 11, 'name' => 'Personal con mas 1 Funcion con vivienda', 'description' => 'Personal con mas 1 Funcion con vivienda', 'has_department' => true,  'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 12, 'name' => 'Personal con mas 1 Funcion sin vivienda', 'description' => 'Personal con mas 1 Funcion sin vivienda', 'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 13, 'name' => 'Encargado Guardacoches con vivienda',     'description' => 'Encargado Guardacoches con vivienda',     'has_department' => true,  'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 14, 'name' => 'Encargado Guardacoches sin vivienda',     'description' => 'Encargado Guardacoches sin vivienda',     'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 15, 'name' => 'Personal Vigilancia Nocturna',            'description' => 'Personal Vigilancia Nocturna',            'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 16, 'name' => 'Personal Vigilancia Diurna',              'description' => 'Personal Vigilancia Diurna',              'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 17, 'name' => 'Personal Vigilancia Media Jornada',       'description' => 'Personal Vigilancia Media Jordnada',      'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 1, 'part_time' => true,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 18, 'name' => 'Encargado No Permanente Con vivienda',    'description' => 'Encargado No Permanente Con vivienda',    'has_department' => true,  'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 1, 'part_time' => true,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 19, 'name' => 'Encargado No Permanente Sin vivienda',    'description' => 'Encargado No Permanente Sin vivienda',    'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 1, 'part_time' => true,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 20, 'name' => 'Ayudante Temporario',                     'description' => 'Ayudante Temporario',                     'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 2, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 21, 'name' => 'Ayudante Temporario Media Jornada',       'description' => 'Ayudante Temporario Media Jornada',       'has_department' => false, 'jornalizado' => false, 'surrogate' => false, 'type_seniority' => 1, 'part_time' => true,  'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 22, 'name' => 'Personal Jornalizado no mas 18 horas',    'description' => 'Personal Jornalizado no mas 18 horas',    'has_department' => false, 'jornalizado' => true,  'surrogate' => false, 'type_seniority' => 1, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
            [ 'id' => 23, 'name' => 'Suplente con horario por dia',            'description' => 'Suplente con horario por dia',            'has_department' => false, 'jornalizado' => false, 'surrogate' => true,  'type_seniority' => 1, 'part_time' => false, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ],
        ]);
    }
}
