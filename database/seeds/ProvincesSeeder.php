<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvincesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('provinces')->insert(
            [
                [
                    'country_id' => 1,
                    'name' => 'Capital Federal',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Provincia de Buenos Aires',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Catamarca',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Chaco',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Chubut',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Córdoba',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Corrientes',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Entre Ríos',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Formosa',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Jujuy',
                ],
                [
                    'country_id' => 1,
                    'name' => 'La Pampa',
                ],
                [
                    'country_id' => 1,
                    'name' => 'La Rioja',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Mendoza',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Misiones',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Neuquen',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Río Negro',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Salta',
                ],
                [
                    'country_id' => 1,
                    'name' => 'San Juan',
                ],
                [
                    'country_id' => 1,
                    'name' => 'San Luis',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Santa Cruz',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Santa Fe',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Santiago del Estero',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Tierra del Fuego',
                ],
                [
                    'country_id' => 1,
                    'name' => 'Tucumán',
                ]
            ]
        );

    }
}
