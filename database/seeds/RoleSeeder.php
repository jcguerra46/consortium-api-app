<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                [
                    'name' => 'superadmin',
                    'slug' => 'super-admin',
                    'display_name' => 'Super Administrador',
                    'description' => 'Super Administrador',
                    'active' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ],
                [
                    'name' => 'admin',
                    'slug' => 'admin',
                    'display_name' => 'Administrador',
                    'description' => 'Administrador',
                    'active' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ],
                [
                    'name' => 'operador',
                    'slug' => 'operador',
                    'display_name' => 'Operador',
                    'description' => 'Operador',
                    'active' => true,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ],
            ]
        );
    }
}
