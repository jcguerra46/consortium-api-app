<?php

use App\Models\SalaryPlus;
use Illuminate\Database\Seeder;

class SalaryPlusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SalaryPlus::insert([
            [
                'name' => 'RETIRO RESIDUOS',
                'description' => 'RETIRO DE RESIDUOS POR UNIDAD DESTINADA A VIVIENDA U OFICINA',
                'value' => 30.1,
                'internal_class' => 'RetiroResiduos',
            ],
            [
                'name' => 'CLASIF. RESIDUOS',
                'description' => 'CLASIFICIACIÓN DE RESIDUOS RESOL. 2013 243 SSRT - GCABA',
                'value' => 518.4,
                'internal_class' => 'ClasificacionResiduos'
            ],
            [
                'name' => 'LIMPIEZA COCHERAS',
                'description' => 'PLUS LIMPIEZA DE COCHERAS',
                'value' => 410.7,
                'internal_class' => 'LimpiezaCocheras',
            ],
            [
                'name' => 'MOVIMIENTO COCHES',
                'description' => "PLUS MOVIMIENTO DE COCHES - HASTA 20 UNIDADES",
                'value' => 608.1,
                'internal_class' => 'MovimientoCoches',
            ],
            [
                'name' => 'JARDIN',
                'description' => 'PLUS JARDÍN',
                'value' => 410.7,
                'internal_class' => 'Jardin',
            ],
            [
                'name' => 'ZONA DESFAVORABLE',
                'description' => 'PLUS ZONA DESFAVORABLE',
                'value' => 50,
                'internal_class' => 'ZonaDesfavorable',
            ],
            [
                'name' => 'TITULO ENCARGADO',
                'description' => 'TITULO DE ENCARGADO INTEGRAL DE EDIFICIO',
                'value' => 10,
                'internal_class' => 'TituloEncargado',
            ],
            [
                'name' => 'LIMPIEZA PILETAS',
                'description' => 'PLUS LIMPIEZA DE PILETAS Y MANTENIMIENTO DEL AGUA',
                'value' => 690.9,
                'internal_class' => 'LimpiezaPiletas'
            ],
            [
                'name' => 'CAPACITACION SEGURIDAD E HIGIENE',
                'description' => 'PLUS CAPACITACION SEGURIDAD E HIGIENE',
                'value' => 5,
                'internal_class' => 'CapacitacionSeguridadHigiene',
            ]
        ]);
    }
}
