<?php

use Illuminate\Database\Seeder;

class SpendingDescriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\SpendingDescription::class, 100)->create();
    }
}
