<?php

use Illuminate\Database\Seeder;

class SpendingRecurrentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\SpendingRecurrent::class, 50)->create();
    }
}
