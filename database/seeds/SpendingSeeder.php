<?php

use App\Models\Spending;
use App\Models\SpendingInstallment;
use Illuminate\Database\Seeder;

class SpendingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Spending::class, 50)->create()->each(
            function (Spending $spending) {
                $countInstallments = rand(1, 4);
                $spending->installments()->saveMany(factory(SpendingInstallment::class, $countInstallments)->make([
                    'amount' => round($spending->amount / $countInstallments, 2)
                ]));
            }
        );
    }
}
