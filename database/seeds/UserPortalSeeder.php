<?php

use Illuminate\Database\Seeder;

class UserPortalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\UserPortal::class, 50)->create();
    }
}
