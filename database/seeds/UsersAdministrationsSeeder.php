<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersAdministrationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_administrations')->insert(
            [
                [
                    'administration_id' => '1',
                    'principal' => true,
                    'first_name' => 'Octopus',
                    'last_name' => 'Sistemas',
                    'email' => 'sistemas@octopus.com.ar',
                    'password' => Hash::make('123456'),
                    'active' => true,
                    'menu' => null,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
                [
                    'administration_id' => '1',
                    'principal' => true,
                    'first_name' => 'Admin',
                    'last_name' => 'Sistemas Octopus',
                    'email' => 'admin@octopus.com.ar',
                    'password' => Hash::make('123456'),
                    'active' => true,
                    'menu' => null,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
                [
                    'administration_id' => '1',
                    'principal' => true,
                    'first_name' => 'consorcista',
                    'last_name' => 'Sistemas Octopus',
                    'email' => 'consorcista@octopus.com.ar',
                    'password' => Hash::make('123456'),
                    'active' => true,
                    'menu' => null,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
                [
                    'administration_id' => '2',
                    'principal' => true,
                    'first_name' => 'Admin',
                    'last_name' => 'Sistemas Octopus',
                    'email' => 'admin2@octopus.com.ar',
                    'password' => Hash::make('123456'),
                    'active' => true,
                    'menu' => null,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
                [
                    'administration_id' => '3',
                    'principal' => true,
                    'first_name' => 'Admin',
                    'last_name' => 'Sistemas Octopus',
                    'email' => 'admin3@octopus.com.ar',
                    'password' => Hash::make('123456'),
                    'active' => true,
                    'menu' => null,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
            ]
        );

//        $count = \App\Models\UserAdministration::all()->count();;
//        for ($i = 1; $i < $count + 1; $i++) {
//            $consortium = \App\Models\Consortium::all()->random(mt_rand(2,4))->pluck('id');
//            $user = \App\Models\UserAdministration::find($i);
//            $user->consortiums()->attach($consortium);
//        }
    }
}
