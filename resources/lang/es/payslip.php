<?php
return [
    'seniority_concept' => 'ANTIGÜEDAD - :percentage%',
    'seniority_concept_year' => 'AÑO',
    'seniority_concept_years' => 'AÑOS',
    'apartment' => 'VIVIENDA',
    'aporte_jubilatorio' => 'APORTE JUBILATORIO :number%',
    'aporte_ley_19032' => 'I.N.S.S.J.P LEY 19032 :number%',
    'aporte_obra_social' => 'OBRA SOCIAL :number% :code',
    'aporte_obra_social_2' => 'OBRA SOCIAL S/ :value :number% :code',
    'aporte_adicional_obra_social' => 'ADICIONAL OBRA SOCIAL :number%',
    'aporte_adicional_obra_social_2' => 'ADICIONAL OBRA SOCIAL S/ :value :number%',
    'aporte_adicional_cpf' => 'C.P.F. :number%',
    'aporte_adicional_fateryh' => 'FATERYH (F.M.V.D.D) :number%',
    'aporte_adicional_cuota_sindical' => 'CUOTA SINDICAL SUTERH :number%',
    'aporte_caja_subsidios' => 'CAJA SUBSIDIOS FAMILIARES :number%',
    'aporte_obra_social_employer' => 'OBRA SOCIAL :number% :code',
    'aporte_obra_social_employer_2' => 'OBRA SOCIAL S/ :value :number% :code',
    'aporte_caja_art_amount' => 'ART FIJO RES. 590/97',
    'aporte_caja_art_percentage' => 'ART PORCENTAJE',
    'aporte_seguro_vida' => 'SEGURO VIDA',
    'aporte_caja_proteccion_familia' => 'CAJA PROTECCION FAMILIA :number%',
    'aporte_contribucion_solidaria' => 'CONTRIBUCION SOLIDARIA CCT 589/10',
    'aporte_afip' => 'AFIP',
    'aporte_suterh' => 'SUTERH',
    'aporte_seracarh' => 'SERACARH :number%',
    'aporte_fateryh' => 'FATERYH (F.M.V.D.D) :number%',
    'concept_rounding' => 'REDONDEO DE CENTAVOS',
    'extra_hours_pretext' => 'HORAS EXTRAS AL ',
    'basic_salary' => 'SUELDO BÁSICO',
    'employee_salary' => 'SUELDO NETO DEL PERIODO',
    'movement_salary' => 'SUELDO :name - :month/:year',
    'movement_salary_contributions' => 'CONTRIBUCIONES :name - :month/:year',
    'print' => 'Envios - Imprimir',
    'printed_list' => 'Procesar Sueldos',
    'create_package' => 'Enviar a Procesar',
    'package_title_data' => 'Selecciona Liquidaciones de Sueldo',
    'package_count_files' => 'Cantidad de recibos',
    'download_package' => 'Descargar paquete',
    'send_package' => 'Reenviar a imprimir',
    'send_package_subject' => 'Envío liquidaciones de sueldo para imprimir :id',
    'send_package_success' => 'Liquidaciones de sueldo enviadas',
    'no_payslips' => 'No hay recibos de sueldos a procesar por favor genere los mismos.',
    'art_fixed' => 'ART Fijo',
    'art_percentage' => 'ART %',
    'entry' => 'Ingreso',
    'social_work' => 'Obra Social',
    'salary' => 'Sueldo',
    'extra_hours' => 'HS. Extras',
    'count_extra_hours' => 'Cant.H.Ext',
    'sac_bruto' => 'SAC Bruto',
    'afip' => 'AFIP',
    'suterh' => 'SUTERH',
    'fateryh' => 'FATERYH',
    'seracarh' => 'SERACARH',
    'total_bruto' => 'Total Bruto',
    'total_neto' => 'Neto',
    'contriubution_location' => 'CONS.PROP.',
    'suterh_key' => 'Clave SUTERH',
    'summary_contribution_title' => 'Detalle de gasto por empleado',
    'summary_contribution_by_consortia_title' => 'RESUMEN DE CARGAS SOCIALES',
    'salary_label' => 'Nombre del concepto sueldo para expensa',
    'salary_value' => 'Valor del sueldo empleado ($)',
    'afip_value' => 'Valor Boleta AFIP 931 ($)',
    'suterh_value' => 'Valor Boleta SUTERH ($)',
    'fateryh_value' => 'Valor Boleta FATERYH ($)',
    'seracarh_value' => 'Valor Boleta SERACARH ($)',
    'liquidation_month_year' => 'Liquidacion del mes de :month de :year',
    'salary_birth_date' => 'Nació el',
    'civil_state' => 'Estado civil',
    'detail_liquidation' => 'Detalle de la liquidación',
    'haberes_aportes' => 'Haberes sujetos a aportes',
    'haberes_otros' => 'Otros haberes',
    'family_salary' => 'Salario Familiar',
    'discounts' => 'Descuentos',
    'neto_amount' => 'Neto cobrado',
    'total_legajo' => 'Totales de Legajo',
    'total_consortium' => 'Total de Consorcio',
    'no_payslips_period' => 'No hay liquidaciones de sueldo entre los periodos dados.',
    'vacation_adjust_concept' => 'Días no trabajados por vacaciones',
    'licence_days_justified_label' => 'Licencia por enfermedad',
    'licence_days_unjustified_label' => 'Inasistencia',
    'licence_days_justified' => 'Inasistencia por enfermedad',
    'select_all' => 'Seleccionar todos',
    'payslips' => 'Liquidaciones',
    'search_payslips' => 'Buscar Liquidaciones',
    'payslip_error' => 'Esta intentando procesar una liquidación inexistente',
    'send_to_process' => 'Recuerde enviar a procesar una vez terminado de liquidar todos los sueldos.',
    'inactive' => 'Desactivar',
    'active' => 'Activar',
    'select_all_consortia' => 'Seleccionar todos los edificios',
    'no_duplicate_payslip' => 'No se puede crear más de un sueldo mensual con el mismo periodo',
    'after_package' => 'Ya procesado',
    'delete_error' => 'El sueldo se encuentra liquidado para la próxima expensa. Para borrarlo deberá primero eliminar los gastos correspondientes al mismo',
    'include_in_expense' => 'Incluir en próxima expensa',
    'contributions' => 'CONTRIBUCIONES',
    'delete_in_expense_error' => 'No se puede borrar el sueldo ya que esta incluido en una expensa',
    'liquidation_error' => 'Debe tener seleccionado al menos un sueldo para liquidar',
];
