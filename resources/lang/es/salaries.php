<?php
return [
    'employee_without_professional_function' => 'El empleado no tiene una Funcion Profesional asignada',
    'basic_salary' => 'Sueldo Básico',
    'jornal_salary' => 'Sueldo Jornal',
    'total_neto' => 'Total Neto',
    'total_bruto' => 'Total Bruto',
];
