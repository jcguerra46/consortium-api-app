<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="robots" content="NOINDEX, NOFOLLOW">
    @include('payment-order.styles')
</head>
<body>
<section>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="invoice-title">
                                <img src="{{ url('img/logo_octopus_black.png') }}" style="max-width:100px;">
                            </td>

                            <td>
                                Orden de Pago #: {{ $paymentOrder->id }}<br>
                                {{ $paymentOrder->date->format('d/m/Y') }}<br>
                                <b>Proveedor: {{ $paymentOrder->provider->business_name }}</b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                {{ $administration->business_name }}<br>
                                {{ $administration->getAddress() }} ({{ $administration->postal_code }})<br>
                                {{ $administration->province->name }}
                            </td>

                            <td>
                                {{ $administration->responsible }}<br>
                                {{ $administration->phone }}<br>
                                {{ $administration->email }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="heading">
                <td>
                    Items
                </td>

                <td>
                    Monto
                </td>
            </tr>

            @foreach($paymentOrder->invoices as $invoice)
                <tr class="item">
                    <td>
                        Factura N°: {{ $invoice->invoice_number }}<br>
                        Fecha: {{ $invoice->date->format('d/m/Y') }}
                    </td>

                    <td style="vertical-align: middle;">
                        {{ \App\Utils\CurrencyHelper::format($invoice->amount) }}
                    </td>
                </tr>
            @endforeach

            <tr class="total">
                <td></td>

                <td>
                    Total: {{ \App\Utils\CurrencyHelper::format($paymentOrder->amount) }}
                </td>
            </tr>
        </table>
    </div>
</section>
</body>
</html>



