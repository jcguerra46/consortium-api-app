<style>
    .liquidation-salary-water-mark {
        position: absolute;
        z-index: -1000;
    }

    .liquidation-salary-container {
        width: 100%;
        height: 478px;
        font-size: 11px;
        font-family: 'Lato', sans-serif;
    }

    .liquidation-salary-template-container {
        padding-right: 3%;
    }

    .liquidation-salary-template-container-duplicate {
        margin-left: 3%;
    }

    .liquidation-salary-template {
        height: 680px;
    }

    .liquidation-salary-amount {
        padding-right: 2px;
        text-align: right;
    }

    .liquidation-salary-table {
        margin-left: 5px;
        margin-right: 5px;
    }

    .liquidation-salary-title {
        text-align: right;
        color: #1c95c8;
        font-size: 15px;
    }

    .liquidation-salary-original-duplicate {
        text-align: center;
        text-transform: uppercase;
        font-weight: bold;
    }

    .liquidation-salary-consortium-info {
        text-transform: uppercase !important;
        font-size: 10px;
        font-weight: bold !important;
        height: 55px;
        margin-top: 12px;
    }

    .liquidation-salary-consortium-cell-subtitle {
        background-color: #e6e7e9;
        border-right: 1px solid #1c95c8;
    }

    .liquidation-salary-consortium-last-cell-subtitle {
        background-color: #e6e7e9;
    }

    .liquidation-salary-consortium-cell-second-row-subtitle {
        background-color: #e6e7e9;
        border-right: 1px solid #1c95c8;
        border-top: 1px solid #1c95c8;
    }

    .liquidation-salary-consortium-last-cell-second-row-subtitle {
        background-color: #e6e7e9;
        border-top: 1px solid #1c95c8;
    }

    .liquidation-salary-common-data {
        margin-left: 3px;
    }

    .liquidation-salary-row-title-concepts {
        background-color: #1c95c8;
        color: white;
    }

    .liquidation-salary-title-concepts {
        border-right: 1px solid white;
        height: 20px;
    }

    .liquidation-salary-table-concepts {
        margin-right: 10px;
        height: 235px;
    }

    .liquidation-salary-table-concepts tr {
        vertical-align: top;
    }

    .liquidation-salary-qualification-title {
        text-transform: none !important;
        background-color: #e6e7e9;
        height: 20px;
    }

    .liquidation-salary-qualification-total-title {
        background-color: #1c95c8;
        text-align: center;
        color: white;
    }

    .liquidation-salary-qualification-profession {
        height: 30px;
    }

    .liquidation-salary-total-text-title {
        background-color: #e6e7e9;
        text-transform: none !important;
        height: 20px;
    }

    .liquidation-salary-total-text {
        height: 20px;
    }

    .liquidation-salary-row-title-contributions {
        background-color: #1c95c8;
        color: white;
    }

    .liquidation-salary-art-title-contributions {
        text-align: center;
        height: 20px;
    }

    .liquidation-salary-total-title-contributions {
        text-align: center;
        font-weight: bold;
        text-transform: uppercase;
    }

    .liquidation-salary-period-title-contributions {
        background-color: #e6e7e9;
        border-right: 1px solid #1c95c8;
        padding: 2px;
        text-align: center;
        text-transform: none !important;
    }

    .liquidation-salary-bank-title-contributions {
        background-color: #e6e7e9;
        border-right: 1px solid #1c95c8;
        padding: 2px;
        text-transform: none !important;
    }

    .liquidation-salary-title-contributions {
        border-right: 1px solid #1c95c8;
    }

    .liquidation-salary-period-dmy-title-contributions {
        text-align: center;
        padding: 2px;
    }

    .liquidation-salary-period-bank-contributions {
        padding: 2px;
    }

    .liquidation-salary-art-contributions {
        text-align: center;
    }

    .liquidation-salary-period-value-contributions {
        text-align: center;
        padding: 1px;
    }

    .liquidation-salary-table-firm {
        margin-top: 10px;
        margin-left: 5px;
    }

    .liquidation-salary-firm {
        text-transform: none !important;
        margin-left: 3px;
        margin-top: 30px;
    }

    .liquidation-salary-payslips-concepts {
        position: relative;
        height: 235px;
        width: 500px;
        margin-left: 5px;
        text-transform: uppercase !important;
        font-size: 10px;
        font-weight: bold !important;
    }

    .liquidation-salary-qualification-contribution {
        text-transform: uppercase !important;
        font-size: 10px;
        font-weight: bold !important;
    }

    .liquidation-salary-total-amount-contributions {
        text-align: right;
        font-size: 15px;
        font-weight: bold;
    }

    .liquidation-salary-first-title-contributions {
        text-transform: none !important;
    }

    .liquidation-salary-consortium-data {
        text-transform: uppercase;
        font-weight: bold;
    }

    .liquidation-salary-concepts-title {
        text-align: center;
    }

    .with-skeleton .liquidation-salary-template {
        border: 1px solid #1c95c8;
        border-radius: 20px;
    }

    .with-skeleton .liquidation-salary-template-container {
        border-right: 1px dashed black;
    }

    .with-skeleton .liquidation-salary-consortium-last-cell-data, .with-skeleton .liquidation-salary-art-contributions, .with-skeleton .liquidation-salary-period-bank-contributions, .with-skeleton .liquidation-salary-period-value-contributions-border {
        border-right: 1px solid #1c95c8;
    }

    .with-skeleton .liquidation-salary-consortium-table, .with-skeleton .liquidation-salary-table-title-concepts, .with-skeleton .liquidation-salary-table-qualification, .with-skeleton .liquidation-salary-table-total-text {
        border-top: 2px solid #1c95c8;
    }

    .with-skeleton .liquidation-salary-qualification-total {
        border-right: 1px solid #1c95c8;
        border-left: 1px solid #1c95c8;
    }

    .with-skeleton .liquidation-salary-table-contributions {
        border-top: 2px solid #1c95c8;
        border-bottom: 2px solid #1c95c8;
    }

    .with-skeleton .liquidation-salary-art-title-contributions, .with-skeleton .liquidation-salary-total-title-contributions {
        border-left: 1px solid white;
        border-right: 1px solid white;
    }

    .with-skeleton .liquidation-salary-period-dmy-title-contributions {
        border-right: 1px solid #1c95c8;
        border-bottom: 1px solid #1c95c8;
    }

    .without-skeleton .invisibility, .without-skeleton .liquidation-salary-title, .without-skeleton .liquidation-salary-original-duplicate, .without-skeleton .liquidation-salary-consortium-cell-subtitle, .without-skeleton .liquidation-salary-consortium-last-cell-subtitle, .without-skeleton .liquidation-salary-consortium-cell-second-row-subtitle, .without-skeleton .liquidation-salary-consortium-last-cell-second-row-subtitle, .without-skeleton .liquidation-salary-title-concepts, .without-skeleton .liquidation-salary-qualification-title, .without-skeleton .liquidation-salary-qualification-total-title, .without-skeleton .liquidation-salary-total-text-title, .without-skeleton .liquidation-salary-first-title-contributions, .without-skeleton .liquidation-salary-art-title-contributions, .without-skeleton .liquidation-salary-total-title-contributions, .without-skeleton .liquidation-salary-period-title-contributions, .without-skeleton .liquidation-salary-bank-title-contributions, .without-skeleton .liquidation-salary-title-contributions, .without-skeleton .liquidation-salary-period-dmy-title-contributions, .without-skeleton .liquidation-salary-table-firm {
        visibility: hidden;
    }

    .without-skeleton .liquidation-salary-consortium-data-first {
        margin-top: -8px;
    }

    .without-skeleton .liquidation-salary-consortium-data-second {
        margin-top: 4px;
    }

    .without-skeleton .liquidation-salary-table-concepts {
        margin-top: 10px;
    }

    .without-skeleton .liquidation-salary-qualification-contribution {
        margin-top: 23px;
    }

    .without-skeleton .liquidation-salary-template-container-duplicate {
        margin-left: 6%;
    }

    .without-skeleton .liquidation-salary-template-container-duplicate .liquidation-salary-consortium-data-first-last-child {
        text-align: center !important;
        padding-right: 0px !important;
    }
</style>