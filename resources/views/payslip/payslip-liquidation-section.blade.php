<div class="liquidation-salary-template">
    <table width="100%" cellspacing="0" cellpadding="0" class="liquidation-salary-table">
        <tr>
            <td width="70%" class="liquidation-salary-title">{{ trans('employee.liquidation_salary_title') }}</td>
            <td width="30%" class="liquidation-salary-original-duplicate">{{ $orginalDuplicadoTexto }}</td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="liquidation-salary-consortium-info">
                    {{ $consortium->fancy_name }} <br>
                    DOMICILIO: {{ $consortium->getAddress() }}
                    - {{ 'CABA' }}<br>
                    CUIT.Nro: @if(!$consortium->profile->fideicomiso) {{ $consortium->cuit }} @else @endif &nbsp;&nbsp;&nbsp;Nro.Cuenta.SUTERH {{ $consortium->suterh_code }}
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0"
           class="liquidation-salary-table liquidation-salary-consortium-table">
        <tr>
            <td width="25%" class="liquidation-salary-consortium-cell-subtitle">
                <div class="liquidation-salary-common-data">{{ trans('employee.legajo') }}</div>
            </td>
            <td colspan="2" width="50%" class="liquidation-salary-consortium-cell-subtitle">
                <div class="liquidation-salary-common-data">{{ trans('employee.first_second_name') }}</div>
            </td>
            <td width="25%" class="liquidation-salary-consortium-last-cell-subtitle">
                <div class="liquidation-salary-common-data">{{ trans('employee.payed_period') }}</div>
            </td>
        </tr>
        <tr>
            <td class="liquidation-salary-consortium-last-cell-data">
                <div
                    class="liquidation-salary-common-data liquidation-salary-consortium-data liquidation-salary-consortium-data-first">@if($employee->number_docket != '') {{ $employee->number_docket }} @else
                        &nbsp; @endif</div>
            </td>
            <td class="liquidation-salary-consortium-last-cell-data" colspan="2">
                <div
                    class="liquidation-salary-common-data liquidation-salary-consortium-data liquidation-salary-consortium-data-first">{{ $employee->first_name. ' '.$employee->last_name }}</div>
            </td>
            <td>
                <div
                    class="liquidation-salary-common-data liquidation-salary-consortium-data liquidation-salary-consortium-data-first liquidation-salary-consortium-data-first-last-child">{{ $salary->getPeriod()->format('d-m-Y') }}
                    / {{ $salary->getPeriod()->lastOfMonth()->format('d-m-Y')  }}</div>
            </td>
        </tr>
        <tr>
            <td width="25%" class="liquidation-salary-consortium-cell-second-row-subtitle">
                <div class="liquidation-salary-common-data">{{ trans('employee.fecha_ingreso') }}</div>
            </td>
            <td width="25%" class="liquidation-salary-consortium-cell-second-row-subtitle">
                <div class="liquidation-salary-common-data">{{ trans('employee.liquidation_salary_cuil') }}</div>
            </td>
            <td width="25%" class="liquidation-salary-consortium-cell-second-row-subtitle">
                <div class="liquidation-salary-common-data">{{ trans('employee.liquidation_salary_dni') }}</div>
            </td>
            <td width="25%" class="liquidation-salary-consortium-last-cell-second-row-subtitle">
                <div class="liquidation-salary-common-data">{{ trans('employee.liquidation_salary_sueldo') }}</div>
            </td>
        </tr>
        <tr>
            <td class="liquidation-salary-consortium-last-cell-data">
                <div
                    class="liquidation-salary-common-data liquidation-salary-consortium-data liquidation-salary-consortium-data-second">{{ $employee->profile->entry_date->format('d-m-Y') }}</div>
            </td>
            <td class="liquidation-salary-consortium-last-cell-data">
                <div
                    class="liquidation-salary-common-data liquidation-salary-consortium-data liquidation-salary-consortium-data-second">{{ $employee->cuil }}</div>
            </td>
            <td class="liquidation-salary-consortium-last-cell-data">
                <div
                    class="liquidation-salary-common-data liquidation-salary-consortium-data liquidation-salary-consortium-data-second">{{ $employee->dni }}</div>
            </td>
            <td class="liquidation-salary-amount">
                <div
                    class="liquidation-salary-common-data liquidation-salary-consortium-data liquidation-salary-consortium-data-second">{{ $salary->getJornalSalary() }}</div>
            </td>
        </tr>
    </table>
    <table width="100%" cellspacing="0" cellpadding="0"
           class="liquidation-salary-table liquidation-salary-table-title-concepts">
        <tr class="liquidation-salary-row-title-concepts">
            <td width="40%" class="liquidation-salary-title-concepts">
                <div class="liquidation-salary-common-data">{{ trans('generic.detail') }}</div>
            </td>
            <td width="20%" class="liquidation-salary-title-concepts">
                <div
                    class="liquidation-salary-common-data liquidation-salary-concepts-title">{{ trans('generic.unit') }}</div>
            </td>
            <td width="20%" class="liquidation-salary-title-concepts">
                <div
                    class="liquidation-salary-common-data liquidation-salary-concepts-title">{{ trans('employee.adjustment_haberes') }}</div>
            </td>
            <td width="20%" class="invisibility">
                <div
                    class="liquidation-salary-common-data liquidation-salary-concepts-title">{{ trans('employee.adjustment_descuento') }}</div>
            </td>
        </tr>
    </table>
    <div class="liquidation-salary-payslips-concepts">
        @if ($watermark)
            <div class="liquidation-salary-water-mark"><img src="./panel_assets/images/marcaagua.png" height="100%"
                                                            width="96%"></div>
        @endif
        <table width="100%" cellspacing="0" cellpadding="0" class="liquidation-salary-table-concepts">
            @foreach($concepts as $concept)
                @if	( $concept->value == 0 )
                    @continue
                @endif
                <tr>
                    <td width="40%">
                        <div class="liquidation-salary-common-data">{{ $concept->description }}</div>
                    </td>
                    <td width="20%">
                        <div
                            class="liquidation-salary-amount">{{ (isset($concept->unit))? $concept->unit : ' '}}</div>
                    </td>
                    <td width="20%" class="liquidation-salary-amount">
                        <div class="liquidation-salary-common-data">
                            @if ($concept->type == 'haber')
                                {{ \App\Utils\CurrencyHelper::formatWithoutCurrency($concept->value) }}
                            @endif
                        </div>
                    </td>
                    <td width="20%" class="liquidation-salary-amount">
                        <div class="liquidation-salary-common-data">
                            @if ($concept->type == 'descuento')
                                {{ \App\Utils\CurrencyHelper::formatWithoutCurrency($concept->value) }}
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
    <div class="liquidation-salary-qualification-contribution">
        <table width="100%" cellspacing="0" cellpadding="0"
               class="liquidation-salary-table liquidation-salary-table-qualification">
            <tr>
                <td width="50%" class="liquidation-salary-qualification-title">
                    <div class="liquidation-salary-common-data">{{ trans('employee.professional_qualification') }}</div>
                </td>
                <td colspan="2" class="liquidation-salary-qualification-total-title">
                    {{ trans('generic.totals') }}
                </td>
            </tr>
            <tr>
                <td width="50%" class="liquidation-salary-qualification-profession">
                    {{ $professionalFunction->name }}
                    - {{ substr($consortium->category.'° Categoría', 0, 7) }}.
                </td>
                <td class="liquidation-salary-amount liquidation-salary-qualification-total" width="25%">
                    {{ $totalConceptsPages? $totalHaberes : '' }}
                </td>
                <td width="25%" class="liquidation-salary-amount">
                    {{ $totalConceptsPages? $totalDescuentos : '' }}
                </td>
            </tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0"
               class="liquidation-salary-table liquidation-salary-table-total-text">
            <tr>
                <td width="100%" class="liquidation-salary-total-text-title">
                    <div
                        class="liquidation-salary-common-data">{{ trans('employee.liquidation_salary_count_amount') }}</div>
                </td>
            </tr>
            <tr>
                <td class="liquidation-salary-total-text">
                    {{ $netoSalaryText }}
                </td>
            </tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0"
               class="liquidation-salary-table liquidation-salary-table-contributions">
            <tr class="liquidation-salary-row-title-contributions">
                <td colspan="6" class="liquidation-salary-first-title-contributions">
                    <div class="liquidation-salary-common-data">{{ trans('employee.title_contribution') }}</div>
                </td>
                <td class="liquidation-salary-art-title-contributions">
                    {{ trans('employee.liquidation_salary_art') }}
                </td>
                <td class="liquidation-salary-total-title-contributions">
                    {{ trans('employee.liquidation_salary_total') }}
                </td>
            </tr>
            <tr>
                <td colspan="2" class="liquidation-salary-period-title-contributions">
                    {{ trans('generic.period') }}
                </td>
                <td colspan="3" class="liquidation-salary-period-title-contributions">
                    {{ trans('generic.deposit') }}
                </td>
                <td class="liquidation-salary-bank-title-contributions">
                    {{ trans('generic.bank') }}
                </td>
                <td class="liquidation-salary-title-contributions">&nbsp;</td>
                <td class="invisibility">&nbsp;</td>
            </tr>
            <tr>
                <td class="liquidation-salary-period-dmy-title-contributions">
                    {{ trans('generic.month') }}
                </td>
                <td class="liquidation-salary-period-dmy-title-contributions">
                    {{ trans('generic.year') }}
                </td>
                <td class="liquidation-salary-period-dmy-title-contributions">
                    {{ trans('generic.day') }}
                </td>
                <td class="liquidation-salary-period-dmy-title-contributions">
                    {{ trans('generic.month') }}
                </td>
                <td class="liquidation-salary-period-dmy-title-contributions">
                    {{ trans('generic.year') }}
                </td>
                <td class="liquidation-salary-period-bank-contributions" width="20%" rowspan="2">
                    {{ $salary->getContributionsBank() }}
                </td>
                <td width="25%" rowspan="2" class="liquidation-salary-art-contributions">
                    {{ \App\Utils\CurrencyHelper::formatWithoutCurrency($salary->getArtPercentage()) }}%<br>
                    + &nbsp; {{ \App\Utils\CurrencyHelper::format($salary->getArtFijo()) }}
                </td>
                <td width="25%" rowspan="2" class="liquidation-salary-total-amount-contributions">
                    {{ \App\Utils\CurrencyHelper::formatWithoutCurrency($salary->getNetoSalary()) }}
                </td>
            </tr>
            <tr>
                <td class="liquidation-salary-period-value-contributions">
                    {{ $salary->getPeriod()->format('m') }}
                </td>
                <td class="liquidation-salary-period-value-contributions">
                    {{ $salary->getPeriod()->format('Y') }}
                </td>
                <td class="liquidation-salary-period-value-contributions">
                    {{ $salary->getContributionsPeriod()->format('d') }}
                </td>
                <td class="liquidation-salary-period-value-contributions">
                    {{ $salary->getContributionsPeriod()->format('m') }}
                </td>
                <td class="liquidation-salary-period-value-contributions">
                    {{ $salary->getContributionsPeriod()->format('Y') }}
                </td>
            </tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0" class="liquidation-salary-table-firm">
            <tr>
                <td colspan="2">
                    <div class="liquidation-salary-common-data">{!! trans('employee.liquidation_salary_firm_title') !!}</div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="liquidation-salary-firm">{!! trans('employee.liquidation_salary_pay_date') !!} _ _ _ _ _
                        _ _ _ _ _ _ _ _ _ _
                    </div>
                </td>
                <td>
                    <div class="liquidation-salary-firm">{{$signatureEmployee}} _ _ _ _ _ _ _ _ _ _ _ _ _ _ _</div>
                </td>
            </tr>
        </table>
    </div>
</div>
