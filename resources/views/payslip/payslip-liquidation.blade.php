@extends('pdf.default')

@section('styles')

    @include('payslip.payslip-css')

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
@stop

@section('content')
    <?php $i = 0;?>

    @foreach ($paginateConcepts as $concepts)
        <?php $i++ ?>
        <table class="liquidation-salary-container {{ $skeletonPrint ? 'with-skeleton' : 'without-skeleton' }}"
               width="100%" style="page-break-after: {{ ($totalConceptsPages == $i) ? 'avoid' : 'always' }};"
               cellpadding="0" cellspacing="0">
            <tr>
                <td width="50%">
                    <div class="liquidation-salary-template-container">
                        @include('payslip.payslip-liquidation-section', [
                                    'concepts' => $concepts['concepts'],
                                    'data' => $concepts['data'],
                                    'consortium' => $consortium,
                                    'employee' => $employee,
                                    'professionalFunction' => $professionalFunction,
                                    'salary' => $salary,
                                    'watermark' => $watermark,
                                    'totalConceptsPages' => $totalConceptsPages,
                                    'netoSalaryText' => $netoSalaryText,
                                    'i' => $i,
                                    'totalHaberes' => $totalHaberes,
                                    'totalDescuentos' => $totalDescuentos,
                                    'orginalDuplicadoTexto' => trans('generic.original'),
                                    'signatureEmployee' => trans('employee.liquidation_salary_employer_firm'),
                                    ])
                    </div>
                </td>
                <td width="50%">
                    <div class="liquidation-salary-template-container-duplicate">
                        @include('payslip.payslip-liquidation-section', [
                                    'concepts' => $concepts['concepts'],
                                    'data' => $concepts['data'],
                                    'consortium' => $consortium,
                                    'employee' => $employee,
                                    'professionalFunction' => $professionalFunction,
                                    'salary' => $salary,
                                    'watermark' => $watermark,
                                    'totalConceptsPages' => $totalConceptsPages,
                                    'netoSalaryText' => $netoSalaryText,
                                    'i' => $i,
                                    'totalHaberes' => $totalHaberes,
                                    'totalDescuentos' => $totalDescuentos,
                                    'orginalDuplicadoTexto' => trans('generic.duplicate'),
                                    'signatureEmployee' => trans('employee.liquidation_salary_employee_firm'),
                                    ])
                    </div>
                </td>
            </tr>
        </table>
    @endforeach
@stop
