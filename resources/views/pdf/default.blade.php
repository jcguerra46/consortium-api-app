<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    @yield('styles')
</head>
<body>
@yield('content')
</body>
</html>
