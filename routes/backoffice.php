<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Backoffice Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for Backoffice App.
|
*/

$router->get('backoffice', function () use ($router) {
    return 'Octopus Backoffice';
});
