<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Portal Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for Portal App.
|
*/

$router->get('portal', function () use ($router) {
    return 'Octopus Portal';
});
