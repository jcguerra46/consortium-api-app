<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Octopus API App v2.1';
});

/* Authentication Routes */
$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->post('/login', 'AuthController@login');
    $router->post('/forgotPassword', 'AuthController@sendResetLinkEmail');
    $router->post('/resetPassword', 'AuthController@resetPassword');
    $router->post('/logout', ['middleware' => 'auth', 'uses' => 'AuthController@logout']);
    $router->post('/renovate-token', ['middleware' => 'auth', 'uses' => 'AuthController@renovateToken']);
    $router->get('/user', ['middleware' => 'auth', 'uses' => 'AuthController@getAuthUser']);
});

$router->group(['middleware' => ['auth', 'roles:superadmin,admin']], function () use ($router) {

    /** Consortia Routes */
    $router->group(['prefix' => 'consortium'], function () use ($router) {
        $router->get('/', 'ConsortiumController@index');
        $router->get('/{id:[0-9]+}', 'ConsortiumController@show');
        $router->post('/', 'ConsortiumController@store');
        $router->put('/{id:[0-9]+}', 'ConsortiumController@update');
        $router->delete('/{id:[0-9]+}', 'ConsortiumController@destroy');
        $router->get('/export', 'ConsortiumController@export');

        /** Consortium Functional Units */
        $router->get('/{consortiumId:[0-9]+}/functional-units', 'ConsortiumController@getFunctionalUnits');

        /** Checks by consortium */
        $router->get('/{consortiumId:[0-9]+}/checks', 'ConsortiumController@getChecks');

        /** Cashbox  */
        $router->get('/{id:[0-9]+}/cashbox', 'ConsortiumController@getCashbox');

        /** Consortium Profiles */
        $router->group(['prefix' => '/{id:[0-9]+}/profile'], function () use ($router) {
            $router->get('/', 'ConsortiumProfileController@show');
            $router->put('/', 'ConsortiumProfileController@update');
        });

        /** Consortium Portal Profiles */
        $router->group(['prefix' => '/{id:[0-9]+}/portal/profile'], function () use ($router) {
            $router->get('/', 'ConsortiumPortalProfileController@show');
            $router->put('/', 'ConsortiumPortalProfileController@edit');
        });

        /** Consortium Percentages  */
        $router->group(['prefix' => '/{consortiumId:[0-9]+}/percentage'], function () use ($router) {
            $router->get('/', 'ConsortiumPercentageController@index');
            $router->post('/', 'ConsortiumPercentageController@store');
            $router->get('/{percentageId:[0-9]+}', 'ConsortiumPercentageController@show');
            $router->put('/{percentageId:[0-9]+}', 'ConsortiumPercentageController@update');
            $router->delete('/{percentageId:[0-9]+}', 'ConsortiumPercentageController@destroy');
            $router->get('/export', 'ConsortiumPercentageController@export');
            $router->post('/{percentageId:[0-9]+}/duplicate', 'ConsortiumPercentageController@duplicate');
            $router->post('/batch', 'ConsortiumPercentageController@massStore');

            /** Functional Units Percentages */
            $router->group(['prefix' => '/{percentageId:[0-9]+}/functional-units-percentages'], function () use ($router) {
                $router->get('/', 'ConsortiumPercentageController@getFunctionalUnitsPercentages');
                $router->put('/batch', 'ConsortiumPercentageController@massUpdateFunctionalUnitsPercentages');
            });
        });

        /** Consortium Periods  */
        $router->group(['prefix' => '/{consortiumId:[0-9]+}/period'], function () use ($router) {
            $router->get('/', 'PeriodController@index');
            $router->get('/{periodId:[0-9]+}', 'PeriodController@show');
            $router->put('/{periodId:[0-9]+}', 'PeriodController@update');
            $router->get('/export', 'PeriodController@export');
            $router->get('/open', 'PeriodController@getOpenPeriod');
        });

        /** Consortium Notes  */
        $router->group(['prefix' => '/{id:[0-9]+}/note'], function () use ($router) {
            $router->get('/', 'ConsortiumNoteController@index');
            $router->get('/{consortiumNoteId:[0-9]+}', 'ConsortiumNoteController@show');
            $router->post('/', 'ConsortiumNoteController@store');
            $router->put('/{consortiumNoteId:[0-9]+}', 'ConsortiumNoteController@update');
            $router->delete('/{consortiumNoteId:[0-9]+}', 'ConsortiumNoteController@destroy');
        });

        /** Consortium Current Account */
        $router->group(['prefix' => '/{consortiumId:[0-9]+}/movement'], function () use ($router) {
            $router->get('/', 'ConsortiumMovementController@index');
            $router->get('/{id:[0-9]+}', 'ConsortiumMovementController@show');
            $router->get('/export', 'ConsortiumMovementController@export');
            $router->get('/balance', 'ConsortiumMovementController@getBalance');
        });

        /** Consortium Bank Accounts  */
        $router->group(['prefix' => '/{consortiumId:[0-9]+}/bank-account'], function () use ($router) {
            $router->get('/', 'BankAccountController@index');
            $router->get('/{id:[0-9]+}', 'BankAccountController@show');
            $router->post('/', 'BankAccountController@store');
            $router->put('/{id:[0-9]+}', 'BankAccountController@update');
            $router->delete('/{id:[0-9]+}', 'BankAccountController@destroy');
            $router->get('/export', 'BankAccountController@export');

            /** Bank Account Movements */
            $router->group(['prefix' => '/{bankAccountId:[0-9]+}/movement'], function () use ($router) {
                $router->get('/', 'BankAccountMovementController@index');
                $router->get('/{id:[0-9]+}', 'BankAccountMovementController@show');
                $router->post('/', 'BankAccountMovementController@store');
                $router->put('/{id:[0-9]+}', 'BankAccountMovementController@update');
                $router->delete('/{id:[0-9]+}', 'BankAccountMovementController@destroy');
                $router->get('/export', 'BankAccountMovementController@export');
            });

            /** Bank Account Balance */
            $router->get('/{bankAccountId:[0-9]+}/balance', 'BankAccountMovementController@getBalance');
        });

        /** Consortium Virtual Cashboxes  */
        $router->group(['prefix' => '/{consortiumId:[0-9]+}/virtual-cashbox'], function () use ($router) {
            $router->get('/', 'VirtualCashboxController@index');
            $router->get('/{id:[0-9]+}', 'VirtualCashboxController@show');
            $router->post('/', 'VirtualCashboxController@store');
            $router->put('/{id:[0-9]+}', 'VirtualCashboxController@update');
            $router->delete('/{id:[0-9]+}', 'VirtualCashboxController@destroy');
            $router->get('/export', 'VirtualCashboxController@export');

            /** Consortium Virtual Cashboxes Movements */
            $router->group(['prefix' => '/{virtualCashboxId:[0-9]+}/movement'], function () use ($router) {
                $router->get('/', 'VirtualCashboxMovementController@index');
                $router->get('/{id:[0-9]+}', 'VirtualCashboxMovementController@show');
                $router->post('/initial-balance', 'VirtualCashboxMovementController@createInitialBalance');
                $router->post('/income', 'VirtualCashboxMovementController@createIncome');
                $router->post('/outflow', 'VirtualCashboxMovementController@createOutflow');
                $router->delete('/', 'VirtualCashboxMovementController@destroy');
                $router->get('/export', 'VirtualCashboxMovementController@export');
            });

            /** Consortium Virtual Cashboxes Balance */
            $router->get('/{id:[0-9]+}/balance', 'VirtualCashboxController@getBalance');
        });
    });

    /** Functional Units Routes */
    $router->group(['prefix' => 'functional-units', 'middleware' => 'functional-units'], function () use ($router) {
        $router->get('/', 'FunctionalUnitController@index');
        $router->get('/{id:[0-9]+}', 'FunctionalUnitController@show');
        $router->post('/', 'FunctionalUnitController@store');
        $router->post('/batch', 'FunctionalUnitController@storeBatchOfFunctionalUnits');
        $router->put('/{id:[0-9]+}', 'FunctionalUnitController@update');
        $router->delete('/{id:[0-9]+}', 'FunctionalUnitController@destroy');
        $router->get('/export', 'FunctionalUnitController@export');

        /** Functional Unit Contacts  */
        $router->group(['prefix' => '/{functional_unit_id:[0-9]+}/contact'], function () use ($router) {
            $router->get('/', 'FunctionalUnitContactController@index');
            $router->get('/{functionalUnitContactId:[0-9]+}', 'FunctionalUnitContactController@show');
            $router->post('/', 'FunctionalUnitContactController@store');
            $router->put('/{functionalUnitContactId:[0-9]+}', 'FunctionalUnitContactController@update');
            $router->delete('/{functionalUnitContactId:[0-9]+}', 'FunctionalUnitContactController@destroy');
        });

        /** Functional Unit Percentages  */
        $router->group(['prefix' => '/{functional_unit_id:[0-9]+}/percentage'], function () use ($router) {
            $router->get('/', 'FunctionalUnitPercentageController@index');
            $router->get('/{percentageFUId:[0-9]+}', 'FunctionalUnitPercentageController@show');
            $router->put('/{percentageFUId:[0-9]+}', 'FunctionalUnitPercentageController@update');
            $router->patch('/', 'FunctionalUnitPercentageController@massUpdate');
        });

        /** Functional Unit Current Account */
        $router->group(['prefix' => '/{functional_unit_id:[0-9]+}/movement'], function () use ($router) {
            $router->get('/', 'FunctionalUnitMovementController@index');
            $router->get('/{functionalUnitMovementId:[0-9]+}', 'FunctionalUnitMovementController@show');
            $router->get('/export', 'FunctionalUnitMovementController@export');
            $router->get('/balance', 'FunctionalUnitMovementController@getBalance');
            $router->post('/', 'FunctionalUnitMovementController@store');
            $router->get('/debts', 'FunctionalUnitMovementController@getDebts');
        });
    });

    /** Amenities Routes */
    $router->group(['prefix' => 'amenity'], function () use ($router) {
        $router->get('/', 'AmenityController@index');
        $router->get('/{id:[0-9]+}', 'AmenityController@show');
        $router->post('/', 'AmenityController@store');
        $router->put('/{id:[0-9]+}', 'AmenityController@update');
        $router->delete('/{id:[0-9]+}', 'AmenityController@destroy');
        $router->get('/export', 'AmenityController@export');
    });

    /** Assemblies Routes */
    $router->group(['prefix' => 'assembly'], function () use ($router) {
        $router->get('/', 'AssemblyController@index');
        $router->get('/{id:[0-9]+}', 'AssemblyController@show');
        $router->post('/', 'AssemblyController@store');
        $router->put('/{id:[0-9]+}', 'AssemblyController@update');
        $router->delete('/{id:[0-9]+}', 'AssemblyController@destroy');
        $router->get('/export', 'AssemblyController@export');
    });

    /** Judgments Routes */
    $router->group(['prefix' => 'judgment'], function () use ($router) {
        $router->get('/', 'JudgmentController@index');
        $router->get('/{id:[0-9]+}', 'JudgmentController@show');
        $router->post('/', 'JudgmentController@create');
        $router->put('/{id:[0-9]+}', 'JudgmentController@edit');
        $router->delete('/{id:[0-9]+}', 'JudgmentController@destroy');
        $router->get('/export', 'JudgmentController@export');
    });

    /** Providers Routes */
    $router->group(['prefix' => 'provider'], function () use ($router) {
        $router->get('/', 'ProviderController@index');
        $router->get('/{id:[0-9]+}', 'ProviderController@show');
        $router->post('/', 'ProviderController@store');
        $router->put('/{id:[0-9]+}', 'ProviderController@update');
        $router->delete('/{id:[0-9]+}', 'ProviderController@destroy');
        $router->get('/export', 'ProviderController@export');

        /* Provider Current Account */
        $router->group(['prefix' => '{providerId:[0-9]+}/movement'], function () use ($router) {
            $router->get('/', 'ProviderMovementController@index');
            $router->get('/{id:[0-9]+}', 'ProviderMovementController@show');
            $router->get('/export', 'ProviderMovementController@export');
            $router->get('/balance', 'ProviderMovementController@getBalance');
        });

        /** Provider Services */
        $router->get('/{id:[0-9]+}/services', 'ProviderController@getProviderServices');
    });

    /** Services Routes */
    $router->group(['prefix' => 'service'], function () use ($router) {
        $router->get('/', 'ServiceController@index');
        $router->get('/{id:[0-9]+}', 'ServiceController@show');
        $router->post('/', 'ServiceController@store');
        $router->put('/{id:[0-9]+}', 'ServiceController@update');
        $router->delete('/{id:[0-9]+}', 'ServiceController@destroy');
        $router->get('/export', 'ServiceController@export');
    });

    /** Contacts Routes */
    $router->group(['prefix' => 'contact'], function () use ($router) {
        $router->get('/', 'ContactController@index');
        $router->get('/{id:[0-9]+}', 'ContactController@show');
        $router->post('/', 'ContactController@create');
        $router->put('/{id:[0-9]+}', 'ContactController@edit');
        $router->delete('/{id:[0-9]+}', 'ContactController@destroy');
        $router->get('/export', 'ContactController@export');
    });

    /** Employees Routes */
    $router->group(['prefix' => 'employee'], function () use ($router) {
        $router->get('/', 'EmployeeController@index');
        $router->get('/{id:[0-9]+}', 'EmployeeController@show');
        $router->post('/', 'EmployeeController@store');
        $router->put('/{id:[0-9]+}', 'EmployeeController@update');
        $router->delete('/{id:[0-9]+}', 'EmployeeController@destroy');
        $router->get('/export', 'EmployeeController@export');

        /** Employee Profile  */
        $router->get('/{id:[0-9]+}/profile', 'EmployeeProfileController@show');
        $router->put('/{id:[0-9]+}/profile', 'EmployeeProfileController@update');

        /** Employee Data  */
        $router->get('/{id:[0-9]+}/data', 'EmployeeDataController@show');
        $router->put('/{id:[0-9]+}/data', 'EmployeeDataController@update');

        /** Employee AFIP */
        $router->get('/{id:[0-9]+}/afip', 'EmployeeAfipController@show');
        $router->put('/{id:[0-9]+}/afip', 'EmployeeAfipController@update');

        /** Employee Beneficiaries */
        $router->get('/{id:[0-9]+}/beneficiaries', 'EmployeeBeneficiaryController@index');
        $router->post('/{id:[0-9]+}/beneficiaries', 'EmployeeBeneficiaryController@store');
        $router->get('/{id:[0-9]+}/beneficiaries/{beneficiary_id:[0-9]+}', 'EmployeeBeneficiaryController@show');
        $router->put('/{id:[0-9]+}/beneficiaries/{beneficiary_id:[0-9]+}', 'EmployeeBeneficiaryController@update');
        $router->delete('/{id:[0-9]+}/beneficiaries/{beneficiary_id:[0-9]+}', 'EmployeeBeneficiaryController@destroy');

        /** Employee Adjustments */
        $router->group(['prefix' => '/{employeeId:[0-9]+}/adjustment'], function () use ($router) {
            $router->get('/', 'EmployeeAdjustmentController@index');
            $router->get('/{id:[0-9]+}', 'EmployeeAdjustmentController@show');
            $router->post('/', 'EmployeeAdjustmentController@store');
            $router->put('/{id:[0-9]+}', 'EmployeeAdjustmentController@update');
            $router->delete('/{id:[0-9]+}', 'EmployeeAdjustmentController@destroy');
            $router->get('/export', 'EmployeeAdjustmentController@export');
        });
    });

    /** Claims Routes */
    $router->group(['prefix' => 'claims'], function () use ($router) {
        $router->get('/', 'ClaimController@index');
        $router->post('/', 'ClaimController@store');
        $router->get('/{id:[0-9]+}', 'ClaimController@show');
        $router->put('/{id:[0-9]+}', 'ClaimController@update');
        $router->delete('/{id:[0-9]+}', 'ClaimController@destroy');
        $router->get('/export', 'ClaimController@export');

        /** Claims Comments Routes */
        $router->get('/{id:[0-9]+}/comments', 'ClaimCommentController@index');
        $router->post('/{id:[0-9]+}/comments', 'ClaimCommentController@store');
        $router->get('/{id:[0-9]+}/comments/{comment_id:[0-9]+}', 'ClaimCommentController@show');
        $router->put('/{id:[0-9]+}/comments/{comment_id:[0-9]+}', 'ClaimCommentController@update');
        $router->delete('/{id:[0-9]+}/comments/{comment_id:[0-9]+}', 'ClaimCommentController@destroy');
    });

    /** Cashboxes Routes */
    $router->group(['prefix' => 'cashbox'], function () use ($router) {
        $router->get('/', 'CashboxController@index');
        $router->get('/{id:[0-9]+}', 'CashboxController@show');
        $router->put('/{id:[0-9]+}', 'CashboxController@update');
        $router->get('/export', 'CashboxController@export');

        /** Cashbox Movements Routes */
        $router->get('/{cashboxId:[0-9]+}/movement', 'CashboxMovementController@index');
        $router->get('/{cashboxId:[0-9]+}/movement/{id:[0-9]+}', 'CashboxMovementController@show');
        $router->get('/{cashboxId:[0-9]+}/movement/export', 'CashboxMovementController@export');
        $router->post('/{cashboxId:[0-9]+}/initial-balance', 'CashboxMovementController@createInitialBalance');
        $router->post('/{cashboxId:[0-9]+}/income', 'CashboxMovementController@createIncome');
        $router->post('/{cashboxId:[0-9]+}/outflow', 'CashboxMovementController@createOutflow');
        $router->get('/{cashboxId:[0-9]+}/balances', 'CashboxMovementController@getBalances');
    });

    /** Categories Routes */
    $router->group(['prefix' => 'categories'], function () use ($router) {
        $router->get('/', 'CategoriesController@index');
        $router->post('/', 'CategoriesController@store');
        $router->get('/{id:[0-9]+}', 'CategoriesController@show');
        $router->put('/{id:[0-9]+}', 'CategoriesController@update');
        $router->delete('/{id:[0-9]+}', 'CategoriesController@destroy');
    });

    /** Notes Routes */
    $router->group(['prefix' => 'note'], function () use ($router) {
        $router->get('/', 'NoteController@index');
        $router->get('/{id:[0-9]+}', 'NoteController@show');
        $router->post('/', 'NoteController@store');
        $router->put('/{id:[0-9]+}', 'NoteController@update');
        $router->delete('/{id:[0-9]+}', 'NoteController@destroy');
        $router->get('/export', 'NoteController@export');
    });

    /** Spending Routes */
    $router->group(['prefix' => 'spending'], function () use ($router) {
        $router->get('/', 'SpendingController@index');
        $router->get('/{id:[0-9]+}', 'SpendingController@show');
        $router->post('/', 'SpendingController@store');
        // TODO
        $router->put('/{id:[0-9]+}', 'SpendingController@update');
        $router->delete('/{id:[0-9]+}', 'SpendingController@destroy');
        // END TODO
        $router->get('/export', 'SpendingController@export');
    });

    /** Spending Installments Routes */
    $router->group(['prefix' => 'spending-installment'], function () use ($router) {
        $router->get('/', 'SpendingInstallmentController@index');
        $router->get('/{id:[0-9]+}', 'SpendingInstallmentController@show');
        $router->get('/export', 'SpendingInstallmentController@export');
    });

    /** Spending description */
    $router->group(['prefix' => 'spending-description'], function () use ($router) {
        $router->get('/', 'SpendingDescriptionController@index');
        $router->get('/{id:[0-9]+}', 'SpendingDescriptionController@show');
        $router->post('/', 'SpendingDescriptionController@store');
        $router->put('/{id:[0-9]+}', 'SpendingDescriptionController@update');
        $router->delete('/{id:[0-9]+}', 'SpendingDescriptionController@destroy');
        $router->get('/export', 'SpendingDescriptionController@export');
    });

    /** Spending Recurrent */
    $router->group(['prefix' => 'spending-recurrent'], function () use ($router) {
        $router->get('/', 'SpendingRecurrentController@index');
        $router->get('/{id:[0-9]+}', 'SpendingRecurrentController@show');
        $router->post('/', 'SpendingRecurrentController@create');
        $router->put('/{id:[0-9]+}', 'SpendingRecurrentController@edit');
        $router->delete('/{id:[0-9]+}', 'SpendingRecurrentController@destroy');
        $router->get('/export', 'SpendingRecurrentController@export');
    });

    /** Invoice */
    $router->group(['prefix' => 'invoice'], function () use ($router) {
        $router->get('/', 'InvoiceController@index');
        $router->get('/{id:[0-9]+}', 'InvoiceController@show');
        $router->post('/', 'InvoiceController@store');
        $router->put('/{id:[0-9]+}', 'InvoiceController@update');
        $router->delete('/{id:[0-9]+}', 'InvoiceController@destroy');
        $router->get('/export', 'InvoiceController@export');
    });

    /** Payment Order */
    $router->group(['prefix' => 'payment-order'], function () use ($router) {
        $router->get('/', 'PaymentOrderController@index');
        $router->get('/{id:[0-9]+}', 'PaymentOrderController@show');
        $router->post('/', 'PaymentOrderController@store');
        $router->put('/{id:[0-9]+}', 'PaymentOrderController@update');
        $router->delete('/{id:[0-9]+}', 'PaymentOrderController@destroy');
        $router->get('/export', 'PaymentOrderController@export');
        $router->post('/{id:[0-9]+}/duplicate', 'PaymentOrderController@duplicate');
        $router->put('/{id:[0-9]+}/annular', 'PaymentOrderController@annular');
        $router->get('/{id:[0-9]+}/print', 'PaymentOrderController@printOrder');
        $router->post('/{id:[0-9]+}/do-payment', 'PaymentOrderController@doPayment');
    });

    /** User Administrations */
    $router->group(['prefix' => 'user-administrations'], function () use ($router) {
        $router->get('/', 'UserAdministrationController@index');
        $router->get('/{id:[0-9]+}', 'UserAdministrationController@show');
        $router->post('/', 'UserAdministrationController@store');
        $router->put('/{id:[0-9]+}', 'UserAdministrationController@update');
        $router->delete('/{id:[0-9]+}', 'UserAdministrationController@destroy');
        $router->get('/export', 'UserAdministrationController@export');
    });

    /** Menus */
    $router->group(['prefix' => 'menus'], function () use ($router) {
        $router->get('/', 'MenuController@show');
        $router->put('/', 'MenuController@setUserMenu');
        $router->get('/default', 'MenuController@getMenuDefault');
        $router->put('/default', 'MenuController@setMenuDefault');
    });

    /** Expense Headers */
    $router->group(['prefix' => 'expense-headers'], function () use ($router) {
        $router->get('/', 'ExpenseHeaderController@index');
        $router->post('/', 'ExpenseHeaderController@store');
        $router->get('/{id:[0-9]+}', 'ExpenseHeaderController@show');
        $router->put('/{id:[0-9]+}', 'ExpenseHeaderController@update');
        $router->delete('/{id:[0-9]+}', 'ExpenseHeaderController@destroy');
        $router->get('/export', 'ExpenseHeaderController@export');
    });

    /** Checkbook */
    $router->group(['prefix' => 'checkbook'], function () use ($router) {
        $router->get('/', 'CheckbookController@index');
        $router->get('/{id:[0-9]+}', 'CheckbookController@show');
        $router->post('/', 'CheckbookController@store');
        $router->put('/{id:[0-9]+}', 'CheckbookController@update');
        $router->delete('/{id:[0-9]+}', 'CheckbookController@destroy');
        $router->get('/export', 'CheckbookController@export');
    });

    /** Checks */
    $router->group(['prefix' => 'check'], function () use ($router) {
        $router->get('/', 'CheckController@index');
        $router->get('/{id:[0-9]+}', 'CheckController@show');
        $router->post('/', 'CheckController@store');
        $router->put('/{id:[0-9]+}', 'CheckController@update');
        $router->delete('/{id:[0-9]+}', 'CheckController@destroy');
        $router->get('/export', 'CheckController@export');
    });

    /** Banks Routes */
    $router->group(['prefix' => 'banks'], function () use ($router) {
        $router->get('/', 'BankController@index');
        $router->get('/{id:[0-9]+}', 'BankController@show');
    });

    /** Professional Functions Routes */
    $router->group(['prefix' => 'professional-functions'], function () use ($router) {
        $router->get('/', 'ProfessionalFunctionController@index');
        $router->get('/{id:[0-9]+}', 'ProfessionalFunctionController@show');
    });

    /** Salary Pluses */
    $router->group(['prefix' => 'salary-pluses'], function () use ($router) {
        $router->get('/', 'SalaryPlusController@index');
        $router->get('/{id:[0-9]+}', 'SalaryPlusController@show');
    });

    /** Afip Actividades Routes */
    $router->group(['prefix' => 'afip-actividades'], function () use ($router) {
        $router->get('/', 'AfipActivityController@index');
        $router->get('/{id:[0-9]+}', 'AfipActivityController@show');
    });

    /** Afip Condiciones Routes */
    $router->group(['prefix' => 'afip-condiciones'], function () use ($router) {
        $router->get('/', 'AfipConditionController@index');
        $router->get('/{id:[0-9]+}', 'AfipConditionController@show');
    });

    /** Afip Modos Contratación Routes */
    $router->group(['prefix' => 'afip-modos-contratacion'], function () use ($router) {
        $router->get('/', 'AfipModoContratacionController@index');
        $router->get('/{id:[0-9]+}', 'AfipModoContratacionController@show');
    });

    /** Afip Provincias Routes */
    $router->group(['prefix' => 'afip-provincias'], function () use ($router) {
        $router->get('/', 'AfipProvinciaController@index');
        $router->get('/{id:[0-9]+}', 'AfipProvinciaController@show');
    });

    /** Afip Siniestros Routes */
    $router->group(['prefix' => 'afip-siniestros'], function () use ($router) {
        $router->get('/', 'AfipSiniestroController@index');
        $router->get('/{id:[0-9]+}', 'AfipSiniestroController@show');
    });

    /** Afip Situaciones Routes */
    $router->group(['prefix' => 'afip-situaciones'], function () use ($router) {
        $router->get('/', 'AfipSituacionController@index');
        $router->get('/{id:[0-9]+}', 'AfipSituacionController@show');
    });

    /** Afip Zonas Routes */
    $router->group(['prefix' => 'afip-zonas'], function () use ($router) {
        $router->get('/', 'AfipZonaController@index');
        $router->get('/{id:[0-9]+}', 'AfipZonaController@show');
    });

    /** Collections Routes */
    $router->group(['prefix' => 'collection'], function () use ($router) {
        $router->get('/', 'CollectionController@index');
        $router->get('/{id:[0-9]+}', 'CollectionController@show');
        $router->post('/', 'CollectionController@store');
        $router->get('/export', 'CollectionController@export');
    });

    /** Payment Methods Routes */
    $router->group(['prefix' => 'payment-methods'], function () use ($router) {
        $router->get('/', 'PaymentMethodsController@index');
        $router->get('/{id:[0-9]+}', 'PaymentMethodsController@show');
    });

    /** Payments Routes */
    $router->group(['prefix' => 'payments'], function () use ($router) {
        $router->get('/', 'PaymentsController@index');
        $router->get('/{id:[0-9]+}', 'PaymentsController@show');
        $router->post('/', 'PaymentsController@store');
        $router->post('/direct', 'PaymentsController@createDirect');
        $router->get('/export', 'PaymentsController@export');
    });

    /** Agenda Events Routes */
    $router->group(['prefix' => 'agenda-events'], function () use ($router) {
        $router->get('/', 'AgendaEventController@index');
        $router->get('/{id:[0-9]+}', 'AgendaEventController@show');
        $router->post('/', 'AgendaEventController@store');
        $router->put('/{id:[0-9]+}', 'AgendaEventController@update');
        $router->delete('/{id:[0-9]+}', 'AgendaEventController@destroy');
        $router->get('/export', 'AgendaEventController@export');
    });

    /** Social Works */
    $router->group(['prefix' => 'social-works'], function () use ($router) {
        $router->get('/', 'SocialWorkController@index');
        $router->get('/{id:[0-9]+}', 'SocialWorkController@show');
    });

    /** Employee Status */
    $router->group(['prefix' => 'employee-status'], function () use ($router) {
        $router->get('/', 'EmployeeStatusController@index');
        $router->get('/{id:[0-9]+}', 'EmployeeStatusController@show');
    });

    /** Countries */
    $router->group(['prefix' => 'countries'], function () use ($router) {
        $router->get('/', 'CountryController@index');
        $router->get('/{id:[0-9]+}', 'CountryController@show');

        /** Provinces */
        $router->get('/{id:[0-9]+}/provinces', 'ProvinceController@index');
        $router->get('/{id:[0-9]+}/provinces/{province_id:[0-9]+}', 'ProvinceController@show');
    });

    /** Salaries Routes */
    $router->group(['prefix' => 'salaries'], function () use ($router) {
        $router->get('/', 'SalariesController@index');
        $router->get('/{id:[0-9]+}', 'SalariesController@show');

        /** Salary Monthly */
        $router->group(['prefix' => '/monthly'], function () use ($router) {
            $router->get('/', 'SalaryMonthlyController@calculate');
            $router->get('/payslip', 'SalaryMonthlyController@getPayslip');
            $router->post('/', 'SalaryMonthlyController@create');
        });

        /** Salary Sac */
        $router->group(['prefix' => '/sac'], function () use ($router) {
            $router->get('/', 'SalarySacController@calculate');
            $router->get('/payslip', 'SalarySacController@getPayslip');
            $router->post('/', 'SalarySacController@create');
        });

        /** Salary Intermediate */
        $router->group(['prefix' => '/intermediate'], function () use ($router) {
            $router->get('/', 'SalaryIntermediateController@calculate');
            $router->get('/payslip', 'SalaryIntermediateController@getPayslip');
            $router->post('/', 'SalaryIntermediateController@create');
        });

        /** Salary Vacations */
        $router->group(['prefix' => '/vacations'], function () use ($router) {
            $router->get('/', 'SalaryVacationsController@calculate');
            $router->get('/payslip', 'SalaryVacationsController@getPayslip');
            $router->post('/', 'SalaryVacationsController@create');
        });
    });

    /** Functional Unit Types Routes */
    $router->group(['prefix' => 'functional-unit-types'], function () use ($router) {
        $router->get('/', 'FunctionalUnitTypeController@index');
        $router->post('/', 'FunctionalUnitTypeController@store');
        $router->get('/{id:[0-9]+}', 'FunctionalUnitTypeController@show');
        $router->put('/{id:[0-9]+}', 'FunctionalUnitTypeController@update');
        $router->delete('/{id:[0-9]+}', 'FunctionalUnitTypeController@destroy');
        $router->get('/export', 'FunctionalUnitTypeController@export');
    });

    /** Roles Routes */
    $router->group(['prefix' => 'roles'], function () use ($router) {
        $router->get('/', 'RoleController@index');
        $router->get('/{id:[0-9]+}', 'RoleController@show');
        $router->post('/', 'RoleController@store');
        $router->put('/{id:[0-9]+}', 'RoleController@update');
        $router->delete('/{id:[0-9]+}', 'RoleController@destroy');
        $router->get('/export', 'RoleController@export');

        /**  Assign, update or delete permissions to role */
        $router->post('/{id:[0-9]+}/assign-permissions', 'RoleController@assignPermissionsToRole');
        $router->post('/{id:[0-9]+}/update-permissions', 'RoleController@updateRolePermissions');
        $router->post('/{id:[0-9]+}/delete-permissions', 'RoleController@deleteRolePermissions');
    });

    /** Permissions Routes */
    $router->group(['prefix' => 'permissions'], function () use ($router) {
        $router->get('/', 'PermissionController@index');
        $router->get('/{id:[0-9]+}', 'PermissionController@show');
//        $router->post('/', 'PermissionController@store');
//        $router->put('/{id:[0-9]+}', 'PermissionController@update');
//        $router->delete('/{id:[0-9]+}', 'PermissionController@destroy');
        $router->get('/export', 'PermissionController@export');
    });

});
