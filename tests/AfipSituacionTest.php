<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AfipSituacionTest extends TestCase
{
    const baseURL = 'afip-situaciones';

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'code',
                                'description'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'code',
                    'description'
                ]
            ]);
    }

}
