<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Auth;

class AgendaEventTest extends TestCase
{
    const baseURL = 'agenda-events';

    public function setUp(): void
    {
        parent::setUp();

        $this->consortium = factory('App\Models\Consortium')->create();
    }

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'administration_id',
                                'consortium_id',
                                'eventable',
                                'title',
                                'description',
                                'date',
                                'trans',
                                'service_class_name',
                                'route_class_name',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    /**
     * Create a claim.
     * @test
     */
    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'administration_id' => 1,
            'consortium_id' => $this->consortium->id,
            'eventable' => $faker->randomElement([1,2,3,4,5,6]),
            'title' => $faker->sentence(3, true),
            'description' => $faker->paragraph(3, true),
            'date' => $faker->date('Y-m-d'),
            'trans' => $faker->sentence(2, true),
            'service_class_name' => $faker->sentence(2, true),
            'route_class_name' => $faker->sentence(2, true)
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'consortium_id',
                    'eventable',
                    'title',
                    'description',
                    'date',
                    'trans',
                    'service_class_name',
                    'route_class_name',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Edit
     * @test
     */
    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'administration_id' => 1,
            'user_portal_id' => $this->consortium->id,
            'eventable' => $faker->randomElement([1,2,3,4,5,6]),
            'title' => $faker->sentence(3, true) . ' Updated',
            'description' => $faker->paragraph(3, true),
            'date' => $faker->date('Y-m-d'),
            'trans' => $faker->sentence(2, true),
            'service_class_name' => $faker->sentence(2, true),
            'route_class_name' => $faker->sentence(2, true)
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'consortium_id',
                    'eventable',
                    'title',
                    'description',
                    'date',
                    'trans',
                    'service_class_name',
                    'route_class_name',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'consortium_id',
                    'eventable',
                    'title',
                    'description',
                    'date',
                    'trans',
                    'service_class_name',
                    'route_class_name',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    /**
     * Export
     * @test
     */
    public function testExport()
    {
        $filename = 'agendaevents.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename='.$filename,
            $response->headers->get('content-disposition'));
    }
}
