<?php

class BankAccountTest extends TestCase
{
    const baseURL = 'consortium/1/bank-account';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'bank_id',
                                'account_number',
                                'cbu',
                                'alias',
                                'cuit',
                                'type',
                                'branch_office',
                                'owner',
                                'show_data_in_expense',
                                'email',
                                'signatorie_1',
                                'signatorie_2',
                                'signatorie_3',
                                'consortium_id',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'bank_id' => $faker->numberBetween(1, 77),
            'account_number' => (string)$faker->randomNumber(),
            'cbu' => (string)$faker->randomNumber(),
            'alias' => $faker->userName,
            'cuit' => '20-35226046-1',
            'type' => $faker->randomElement(['savings_box', 'current_account', 'single_account']),
            'branch_office' => $faker->city,
            'owner' => $faker->name,
            'show_data_in_expense' => $faker->boolean,
            'email' => $faker->email,
            'signatorie_1' => $faker->name,
            'signatorie_2' => $faker->name,
            'signatorie_3' => $faker->name,
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'bank_id',
                    'account_number',
                    'cbu',
                    'alias',
                    'cuit',
                    'type',
                    'branch_office',
                    'owner',
                    'show_data_in_expense',
                    'email',
                    'signatorie_1',
                    'signatorie_2',
                    'signatorie_3',
                    'consortium_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'bank_id' => $faker->numberBetween(1, 77),
            'account_number' => (string)$faker->randomNumber(),
            'cbu' => (string)$faker->randomNumber(),
            'alias' => $faker->userName,
            'cuit' => '20-35226046-1',
            'type' => $faker->randomElement(['savings_box', 'current_account', 'single_account']),
            'branch_office' => $faker->city,
            'owner' => $faker->name,
            'show_data_in_expense' => $faker->boolean,
            'email' => $faker->email,
            'signatorie_1' => $faker->name,
            'signatorie_2' => $faker->name,
            'signatorie_3' => $faker->name,
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'bank_id',
                    'account_number',
                    'cbu',
                    'alias',
                    'cuit',
                    'type',
                    'branch_office',
                    'owner',
                    'show_data_in_expense',
                    'email',
                    'signatorie_1',
                    'signatorie_2',
                    'signatorie_3',
                    'consortium_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'bank_id',
                    'account_number',
                    'cbu',
                    'alias',
                    'cuit',
                    'type',
                    'branch_office',
                    'owner',
                    'show_data_in_expense',
                    'email',
                    'signatorie_1',
                    'signatorie_2',
                    'signatorie_3',
                    'consortium_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'bank-accounts.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }
}
