<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    const baseURL = 'categories';

    public function setUp(): void
    {
        parent::setUp();

        $this->administration = factory('App\Models\Administration')->create();
        $this->category = factory('App\Models\Category')->create();
    }

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    '*' =>
                        [
                            'id',
                            'number',
                            'name',
                            'position',
                            'hidden',
                            'salaries',
                            'administration_id',
                            'created_at',
                            'updated_at',
                        ]
                ]
            ]);
    }

    /**
     * Create a claim.
     * @test
     */
    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'number' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8]),
            'name' => $faker->sentence(1, true),
            'position' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8]),
            'hidden' => true,
            'salaries' => true,
            'administration_id' => $this->administration->id
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'number',
                    'name',
                    'position',
                    'hidden',
                    'salaries',
                    'administration_id',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Edit
     * @test
     */
    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'number' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8]),
            'name' => $faker->sentence(1, true),
            'position' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8]),
            'hidden' => true,
            'salaries' => true,
            'administration_id' => $this->administration->id
        ];

        $response = $this->json('PUT',
            self::baseURL . '/' . $this->category->id,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'number',
                    'name',
                    'position',
                    'hidden',
                    'salaries',
                    'administration_id',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/' . $this->category->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'number',
                    'name',
                    'position',
                    'hidden',
                    'salaries',
                    'administration_id',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/' . $this->category->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }
}
