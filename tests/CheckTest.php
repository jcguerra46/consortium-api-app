<?php


class CheckTest extends TestCase
{
    const baseURL = 'check';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'number',
                                'deposit_date',
                                'issuance_date',
                                'amount',
                                'crossed',
                                'is_to_the_order',
                                'endorsements',
                                'own',
                                'bank_id',
                                'checkbook_id',
                                'consortium_id',
                                'administration_id',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'bank_id' => $faker->numberBetween(1, 77),
            'number' => $faker->randomNumber(),
            'deposit_date' => $faker->date('d-m-Y'),
            'issuance_date' => $faker->date('d-m-Y'),
            'amount' => $faker->randomNumber(),
            'crossed' => $faker->boolean,
            'is_to_the_order' => $faker->boolean,
            'endorsements' => $faker->boolean,
            'own' => $faker->boolean,
            'checkbook_id' => null,
            'consortium_id' => null,
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'number',
                    'deposit_date',
                    'issuance_date',
                    'amount',
                    'crossed',
                    'is_to_the_order',
                    'endorsements',
                    'own',
                    'bank_id',
                    'checkbook_id',
                    'consortium_id',
                    'administration_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'bank_id' => $faker->numberBetween(1, 77),
            'number' => $faker->randomNumber(),
            'deposit_date' => $faker->date('d-m-Y'),
            'issuance_date' => $faker->date('d-m-Y'),
            'amount' => $faker->randomNumber(),
            'crossed' => $faker->boolean,
            'is_to_the_order' => $faker->boolean,
            'endorsements' => $faker->boolean,
            'own' => $faker->boolean,
            'checkbook_id' => null,
            'consortium_id' => null,
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'number',
                    'deposit_date',
                    'issuance_date',
                    'amount',
                    'crossed',
                    'is_to_the_order',
                    'endorsements',
                    'own',
                    'bank_id',
                    'checkbook_id',
                    'consortium_id',
                    'administration_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'number',
                    'deposit_date',
                    'issuance_date',
                    'amount',
                    'crossed',
                    'is_to_the_order',
                    'endorsements',
                    'own',
                    'bank_id',
                    'checkbook_id',
                    'consortium_id',
                    'administration_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
    }

    public function testExport()
    {
        $filename = 'checks.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }
}
