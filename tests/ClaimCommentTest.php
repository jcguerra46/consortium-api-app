<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\UserPortal;

class ClaimCommentTest extends TestCase
{
    const baseURL = 'claims';

    public function setUp(): void
    {
        parent::setUp();

        $this->functional_unit = factory('App\Models\FunctionalUnit')->create();
        $this->user_portal = factory('App\Models\UserPortal')->create();
        $this->claim = factory('App\Models\Claim')->create([
            'functional_unit_id' => $this->functional_unit->id,
            'user_portal_id' => $this->user_portal->id
        ]);
    }

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL . '/' . $this->claim->id . '/comments', [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'user_portal_id',
                                'title',
                                'body',
                                'viewed',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    /**
     * Create a claim comment.
     * @test
     */
    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'user_portal_id' => UserPortal::all()->random()->id,
            'title' => $faker->sentence(6, true),
            'body' => $faker->paragraph(1, true),
            'viewed' => $faker->randomElement([true, false]),
        ];

        $response = $this->json('POST',
            self::baseURL . '/' . $this->claim->id . '/comments',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'user_portal_id',
                    'title',
                    'body',
                    'viewed',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    /**
     * Edit
     * @test
     */
    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'user_portal_id' => UserPortal::all()->random()->id,
            'title' => $faker->sentence(6, true),
            'body' => $faker->paragraph(1, true),
            'viewed' => $faker->randomElement([true, false]),
        ];

        $response = $this->json('PUT',
            self::baseURL . '/' . $this->claim->id . '/comments/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'user_portal_id',
                    'title',
                    'body',
                    'viewed',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/' . $this->claim->id . '/comments/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'user_portal_id',
                    'title',
                    'body',
                    'viewed',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/' . $this->claim->id . '/comments/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

}
