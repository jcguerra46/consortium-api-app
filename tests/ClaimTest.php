<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ClaimTest extends TestCase
{
    const baseURL = 'claims';

    public function setUp(): void
    {
        parent::setUp();

        $this->user_portal = factory('App\Models\UserPortal', 1)->create();
    }

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);


        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'functional_unit_id',
                                'user_portal_id',
                                'title',
                                'body',
                                'state',
                                'created_at',
                                'updated_at',
                                'deleted_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    /**
     * Create a claim.
     * @test
     */
    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'functional_unit_id' => App\Models\FunctionalUnit::all()->random()->id,
            'user_portal_id' => $this->user_portal->first()->id,
            'title' => $faker->sentence(6, true),
            'body' => $faker->paragraph(1, true),
            'state' => $faker->randomElement(['INICIADO', 'RESUELTO', 'EJECUCION', 'PROCESADO'])
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'functional_unit_id',
                    'user_portal_id',
                    'title',
                    'body',
                    'state',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Edit
     * @test
     */
    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'functional_unit_id' => App\Models\FunctionalUnit::all()->random()->id,
            'user_portal_id' => App\Models\UserPortal::all()->random()->id,
            'title' => $faker->sentence(6, true),
            'body' => $faker->paragraph(1, true),
            'state' => $faker->randomElement(['INICIADO', 'RESUELTO', 'EJECUCION', 'PROCESADO'])
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'functional_unit_id',
                    'user_portal_id',
                    'title',
                    'body',
                    'state',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'functional_unit_id',
                    'user_portal_id',
                    'title',
                    'body',
                    'state',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    /**
     * Export
     * @test
     */
    public function testExport()
    {
        $filename = 'claim.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename='.$filename,
            $response->headers->get('content-disposition'));
    }
}
