<?php


class ConsortiumNoteTest extends TestCase
{
    const baseURL = 'consortium';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL . '/1/note', [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'consortium_id',
                                'note_id',
                                'position',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'note_id' => $faker->numberBetween(1, 50),
            'position' => $faker->numberBetween(1, 100)
        ];

        $response = $this->json('POST',
            self::baseURL . '/1/note',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'note_id',
                    'position',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'note_id' => $faker->numberBetween(1, 50),
            'position' => $faker->numberBetween(1, 100)
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1/note/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'note_id',
                    'position',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1/note/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'note_id',
                    'position',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1/note/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }
}
