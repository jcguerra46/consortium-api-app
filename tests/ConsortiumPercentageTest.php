<?php

class ConsortiumPercentageTest extends TestCase
{
    const baseURL = 'consortium';

    public function setUp(): void
    {
        parent::setUp();

        // Create a new consortium
        $this->consortium = factory(\App\Models\Consortium::class)->create([ 'administration_id' => 1 ]);

        // Assign the new consortium to a user
        App\Models\ConsortiumUserAdministration::insert([
            'consortium_id' => $this->consortium->id,
            'user_administration_id' => 1
        ]);

        // Create percentage for the consortium
        $this->percentageConsortium = factory(\App\Models\PercentageConsortium::class)
            ->create([ 'consortium_id' => $this->consortium->id ]);

        // Create functional units for the consortium
        factory(\App\Models\FunctionalUnit::class, 10)->create([ 'consortium_id' => $this->consortium->id ]);

        // Create percentage for functional units of the consortium
        $this->storeFunctionalUnitPercentages($this->consortium->id, $this->percentageConsortium->id, $equalParts = false);
    }

    public function testStore()
    {
        $dataBody = $this->fakerGenerator();

        $response = $this->json('POST',
            self::baseURL . '/'.$this->consortium->id.'/percentage',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'name',
                    'second_line_name',
                    'description',
                    'type',
                    'position',
                    'last_fixed_amount',
                    'particular_percentage',
                    'hidden_expense_spending',
                    'hidden_in_proration',
                    'hidden_percentage_value',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testUpdate()
    {
        $dataBody = $this->fakerGenerator();

        $response = $this->json('PUT',
            self::baseURL . '/'.$this->consortium->id.'/percentage/'.$this->percentageConsortium->id,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'name',
                    'second_line_name',
                    'description',
                    'type',
                    'position',
                    'last_fixed_amount',
                    'particular_percentage',
                    'hidden_expense_spending',
                    'hidden_in_proration',
                    'hidden_percentage_value',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/'.$this->consortium->id.'/percentage/'.$this->percentageConsortium->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'name',
                    'second_line_name',
                    'description',
                    'type',
                    'position',
                    'last_fixed_amount',
                    'particular_percentage',
                    'hidden_expense_spending',
                    'hidden_in_proration',
                    'hidden_percentage_value',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL . '/'.$this->consortium->id.'/percentage', [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'consortium_id',
                                'name',
                                'second_line_name',
                                'description',
                                'type',
                                'position',
                                'last_fixed_amount',
                                'particular_percentage',
                                'hidden_expense_spending',
                                'hidden_in_proration',
                                'hidden_percentage_value',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'consortium-percentages.xlsx';
        $response = $this->json('GET', self::baseURL . '/'.$this->consortium->id.'/percentage/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }

    public function testDuplicate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'name' => $faker->text,
        ];

        $response = $this->json('POST',
            self::baseURL . '/'.$this->consortium->id.'/percentage/'.$this->percentageConsortium->id.'/duplicate',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'name',
                    'second_line_name',
                    'description',
                    'type',
                    'position',
                    'last_fixed_amount',
                    'particular_percentage',
                    'hidden_expense_spending',
                    'hidden_in_proration',
                    'hidden_percentage_value',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testMassStore()
    {
        $percentages = [];
        for($i = 0; $i < 6; $i++){
            $percentages[] = $this->fakerGenerator();
        }

        $dataBody = [
            'consortiumID' => 1,
            'percentages' => $percentages
        ];

        $response = $this->json('POST',
            self::baseURL . '/'.$this->consortium->id.'/percentage/batch',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' => [
                '*' => [
                    'id',
                    'consortium_id',
                    'name',
                    'second_line_name',
                    'description',
                    'type',
                    'position',
                    'last_fixed_amount',
                    'particular_percentage',
                    'hidden_expense_spending',
                    'hidden_in_proration',
                    'hidden_percentage_value',
                    'created_at',
                    'updated_at',
                ]
            ]
            ]);
    }

    public function testGetFunctionalUnitsPercentages()
    {
        $response = $this->json('GET',
            self::baseURL . '/'.$this->consortium->id.'/percentage/'.$this->percentageConsortium->id.'/functional-units-percentages',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    '*' => [
                        'id',
                        'functional_unit_id',
                        'percentage_consortium_id',
                        'value',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/'.$this->consortium->id.'/percentage/'.$this->percentageConsortium->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function storeFunctionalUnitPercentages($consortium_id, $percentage_consortium_id, $equalParts = null)
    {
        $functionalUnits = App\Models\FunctionalUnit::where('consortium_id', $consortium_id)->get();
        $countFunctionalUnits = $functionalUnits->count();

        $percentagesFunctionalUnits = [];
        if ($equalParts) {
            foreach ($functionalUnits as $fu) {
                $percentagesFunctionalUnits[] = [
                    'functional_unit_id' => $fu->id,
                    'percentage_consortium_id' => $percentage_consortium_id,
                    'value' => round(100 / $countFunctionalUnits, 4),
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ];
            }
        } else {
            foreach ($functionalUnits as $fu) {
                $percentagesFunctionalUnits[] = [
                    'functional_unit_id' => $fu->id,
                    'percentage_consortium_id' => $percentage_consortium_id,
                    'value' => 0,
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now()
                ];
            }
        }

        App\Models\PercentageFunctionalUnit::insert($percentagesFunctionalUnits);
    }

    public function fakerGenerator()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        return [
            'name' => $faker->text,
            'second_line_name' => $faker->text,
            'description' => $faker->realText(),
            'type' => $faker->randomElement(['spendings', 'fixed', 'forced_fixed']),
            'position' => $faker->numberBetween(1, 100),
            'last_fixed_amount' => $faker->randomNumber(),
            'particular_percentage' => $faker->boolean,
            'hidden_expense_spending' => $faker->boolean,
            'hidden_in_proration' => $faker->boolean,
            'hidden_percentage_value' => $faker->boolean,
            'equal_parts' => $faker->boolean,
        ];
    }

}
