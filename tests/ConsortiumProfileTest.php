<?php


class ConsortiumProfileTest extends TestCase
{
    const baseURL = 'consortium';

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'bank_account_id' => '',
            'escrow' => '',
            'license' => '',
            'liquidation' => '',
            'rounding_type' => '',
            'first_due_date' => '',
            'second_due_date' => '',
            'second_due_date_interests' => '',
            'penalty_interests' => '',
            'penalty_interests_mode' => '',
            'rounding_payslips' => '',
            'garage_amount' => '',
            'art_amount' => '',
            'art_percentage' => '',
            'collect_interest_outside_due_date' => '',
            'independent_cashbox' => '',
            'notes' => '',
            'next_payment_receipt_number' => '',
            'payslips_details' => '',
            'next_moving_sheet_number' => '',
            'private_spendings_title' => '',
            'self_managment' => '',
            'early_payment' => '',
            'early_payment_discount_days' => '',
            'early_payment_discount' => '',
            'show_bank_movements' => '',
            'show_claims' => '',
            'show_patrimonial_status' => '',
            'show_provider_data' => '',
            'legends_my_expenses' => '',
            'show_judgments_section' => '',
            'auto_billing' => '',
            'package_id' => '',
            'expense_header_id' => '',
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1/profile',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'bank_account_id',
                    'escrow',
                    'license',
                    'liquidation',
                    'rounding_type',
                    'first_due_date',
                    'second_due_date',
                    'second_due_date_interests',
                    'penalty_interests',
                    'penalty_interests_mode',
                    'rounding_payslips',
                    'garage_amount',
                    'art_amount',
                    'art_percentage',
                    'collect_interest_outside_due_date',
                    'independent_cashbox',
                    'notes',
                    'next_payment_receipt_number',
                    'payslips_details',
                    'next_moving_sheet_number',
                    'private_spendings_title',
                    'self_managment',
                    'early_payment',
                    'early_payment_discount_days',
                    'early_payment_discount',
                    'show_bank_movements',
                    'show_claims',
                    'show_patrimonial_status',
                    'show_provider_data',
                    'legends_my_expenses',
                    'show_judgments_section',
                    'auto_billing',
                    'package_id',
                    'expense_header_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1/profile',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'bank_account_id',
                    'escrow',
                    'license',
                    'liquidation',
                    'rounding_type',
                    'first_due_date',
                    'second_due_date',
                    'second_due_date_interests',
                    'penalty_interests',
                    'penalty_interests_mode',
                    'rounding_payslips',
                    'garage_amount',
                    'art_amount',
                    'art_percentage',
                    'collect_interest_outside_due_date',
                    'independent_cashbox',
                    'notes',
                    'next_payment_receipt_number',
                    'payslips_details',
                    'next_moving_sheet_number',
                    'private_spendings_title',
                    'self_managment',
                    'early_payment',
                    'early_payment_discount_days',
                    'early_payment_discount',
                    'show_bank_movements',
                    'show_claims',
                    'show_patrimonial_status',
                    'show_provider_data',
                    'legends_my_expenses',
                    'show_judgments_section',
                    'auto_billing',
                    'package_id',
                    'expense_header_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

}
