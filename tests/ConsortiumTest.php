<?php


class ConsortiumTest extends TestCase
{
    const baseURL = 'consortium';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'administration_id',
                                'code',
                                'fancy_name',
                                'business_name',
                                'address_number',
                                'address_street',
                                'province_id',
                                'postal_code',
                                'cuit',
                                'start_date',
                                'payment_method_id',
                                'suterh_code',
                                'siro_code',
                                'pmc_code',
                                'pme_code',
                                'ep_code',
                                'siro_verification_number',
                                'count_floors',
                                'count_functional_units',
                                'type_of_building',
                                'state',
                                'category',
                                'created_at',
                                'updated_at',
                                'deleted_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testStore()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'administration_id' => 1,
            'code' => (string)$faker->randomNumber(),
            'order' => $faker->numerify('####'),
            'fancy_name' => $faker->name,
            'business_name' => $faker->streetName,
            'address_number' => $faker->numerify('####'),
            'address_street' => $faker->streetName,
            'country_id' => 1,
            'province_id' => 1,
            'postal_code' => $faker->numerify('####'),
            'cuit' => '20-35226046-1',
            'start_date' => \Carbon\Carbon::now()->format('d-m-Y'),
            'payment_method_id' => 1,
            'suterh_code' => $faker->numerify('####'),
            'siro_code' => (string)$faker->numerify('####'),
            'pmc_code' => $faker->numerify('####'),
            'pme_code' => $faker->numerify('####'),
            'ep_code' => $faker->numerify('####'),
            'siro_verification_number' => $faker->numerify('####'),
            'count_floors' => $faker->numberBetween(1, 25),
            'count_functional_units' => $faker->numberBetween(1, 1000),
            'type_of_building' => 'H',
            'state' => $faker->randomElement(['PENDIENTE', 'INACTIVO', 'ACTIVO', 'BORRADO']),
            'category' => $faker->numberBetween(1, 4),
            'first_due_date' => 15
        ];
        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'order',
                    'code',
                    'fancy_name',
                    'business_name',
                    'address_number',
                    'address_street',
                    'country_id',
                    'province_id',
                    'postal_code',
                    'cuit',
                    'start_date',
                    'payment_method_id',
                    'suterh_code',
                    'siro_code',
                    'pmc_code',
                    'pme_code',
                    'ep_code',
                    'siro_verification_number',
                    'count_floors',
                    'count_functional_units',
                    'type_of_building',
                    'state',
                    'category',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    public function testUpdate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'administration_id' => 1,
            'code' => (string)$faker->randomNumber(),
            'order' => $faker->numerify('####'),
            'fancy_name' => $faker->name,
            'business_name' => $faker->streetName,
            'address_number' => $faker->numerify('####'),
            'address_street' => $faker->streetName,
            'country_id' => 1,
            'province_id' => 1,
            'postal_code' => $faker->numerify('####'),
            'cuit' => '20-35226046-1',
            'start_date' => \Carbon\Carbon::now()->format('d-m-Y'),
            'payment_method_id' => 1,
            'suterh_code' => $faker->numerify('####'),
            'siro_code' => (string)$faker->numerify('####'),
            'pmc_code' => $faker->numerify('####'),
            'pme_code' => $faker->numerify('####'),
            'ep_code' => $faker->numerify('####'),
            'siro_verification_number' => $faker->numerify('####'),
            'count_floors' => $faker->numberBetween(1, 25),
            'count_functional_units' => $faker->numberBetween(1, 1000),
            'type_of_building' => 'H',
            'state' => $faker->randomElement(['PENDIENTE', 'INACTIVO', 'ACTIVO', 'BORRADO']),
            'category' => $faker->numberBetween(1, 4),
            'first_due_date' => 15
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'order',
                    'code',
                    'fancy_name',
                    'business_name',
                    'address_number',
                    'address_street',
                    'country_id',
                    'province_id',
                    'postal_code',
                    'cuit',
                    'start_date',
                    'payment_method_id',
                    'suterh_code',
                    'siro_code',
                    'pmc_code',
                    'pme_code',
                    'ep_code',
                    'siro_verification_number',
                    'count_floors',
                    'count_functional_units',
                    'type_of_building',
                    'state',
                    'category',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'code',
                    'fancy_name',
                    'business_name',
                    'address_number',
                    'address_street',
                    'province_id',
                    'postal_code',
                    'cuit',
                    'start_date',
                    'payment_method_id',
                    'suterh_code',
                    'siro_code',
                    'pmc_code',
                    'pme_code',
                    'ep_code',
                    'siro_verification_number',
                    'count_floors',
                    'count_functional_units',
                    'type_of_building',
                    'state',
                    'category',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'consortia.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }

    public function testGetFunctionalUnitsOfConsortium()
    {
        $response = $this->json('GET',
            self::baseURL . '/4/functional-units',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                        [
                            'id',
                            'consortium_id',
                            'package_id',
                            'functional_unit_type_id',
                            'owner_first_name',
                            'owner_last_name',
                            'floor',
                            'department',
                            'functional_unit_number',
                            'initial_balance',
                            'type_forgive_interest',
                            'legal_state',
                            'm2',
                            'order',
                            'created_at',
                        ]
                    ]
                ]
            ]);
    }

    public function testGetCashboxOfConsortium()
    {
        $response = $this->json('GET',
            self::baseURL . '/4/cashbox',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'name',
                    'consortium_id',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    public function testGetChecksOfConsortium()
    {
        $response = $this->json('GET',
            self::baseURL . '/4/checks',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [ '*' =>
                    [
                        'id',
                        'number',
                        'deposit_date',
                        'issuance_date',
                        'due_date',
                        'amount',
                        'crossed',
                        'is_to_the_order',
                        'endorsements',
                        'own',
                        'bank_id',
                        'cashbox_movement_income_id',
                        'cashbox_movement_outflow_id',
                        'checkbook_id',
                        'consortium_id',
                        'administration_id',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]);
    }
}
