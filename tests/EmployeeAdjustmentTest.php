<?php

class EmployeeAdjustmentTest extends TestCase
{
    const base1SegmentURL = 'employee';
    const base2SegmentURL = 'adjustment';

    private $employee;
    private $employeeAdjustment;

    public function setUp(): void
    {
        parent::setUp();
        $this->employee = factory('App\Models\Employee', 1)->create()->first();
        $this->employeeAdjustment = factory('App\Models\EmployeeAdjustment', 1)->create()->first();
    }

    public function testIndex()
    {
        $response = $this->json('GET', self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'employee_id',
                                'type',
                                'haber_descuento',
                                'description',
                                'value',
                                'used',
                                'suma_sueldo_jornal',
                                'recurrent',
                                'es_remunerativo',
                                'unit',
                                'percentage',
                                'porcentage_sobre_base',
                                'active',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'type' => $faker->randomElement(['value', 'percentage']),
            'haber_descuento' => $faker->randomElement(['haber', 'descuento']),
            'description' => $faker->text,
            'value' => $faker->numberBetween(0, 5000),
            'used' => $faker->boolean,
            'suma_sueldo_jornal' => $faker->boolean,
            'recurrent' => $faker->boolean,
            'es_remunerativo' => $faker->boolean,
            'unit' => $faker->randomElement(['a', 'b']),
            'percentage' => $faker->numberBetween(0, 100),
            'percentage_sobre_base' => $faker->randomElement(['basic', 'jornal', 'total_neto', 'total_bruto', null]),
            'active' => $faker->boolean
        ];

        $response = $this->json('POST',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL,
            $dataBody, ['Authorization' => 'Bearer ' . static::$token]);

        $response->assertResponseStatus(201);
        $response->seeJson(['status' => 'success']);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'type',
                    'haber_descuento',
                    'description',
                    'value',
                    'used',
                    'suma_sueldo_jornal',
                    'recurrent',
                    'es_remunerativo',
                    'unit',
                    'percentage',
                    'percentage_sobre_base',
                    'active',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public
    function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'type' => $faker->randomElement(['value', 'percentage']),
            'haber_descuento' => $faker->randomElement(['haber', 'descuento']),
            'description' => $faker->text,
            'value' => $faker->numberBetween(0, 5000),
            'used' => $faker->boolean,
            'suma_sueldo_jornal' => $faker->boolean,
            'recurrent' => $faker->boolean,
            'es_remunerativo' => $faker->boolean,
            'unit' => $faker->randomElement(['a', 'b']),
            'percentage' => $faker->numberBetween(0, 100),
            'percentage_sobre_base' => $faker->randomElement(['basic', 'jornal', 'total_neto', 'total_bruto', null]),
            'active' => $faker->boolean
        ];

        $response = $this->json('PUT',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/51',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'type',
                    'haber_descuento',
                    'description',
                    'value',
                    'used',
                    'suma_sueldo_jornal',
                    'recurrent',
                    'es_remunerativo',
                    'unit',
                    'percentage',
                    'percentage_sobre_base',
                    'active',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public
    function testShow()
    {
        $response = $this->json('GET',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/' . $this->employeeAdjustment->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'type',
                    'haber_descuento',
                    'description',
                    'value',
                    'used',
                    'suma_sueldo_jornal',
                    'recurrent',
                    'es_remunerativo',
                    'unit',
                    'percentage',
                    'percentage_sobre_base',
                    'active',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public
    function testDestroy()
    {
        $response = $this->json('DELETE',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/' . $this->employeeAdjustment->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public
    function testExport()
    {
        $filename = 'employee-adjustments.xlsx';
        $response = $this->json('GET', self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }
}
