<?php


class EmployeeAfipTest extends TestCase
{
    const baseURL = 'employee';

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'conyuge' => $faker->boolean,
            'seguro_colectivo_vida' => $faker->boolean,
            'trabajador_convencionado' => $faker->boolean,
            'adicional_obra_sociales' => $faker->boolean,
            'maternidad' => $faker->boolean,
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1' . '/afip',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'conyuge',
                    'seguro_colectivo_vida',
                    'trabajador_convencionado',
                    'adicional_obra_sociales',
                    'maternidad',
                    'situacion',
                    'condicion',
                    'actividad',
                    'zona',
                    'adicional_seguridad_social',
                    'adicionales',
                    'cantidad_hijos',
                    'cantidad_adherentes',
                    'modalidad_contratacion',
                    'provincia_localidad',
                    'siniestrado',
                    'importe_adicional_obra_social',
                    'revista1',
                    'revista2',
                    'revista3',
                    'dia_revista1',
                    'dia_revista2',
                    'dia_revista3',
                    'premios',
                    'conceptos_no_remunerativos',
                    'rectificacion_remuneracion',
                    'contribucion_tarea_diferencial',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1' . '/afip',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'conyuge',
                    'seguro_colectivo_vida',
                    'trabajador_convencionado',
                    'adicional_obra_sociales',
                    'maternidad',
                    'situacion',
                    'condicion',
                    'actividad',
                    'zona',
                    'adicional_seguridad_social',
                    'adicionales',
                    'cantidad_hijos',
                    'cantidad_adherentes',
                    'modalidad_contratacion',
                    'provincia_localidad',
                    'siniestrado',
                    'importe_adicional_obra_social',
                    'revista1',
                    'revista2',
                    'revista3',
                    'dia_revista1',
                    'dia_revista2',
                    'dia_revista3',
                    'premios',
                    'conceptos_no_remunerativos',
                    'rectificacion_remuneracion',
                    'contribucion_tarea_diferencial',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

}
