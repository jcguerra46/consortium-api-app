<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class EmployeeBeneficiaryTest extends TestCase
{
    const base1SegmentURL = 'employee';
    const base2SegmentURL = 'beneficiaries';

    public function setUp(): void
    {
        parent::setUp();

        $this->employee = factory('App\Models\Employee')->create();
        $this->employeeBeneficiary = factory('App\Models\EmployeeBeneficiary')->create([
            'employee_id' => $this->employee->id,
            'cuil' => '20-95660815-6',
            'first_name' => 'Juan Carlos',
            'last_name' => 'Guerra',
            'document_type' => 'dni',
            'dni' => '95660815',
            'relationship' => 'Brother',
            'porcentage' => 75
        ]);
    }

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [
                    '*' =>
                        [
                            'id',
                            'employee_id',
                            'cuil',
                            'first_name',
                            'last_name',
                            'document_type',
                            'dni',
                            'relationship',
                            'porcentage',
                            'created_at',
                            'updated_at'
                        ]
                ]
            ]);
    }

    /**
     * Create a claim comment.
     * @test
     */
    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'employee_id' => \App\Models\Employee::all()->random()->id,
            'cuil' => '20-95660815-6',
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'document_type' => $faker->randomElement(['le', 'lc', 'dni']),
            'dni' => '95660815',
            'relationship' => $faker->sentence(1, true),
            'porcentage' => $faker->randomFloat(3, 0, 90)
        ];

        $response = $this->json('POST',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'cuil',
                    'first_name',
                    'last_name',
                    'document_type',
                    'dni',
                    'relationship',
                    'porcentage',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    /**
     * Edit
     * @test
     */
    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'employee_id' => \App\Models\Employee::all()->random()->id,
            'cuil' => '20-95660815-6',
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'document_type' => $faker->randomElement(['le', 'lc', 'dni']),
            'dni' => '95660815',
            'relationship' => $faker->sentence(1, true),
            'porcentage' => $faker->randomFloat(3, 0, 90)
        ];

        $response = $this->json('PUT',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/51',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'cuil',
                    'first_name',
                    'last_name',
                    'document_type',
                    'dni',
                    'relationship',
                    'porcentage',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/' . $this->employeeBeneficiary->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);
        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/' . $this->employeeBeneficiary->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    /**
     * Paginate
     * @test
     */
    public function testPaginate()
    {
        $response = $this->json('GET',
            self::base1SegmentURL . '/' . $this->employee->id . '/' . self::base2SegmentURL . '/paginate',
            [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'employee_id',
                                'cuil',
                                'first_name',
                                'last_name',
                                'document_type',
                                'dni',
                                'relationship',
                                'porcentage',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }
}
