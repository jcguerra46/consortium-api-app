<?php


class EmployeeDataTest extends TestCase
{
    const baseURL = 'employee';

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'sex' => $faker->randomElement(['M', 'F']),
            'civil_status_code' => $faker->numberBetween(1, 1000),
            'address_street' => $faker->streetName,
            'address_number' => $faker->randomNumber(),
            'address_floor' => $faker->randomLetter,
            'address_departament' => $faker->randomLetter,
            'postal_code' => (int)$faker->postcode,
            'phone' => $faker->phoneNumber,
            'email' => $faker->email,
            'nationality_code' => $faker->numberBetween(1, 1000),
            'nationality' => $faker->country,
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1' . '/data',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'sex',
                    'civil_status_code',
                    'address_street',
                    'address_number',
                    'address_floor',
                    'address_departament',
                    'postal_code',
                    'phone',
                    'email',
                    'nationality_code',
                    'nationality',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1' . '/data',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'sex',
                    'civil_status_code',
                    'address_street',
                    'address_number',
                    'address_floor',
                    'address_departament',
                    'postal_code',
                    'phone',
                    'email',
                    'nationality_code',
                    'nationality',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

}
