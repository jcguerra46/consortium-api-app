<?php


class EmployeeProfileTest extends TestCase
{
    const baseURL = 'employee';

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'social_work_id' => $faker->numberBetween(1, 379),
            'retired' => $faker->boolean,
            'adicional_obra_social' => $faker->boolean,
            'jornalizado_hours_per_day' => $faker->numberBetween(1, 500),
            'entry_date' => $faker->date('d-m-Y'),
            'departure_date' => $faker->date('d-m-Y'),
            'antiquity' => $faker->boolean,
            'afiliado_sindicato' => $faker->boolean,
            'afiliado_f_m_v_d_d' => $faker->boolean,
            'descuento_c_p_f' => $faker->boolean,
            'd_g_d_y_p_c' => $faker->boolean,
            'calculate_obra_social_by_law' => $faker->boolean,
            'extra_hours_50' => $faker->numberBetween(0, 500),
            'extra_hours_saturday_50' => $faker->numberBetween(0, 500),
            'extra_hours_saturday_100' => $faker->numberBetween(0, 500),
            'extra_hours_sunday_100' => $faker->numberBetween(0, 500),
            'extra_hours_holiday_100' => $faker->numberBetween(0, 500),
            'law27430' => $faker->boolean,
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1' . '/profile',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'social_work_id',
                    'retired',
                    'adicional_obra_social',
                    'jornalizado_hours_per_day',
                    'entry_date',
                    'departure date',
                    'antiquity',
                    'afiliado_sindicato',
                    'afiliado_f_m_v_d_d',
                    'descuento_c_p_f',
                    'd_g_d_y_p_c',
                    'calculate_obra_social_by_law',
                    'extra_hours_50',
                    'extra_hours_saturday_50',
                    'extra_hours_saturday_100',
                    'extra_hours_sunday_100',
                    'extra_hours_holiday_100',
                    'law27430',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1' . '/profile',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'employee_id',
                    'social_work_id',
                    'retired',
                    'adicional_obra_social',
                    'jornalizado_hours_per_day',
                    'entry_date',
                    'departure date',
                    'antiquity',
                    'afiliado_sindicato',
                    'afiliado_f_m_v_d_d',
                    'descuento_c_p_f',
                    'd_g_d_y_p_c',
                    'calculate_obra_social_by_law',
                    'extra_hours_50',
                    'extra_hours_saturday_50',
                    'extra_hours_saturday_100',
                    'extra_hours_sunday_100',
                    'extra_hours_holiday_100',
                    'law27430',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

}
