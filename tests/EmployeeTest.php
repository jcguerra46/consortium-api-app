<?php


class EmployeeTest extends TestCase
{
    const baseURL = 'employee';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'professional_function_id',
                                'first_name',
                                'last_name',
                                'birth_date',
                                'type_dni',
                                'dni',
                                'cuil',
                                'number_docket',
                                'active',
                                'employee_status_id',
                                'consortium_id',
                                'created_at',
                                'updated_at',
                                'deleted_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'professional_function_id' => $faker->numberBetween(1, 23),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'birth_date' => $faker->date('d-m-Y'),
            'type_dni' => $faker->randomElement(['LC', 'LE', 'DNI']),
            'dni' => $faker->numerify('########'),
            'cuil' => '20-35226046-1',
            'number_docket' => $faker->numerify('########'),
            'active' => $faker->boolean,
            'employee_status_id' => $faker->numberBetween(1, 9),
            'consortium_id' => $faker->numberBetween(1, 50),
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'professional_function_id',
                    'first_name',
                    'last_name',
                    'birth_date',
                    'type_dni',
                    'dni',
                    'cuil',
                    'number_docket',
                    'active',
                    'employee_status_id',
                    'consortium_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'professional_function_id' => $faker->numberBetween(1, 23),
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'birth_date' => $faker->date('d-m-Y'),
            'type_dni' => $faker->randomElement(['LC', 'LE', 'DNI']),
            'dni' => $faker->numerify('########'),
            'cuil' => '20-35226046-1',
            'number_docket' => $faker->numerify('########'),
            'active' => $faker->boolean,
            'employee_status_id' => $faker->numberBetween(1, 9),
            'consortium_id' => $faker->numberBetween(1, 50),
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'professional_function_id',
                    'first_name',
                    'last_name',
                    'birth_date',
                    'type_dni',
                    'dni',
                    'cuil',
                    'number_docket',
                    'active',
                    'employee_status_id',
                    'consortium_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'professional_function_id',
                    'first_name',
                    'last_name',
                    'birth_date',
                    'type_dni',
                    'dni',
                    'cuil',
                    'number_docket',
                    'active',
                    'employee_status_id',
                    'consortium_id',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'employees.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }
}
