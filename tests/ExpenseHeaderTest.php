<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExpenseHeaderTest extends TestCase
{
    const baseURL = 'expense-headers';

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'administration_id',
                                'name_header',
                                'cuit',
                                'name',
                                'address_street',
                                'address_number',
                                'email',
                                'phone',
                                'fiscal_situation',
                                'postal_code',
                                'rpa',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    /**
     * Create a claim.
     * @test
     */
    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'administration_id' => \App\Models\Administration::all()->random()->id,
            'name_header' => $faker->sentence(2, true),
            'cuit' => '20-95660815-6',
            'name' => $faker->sentence(2, true),
            'address_street' => $faker->streetName,
            'address_number' => $faker->buildingNumber,
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->phoneNumber,
            'fiscal_situation' => $faker->randomElement(['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento']),
            'postal_code' => $faker->postcode,
            'rpa' => $faker->numerify('########')
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);
        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'name_header',
                    'cuit',
                    'name',
                    'address_street',
                    'address_number',
                    'email',
                    'phone',
                    'fiscal_situation',
                    'postal_code',
                    'rpa',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Edit
     * @test
     */
    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'administration_id' => \App\Models\Administration::all()->random()->id,
            'name_header' => $faker->sentence(2, true),
            'cuit' => '20-95660815-6',
            'name' => $faker->sentence(2, true),
            'address_street' => $faker->streetName,
            'address_number' => $faker->buildingNumber,
            'email' => $faker->unique()->safeEmail,
            'phone' => $faker->phoneNumber,
            'fiscal_situation' => $faker->randomElement(['responsable_no_inscripto', 'responsable_inscripto', 'monotributista', 'exento']),
            'postal_code' => $faker->postcode,
            'rpa' => $faker->numerify('########')
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'name_header',
                    'cuit',
                    'name',
                    'address_street',
                    'address_number',
                    'email',
                    'phone',
                    'fiscal_situation',
                    'postal_code',
                    'rpa',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'name_header',
                    'cuit',
                    'name',
                    'address_street',
                    'address_number',
                    'email',
                    'phone',
                    'fiscal_situation',
                    'postal_code',
                    'rpa',
                    'updated_at',
                    'created_at',
                ]
            ]);
    }

    /**
     * Destroy
     * @test
     */
    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    /**
     * Export
     * @test
     */
    public function testExport()
    {
        $filename = 'expenseheaders.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename='.$filename,
            $response->headers->get('content-disposition'));
    }
}
