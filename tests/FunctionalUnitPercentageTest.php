<?php


class FunctionalUnitPercentageTest extends TestCase
{
    const baseURL = 'functional-unit';

    public function testIndex()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $this->call('POST',
            'consortium/1/percentage',
            [
                'name' => $faker->text,
                'second_line_name' => $faker->text,
                'description' => $faker->realText(),
                'type' => $faker->randomElement(['spendings', 'fixed', 'forced_fixed']),
                'position' => $faker->numberBetween(1, 100),
                'last_fixed_amount' => $faker->randomNumber(),
                'particular_percentage' => $faker->boolean,
                'hidden_expense_spending' => $faker->boolean,
                'hidden_in_proration' => $faker->boolean,
                'hidden_percentage_value' => $faker->boolean,
                'equal_parts' => $faker->boolean,
            ],
            [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response = $this->json('GET', self::baseURL . '/1/percentage', [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'functional_unit_id',
                                'percentage_consortium_id',
                                'value',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'value' => $faker->numberBetween(0, 100),
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1/percentage/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'functional_unit_id',
                    'percentage_consortium_id',
                    'value',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1/percentage/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'functional_unit_id',
                    'percentage_consortium_id',
                    'value',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

}
