<?php


class FunctionalUnitTest extends TestCase
{
    const baseURL = 'functional-units';

    public function setUp(): void
    {
        parent::setUp();

        $this->functional_unit = factory('App\Models\FunctionalUnit', 1)->create([
            'consortium_id' => 4
        ]);
    }

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [
            'consortium_id' => 4
        ], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'consortium_id',
                                'package_id',
                                'functional_unit_type_id',
                                'owner_first_name',
                                'owner_last_name',
                                'floor',
                                'functional_unit_number',
                                'initial_balance',
                                'type_forgive_interest',
                                'legal_state',
                                'm2',
                                'order',
                                'created_at',
                                'updated_at',
                                'deleted_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testStore()
    {
        $dataBody = $this->fakerFunctionalUnit();

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'package_id',
                    'functional_unit_type_id',
                    'owner_first_name',
                    'owner_last_name',
                    'floor',
                    'functional_unit_number',
                    'initial_balance',
                    'type_forgive_interest',
                    'legal_state',
                    'm2',
                    'order',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    public function testUpdate()
    {
        $dataBody = $this->fakerFunctionalUnit();

        $response = $this->json('PUT',
            self::baseURL . '/' . $this->functional_unit[0]->id,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'package_id',
                    'functional_unit_type_id',
                    'owner_first_name',
                    'owner_last_name',
                    'floor',
                    'functional_unit_number',
                    'initial_balance',
                    'type_forgive_interest',
                    'legal_state',
                    'm2',
                    'order',
                    'created_at',
                    'updated_at',
                    'deleted_at'
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/' . $this->functional_unit[0]->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'package_id',
                    'functional_unit_type_id',
                    'owner_first_name',
                    'owner_last_name',
                    'floor',
                    'functional_unit_number',
                    'initial_balance',
                    'type_forgive_interest',
                    'legal_state',
                    'm2',
                    'order',
                    'created_at',
                    'updated_at',
                    'deleted_at'
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/' . $this->functional_unit[0]->id,
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);
        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'functional_units.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [
            'consortium_id' => 4
        ], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }

    public function testStoreBatchOfFunctionalUnits()
    {
        $functionalUnits = [];
        for($i = 0; $i < 10; $i++){
            $functionalUnits[] = $this->fakerFunctionalUnit();
        }

        $batchOfFunctionalUnits = [
            'consortium_id' => 4,
            'functional_units' => $functionalUnits
        ];

        $response = $this->json('POST',
            self::baseURL . '/batch',
            $batchOfFunctionalUnits, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);

        $response->seeJsonStructure(
            ['data' =>
                [ '*' =>
                    [
                        'id',
                        'consortium_id',
                        'package_id',
                        'functional_unit_type_id',
                        'owner_first_name',
                        'owner_last_name',
                        'floor',
                        'functional_unit_number',
                        'initial_balance',
                        'type_forgive_interest',
                        'legal_state',
                        'm2',
                        'order',
                        'created_at',
                        'updated_at'
                    ]
                ]
            ]);
    }

    public function fakerFunctionalUnit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        return [
            'consortium_id' => '1',
            'package_id' => '1',
            'functional_unit_type_id' => '1',
            'owner_first_name' => $faker->firstName,
            'owner_last_name' => $faker->lastName,
            'floor' => $floor = $faker->numberBetween($min = 1, $max = 20),
            'functional_unit_number' => $floor . strtoupper($faker->randomLetter),
            'initial_balance' => $faker->randomFloat(2, 0, 10000),
            'type_forgive_interest' => $faker->randomElement([null, 'until_further_notice', 'this_liquidation']),
            'legal_state' => $faker->randomElement([null, 'trial', 'agreement', 'lawyer']),
            'm2' => $faker->randomFloat('2'),
            'order' => $faker->randomNumber('1'),
        ];
    }
}
