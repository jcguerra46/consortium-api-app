<?php


class JudgmentTest extends TestCase
{
    const baseURL = 'judgment';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'consortium_id',
                                'cover',
                                'number_expedient',
                                'judged',
                                'objet',
                                'state',
                                'reclaimed_amount',
                                'active',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'consortium_id' => 1,
            'cover' => $faker->text,
            'number_expedient' => $faker->numerify('######'),
            'judged' => $faker->city,
            'objet' => $faker->title,
            'state' => $faker->state,
            'reclaimed_amount' => $faker->numerify('####'),
            'active' => $faker->boolean,
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'cover',
                    'number_expedient',
                    'judged',
                    'objet',
                    'state',
                    'reclaimed_amount',
                    'active',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'consortium_id' => 1,
            'cover' => $faker->text,
            'number_expedient' => $faker->numerify('######'),
            'judged' => $faker->city,
            'objet' => $faker->title,
            'state' => $faker->state,
            'reclaimed_amount' => $faker->numerify('####'),
            'active' => $faker->boolean,
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'cover',
                    'number_expedient',
                    'judged',
                    'objet',
                    'state',
                    'reclaimed_amount',
                    'active',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'cover',
                    'number_expedient',
                    'judged',
                    'objet',
                    'state',
                    'reclaimed_amount',
                    'active',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'judgments.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }
}
