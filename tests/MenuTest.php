<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class MenuTest extends TestCase
{
    const baseMenusURL = 'menus';
    const baseUserAdministrationsURL = 'user-administrations';

    /**
     * Show Menu.
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET', self::baseMenusURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'menu' => [
                        '*' =>
                            [
                                'fav',
                                'icon',
                                'route',
                                'title',
                                'childs'
                            ]
                    ]
                ]
            ]);
    }

    /**
     * Set User Menu
     * @test
     */
    public function testSetUserMenu()
    {
        $dataBody = [
            'menu' => '
                {
                  "menu": [
                    {
                      "title": "Consorcios (Home)",
                      "fav": true,
                      "icon": "office-building",
                      "route": "/dashboard",
                      "childs": null
                    }
                  ]
                }'
        ];

        $response = $this->json('PUT',
            self::baseMenusURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'menu' => [
                        '*' =>
                            [
                                'fav',
                                'icon',
                                'route',
                                'title',
                                'childs'
                            ]
                    ]
                ]
            ]);
    }

    /**
     * Get Menu Default
     * @test
     */
    public function testGetMenuDefault()
    {
        $response = $this->json('GET', self::baseMenusURL . '/default', [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'menu' => [
                        '*' =>
                            [
                                'fav',
                                'icon',
                                'route',
                                'title',
                                'childs'
                            ]
                    ]
                ]
            ]);
    }

    /**
     * Set Menu Default
     * @test
     */
    public function testSetMenuDefault()
    {
        $response = $this->json('PUT', self::baseMenusURL . '/default', [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
    }

}
