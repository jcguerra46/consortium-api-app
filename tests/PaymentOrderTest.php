<?php


class PaymentOrderTest extends TestCase
{
    const baseURL = 'payment-order';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'provider_id',
                                'administration_id',
                                'amount',
                                'description',
                                'status',
                                'date',
                                'created_at',
                                'updated_at',
                                'deleted_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'description' => $faker->realText(),
            'provider_id' => \App\Models\Provider::all()->random()->id,
            'invoices' => [1, 2]
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'provider_id',
                    'administration_id',
                    'amount',
                    'description',
                    'status',
                    'date',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'invoices'
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'description' => $faker->realText(),
            'invoices' => [1]
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'provider_id',
                    'administration_id',
                    'amount',
                    'description',
                    'status',
                    'date',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'provider_id',
                    'administration_id',
                    'amount',
                    'description',
                    'status',
                    'date',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'payment-orders.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }

    public function testDuplicate()
    {
        $response = $this->json('POST',
            self::baseURL . '/2/duplicate',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'provider_id',
                    'administration_id',
                    'amount',
                    'description',
                    'status',
                    'date',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testAnnular()
    {
        $response = $this->json('PUT',
            self::baseURL . '/2/annular',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'provider_id',
                    'administration_id',
                    'amount',
                    'description',
                    'status',
                    'date',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testPrint()
    {
        $response = $this->json('GET',
            self::baseURL . '/5/print',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'html',
                ]
            ]);
    }

}
