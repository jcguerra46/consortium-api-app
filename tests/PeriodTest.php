<?php


class PeriodTest extends TestCase
{
    const baseURL = 'consortium';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL . '/1/period', [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'consortium_id',
                                'name',
                                'description',
                                'start_date',
                                'end_date',
                                'state',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'name' => $faker->text,
            'description' => $faker->realText()
        ];

        $response = $this->json('PUT',
            self::baseURL . '/1/period/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'name',
                    'description',
                    'start_date',
                    'end_date',
                    'state',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1/period/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'name',
                    'description',
                    'start_date',
                    'end_date',
                    'state',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'periods.xlsx';
        $response = $this->json('GET', self::baseURL . '/1/period/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }

    public function testGetOpenPeriod()
    {
        $response = $this->json('GET',
            self::baseURL . '/1/period/open',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'name',
                    'description',
                    'start_date',
                    'end_date',
                    'state',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }
}
