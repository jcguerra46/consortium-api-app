<?php


class ProfessionalFunctionTest extends TestCase
{
    const baseURL = 'professional-functions';

    /**
     * index.
     * @test
     */
    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'name',
                                'description',
                                'has_department',
                                'jornalizado',
                                'surrogate',
                                'type_seniority',
                                'part_time',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    /**
     * Show
     * @test
     */
    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'name',
                    'description',
                    'has_department',
                    'jornalizado',
                    'surrogate',
                    'type_seniority',
                    'part_time',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

}
