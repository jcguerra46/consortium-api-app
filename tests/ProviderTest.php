<?php


class ProviderTest extends TestCase
{
    const baseURL = 'provider';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'business_name',
                                'name',
                                'address',
                                'location',
                                'province_id',
                                'postal_code',
                                'cuit',
                                'administration_id',
                                'created_at',
                                'updated_at'
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'business_name' => $faker->name,
            'name' => $faker->name,
            'address' => $faker->address,
            'location' => $faker->city,
            'province_id' => 1,
            'postal_code' => $faker->postcode,
            'cuit' => '20-35226046-1',
            'administration_id' => 1,
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'business_name',
                    'name',
                    'address',
                    'location',
                    'province_id',
                    'postal_code',
                    'cuit',
                    'administration_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'business_name' => $faker->name,
            'name' => $faker->name,
            'address' => $faker->address,
            'location' => $faker->city,
            'province_id' => 1,
            'postal_code' => $faker->postcode,
            'cuit' => '20-35226046-1',
            'administration_id' => 1,
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'business_name',
                    'name',
                    'address',
                    'location',
                    'province_id',
                    'postal_code',
                    'cuit',
                    'administration_id',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'business_name',
                    'name',
                    'address',
                    'location',
                    'province_id',
                    'postal_code',
                    'cuit',
                    'administration_id',
                    'created_at',
                    'updated_at'
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'providers.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename='.$filename,
                            $response->headers->get('content-disposition'));
    }
}
