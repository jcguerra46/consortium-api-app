<?php


class SpendingDescriptionTest extends TestCase
{
    const baseURL = 'spending-description';

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'administration_id',
                                'title',
                                'description',
                                'created_at',
                                'updated_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'title' => $faker->text,
            'description' => $faker->realText(),
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'title',
                    'description',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $dataBody = [
            'title' => $faker->text,
            'description' => $faker->realText(),
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'title',
                    'description',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'administration_id',
                    'title',
                    'description',
                    'created_at',
                    'updated_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'spending-descriptions.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }
}
