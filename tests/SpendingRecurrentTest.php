<?php


class SpendingRecurrentTest extends TestCase
{
    const baseURL = 'spending-recurrent';
    public $idSpendingRecurrentCreated;

    public function testIndex()
    {
        $response = $this->json('GET', self::baseURL, [], [
            'Authorization' => 'Bearer ' . static::$token
        ]);

        $response->assertResponseStatus(200);
        $response->assertResponseOk();
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows' => [
                        '*' =>
                            [
                                'id',
                                'consortium_id',
                                'provider_id',
                                'percentage_consortium_id',
                                'category_id',
                                'description',
                                'amount',
                                'created_at',
                                'updated_at',
                                'deleted_at',
                            ]
                    ],
                    'metadata' => [
                        'total',
                        'count',
                        'per_page',
                        'current_page',
                        'total_pages'
                    ]
                ]
            ]);
    }

    public function testCreate()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $consortia = \App\Models\Consortium::fromAdministration()->pluck('id')->toArray();
        $providers = \App\Models\Provider::fromAdministration()->pluck('id')->toArray();
        $percentages = \App\Models\PercentageConsortium::fromAdministration()->pluck('id')->toArray();
        $categories = \App\Models\Category::fromAdministration()->pluck('id')->toArray();

        $dataBody = [
            'consortium_id' => $faker->randomElement($consortia),
            'provider_id' => $faker->randomElement($providers),
            'percentage_consortium_id' => $faker->randomElement($percentages),
            'category_id' => $faker->randomElement($categories),
            'description' => $faker->realText(),
            'amount' => $faker->numberBetween(1),
        ];

        $response = $this->json('POST',
            self::baseURL,
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);
        dd($response->response->getContent());

        $response->assertResponseStatus(201);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'provider_id',
                    'percentage_consortium_id',
                    'category_id',
                    'description',
                    'amount',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
       //$this->idSpendingRecurrentCreated = ;
    }

    public function testEdit()
    {
        /** @var \Faker\Generator $faker */
        $faker = app(\Faker\Generator::class);
        $consortia = \App\Models\Consortium::fromAdministration()->pluck('id')->toArray();
        $providers = \App\Models\Provider::fromAdministration()->pluck('id')->toArray();
        $percentages = \App\Models\PercentageConsortium::fromAdministration()->pluck('id')->toArray();
        $categories = \App\Models\Category::fromAdministration()->pluck('id')->toArray();

        $dataBody = [
            'consortium_id' => $faker->randomElement($consortia),
            'provider_id' => $faker->randomElement($providers),
            'percentage_consortium_id' => $faker->randomElement($percentages),
            'category_id' => $faker->randomElement($categories),
            'description' => $faker->realText(),
            'amount' => $faker->numberBetween(1),
        ];

        $response = $this->json('PUT',
            self::baseURL.'/1',
            $dataBody, [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'provider_id',
                    'percentage_consortium_id',
                    'category_id',
                    'description',
                    'amount',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testShow()
    {
        $response = $this->json('GET',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'id',
                    'consortium_id',
                    'provider_id',
                    'percentage_consortium_id',
                    'category_id',
                    'description',
                    'amount',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]
            ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE',
            self::baseURL . '/1',
            [], [
                'Authorization' => 'Bearer ' . static::$token
            ]);

        $response->assertResponseStatus(200);
        $response->seeJson([
            'status' => 'success'
        ]);
        $response->seeJsonStructure(
            ['data' =>
                [
                    'rows_deleted'
                ]
            ]);
    }

    public function testExport()
    {
        $filename = 'spendings-recurrents.xlsx';
        $response = $this->json('GET', self::baseURL . '/export', [], [
            'Authorization' => 'Bearer ' . static::$token
        ])->response;

        $this->assertEquals('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            $response->headers->get('content-type'));

        $this->assertEquals('attachment; filename=' . $filename,
            $response->headers->get('content-disposition'));
    }
}
